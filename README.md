This is a side project for platform team to automatically get the screenshots on Android devices(Tablet, TV and Mobile).

Before running the comment, please check out https://confluence.mobitv.corp/display/ClientQA/How+to+Run+Android+Client+Automation to get the environment ready.

After that please follow：

1. in ./utilities/com/mobitv/client/qa/constants/ConfigurationConstants.java edit the parameters.
2. if it is a new flavor, under testConfigurations/ create a new properties file and under /testSuites create a directory with new flavor name and corresponding xml file(can copy from other existing flavor and adjust it)

Run automation code use Ant in terminal:

ant email -DplatformName=Android -DdeviceType=Phone -DdeviceName=Gen2 -Dcarrier=cspire -Dhost=production -DbuildVersion=1.10.2
