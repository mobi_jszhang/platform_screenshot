package com.mobitv.client.qa.validators;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;

import com.google.common.collect.ImmutableMap;
import com.mobitv.client.qa.Comparators.ProgramStartTimeComparator;
import com.mobitv.client.qa.baseTests.AndroidTVBaseTest;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.constants.XPaths;
import com.mobitv.client.qa.models.Assets;
import com.mobitv.client.qa.models.Movie;
import com.mobitv.client.qa.models.Program;
import com.mobitv.client.qa.models.Recording;
import com.mobitv.client.qa.models.SeriesMovies;
import com.mobitv.client.qa.models.SeriesRecording;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

public abstract class RecordingValidationsUtils extends AndroidTVBaseTest {
	
	private final int SERIES_ID_INDEX = 0;
	private final int MOVIE_ID_INDEX = 0;
	private final int THUMBNAIL_ID_INDEX = 1;
	protected final int maxNumOfVerification = 2;
	
	/**
	 * Method to verify series content on ManageRecording screen and detail
	 * screen.
	 */
	protected void verifySeriesContents(final int itemIndex, final String validString, boolean verifyEpisodes) {
			String[] seriesMetaDataParts = getSelectedProgramMetaData();
			final String seriesId = seriesMetaDataParts[SERIES_ID_INDEX];
			final String actualSeriesThumbnailId = seriesMetaDataParts[THUMBNAIL_ID_INDEX];
			// Series tile image thumbnailId verification.
			SeriesMovies selectedSeries = recordingsJsonUtility.getSeriesDetailWithId(seriesId);
			String message = "Actual series thumbnail id on recording screen is not matching with expected.";
			final String expectedSeriesThumbnailId = recordingsJsonUtility.getSeriesPosterThumbnailId(selectedSeries);
			Assert.assertEquals(actualSeriesThumbnailId, expectedSeriesThumbnailId, message);
			// Series tile title text verification.
			mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
			final String actualSeriesNameOnRecordingScreen = mElement.getText();
			message = "Actual series name on recording screen is not matching with expected.";
			Assert.assertEquals(actualSeriesNameOnRecordingScreen, selectedSeries.getName(), message);
			tap(1);
			// Series title text verification on detail screen.
			mElement = androidDriver.findElement(MobileBy.id(XPaths.episodeRecordingTitle));
			final String actualSeriesNameOnDetailScreen = mElement.getText();
			final String expectedSeriesName = utility.getStringReplaced(validString, ImmutableMap.of("{SERIES_TITLE}", selectedSeries.getName()));
			message = "Actual series name on detail is not matching with expected.";
			Assert.assertEquals(actualSeriesNameOnDetailScreen, expectedSeriesName, message);
		if (verifyEpisodes)
			verifySeriesEpisodes(seriesId);
	}

	/**
	 * Method to verify movie content on ManageRecording screen and detail
	 * screen.
	 */
	protected void verifyMovieContents() {
			// Movie tile image thumbnailId verification.
			String[] moviesMetaDataParts = getSelectedProgramMetaData();
			final String movieId = moviesMetaDataParts[MOVIE_ID_INDEX];
			final String actualMoiveThumbnailId = moviesMetaDataParts[THUMBNAIL_ID_INDEX];
			// Movie tile image thumbnailId verification.
			SeriesMovies selectedMovie = recordingsJsonUtility.getMovieDetailWithId(movieId);
			final String expectedMovieThumbnailId = recordingsJsonUtility.getMoviePosterThumbnailId(selectedMovie);
			String message = "Actual movie thumbnail id on recording screen is not matching with an expected.";
			Assert.assertTrue(actualMoiveThumbnailId.equals(expectedMovieThumbnailId), message);
			// Movie tile title text verification.
			mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
			final String actualMoviewNameOnRecordingScreen = mElement.getText();
			final String expectedMovieNameFromAPI = selectedMovie.getName();
			message = "Actual series name on recording screen is not matching with expected.";
			Assert.assertTrue(actualMoviewNameOnRecordingScreen.equals(expectedMovieNameFromAPI), message);
			tap(1);
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.movieRecordingDetailsInfoButton, true)) {
				navigateDown(2);
				tap(1);
				waitForSomeTime(Constants.VERY_SMALL_SLEEP);
				mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleTitle));
				final String actualMoviewNameOnDetailScreen = mElement.getText();
				message = "Actual movie name on detail is not matching with expected.";
				Assert.assertTrue(actualMoviewNameOnDetailScreen.equals(expectedMovieNameFromAPI), message);
				verifyMovieOverlay();
			}
	}

	/**
	 * Method to verify episodes of a series.
	 */
	protected void verifySeriesEpisodes(final String seriesId) {
		final int length = 100;
		SeriesRecording series = recordingsJsonUtility.getSeriesRecordingById(seriesId);
		List<Program> seriesPrograms = searchJSONUtility.getProgramByChannelId(series.getChannel_id(), seriesId,
			length);
		//Verify no records alert message.
		if (seriesPrograms.isEmpty())
				return;		
		List<MobileElement> mElementsList = androidDriver.findElements(MobileBy.id(XPaths.episodeListName));
		final int totalEpisodes = seriesPrograms.size();
		final int totalElements = mElementsList.size();
		for (int episodeIndex = 0; episodeIndex < totalEpisodes && episodeIndex < totalElements; episodeIndex++) {
			String message = "Name of episode on screen is not equal to expected name.";
			Program episode = seriesPrograms.get(episodeIndex);
			String extectedEpisodeName = episode.getName();
			if(episode.getSeason_number() != null && episode.getEpisode_number() != null)
				extectedEpisodeName = StringUtils.join("S", episode.getSeason_number()," : E",episode.getEpisode_number()," - ", extectedEpisodeName);
			final String actualEpisodeName = mElementsList.get(episodeIndex).getText();
			Assert.assertEquals(actualEpisodeName, extectedEpisodeName, message);
			// Episode buttons verification.
			if (!episode.getType().equals(Constants.REF_TYPE_MOVIE)) {
				// Info button verification.
				validateElementTitleEquals(XPaths.btnInfoRecording,"info_button_title", "Actual Info button title not matching with expected.");
				navigateRight(1);
				//Cancel button verification.
				validateElementTitleEquals(XPaths.btnCancelRecording,"cancel_btn", "Actual Cancel button title not matching with expected.");
				navigateRight(1);
				// Extend button verification.
				validateElementTitleEquals(XPaths.btnExtendRecording,"extend_button_title", "Actual Extend button title not matching with expected.");
			}
			navigateDown(1);
		}
	}

	/**
	 * Method to verify movie overlay option.
	 */
	protected void verifyMovieOverlay() {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
		final String recordingBtnLbl = mElement.getText();
		navigateBack(1);
		verifyRecordedTabMovieOverlayButtons(recordingBtnLbl);
		navigateBack(1);
	}

	/**
	 * Method to verify buttons label text on movie overlay screen in recorded
	 * tab.
	 */
	protected void verifyRecordedTabMovieOverlayButtons(String recordingBtnLbl) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.movieRecordingDetailsPlayButton));
		final String actualPlayBtnTitleText = mElement.getText();
		final String expectedPlayBtnTitleText = dictionaryReader.getDictionaryValueForKey("play_button_title");
		String message = "Actual play button title not matching with expected.";
		Assert.assertTrue(actualPlayBtnTitleText.equals(expectedPlayBtnTitleText), message);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.movieRecordingDetailsRecordButton));
		final String actualDeleteStopBtnTitleText = mElement.getText();
		final String recordingBtnDictionaryLbl = dictionaryReader
				.getDictionaryValueForKey("stop_recording_text");
		final String expectedDeleteBtnTitleText = recordingBtnLbl.equals(recordingBtnDictionaryLbl)
				? dictionaryReader.getDictionaryValueForKey("stop_button_title")
				: dictionaryReader.getDictionaryValueForKey("delete_button_title");
		message = "Actual " + expectedDeleteBtnTitleText + " button title not matching with expected.";
		Assert.assertTrue(actualDeleteStopBtnTitleText.equals(expectedDeleteBtnTitleText), message);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.movieRecordingDetailsInfoButton));
		final String actualInfoBtnTitleText = mElement.getText();
		final String expectedInfoBtnTitleText = dictionaryReader.getDictionaryValueForKey("info_button_title");
		message = "Actual Info button title not matching with expected.";
		Assert.assertTrue(actualInfoBtnTitleText.equals(expectedInfoBtnTitleText), message);
	}

	/**
	 * Method to verify is series recording get deleted successfully if no error
	 * occurs. If any network error occurs then recording should not get
	 * deleted.
	 */
	protected void verifyRecordingDeletedSuccessfully(String elementId, String expectedSeriesNameOnRecordingScreen) {
		mElement = androidDriver.findElement(MobileBy.id(elementId));
		final String actualSeriesNameOnRecordingScreen = mElement.getText();
		String message = "Program not removed from recording screen on deleting all recording.";
		if (!androidUtility.isElementPresentByID(androidDriver, XPaths.networkErrorAlertMsg, true)) {
			Assert.assertFalse(actualSeriesNameOnRecordingScreen.equals(expectedSeriesNameOnRecordingScreen), message);
		} else {
			message = "Program removed from recording screen when network error occurs.";
			Assert.assertFalse(actualSeriesNameOnRecordingScreen.equals(expectedSeriesNameOnRecordingScreen), message);
		}
	}
	
	/**
	 * Method to verify alert message when scheduled tab screen is empty.
	 */
	protected boolean verifyEmptyScheduledAlertMsg(){
		if (recordingsJsonUtility.hasNoScheduledRecordings()) {
			final String expectedEmptyScreenAlertMessage =
					dictionaryReader.getDictionaryValueForKey("scheduled_tab_empty_state_text");
			validateEmptyRecordedAndScheduledAlertMsg(expectedEmptyScreenAlertMessage);
			return true;
		}
		return false;
	}
	
	/**
	 * Method to verify alert message on an empty recorded tab or scheduled tab
	 * in manage recording screen.
	 */
	protected void validateEmptyRecordedAndScheduledAlertMsg(String expectedEmptyScreenAlertMessage) {
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.manageRecordingNoResults, true)) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.manageRecordingNoResults));
			final String actualEmptyScreenAlertMessage = mElement.getText();
			final String message = "Correct alert message not displaying on an empty screen.";
			Assert.assertEquals(actualEmptyScreenAlertMessage, expectedEmptyScreenAlertMessage, message);
		}
	}
	
	/**
	 * Method to validate buttons on movie detail screen in recorded tab on manage recording screen.
	 */
	protected void validateRecordedTabMovieOverlayAndDetailScreenButtons() {
		Movie movieToValidate = getMovieDetailsAndSetMovieAssetsForSeletectTile();
		tap(1);
		if (!androidUtility.isElementPresentByID(androidDriver, XPaths.movieRecordingDetailsInfoButton, true))
			return;
		androidDriver.findElementById(XPaths.movieRecordingDetailsInfoButton).click();
		// Validate detail screen buttons.
		validateMovieButtonRow(movieToValidate);
		navigateBack(1);
		// Validate overlay screen buttons.
		validateMovieOverlayButtons(movieToValidate);
		navigateBack(1);
	}

	/**
	 * Method to validate buttons on movie inline info screen in movie modular screen.
	 */
	protected void validateMovieModularScreenMovieButtons() {
		Movie movieToValidate = getMovieDetailsAndSetMovieAssetsForSeletectTile();
		tap(1);
		// Validate detail screen buttons.
		validateMovieButtonRow(movieToValidate);
		navigateBack(1);
	}

	/**
	 * Method to validate buttons on individual recording section in scheduled tab on manage recording screen.
	 */
	protected void validateScheduledTabMovieButtons(Map<String, String> scheduledProgramLst) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.individualRecordingTitleText));
		final String programTitle = mElement.getText();
		final boolean isProgramSeriesType = scheduledProgramLst.get(programTitle).equals(Constants.CATEGORY_TYPE_TV);
		if (!isProgramSeriesType) {
			// Info button verification.
			validateElementTitleEquals(XPaths.btnInfoRecording, "info_button_title",
					"Actual Info button title not matching with expected.");
			// Cancel button verification.
			validateElementTitleEquals(XPaths.btnCancelRecording, "cancel_btn",
					"Actual play button title not matching with expected.");
		}
	}

	/**
	 * Method to validate deletion of series from recorded tab on manage recording screen.
	 */
	protected void validateSeriesDeletionFromRecordedTab() {
		// Steps to verify deletion of series recorded episodes from series detail section.
		if(!selectExpectedRow("manage_recordings_series_title"))
			return;
		RecordingUIActionUtils recordingUIActionUtils = new RecordingUIActionUtils();
		mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
		final String expectedSeriesNameOnRecordingScreen = mElement.getText();
		String[] metaData = getSelectedProgramMetaData();
		List<Recording> seriesRecordings = recordingsJsonUtility.getAllOnGoingAndCompletedRecordingsForSeriesId(metaData[SERIES_ID_INDEX]);
		if(seriesRecordings.isEmpty())
			return;
		tap(1);
		recordingUIActionUtils.deleteAllRecording(seriesRecordings.size());
		waitForSomeTime(Constants.MEDIUM_SLEEP);
		if(!selectExpectedRow("manage_recordings_series_title"))
			return;
		verifyRecordingDeletedSuccessfully(XPaths.titleText, expectedSeriesNameOnRecordingScreen);
	}
	
	/**
	 * Method to validate deletion of series from scheduled tab on manage recording screen.
	 */
	protected void validateSeriesDetailsAndDeletionFromDetailScreenOnScheduledTab() {
		// Steps to verify deletion of series by canceling all series recordings from series detail section.
		if(!selectExpectedRow("series_scheduled_header"))
			return;
		final int seriesTileIndex = 0;
    	final String validString = dictionaryReader.getDictionaryValueForKey("scheduled_episodes_screen_title");
		// Series tile title text verification.
		mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
		final String expectedSeriesNameOnRecordingScreen = mElement.getText();
		verifySeriesContents(seriesTileIndex, validString, false);
		final String expectedAlertDialogTitle = dictionaryReader.getDictionaryValueForKey("cancel_series_recording_alert_title");
		androidDriver.findElementById(XPaths.episodeRecordingCancelBtn).click();
		verifyAlertDialogDetails(expectedAlertDialogTitle);
		androidDriver.findElementById(XPaths.button1).click();
		waitForBuffering();
		if(!selectExpectedRow("series_scheduled_header"))
			return;
		verifyRecordingDeletedSuccessfully(XPaths.titleText, expectedSeriesNameOnRecordingScreen);
	}
	
	/**
	 * Method to validate deletion of movie from recorded tab on manage recording screen.
	 */
	protected void validateMovieDeletionFromRecordedTab() {
		// Steps to verify movie deleting from detail section delete button.
		if(!selectExpectedRow("manage_recordings_movies_title"))
			return;
		Movie movieToValidate = getMovieDetailsAndSetMovieAssetsForSeletectTile();
		if (!recordingsJsonUtility.isRecordedProgram(movieToValidate.getID()))
			return;
		mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
		String expectedMovieNameOnRecordingScreen = mElement.getText();
		tap(1);
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.movieRecordingDetailsInfoButton, true)) {
			androidDriver.findElementById(XPaths.movieRecordingDetailsInfoButton).click();
		} else {
			return;
		}
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleTitle));
		final String expectedMovieNameOnDetailScreen = mElement.getText();
		Assert.assertEquals(expectedMovieNameOnRecordingScreen, expectedMovieNameOnDetailScreen,
				"Movie names are not same.");
		validateMovieButtonRow(movieToValidate);
		androidDriver.findElementById(XPaths.infoModuleBtnRecording).click();
		verifyAlertDialogDetails(dictionaryReader.getDictionaryValueForKey("delete_recording_alert_title"));
		androidDriver.findElementById(XPaths.button1).click();
		//Making execution wait, till data get refresh on server.
		waitForSomeTime(Constants.DRIVER_IMPLICIT_WAIT);
		recordingsJsonUtility.parseAllRecordingsData();
		validateMovieButtonRow(movieToValidate);
		navigateBack(2);
		if(!selectExpectedRow("movies"))
			return;
		verifyRecordingDeletedSuccessfully(XPaths.titleText, expectedMovieNameOnRecordingScreen);
	}

	/**
	 * Method to verify movie deletion from scheduled tab on manage recording
	 * screen.
	 */
	protected void validateMovieDeletionFromScheduledTab() {
		if(!selectExpectedRow("individual_header"))
			return;
		mElement = androidDriver.findElements(MobileBy.id(XPaths.individualRecordingTitleText)).get(0);
		final String expectedProgramName = mElement.getText();
		androidDriver.findElementById(XPaths.btnCancelRecording).click();
		waitForBuffering();
		if(!selectExpectedRow("individual_header"))
			return;
		verifyRecordingDeletedSuccessfully(XPaths.individualRecordingTitleText, expectedProgramName);
	}

	/**
	 * Method to verify deletion of recorded movie from movie modular screen.
	 */
	protected void deleteFromMovieModularScreen() {
		if(!selectExpectedRow("recordings_title"))
			return;
		Movie movieToValidate = getMovieDetailsAndSetMovieAssetsForSeletectTile();
		if (!recordingsJsonUtility.isRecordedProgram(movieToValidate.getID()))
			return;
		tap(1);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleTitle));
		final String expectedMovieNameOnInlineInfo = mElement.getText();
		validateMovieButtonRow(movieToValidate);
		androidDriver.findElementById(XPaths.infoModuleBtnRecording).click();
		verifyAlertDialogDetails(dictionaryReader.getDictionaryValueForKey("delete_recording_alert_title"));
		androidDriver.findElementById(XPaths.button1).click();
		waitForBuffering();
		recordingsJsonUtility.parseAllRecordingsData();
		validateMovieButtonRow(movieToValidate);
		goToHomeScreen();
		waitForBuffering();
		selectMoviesTab();
		waitForBuffering();
		tap(1);
		verifyRecordingDeletedSuccessfully(XPaths.infoModuleTitle, expectedMovieNameOnInlineInfo);
		navigateBack(1);
	}

	/**
	 * Method to get movie object from API for selected movie tile on screen.
	 */
	protected Movie getMovieDetailsAndSetMovieAssetsForSeletectTile() {
		String[] moviesMetaDataParts = getSelectedProgramMetaData();
		Movie movieToValidate = recordingsJsonUtility.getRequiredMovieWithId(moviesMetaDataParts[MOVIE_ID_INDEX]);
		List<Assets> movieAssets = recordingsJsonUtility.getAssetsForMovie(movieToValidate.getShared_ref_id());
		movieToValidate.setAssets(movieAssets);
		return movieToValidate;
	}

	/**
	 * Method to verify alert message when recording tab screen is empty.
	 */
	protected boolean verifyEmptyRecordedAlertMsg(){
		if (recordingsJsonUtility.hasNoCompletedRecordings() &&
				recordingsJsonUtility.hasNoOngoingRecordings()) {
			final String expectedEmptyScreenAlertMessage =
					dictionaryReader.getDictionaryValueForKey("recorded_tab_empty_state_text");
			validateEmptyRecordedAndScheduledAlertMsg(expectedEmptyScreenAlertMessage);
			return true;
		}
		return false;
	}
}