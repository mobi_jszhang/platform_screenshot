package com.mobitv.client.qa.validators;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;

import com.mobitv.client.qa.baseTests.AndroidTVBaseTest;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.constants.XPaths;
import com.mobitv.client.qa.models.Channel;
import com.mobitv.client.qa.models.Program;

public abstract class ChannelsDataUtility extends AndroidTVBaseTest {
	protected List<Channel> purchasedChannelList = null;
	protected int total = 0;
	
	/**
	 * Method to initialized purchased channel list with API response.
	 * If user is "Out Of Home" then remove all channels which are not out of home playback enable.
	 */
	protected void fetchListOfPlayableChannels(){
		purchasedChannelList = mainJSONUtility.getListOfPurchasedChannels();
		if(ConfigurationConstants.isOutOfHomeEnabled)
			purchasedChannelList.removeIf(channel -> !channel.is_out_of_home_playback_enabled());
		total = purchasedChannelList.size();
	}

	/**
	 * Method to check if purchased channel list is empty or not.
	 * @return - true if empty else false.
	 */
	protected boolean isPurchasedChannelListEmpty(){
		return !utility.isValidCollection(purchasedChannelList);
	}
	
	/**
	 * Method read current program name from screen.
	 */
	protected String getProgramNameFromScreen(boolean isSeriesType) {
		int maxAttempts = 5;
		String elementText = "";
		for (; maxAttempts >= 0; maxAttempts--) {
			if (!androidUtility.isElementPresentByID(androidDriver, XPaths.txtSeriesTitle, true)) {
				tap(1);
			} else {
				elementText = getElementText(XPaths.txtSeriesTitle);
				if (!elementText.isEmpty()) {
					if(isSeriesType){
						elementText = StringUtils.join(elementText, StringUtils.SPACE, getElementText(XPaths.txtProgramInfo));
					}
					navigateBack(1);
					break;
				}
			}
		}
		return elementText;
	}

	/**
	 * Method to return index of non-restricted program channel and change
	 * channel.
	 * 
	 * @return - index of non-restricted program channel.
	 */
	protected int getIndexAndStartNonRestrictedProgramChannel() {
		final int total = purchasedChannelList.size();
		for (int channelIndex = 0; channelIndex < total; channelIndex++) {
			Channel channel = purchasedChannelList.get(channelIndex);
			if (channelsJSONUtility.getProgramForChannelFromAPIIfNotRestricted(channel) != null) {
				switchToChannel(channel.getChannel_number());
				return channelIndex;
			}
		}
		return -1;
	}
	
	/**
	 * Method to validate current channel is not playing restricted program
	 * using non restricted program name from API with program name on screen.
	 * 
	 * @param programFromChannel - program from API.
	 */
	protected void validateRestrictedChannelSkipped(Program programFromChannel) {
		final boolean isSeriesType = programFromChannel.getCategory().get(0).equals(Constants.CATEGORY_TYPE_TV);
		final String extectedProgramName = getProgramNameFromScreen(isSeriesType);
		String actualProgramName = programFromChannel.getName();
		if(isSeriesType){
			String episodeFormatedName = getEpisodeFormattedName(programFromChannel.getName(), programFromChannel.getSeason_number(), programFromChannel.getEpisode_number());
			actualProgramName = StringUtils.join(programFromChannel.getSeries_name(), StringUtils.SPACE, episodeFormatedName);
		}
		Assert.assertEquals(actualProgramName, extectedProgramName, "Restricted program channel not skipped.");
	}
}
