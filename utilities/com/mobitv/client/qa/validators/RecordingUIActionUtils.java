package com.mobitv.client.qa.validators;

import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;

import com.google.common.collect.ImmutableMap;
import com.mobitv.client.qa.baseTests.AndroidTVBaseTest;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.constants.XPaths;
import com.mobitv.client.qa.functions.DateAndTime;
import com.mobitv.client.qa.helpers.AlertHelper;
import com.mobitv.client.qa.helpers.AlertHelper.AlertType;
import com.mobitv.client.qa.json.RecordingsJSONUtility;
import com.mobitv.client.qa.models.Recording;
import com.mobitv.client.qa.models.SeriesRecording;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

public class RecordingUIActionUtils extends AndroidTVBaseTest{

	private RecordingsJSONUtility recordingsJsonUtility;
	private RecordingUIValidator recordingUIValidator;
	private AlertHelper alertHelper;
	private boolean isRecordingSet = false;
	public RecordingUIActionUtils() {
		recordingsJsonUtility = new RecordingsJSONUtility();
		recordingUIValidator = new RecordingUIValidator();
		alertHelper = new AlertHelper();
	}
	
	public void cancelRecording(MobileElement element, String seriesId) {
		int completedAndOngoingRecordingsCount = 0;
		int ongoingRecordingCount = 0;
		int scheduledRecordingCount = 0;
		List<Recording> allCompletedAndOngoingRecordingsUnderSeries = new ArrayList<>();
		List<Recording> allOngoingRecordingsUnderSeries = new ArrayList<>();
		List<Recording> allScheduledRecordingsUnderSeries = new ArrayList<>();
		
		allCompletedAndOngoingRecordingsUnderSeries.addAll(recordingsJsonUtility.getCompletedIndividualRecordingsForSeries(seriesId));
		allCompletedAndOngoingRecordingsUnderSeries.addAll(recordingsJsonUtility.getOngoingIndividualRecordingsForSeries(seriesId));
		allOngoingRecordingsUnderSeries.addAll(recordingsJsonUtility.getOngoingIndividualRecordingsForSeries(seriesId));
		allScheduledRecordingsUnderSeries.addAll(recordingsJsonUtility.getScheduledIndividualRecordingsForSeries(seriesId));
		
		completedAndOngoingRecordingsCount = utility.isValidCollection(allCompletedAndOngoingRecordingsUnderSeries) ? allCompletedAndOngoingRecordingsUnderSeries.size() : 0;
		ongoingRecordingCount = utility.isValidCollection(allOngoingRecordingsUnderSeries) ? allOngoingRecordingsUnderSeries.size() : 0;
		scheduledRecordingCount = utility.isValidCollection(allScheduledRecordingsUnderSeries) ? allScheduledRecordingsUnderSeries.size() : 0;
		
		SeriesRecording seriesRecording = recordingsJsonUtility.getSeriesRecordingById(seriesId);
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.infoModuleBtnRecording, true)) {
			element = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
			if(androidUtility.getElementStateAttribute(element, "focused"))
				tap(1);
			else {
				navigateRight(1);
				waitForSomeTime(1);
				element = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
				if (androidUtility.getElementStateAttribute(element, "focused")) {
					tap(1);
				}
			}
		}
		waitForSomeTime(2);
		if (seriesRecording != null) {
			System.out.println("Cancelling Series Recording");
			cancelSeriesRecording();
			deleteAllRecording(completedAndOngoingRecordingsCount);
		} else {
			System.out.println("Cancelling Individual recording");
			cancelAndDeleteIndividualRecording(completedAndOngoingRecordingsCount, ongoingRecordingCount, scheduledRecordingCount);
			waitForSomeTime(2);
		}
		waitForSomeTime(25);
	}
	
 	private void cancelAndDeleteIndividualRecording(int completedAndOngoingRecordingsCount, int ongoingRecordingCount, int scheduledRecordingCount) {
		if  (androidUtility.isElementPresentByID(androidDriver, XPaths.recordingListContainer, false)) {
			
			if (completedAndOngoingRecordingsCount > 0) {
				System.out.println("Stopping Individual Recording");
				for (int j = 0; j < ongoingRecordingCount; j++) {
					if (androidUtility.isElementPresentByID(androidDriver, XPaths.btnRecordedStopRecording, false)) {
						mElement = androidDriver.findElement(MobileBy.id(XPaths.btnRecordedStopRecording));
						mElement.click();
						waitForSomeTime(1);
						if (alertHelper.isAlertShown(AlertType.STOP_INDIVIDUAL_RECORDING_ALERT)) {
							recordingUIValidator.validateStopIndividualRecordingAlertTitle();
							waitForSomeTime(1);
							recordingUIValidator.validateStopIndividualRecordingAlertMessage();
							waitForSomeTime(1);
							recordingUIValidator.validateStopIndividualRecordingAlertButtons();
							waitForSomeTime(1);
							mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
							if (androidUtility.getElementStateAttribute(mElement, "focused")) {
								tap(1);
							}else{
								navigateRight(2);
								tap(1);
							}
							waitForSomeTime(Constants.SMALL_SLEEP);
							if (recordingUIValidator.isRecordingErrorAlertShown()) {
								Assert.assertFalse(true, "Recording Error Occurred");
							} else {
								System.out.println("Individual Recording Stopped Successfully");
							}
						}
					} else {
						Assert.assertFalse(!androidUtility.isElementPresentByID(androidDriver, XPaths.btnRecordedStopRecording, false), "Stop Series Recording Button not seen");
					}
				}
				waitForSomeTime(1);
				if (androidUtility.isElementPresentByID(androidDriver, XPaths.episodeRecordingDeleteAllBtn, false)) {
					System.out.println("Deleting All Recording");
					deleteAllRecording(completedAndOngoingRecordingsCount);
				} else {
					System.out.println("Deleting Individual Recording");
					for (int k = 0; k < completedAndOngoingRecordingsCount; k ++) {
						if (androidUtility.isElementPresentByID(androidDriver, XPaths.btnDeleteRecording, false)) {
							mElement = androidDriver.findElement(MobileBy.id(XPaths.btnDeleteRecording));
							mElement.click();

							if (alertHelper.isAlertShown(AlertType.DELETE_INDIVIDUAL_RECORDING_ALERT)) {
								recordingUIValidator.validateDeleteIndividualRecordingAlertTitle();
								waitForSomeTime(1);
								recordingUIValidator.validateDeleteIndividualRecordingAlertButtons();
							}
							waitForSomeTime(1);
							mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
							if (androidUtility.getElementStateAttribute(mElement, "focused")) {
								tap(1);
							}else{
								navigateRight(2);
								tap(1);
							}
							waitForSomeTime(Constants.SMALL_SLEEP);
							if (recordingUIValidator.isRecordingErrorAlertShown()) {
								Assert.assertFalse(true, "Recording Error Occurred");
							} else {
								System.out.println("Individual Recording Deleted Successfully");
							}
						}
					}
					if(androidUtility.isElementPresentByID(androidDriver, XPaths.recordingNoResultView, false)){
						navigateBack(1);
					}
				}
				if (androidUtility.isElementPresentByID(androidDriver, XPaths.recordingNoResultView, false)) {
					navigateBack(1);
				}
				return;
			}
			
			for (int i = 0; i < scheduledRecordingCount; i++) {
				if (androidUtility.isElementPresentByID(androidDriver, XPaths.recordingItemCancelBtn, false)) {
					mElement = androidDriver.findElement(MobileBy.id(XPaths.recordingItemCancelBtn));
					System.out.println("Cancelling Individual Recording from Scheduled Tab");
					mElement.click();
					waitForSomeTime(Constants.SMALL_SLEEP);
					if (recordingUIValidator.isRecordingErrorAlertShown()) {
						Assert.assertFalse(true, "Recording Error Occurred");
					}
				}
			}
			System.out.println("All Inidividually Recordings Cancelled Successfully");
			navigateUp(1);
			waitForSomeTime(Constants.SMALL_SLEEP);
			navigateBack(1);
		}
	}
	
	private void cancelSeriesRecording() {
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.episodeRecordingCancelBtn, false)) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.episodeRecordingCancelBtn));
			navigateUp(1);
			if (androidUtility.getElementStateAttribute(mElement, "focused")) {
				tap(1);
			}else{
				mElement.click();
			}
			waitForSomeTime(1);
			if (alertHelper.isAlertShown(AlertType.CANCEL_SERIES_RECORDING_ALERT)) {
				recordingUIValidator.validateCancelSeriesRecordingAlertTitle();
				waitForSomeTime(1);
				recordingUIValidator.validateCancelSeriesRecordingAlertMessage();
				waitForSomeTime(1);
				recordingUIValidator.validateCancelSeriesRecordingAlertButtons();
				waitForSomeTime(1);
				mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
				if (androidUtility.getElementStateAttribute(mElement, "focused")) {
					tap(1);
				}else{
					navigateRight(2);
					tap(1);
				}
				waitForSomeTime(Constants.SMALL_SLEEP);

				if (recordingUIValidator.isRecordingErrorAlertShown()) {
					Assert.assertFalse(true, "Recording Error Occurred");
				} else {
					System.out.println("Recording Stopped Successfully");
				}
			}

			if (androidUtility.isElementPresentByID(androidDriver, XPaths.recordingNoResultView, false)) {
				navigateBack(1);
			}
		} else {
			Assert.assertFalse(!androidUtility.isElementPresentByID(androidDriver, XPaths.episodeRecordingCancelBtn, false), "Cancel Series Recording Button not seen");
		}
	}

	public void deleteAllRecording(int episodeCount) {
		if(episodeCount!=0) {
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.episodeRecordingDeleteAllBtn, false)) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.episodeRecordingDeleteAllBtn));
			navigateUp(1);
			if (androidUtility.getElementStateAttribute(mElement, "focused")) {
				tap(1);
			}else{
				navigateRight(1);
				tap(1);
			}
			waitForSomeTime(2);
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.alertTitle, false)) {
				recordingUIValidator.validateDeleteAllRecordingsAlertTitle(episodeCount);
				waitForSomeTime(1);
				recordingUIValidator.validateDeleteAllRecordingsAlertMessage();
				waitForSomeTime(1);
				recordingUIValidator.validateDeleteAllRecordingsAlertButtons();
				waitForSomeTime(1);
				mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
				if (androidUtility.getElementStateAttribute(mElement, "focused")) {
					tap(1);
				}else{
					navigateRight(2);
					tap(1);
				}
				waitForSomeTime(Constants.SMALL_SLEEP);
				if (recordingUIValidator.isRecordingErrorAlertShown()) {
					Assert.assertFalse(true, "Recording Error Occurred");
				} else {
					System.out.println("All Compeleted Recording Deleted Successfully");
				}
			}
		} else {
			Assert.assertFalse(!androidUtility.isElementPresentByID(androidDriver, XPaths.episodeRecordingDeleteAllBtn, false), "Delete All Recording Button not seen");
		}
	}
	}

	public void setRecording(long startTime, long endTime, String episodeName, String seriesName, String seriesId, MobileElement element, boolean isSeriesRecordingToBeSet) {
		System.out.println("Setting Recording for : " + episodeName);
		waitForSomeTime(1);
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.infoModuleBtnRecording, true)) {
			element = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
			if(androidUtility.getElementStateAttribute(element, "focused")) {
				tap(1);
			} else {
				navigateRight(1);
				waitForSomeTime(1);
				element = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
				if(androidUtility.getElementStateAttribute(element, "focused")) {
					tap(1);
				}
//				element.click();
			}
		}
		waitForSomeTime(2);
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.recordAssetsOptionsFragmentContainer, false)) {
			tap(1);
			waitForSomeTime(2);
		}
		
		handleRestartRecordingAlert();    

		handleRecordingThresholdAlert();

		handleRecordingSetAlert(startTime, endTime, seriesName, isSeriesRecordingToBeSet);
		mElements = androidDriver.findElements(MobileBy.id(XPaths.alertTitle));
		if(mElements.size()>0){
			if(mElements.get(0).getText().equalsIgnoreCase("Something went wrong")){
				Assert.fail(" \'Something went wrong message\' displayed");
			}
		}


		if (isSeriesRecordingToBeSet && androidUtility.isElementPresentByID(androidDriver, XPaths.recordOptionsContainer, false)) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.recordOptionOKBtn));
			tap(1);
		}

		if (recordingUIValidator.isRecordingErrorAlertShown()) {
			isRecordingSet = false;
		} else {
			isRecordingSet = true;
		}

		recordingUIValidator.validateRecordButtonState(element, isRecordingSet);
		waitForSomeTime(1);
		if (isRecordingSet) {
			System.out.println("Recording is set");
			if(androidUtility.getElementStateAttribute(element, "focused")) {
				tap(1);
			} else {
				navigateRight(1);
				waitForSomeTime(1);
				element = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
				if(androidUtility.getElementStateAttribute(element, "focused")) {
					tap(1);
				}
			}
			waitForSomeTime(1);
			if (isSeriesRecordingToBeSet) {
				recordingUIValidator.validateRecordingScreenForSeriesRecording(seriesName, seriesId, startTime, endTime);
			} else {
				recordingUIValidator.validateRecordingScreenForIndividualRecording(seriesName, seriesId, episodeName, startTime, endTime);
			}
			navigateBack(1);
		}

	}

	private void handleRestartRecordingAlert() {
		if (alertHelper.isAlertShown(AlertType.RESTART_RECORDING_ALERT)) {
			recordingUIValidator.validateRestartRecordingAlert();
			waitForSomeTime(1);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
			if (androidUtility.getElementStateAttribute(mElement, "focused")) {
				tap(1);
			}else{
				navigateRight(2);
				tap(1);
			}
		}
	}

	private void handleRecordingThresholdAlert() {
		if (alertHelper.isAlertShown(AlertType.RECORDING_THRESHOLD_ALERT)) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.alertTitle));
			if ((mElement.getText().trim()).equals(dictionaryReader.getDictionaryValueForKey("are_you_sure_alert_title"))) {
				recordingUIValidator.validateRecordingThresholdAlertTitle(mElement.getText());
				waitForSomeTime(1);

				mElement = androidDriver.findElement(MobileBy.id(XPaths.message));
				recordingUIValidator.validateRecordingThresholdAlertMessage(mElement.getText().trim());
				
				recordingUIValidator.validateRecordingThresholdAlertButtons();
				
				mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
				waitForSomeTime(3);
				if (androidUtility.getElementStateAttribute(mElement, "focused")) {
					tap(1);
				}else{
					navigateRight(2);
					tap(1);
				}
				waitForSomeTime(1);
			}
		}
	}

	private void handleRecordingSetAlert(long startTime, long endTime, String seriesName, boolean isSeriesRecordingToBeSet) {
		if (alertHelper.isAlertShown(AlertType.RECORDING_SET_ALERT)) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.alertTitle));
			//recordingUIValidator.validateRecordingSetAlertTitle(mElement.getText());
			waitForSomeTime(1);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.message));
			if (utility.isLive(startTime, endTime)) {
				recordingUIValidator.validateLiveRecordingSetAlertMessage(mElement.getText().trim(),seriesName);
			} else {
				recordingUIValidator.validateUpcomingRecordingSetAlertMessage(mElement.getText().trim(), seriesName);
			}
			recordingUIValidator.validateRecordingSetAlertButtons();
//			mElement = isSeriesRecordingToBeSet ? androidDriver.findElement(MobileBy.id(XPaths.button1)) : androidDriver.findElement(MobileBy.id(XPaths.button2));
			if(isSeriesRecordingToBeSet){
				waitForSomeTime(1);
				mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
				navigateRight(3);
				tap(1);
			}else{
				waitForSomeTime(1);
				mElement = androidDriver.findElement(MobileBy.id(XPaths.button2));
				navigateLeft(3);
				tap(1);
			}

			waitForSomeTime(Constants.SMALL_SLEEP);
		} else {
			Assert.assertTrue(false, "Recording Set alert didn't appear");
		}
	}
	
	private boolean hasSufficientRecordingDuration(long startTime, long endTime) {
		Long episodeStartTime = null;
		episodeStartTime = utility.isLive(startTime, endTime) ? new DateAndTime().getCurrentEpochTime() : startTime;
		long duration = (endTime - episodeStartTime) / 60;
		long timeRemaining = recordingsJsonUtility.getRemainingMinutes();
		timeRemaining = Math.max(timeRemaining, 0);
		return timeRemaining >= duration;
	}
	
	public class RecordingUIValidator {
		
		public void validateRestartRecordingAlert() {
			System.out.println("\nValidating Restart Recording Alert Title");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.alertTitle));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("restart_recording_alert_title"), "restart_recording_alert_title is not as expected");
			System.out.println("\nValidating Restart Recording Alert Message");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.message));
			Assert.assertEquals(mElement.getText().trim(), (dictionaryReader.getDictionaryValueForKey("restart_recording_alert_message")), "restart_recording_alert_message is not as expected");
			System.out.println("\nValidating Restart Recording Alert Buttons");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button2));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("rec_alert_cancel"), "Cancel button text is not as expected");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("rec_alert_ok"), "OK button text is not as expected");		
		}
		
		public void validateRecordButtonState(MobileElement element, boolean isRecordingSet) {
			System.out.println("\nValidating Record Button State");
			String expectedMessage = isRecordingSet ? dictionaryReader.getDictionaryValueForKey("manage_recordings_text") : dictionaryReader.getDictionaryValueForKey("start_recording");
			Assert.assertEquals(element.getText(), expectedMessage, "Record Button State didn't match");
		}
		
		public void validateToastMessage() {
			//Need to Implement
		}
		
		public void validateRecordingThresholdAlertTitle(String uiTitle) {
			System.out.println("\nValidating Recording Threshold Alert Title");
			Assert.assertEquals(uiTitle, dictionaryReader.getDictionaryValueForKey("are_you_sure_alert_title"), "Recording threshold title didn't match");
		}
		
		public void validateRecordingThresholdAlertMessage(String uiMessage) {
			System.out.println("\nValidating Recording Threshold Alert Message");
			long timeLeft = Math.max(recordingsJsonUtility.getRemainingMinutes(), 0);
			String recThresholdAlertMessage = utility.getStringReplaced(dictionaryReader.getDictionaryValueForKey("time_available_alert_message"), ImmutableMap.of("{TIME_AVAILABLE}", ""));
			Assert.assertEquals(methodReplacingMinutes(uiMessage), recThresholdAlertMessage, "Rec threshold message didn't match");
		}
		
		public void validateRecordingThresholdAlertButtons() {
			System.out.println("\nValidating Recording Threshold Alert Buttons");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("rec_alert_continue"));
			
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button2));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("manage_recording_button_title"));
			
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button3));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("rec_alert_cancel"));
		}
		
		public void validateRecordingSetAlertTitle(String uiTitle) {
			System.out.println("\nValidating Recording Set Alert Title");
//			Assert.assertEquals(uiTitle, dictionaryReader.getDictionaryValueForKey("manage_recording_episode_recording_set_alert_title"), "Recording Set alert title didn't match");
		}
		
		public void validateLiveRecordingSetAlertMessage(String uiMessage,String seriesName) {
			System.out.println("\nValidating Live Recording Set Alert Message");
			String recordingSetAlertMessage = utility.getStringReplaced(dictionaryReader.getDictionaryValueForKey("manage_recording_live_episode_recording_set_alert_message"), ImmutableMap.of("{SERIES_TITLE}", seriesName));
			Assert.assertEquals(recordingSetAlertMessage, uiMessage, "Live Recording Set alert message didn't match");
		}
		
		public void validateUpcomingRecordingSetAlertMessage(String uiMessage,String seriesName) {
			System.out.println("\nValidating Upcoming Recording Set Alert Message");
			String recordingSetAlertMessage = utility.getStringReplaced(dictionaryReader.getDictionaryValueForKey("manage_recording_upcoming_episode_recording_set_alert_message"), ImmutableMap.of("{SERIES_TITLE}", seriesName));
			Assert.assertEquals(uiMessage, recordingSetAlertMessage, "Upcoming Recording Set alert message didn't match");
		}
		
		public void validateRecordingSetAlertButtons() {
			System.out.println("\nValidating Recording Set Alert Buttons");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
			Assert.assertTrue(mElement.getText().trim().equals(dictionaryReader.getDictionaryValueForKey("rec_alert_future_yes")), "Record All Episodes button mismatched");
			
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button2));
			Assert.assertTrue(mElement.getText().trim().equals(dictionaryReader.getDictionaryValueForKey("manage_recording_recording_set_alert_episode")), "Record this episode only button mismatched");
			
		}
		
		public void validateRecordingScreenForIndividualRecording(String seriesName, String seriesId, String episodeName, long startTime, long endTime) {
			System.out.println("\nValidating Recording Screen");
			boolean hasRecordedEpisode = utility.isValidCollection(recordingsJsonUtility.getCompletedIndividualRecordingsForSeries(seriesId)) || utility.isValidCollection(recordingsJsonUtility.getOngoingIndividualRecordingsForSeries(seriesId)) ? true : false;
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.episodeRecordingDetailsHeader, false)) {
				validateSeriesName(seriesName, startTime, endTime, hasRecordedEpisode);
				validateStartSeriesRecordingButton(seriesId);
			}
			waitForSomeTime(1);
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.recordingListFragContainer, false)) {
				validateRecordedEpisodeName(episodeName);
			}
		}
		
		public void validateRecordingScreenForSeriesRecording(String seriesName, String seriesId, long startTime, long endTime) {
			System.out.println("\nValidating Recording Screen");
			boolean hasRecordedEpisode = utility.isValidCollection(recordingsJsonUtility.getCompletedIndividualRecordingsForSeries(seriesId)) || utility.isValidCollection(recordingsJsonUtility.getOngoingIndividualRecordingsForSeries(seriesId)) ? true : false;

			if (androidUtility.isElementPresentByID(androidDriver, XPaths.episodeRecordingDetailsHeader, false)) {
				validateSeriesName(seriesName, startTime, endTime, hasRecordedEpisode);
			}
		}

		public void validateSeriesName(String seriesName, long startTime, long endTime, boolean hasRecordedEpisode) {
			String validString = null;
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.episodeRecordingTitle, false)) {
//			validString =  hasRecordedEpisode || utility.isUpcoming(startTime, endTime) ? dictionaryReader.getDictionaryValueForKey("recorded_episode_screen_title") : dictionaryReader.getDictionaryValueForKey("scheduled_episodes_screen_title");
			if(hasRecordedEpisode || utility.isLive(startTime, endTime) ){
				validString =dictionaryReader.getDictionaryValueForKey("recorded_episode_screen_title");
			}else if(utility.isUpcoming(startTime, endTime)){
				validString = dictionaryReader.getDictionaryValueForKey("scheduled_episodes_screen_title");
			}
			System.out.println("validString : "+ validString);
			seriesName = utility.getStringReplaced(validString, ImmutableMap.of("{SERIES_TITLE}", seriesName));
			mElement = androidDriver.findElement(MobileBy.id(XPaths.episodeRecordingTitle));
			mElement.getText().trim();
			Assert.assertEquals(mElement.getText(), seriesName, "Series name mismatched in the Episode Recording Header Item");
			} else {
				Assert.assertFalse(androidUtility.isElementPresentByID(androidDriver, XPaths.episodeRecordingTitle, false), "Episode Recording header title is missing");
			}
		}
		
		public void validateStartSeriesRecordingButton(String seriesId) {
			if (recordingsJsonUtility.getSeriesRecordingById(seriesId) == null && androidUtility.isElementPresentByID(androidDriver, XPaths.episodeSetSeriesRecordingBtn, false)) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.episodeSetSeriesRecordingBtn));
				mElement.getText().trim();
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("start_series_recording"), "Start Series Recording button mismatched");
			} else {
				Assert.assertFalse(androidUtility.isElementPresentByID(androidDriver, XPaths.episodeSetSeriesRecordingBtn, false), "Start Series Recording Button Should not be shown");
			}
		}
		
		public void validateRecordedEpisodeName(String formatedEpisodeName) {
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.episodeListName, false)) {
				mElements = androidDriver.findElements(MobileBy.id(XPaths.episodeListName));
				mElement = mElements.get(0);
				Assert.assertEquals(mElement.getText().trim(), formatedEpisodeName, "Recorded Episode Name Mismatched");
			} else {
				Assert.assertFalse(androidUtility.isElementPresentByID(androidDriver, XPaths.episodeListName, false), "No Episodes Seen in the UI");
			}
		}
		
		public void validateCancelSeriesRecordingAlertTitle() {
			System.out.println("\nValidating Cancel Series Recording Alert Title");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.alertTitle));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("cancel_series_recording_alert_title"));
		}
		
		public void validateCancelSeriesRecordingAlertMessage() {
			System.out.println("\nValidating Cancel Series Recording Alert Message");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.message));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("cancel_series_recording_alert_message"));
		}
		
		public void validateCancelSeriesRecordingAlertButtons() {
			System.out.println("\nValidating Cancel Series Recording Alert Buttons");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("rec_alert_yes"));
			waitForSomeTime(1);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button2));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("rec_alert_no"));
		}
		
		public void validateStopIndividualRecordingAlertTitle() {
			System.out.println("\nValidating Cancel Individual Recording Alert Title");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.alertTitle));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("stop_recording_alert_title"));
		}
		
		public void validateStopIndividualRecordingAlertMessage() {
			System.out.println("\nValidating Cancel Individual Recording Alert Message");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.message));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("stop_recording_alert_message"));
		}
		
		public void validateStopIndividualRecordingAlertButtons() {
			System.out.println("\nValidating Cancel Individual Recording Alert Buttons");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("rec_alert_yes"));
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button2));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("rec_alert_no"));
		}
		
		public void validateDeleteAllRecordingsAlertTitle(int episodeCount) {
			System.out.println("\nValidating Delete Series Recording Alert Title");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.alertTitle));
			String toBeReplacedString = episodeCount > 1 ? dictionaryReader.getDictionaryValueForKey("delete_all_recordings_alert_title_text") : dictionaryReader.getDictionaryValueForKey("delete_all_recordings_alert_title_text_for_single_recording");
			String deleteAllRecordingAlertTitle = utility.getStringReplaced(toBeReplacedString, ImmutableMap.of("{EPISODE_COUNT}", String.valueOf(episodeCount)));
			Assert.assertEquals(mElement.getText().trim(), deleteAllRecordingAlertTitle);
		}
		
		public void validateDeleteAllRecordingsAlertMessage() {
			System.out.println("\nValidating Delete Series Recording Alert Message");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.message));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("delete_all_recordings_alert_message_text"));
		}
		
		public void validateDeleteAllRecordingsAlertButtons() {
			System.out.println("\nValidating Delete Series Recording Alert Buttons");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("delete_all_button_text"));
			waitForSomeTime(1);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button2));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("rec_alert_no"));
		}
		
		public void validateDeleteIndividualRecordingAlertTitle() {
			System.out.println("\nValidating Delete Individual Recording Alert Title");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.alertTitle));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("delete_recording_alert_title"));
		}
		
		public void validateDeleteIndividualRecordingAlertButtons() {
			System.out.println("\nValidating Delete Individual Recording Alert Buttons");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("rec_alert_yes"));
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button2));
			Assert.assertEquals(mElement.getText().trim(), dictionaryReader.getDictionaryValueForKey("rec_alert_no"));
		}
		
		public boolean isRecordingErrorAlertShown() {
			return alertHelper.isAlertShown(AlertType.RECORDING_ERROR_ALERT);
		}
	}
}
