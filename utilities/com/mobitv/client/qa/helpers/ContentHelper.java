package com.mobitv.client.qa.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.mobitv.client.qa.Comparators.AssetsStartTimeComparator;
import com.mobitv.client.qa.baseTests.BaseTest;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.functions.DateAndTime;
import com.mobitv.client.qa.functions.Utility;
import com.mobitv.client.qa.json.ChannelsJSONUtility;
import com.mobitv.client.qa.json.MainJSONUtility;
import com.mobitv.client.qa.json.RecordingsJSONUtility;
import com.mobitv.client.qa.json.SearchJSONUtility;
import com.mobitv.client.qa.models.Assets;
import com.mobitv.client.qa.models.Channel;
import com.mobitv.client.qa.models.Episode;
import com.mobitv.client.qa.models.Program;
import com.mobitv.client.qa.models.Show;

public class ContentHelper extends BaseTest {
	public Assets getFirstUpcomingAsset(Episode episodedata) {
		List<Episode> allUpcomingEpisodes = mainJSONUtility.getAllUpcomingEpisodes(episodedata.getSeries_id(), "");
		
		for (Episode episode: allUpcomingEpisodes) {
			if (episodedata.getID().equals(episode.getID())) {
				List<Assets> allProgramAssets = new ArrayList<>();
				
				if (utility.isSharedEpisode(episode.getID())) {
					List<Assets> allAssets = episode.getAssets();
					for (Assets asset : allAssets) {
						if (asset.getRef_type().equals(Constants.REF_TYPE_PROGRAM) && asset.getStart_time() > new DateAndTime().getCurrentEpochTime()) {
							allProgramAssets.add(asset);
						}
					}
					if (!allProgramAssets.isEmpty()) {
						if (allProgramAssets.size() > 1) {
							Collections.sort(allProgramAssets, new AssetsStartTimeComparator());
						}
						return allProgramAssets.get(0);
					}
			}
		}
		}
		return null;
	}
	
	public Program getNextAvailableProgram(Show show) {
		List<String> channelIds = new ArrayList<>();
		ArrayList<Channel> channels = recordingsJsonUtility.getChannelListForSeries(show.getID(), channelsJSONUtility.getAllChannels(-1));
		
		for (Channel channel : channels) {
			channelIds.add(channel.getID());
		}
		String channelId = utility.collectionToString(channelIds);
		List<Program> nextAvailableProgram = searchJSONUtility.getProgramByChannelId(channelId, show.getID(), 1);
		return nextAvailableProgram.isEmpty() ? null : nextAvailableProgram.get(0);
	}
	
	/**
	 * 
	 * @param episode
	 * @return 'true' if the episode is airing LIVE
	 */
	public boolean isLiveEpisode(Episode episode) {
		if (utility.isSharedEpisode(episode.getID())) {
			List<Assets> episodeAssets = episode.getAssets();
			for (Assets asset : episodeAssets) {
				if (utility.isLiveAsset(asset)) {
					return true;
				}
			}
		} else {
			return utility.isLive(episode.getStart_time(), episode.getEnd_time());
		}
		return false;
	}
	

	/**
	 * 
	 * @param episode
	 * @return 'true' if episode can be recorded
	 */
	public boolean isRecordable(Episode episode) {
		Long currTime = (new DateAndTime()).getCurrentEpochTime();
		// If Episode is of type 'shared_ref', fetch the LIVE and UPCOMING
		// assets of the episode that can be recorded
		if (utility.isSharedEpisode(episode.getID())
				&& (isEpisodeLiveRecordingEnabled(episode) || isEpisodeUpcomingRecordingenabled(episode))) {
			return true;
		} else if (episode.is_recording_enabled() && currTime >= episode.getStart_time()
				&& currTime < (episode.getEnd_time() - Constants.RECORDING_THRESHOLD_TIME)) {
			return true;
		}
		return false;
	}
	
	/**
	 * method to return Recording enabled for Live episode
	 * 
	 * @param movie
	 * @return
	 */
	public boolean isEpisodeLiveRecordingEnabled(Episode episode) {
		boolean flag = false;
		if (!episode.getAssets().isEmpty())
			for (int j = 0; j < episode.getAssets().size(); j++) {
				if (episode.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isLive(episode.getAssets().get(j).getStart_time(), episode.getAssets().get(j).getEnd_time() - Constants.RECORDING_THRESHOLD_TIME) && (isRecordingEnabledAsset(episode.getAssets().get(j))))
						flag = true;
				}
			}
		return flag;
	}
	
	/**
	 * Method to return Recording available for upcoming episode
	 * 
	 * @param movie
	 * @return
	 */
	public boolean isEpisodeUpcomingRecordingenabled(Episode episode) {
		boolean flag = false;
		if (!episode.getAssets().isEmpty())
			for (int j = 0; j < episode.getAssets().size(); j++) {
				if (episode.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isUpcoming(episode.getAssets().get(j).getStart_time(), episode.getAssets().get(j).getEnd_time()) && (isRecordingEnabledAsset(episode.getAssets().get(j))))
						flag = true;
					break;
				}
			}
		return flag;
	}
	
	/**
	 * Method to test recording enabled or not
	 * 
	 * @param asset
	 * @return
	 */
	public boolean isRecordingEnabledAsset(Assets asset) {
		return asset.is_recording_enabled();
	}
	
	/**
	 * 
	 * @param episode
	 *            Recently watched of current selected Show
	 * @return returns 'true' if the episode's asset has any Completed OR
	 *         Ongoing Recording
	 */
	public boolean isRecordedEpisode(Episode episode) {
		List<Assets> episodeAssets = episode.getAssets();
		if(episodeAssets != null){
		if (!episodeAssets.isEmpty() && utility.isSharedEpisode(episode.getID())) {
			for (Assets asset : episodeAssets) {
				System.out.println("asset.getName() : "+ asset.getName());
				if (recordingsJsonUtility.isRecordingSetForEpisode(asset.getId())) {
					return true;
				}
			}
		}
//		else {
			if (recordingsJsonUtility.isRecordingSetForEpisode(episode.getID())) {
				return true;
			}
//		}
		}
		return false;
	}

}
