package com.mobitv.client.qa.helpers;

import com.mobitv.client.qa.baseTests.AndroidTVBaseTest;
import com.mobitv.client.qa.constants.XPaths;

import io.appium.java_client.MobileBy;

public class AlertHelper extends AndroidTVBaseTest {
	
	public static enum AlertType {
		RESTART_RECORDING_ALERT,
		RECORDING_THRESHOLD_ALERT,
		RECORDING_SET_ALERT,
		RECORDING_ERROR_ALERT,
		STOP_INDIVIDUAL_RECORDING_ALERT,
		CANCEL_SERIES_RECORDING_ALERT,
		DELETE_INDIVIDUAL_RECORDING_ALERT
	}
	public boolean isAlertShown(AlertType type) {
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.alertTitle, true)) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.alertTitle));
			String alertTitle = mElement.getText().trim();
			switch(type) {
			case RESTART_RECORDING_ALERT:
				return alertTitle.equals(dictionaryReader.getDictionaryValueForKey("restart_recording_alert_title"));
			
			case RECORDING_THRESHOLD_ALERT:
				return alertTitle.equals(dictionaryReader.getDictionaryValueForKey("are_you_sure_alert_title"));
				
			case RECORDING_SET_ALERT:
				return alertTitle.equalsIgnoreCase(dictionaryReader.getDictionaryValueForKey("manage_recording_episode_recording_set_alert_title"));
			
			case RECORDING_ERROR_ALERT:
				return alertTitle.equals(dictionaryReader.getDictionaryValueForKey("rec_error_alert_title"));
				
			case STOP_INDIVIDUAL_RECORDING_ALERT:	
				return alertTitle.equals(dictionaryReader.getDictionaryValueForKey("stop_recording_alert_title"));
			
			case CANCEL_SERIES_RECORDING_ALERT:
				return alertTitle.equals(dictionaryReader.getDictionaryValueForKey("cancel_series_recording_alert_title"));
			
			case DELETE_INDIVIDUAL_RECORDING_ALERT:
				return alertTitle.equals(dictionaryReader.getDictionaryValueForKey("delete_recording_alert_title"));
				
			default:
				return false;
			}
		} 
		return false;
	}
}
