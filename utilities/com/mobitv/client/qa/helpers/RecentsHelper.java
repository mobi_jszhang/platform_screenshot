package com.mobitv.client.qa.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.mobitv.client.qa.Comparators.RecentsComparator;
import com.mobitv.client.qa.baseTests.BaseTest;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.functions.Utility;
import com.mobitv.client.qa.json.MainJSONUtility;
import com.mobitv.client.qa.json.RecordingsJSONUtility;
import com.mobitv.client.qa.models.Episode;
import com.mobitv.client.qa.models.Show;
import com.mobitv.client.qa.models.Recents.RecentsListItem;

public class RecentsHelper extends BaseTest {

	/**
	 * 
	 * @param show
	 * @return Fetch the recently watched episode of the selected show
	 */
	public Episode getEpisodeToBeResumed(Show show) {
		System.out.println("\nValidating the Episode to be resumed");
		Episode episode = null;
		List<Episode> allEpisodes = new ArrayList<>();
		allEpisodes = mainJSONUtility.getPlayableEpisodesForSeries(show);

		if (allEpisodes.isEmpty()) {
			return null;
		}

		List<RecentsListItem> allIncompleteRecentsItemsForEpisode = new ArrayList<>();
		List<RecentsListItem> allCompeltedRecentsItemForEpisode = new ArrayList<>();
		List<RecentsListItem> mostRecentsItemForEpisode = new ArrayList<>();

		// Check if Series has any partially watched episode
		for (int i = 0; i < allEpisodes.size(); i++) {
			allIncompleteRecentsItemsForEpisode.addAll(
					mainJSONUtility.getRecentsForRefId(BaseTest.currentProfileID, false, "", allEpisodes.get(i).getID()));
			if (allIncompleteRecentsItemsForEpisode.size() > 1) {
				Collections.sort(allIncompleteRecentsItemsForEpisode, new RecentsComparator());
				mostRecentsItemForEpisode.add(allIncompleteRecentsItemsForEpisode.get(0));
				allIncompleteRecentsItemsForEpisode.clear();
			} else if (allIncompleteRecentsItemsForEpisode.size() == 1) {
				mostRecentsItemForEpisode.add(allIncompleteRecentsItemsForEpisode.get(0));
				allIncompleteRecentsItemsForEpisode.clear();
			}
		}

		if (!mostRecentsItemForEpisode.isEmpty()) {
			if (mostRecentsItemForEpisode.size() > 1) {
				Collections.sort(mostRecentsItemForEpisode, new RecentsComparator());
			}

			for (int j = 0; j < allEpisodes.size(); j++) {
				if (mostRecentsItemForEpisode.get(0).ref_type.equals(Constants.REF_TYPE_RECORDING) && allEpisodes.get(j)
						.getID().equals(recordingsJsonUtility.getEpisodeIdFromRecording(mostRecentsItemForEpisode.get(0).ref_id))) {
					return allEpisodes.get(j);
				}

				if (allEpisodes.get(j).getID().equals(mostRecentsItemForEpisode.get(0).ref_id)) {
					return allEpisodes.get(j);
				}

			}
		} else {
			// If no partial watched episode present, check if series has
			// completely watched episode
			for (int i = 0; i < allEpisodes.size(); i++) {
				allCompeltedRecentsItemForEpisode.addAll(mainJSONUtility.getRecentsForRefId(BaseTest.currentProfileID,
						true, "", allEpisodes.get(i).getID()));
				if (allCompeltedRecentsItemForEpisode.size() > 1) {
					Collections.sort(allCompeltedRecentsItemForEpisode, new RecentsComparator());
					mostRecentsItemForEpisode.add(allCompeltedRecentsItemForEpisode.get(0));
					allCompeltedRecentsItemForEpisode.clear();
				} else if (allCompeltedRecentsItemForEpisode.size() == 1) {
					mostRecentsItemForEpisode.add(allCompeltedRecentsItemForEpisode.get(0));
					allCompeltedRecentsItemForEpisode.clear();
				}
				
				
			}

			if (!mostRecentsItemForEpisode.isEmpty()) {
				if (mostRecentsItemForEpisode.size() > 1) {
					Collections.sort(mostRecentsItemForEpisode, new RecentsComparator());
				}
				for (int j = 0; j < allEpisodes.size(); j++) {
					// If the recent event is of type recording, get the episode
					// data for that recording
					if (mostRecentsItemForEpisode.get(0).ref_type.equals(Constants.REF_TYPE_RECORDING)
							&& allEpisodes.get(j).getID()
									.equals(recordingsJsonUtility.getEpisodeIdFromRecording(mostRecentsItemForEpisode.get(0).ref_id))) {
						episode = allEpisodes.get(j);
					}

					if (allEpisodes.get(j).getID().equals(mostRecentsItemForEpisode.get(0).ref_id)) {
						episode = allEpisodes.get(j);
					}
				}

				// Since episode was completely watched, verify if there is any
				// next playable content available

				int index = getIndexOfNextPlayableEpisode(allEpisodes, episode);
				
				// If no content available Series Info module will be shown
				if (index == -1) {
					return null;
				}
				System.out.println("Index after computation is : " + index);
				episode = allEpisodes.get(index);
				return episode;
			}
		}
		return null;
	}

	private int getIndexOfNextPlayableEpisode(List<Episode> episodes, Episode lastWatchedEpisode) {
		System.out.println("Last watched episode is : " + lastWatchedEpisode.getID() + " with name "+ lastWatchedEpisode.getName());
		int index = episodes.indexOf(lastWatchedEpisode) - 1;

		System.out.println("Index is : " + index);
		
		for (int newIndex = index ; newIndex >= 0 ; newIndex--) {
			if (utility.isSharedEpisode(episodes.get(newIndex).getID())) {
				for (int j = 0; j < episodes.get(newIndex).getAssets().size(); j++) {
					if (episodes.get(newIndex).getAssets().get(j).getRef_type().equals(Constants.REF_TYPE_PROGRAM) && !utility.isUpcomingAsset(episodes.get(newIndex).getAssets().get(j))) {
						return newIndex;
					} else if (episodes.get(newIndex).getAssets().get(j).getRef_type().equals(Constants.REF_TYPE_VOD)){
						return newIndex;
					}
				}
			} else if (episodes.get(newIndex).getType().equals(Constants.REF_TYPE_PROGRAM) && !utility.isUpcoming(episodes.get(newIndex).getStart_time(), episodes.get(newIndex).getEnd_time())){
				return newIndex;
			} else if (episodes.get(newIndex).getType().equals(Constants.REF_TYPE_VOD)) {
				return newIndex;
			}
		}
		return -1;
	}

}
