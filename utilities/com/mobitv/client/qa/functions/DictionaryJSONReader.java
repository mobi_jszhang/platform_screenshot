package com.mobitv.client.qa.functions;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.remote.JsonException;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.json.DictionaryJSONUtility;
import com.mobitv.client.qa.models.ClientConfig;
/**
 * Class for reading and updating local dictionary files with that of the
 * server.
 * 
 * @author MobiTV
 *
 */
public class DictionaryJSONReader {
	private DictionaryJSONUtility dictionaryJSONUtility = new DictionaryJSONUtility();
	private JSONObject dictionaryFileJSON, flavorFileJSON, dictionaryFromServer;
	private String preferredLanguageCode, key, value;
	private JSONArray jsonArray = null;
	private ArrayList<String> arrayList;
	private Pattern pattern;
	private Matcher matcher;

	/**
	 * Constructor of TestConfiguration, read all test configuration based on
	 * carrier and host
	 * 
	 * @param prefferredlocale
	 * @throws IOException
	 * @throws JSONException
	 */
	public DictionaryJSONReader(String currentDevicelocale, ClientConfig clientConfiguration) throws IOException {
		preferredLanguageCode = dictionaryJSONUtility.getPreferredLocale(currentDevicelocale, clientConfiguration);
		dictionaryFileJSON = dictionaryJSONUtility.getJSONFromFile(new FileReader("dictionaries/dictionary_" + preferredLanguageCode + ".json"));
		flavorFileJSON = dictionaryJSONUtility.getJSONFromFile(new FileReader("dictionaries/" + ConfigurationConstants.carrier + "/flavor_" + preferredLanguageCode + ".json"));
		dictionaryFromServer = dictionaryJSONUtility.getDictionaryFromServer(preferredLanguageCode);
		if (null != dictionaryFromServer) {
			for (Iterator<String> iterator = dictionaryFileJSON.keySet().iterator(); iterator.hasNext();) {
				key = (String) iterator.next();
				try {
					value = dictionaryFromServer.getString(key);
					dictionaryFileJSON.put(key, value);
				} catch (JSONException e) {}
			}
			for (Iterator<String> iterator = flavorFileJSON.keySet().iterator(); iterator.hasNext();) {
				key = (String) iterator.next();
				try {
					value = dictionaryFromServer.getString(key);
					flavorFileJSON.put(key, value);
				} catch (JSONException e) {}
			}
		}
	}

	/**
	 * Method for getting a specific value for mentioned key
	 * 
	 * @param key
	 * @return
	 */
	public String getDictionaryValueForKey(String key) {
		try {
            value = flavorFileJSON.getString(key);
		} catch (JSONException e) {
			try {
                value = dictionaryFileJSON.getString(key);
			} catch (JsonException g) {
				return Utility.EMPTY_STRING;
			}
		}
		return validateDictionaryValue(value);
	}

	/**
	 * Method to replace place holders in dictionary value.
	 * 
	 * @param value
	 * @return
	 */
	private String validateDictionaryValue(String s) {
		pattern = Pattern.compile("\\{(.*?)\\}");
		matcher = pattern.matcher(s);
		while (matcher.find()) {
			key = matcher.group().replace("{", "").replace("}", "").toLowerCase();
			try {
				value = flavorFileJSON.getString(key);
			} catch (JSONException e) {
				value = "no matching found";
			}
			if (!value.equals("no matching found"))
				s = (s.replace(matcher.group(), value));
		}
		return s;
	}

	/**
	 * Method to get Android Mobile MenuItems.
	 * 
	 * @return
	 */
	public ArrayList<String> getAndroidMobileMenuItems() {
		try {
			jsonArray = dictionaryFileJSON.getJSONArray("android_mobile_menu_items");
			arrayList = new ArrayList<>();
			for (int i = 0; i < jsonArray.length(); i++) {
				arrayList.add(jsonArray.getString(i));
			}
			return arrayList;
		} catch (JSONException e) {
			return null;
		}
	}

	/**
	 * Method to get Android TV MenuItems.
	 * 
	 * @return
	 */
	public ArrayList<String> getAndroidTVMenuItems() {
		try {
			jsonArray = dictionaryFileJSON.getJSONArray("android_tv_menu_items");
			arrayList = new ArrayList<>();
			for (int i = 0; i < jsonArray.length(); i++) {
				arrayList.add(jsonArray.getString(i));
			}
			return arrayList;
		} catch (JSONException e) {
			return null;
		}
	}

	/**
	 * Method to get Android TV Category filters.
	 * 
	 * @return
	 */
	public ArrayList<String> getAndroidTVCategoryFilters() {
		try {
			jsonArray = dictionaryFileJSON.getJSONArray("android_tv_category_filters");
			arrayList = new ArrayList<>();
			for (int i = 0; i < jsonArray.length(); i++) {
				arrayList.add(jsonArray.getString(i));
			}
			return arrayList;
		} catch (JSONException e) {
			return null;
		}
	}

	/**
	 * Method to get Android TV search results sections.
	 * 
	 * @return
	 */
	public ArrayList<String> getAndroidTVSearchResultsSections() {
		try {
			jsonArray = dictionaryFileJSON.getJSONArray("android_tv_search_results_sections");
			arrayList = new ArrayList<>();
			for (int i = 0; i < jsonArray.length(); i++) {
				arrayList.add(jsonArray.getString(i));
			}
			return arrayList;
		} catch (JSONException e) {
			return null;
		}
	}

	/**
	 * Method to get Android TV settings MenuItems.
	 * 
	 * @return
	 */
	public ArrayList<String> getAndroidTVSettingsMenuItems() {
		try {
			jsonArray = dictionaryFileJSON.getJSONArray("android_tv_settings_menu_items");
			arrayList = new ArrayList<>();
			for (int i = 0; i < jsonArray.length(); i++) {
				arrayList.add(jsonArray.getString(i));
			}
			return arrayList;
		} catch (JSONException e) {
			return null;
		}
	}
}
