package com.mobitv.client.qa.functions;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 * 
 * @author MobiTV
 *
 */
public class TestConfigurationJsonReader implements TestConfigurationReader {
	private String inputLine;
	private StringBuffer response;
	private BufferedReader br;
	private String jsonString;
	private JSONObject testConfigurationJSON, carrierJSON, hostJSON;
	private JSONArray carriers;
	private JSONObject jsonObject;

	/**
	 * Constructor of TestConfiguration, read all test configuration based on
	 * carrier and host
	 * 
	 * @param carrier
	 * @param host
	 * @throws IOException
	 * @throws JSONException
	 */
	public TestConfigurationJsonReader(String carrier, String host) throws IOException, JSONException {
		br = new BufferedReader(new FileReader("./resources/testConfiguration.json"));
		response = new StringBuffer();
		while ((inputLine = br.readLine()) != null) {
			response.append(inputLine);
			response.append('\r');
		}
		br.close();
		jsonString = response.toString();
		testConfigurationJSON = new JSONObject(jsonString);
		carriers = testConfigurationJSON.getJSONArray("carriers");
		for (int i = 0; i < carrier.length(); i++) {
			jsonObject = carriers.getJSONObject(i);
			if (jsonObject.getString("carrierName").equals(carrier)) {
				carrierJSON = jsonObject;
				break;
			}
		}
		switch (host) {
		case "integration":
			hostJSON = carrierJSON.getJSONArray("integration").getJSONObject(0);
			break;
		case "staging":
			hostJSON = carrierJSON.getJSONArray("staging").getJSONObject(0);
			break;
		case "production":
			hostJSON = carrierJSON.getJSONArray("production").getJSONObject(0);
			break;
		}
	}

	/**
	 * Method to get appiumServer
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getAppiumServer() throws JSONException {
		return testConfigurationJSON.getString("appiumServer");
	}

	/**
	 * Method to get product
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getProduct() throws JSONException {
		return testConfigurationJSON.getString("product");
	}

	/**
	 * Method to get version
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getVersion() throws JSONException {
		return testConfigurationJSON.getString("version");
	}

	/**
	 * Method to get appActivity
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getAppActivity() throws JSONException {
		return testConfigurationJSON.getString("appActivity");
	}

	/**
	 * Method to get appPackage
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getAppPackage() throws JSONException {
		return carrierJSON.getString("appPackage");
	}

	/**
	 * Method to get ext_assertion_source
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getExtAssertionSource() throws JSONException {
		return carrierJSON.getString("ext_assertion_source");
	}

	/**
	 * Method to get oauth_client_id
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getOauthClientID() throws JSONException {
		return carrierJSON.getString("oauth_client_id");
	}

	/**
	 * Method to get oauth_client_secret
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getOauthClientSecret() throws JSONException {
		return carrierJSON.getString("oauth_client_secret");
	}

	/**
	 * Method to get native_device_id
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getNativeDeviceID() throws JSONException {
		return carrierJSON.getString("native_device_id");
	}

	/**
	 * Method for getting recording enabling status
	 * 
	 * @return boolean
	 * @throws JSONException
	 */
	public boolean isRecordEnabled() throws JSONException {
		return carrierJSON.getBoolean("isRecordEnabled");
	}

	/**
	 * Method for getting anonymous mode enabling status
	 * 
	 * @return boolean
	 * @throws JSONException
	 */
	public boolean isAnonymousModeEnabled() throws JSONException {
		return carrierJSON.getBoolean("isAnonymousModeEnabled");
	}

	/**
	 * Method for getting catch up enabling status
	 * 
	 * @return boolean
	 * @throws JSONException
	 */
	public boolean isChannelNumberEnabled() throws JSONException {
		return carrierJSON.getBoolean("isChannelNumberEnabled");
	}

	/**
	 * Method for getting catch up enabling status
	 * 
	 * @return boolean
	 * @throws JSONException
	 */
	public boolean isCatchUpEnabled() throws JSONException {
		return carrierJSON.getBoolean("isCatchUpEnabled");
	}

	/**
	 * Method for getting outOfHome status
	 * 
	 * @return boolean
	 * @throws JSONException
	 */
	public boolean isOutOfHomeEnabled() throws JSONException {
		return carrierJSON.getBoolean("isOutOfHomeEnabled");
	}

	/**
	 * Method to get vid
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getVid() throws JSONException {
		return hostJSON.getString("vid");
	}

	/**
	 * Method to get userName
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getUserName() throws JSONException {
		return hostJSON.getString("userName");
	}

	/**
	 * Method to get password
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getPassword() throws JSONException {
		return hostJSON.getString("password");
	}

	/**
	 * Method to get Authorization
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getAuthorization() throws JSONException {
		return hostJSON.getString("Authorization");
	}

	/**
	 * Method to get oauth_host
	 * 
	 * @return String
	 * @throws JSONException
	 */
	public String getOauthHost() throws JSONException {
		return hostJSON.getString("oauth_host");
	}
}
