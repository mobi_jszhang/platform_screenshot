package com.mobitv.client.qa.functions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * This class will be useful for all Date and time related requirements in
 * automation.
 *
 * @author MobiTV
 */
public class DateAndTime {
	private long epochTime;
	private SimpleDateFormat simpleDateFormat;
	private String regXPattern, time;
	private int currentMinute;
	private Date date;
	private static final String HMM = "h:mm";
	private static final String H = "h";
	private static final String A = "a";
    private static SimpleDateFormat sDateFormat;
    private static final String MD = "M/d";


	/**
	 * Method to get current epoch time
	 *
	 * @return currentEpoch time as string
	 */
	public Long getCurrentEpochTime() {
		epochTime = (System.currentTimeMillis() / 1000);
		return (epochTime);
	}

	/**
	 * Method to get one week back epoch time to the current epoch time.
	 *
	 * @return one week back epoch time as string
	 */
	public String getOneWeekBeforeEpochTime() {
		epochTime = (System.currentTimeMillis() / 1000) - (86400 * 7);
		return Long.toString(epochTime);
	}

	/**
	 * Method to get current time in AM or PM
	 *
	 * @return
	 */
	public String getCurrentTimeInAMOrPM() {
		simpleDateFormat = new SimpleDateFormat("hh:mm a");
		regXPattern = "^0+";
		return simpleDateFormat.format(new Date()).replaceAll(regXPattern, "");
	}

	/**
	 * Method to get next 30 minutes time in AM or PM
	 *
	 * @return
	 */
	public String getNext30MinsTimeInAMOrPM() {
		simpleDateFormat = new SimpleDateFormat("hh:mm a");
		regXPattern = "^0+";
		date = new Date(Long.parseLong(getNextRoundedHalfAnHourEpochTime()) * 1000);
		time = simpleDateFormat.format(date).replaceAll(regXPattern, "");
		if (time.contains("00"))
			return time.replace(":00", "");
		return time;
	}

	/**
	 * Method to get the minutes difference to next rounded half an hour to the
	 * current time.
	 *
	 * @return
	 */
	public int getDiffInMinutesToNextHalfAnHour() {
		Calendar calendar = Calendar.getInstance();
		currentMinute = calendar.get(Calendar.MINUTE);
		if (currentMinute >= 30)
			return 60 - currentMinute;
		return 30 - currentMinute;
	}

	/**
	 * Method to get Next rounded half an hour epoch time
	 *
	 * @return
	 */
	public String getNextRoundedHalfAnHourEpochTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, getDiffInMinutesToNextHalfAnHour());
		epochTime = calendar.getTimeInMillis() / 1000;
		return Long.toString(epochTime);
	}

	public String getCurrentTimeZoneOffsetFromGMT() {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		DateFormat date = new SimpleDateFormat("ZZZZZ", Locale.getDefault());
		return date.format(calendar.getTime()).replace(":", "");
	}

	/**
	 * Method for getting epoch time required for Up coming half an hour slots.
	 *
	 * @param epochTime
	 * @param minutesToadd
	 * @return
	 */
	public String getEpochTimeByaddingMinutes(String epochTime, int minutesToadd) {
		this.epochTime = (Long.parseLong(epochTime) * 1000 + minutesToadd * 60 * 1000) / 1000;
		return Long.toString(this.epochTime);
	}

	/**
	 * Method to convert given minutes (String)
	 *
	 * @param seconds
	 * @return
	 */
	public String convertSecondsToMinutes(int seconds) {
		int minutes = seconds / 60;
		return minutes + " minutes";
	}

	public String getDateInFormatEEEE(Date givenDate) {
		DateFormat dayFormat = new SimpleDateFormat("EEEE");
		return dayFormat.format(givenDate);
	}

	public String getDateInFormatMMDD(Date givenDate) {
		DateFormat dayFormat = new SimpleDateFormat("MMMMM d");
		return dayFormat.format(givenDate);
	}

	public String getDateInFormathhmma(Date givenDate) {
		DateFormat dayFormat = new SimpleDateFormat("h:mm a");
//		return dayFormat.format(givenDate);
		if (Integer.parseInt(getDateInFormatmm(givenDate))>0) {
			return dayFormat.format(givenDate);
		} else {
			return getDateInFormathh(givenDate);
		}

	}

	public String getDateInFormatmm(Date givenDate) {
		DateFormat dayFormat = new SimpleDateFormat("mm");
		return dayFormat.format(givenDate);
	}

	public String getDateInFormathh(Date givenDate) {
		DateFormat dayFormat = new SimpleDateFormat("h a");
		return dayFormat.format(givenDate);
	}

	public static String getDateInFormatmdyy(Date givenDate) {
		DateFormat dayFormat = new SimpleDateFormat("M/d/yy");
		return dayFormat.format(givenDate);
	}

	public static String getTimeInFormatHHMMwithTrim(Date startTimeDate, boolean showMeridiem) {
		Calendar calendar = Calendar.getInstance(Locale.US);
		calendar.setTime(startTimeDate);
		int minutes = calendar.get(Calendar.MINUTE);
		SimpleDateFormat timeFormat;
		StringBuilder format = new StringBuilder();
		format.append(minutes > 0 ? HMM : H);
		if (showMeridiem) {
			format.append(" ").append(A);
		}
		timeFormat = new SimpleDateFormat(format.toString(), Locale.US);
		return timeFormat.format(startTimeDate);
	}
	 
	public long getTodaysEpochTimeAt1159PM() {
		ZoneId z = ZoneId.of("America/Los_Angeles");
		ZonedDateTime midnightUtc = LocalDate.now(z).atStartOfDay(z);
		long millisecondsSinceEpoch = midnightUtc.toInstant().toEpochMilli() + 86399000;
		return (millisecondsSinceEpoch / 1000);
	}

	public String getRecordingOptions(long givenAssetEpochStartTime, long givenAssetEpochEndTime) {
		long timeStamp = ((long) givenAssetEpochStartTime ) * 1000;
		Date givenDate = new Date(timeStamp);
		String toReturn = null;
		if ((givenAssetEpochStartTime - getTodaysEpochTimeAt1159PM()) < 0) {
			if (givenAssetEpochStartTime < getCurrentEpochTime() && getCurrentEpochTime() < givenAssetEpochEndTime) {
				toReturn = "Airing Now";
			} else {
				toReturn = "Today" + " at " + getDateInFormathhmma(givenDate);
			}
		} else if ((givenAssetEpochStartTime - getTodaysEpochTimeAt1159PM()) > 0 && (givenAssetEpochStartTime - getTodaysEpochTimeAt1159PM()) < 86400) {
			toReturn = "Tomorrow" + " at " + getDateInFormathhmma(givenDate);
		} else if ((givenAssetEpochStartTime - getTodaysEpochTimeAt1159PM()) < 518400) {
			toReturn = getDateInFormatEEEE(givenDate) + " at " + getDateInFormathhmma(givenDate);
		} else {
			toReturn = getDateInFormatMMDD(givenDate) + " at " + getDateInFormathhmma(givenDate);
		}
		return toReturn.replace(":00", "");
	}

	public String getRecordingOptionsMobile(long givenAssetEpochStartTime, long givenAssetEpochEndTime) {
		long timeStamp = ((long) givenAssetEpochStartTime / 1800) * 1800 * 1000;
		Date givenDate = new Date(timeStamp);
		String toReturn = null;
		if ((givenAssetEpochStartTime - getTodaysEpochTimeAt1159PM()) < 0) {
			if (givenAssetEpochStartTime < getCurrentEpochTime() && getCurrentEpochTime() < givenAssetEpochEndTime) {
				toReturn = "Airing Now";
			} else {
				toReturn = "Today" + " at " + getDateInFormathhmma(givenDate);
			}
		} else if ((givenAssetEpochStartTime - getTodaysEpochTimeAt1159PM()) > 0 && (givenAssetEpochStartTime - getTodaysEpochTimeAt1159PM()) < 86400) {
			toReturn = "Tomorrow" + " at " + getDateInFormathhmma(givenDate);
		} else if ((givenAssetEpochStartTime - getTodaysEpochTimeAt1159PM()) < 518400) {
			toReturn = getDateInFormatEEEE(givenDate) + " at " + getDateInFormathhmma(givenDate);
		} else {
			toReturn = getDateInFormatMMDD(givenDate) + " at " + getDateInFormathhmma(givenDate);
		}
		return toReturn.replace(":00", "");
	}

	public String getDatefromEpoch(long epoch) {
		simpleDateFormat = new SimpleDateFormat("M/d");
		regXPattern = "^0";
		date = new Date(epoch * 1000);
		time = simpleDateFormat.format(date).replaceAll(regXPattern, "");
		return time;
	}

	public String getDateAndTimefromEpoch(long epoch) {
		simpleDateFormat = new SimpleDateFormat("M/d h:mm a");
		date = new Date(epoch * 1000);
		time = simpleDateFormat.format(date).replaceAll("^0", "");
		return time.replace(":00", "");
	}
	
	public static String durationFromSecs(String time) {
		long secs = Long.parseLong(time);
        if (secs <= 0)  {
            return null;
        }
        long durationMinutes = Math.max(1, secs / 60);
        StringBuilder duration = new StringBuilder();
        duration.append(durationMinutes).append(" ");
        if (durationMinutes > 1) {
            duration.append("minutes");
        } else {
            duration.append("minute");
        }
        return duration.toString();
    }
	
	public static  String format(String format, Date date, TimeZone timeZone) {
		try {
	    sDateFormat = new SimpleDateFormat();
        sDateFormat.applyPattern(format);
        sDateFormat.setTimeZone(timeZone);
        return sDateFormat.format(date);
    }
		catch (Exception e) {
			e.printStackTrace();
			}
		return null;
		}
	
	public static String format(String format, Date date) {
        return format(format, date, TimeZone.getDefault());
    }
	
	 public static String getDateInFormatMD(Date date) {
	        return format(MD, date);
	    }
	 
	 public Integer getSecondFromHHMMSS(String formattedTime) {
			String[] h1=formattedTime.split(":"); 
			if (h1.length == 3) {
				int hour=Integer.parseInt(h1[0].trim());
				int minute=Integer.parseInt(h1[1].trim());
				int second=Integer.parseInt(h1[2].trim());
				return second + (60 * minute) + (3600 * hour);
			} else {
				int hour = 0;
				int minute = Integer.parseInt(h1[0].trim());
				int second = Integer.parseInt(h1[1].trim());
				return second + (60 * minute) + (3600 * hour);
			}

	 }
	 
	 public String getExpiryDateFromEpoch(long epoch) {
			LocalDateTime now = LocalDateTime.now().with(LocalTime.MAX);
			ZoneId zoneId = ZoneId.systemDefault();
			long endofDayEpoch = now.with(LocalTime.MAX).atZone(zoneId).toEpochSecond();
			System.out.println(now.with(LocalTime.MAX).atZone(zoneId).toEpochSecond() + " " + epoch * 1000);
			if (epoch == 0)
				return "";
			if (epoch <= endofDayEpoch)
				simpleDateFormat = new SimpleDateFormat("h:mm a");
			else 
				simpleDateFormat = new SimpleDateFormat("M/d");
			regXPattern = "^0";
			date = new Date(epoch * 1000);
			time = simpleDateFormat.format(date).replaceAll(regXPattern, "");
			return time.replace(":00", "");
		}
}
