package com.mobitv.client.qa.functions;
import java.io.IOException;
/**
 * 
 * @author MobiTV
 *
 */
public class MessagesReader extends TestConfigurationPropertiesReader {
	public MessagesReader(String carrier, String host) throws IOException {
		super(carrier, host);
	}

	/**
	 * Method to read message by key.
	 * 
	 * @param key
	 * @return message
	 * @throws IOException
	 */
	public String getMessage(String key) throws IOException {
		return properties.getProperty(key);
	}
}
