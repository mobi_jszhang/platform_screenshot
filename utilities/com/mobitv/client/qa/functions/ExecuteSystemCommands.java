package com.mobitv.client.qa.functions;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
/**
 * 
 * @author MobiTV
 *
 */
public class ExecuteSystemCommands {
	private String osName, cmd;
	private File file;
	private Process process;
	private FileWriter fileWriter;

	public ExecuteSystemCommands(String osName) {
		this.osName = osName;
	}

	/**
	 * Method to destroy the process once it's work is done.
	 */
	private void destroyProcess() {
		if (this.process != null) {
			this.process.destroyForcibly();
		}
	}

	/**
	 * Method to close file writer once it's work is done.
	 */
	private void closeFileWriter() {
		if (this.fileWriter != null) {
			try {
				this.fileWriter.close();
			} catch (IOException e) {}
		}
	}

	/**
	 * Method to clear logs on a specified device.
	 * 
	 * @param serialNumber
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void clearADBLogs(String serialNumber) throws IOException, InterruptedException {
		switch (osName) {
		case "win":
			process = Runtime.getRuntime().exec("cmd /c adb -s " + serialNumber + " logcat -c");
			break;
		case "mac":
			// Needs to be implemented.
			break;
		case "ubu":
			// Needs to be implemented.
			break;
		}
		Thread.sleep(1000);// This is mandatory, no other feasible solution as of now.
		destroyProcess();
	}

	/**
	 * Method to get all error logs from a particular time stamp.
	 * 
	 * @param serialNumber
	 * @param fileName
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void writeADBErrorLogs(String serialNumber, String fileName) throws IOException, InterruptedException {
		try {
			file = new File("./TestReport/Logs/" + fileName + ".log");
			file.createNewFile();
			switch (osName) {
			case "win":
				// cmd = "cmd /c adb -s " + serialNumber + " logcat -d '*:E' > " +
				// file.getAbsolutePath();
				cmd = "cmd /c adb -s " + serialNumber + " logcat -d > " + file.getAbsolutePath();
				process = Runtime.getRuntime().exec(cmd);
				break;
			case "mac":
				fileWriter = new FileWriter(file);
				fileWriter.write("adb logcat (programatically) for mac os is yet to implement");
				break;
			case "ubu":
				fileWriter = new FileWriter(file);
				fileWriter.write("adb logcat (programatically) for linux os is yet to implement");
				break;
			}
			Thread.sleep(1000);// This is mandatory, no other feasible solution as of now.
			closeFileWriter();
			destroyProcess();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
