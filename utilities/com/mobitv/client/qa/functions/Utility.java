package com.mobitv.client.qa.functions;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import org.apache.commons.io.FileDeleteStrategy;

import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.models.Assets;
import com.mobitv.client.qa.models.Episode;
/**
 * 
 * @author MobiTV
 *
 */
public class Utility {
	private File directory = null;
	private StringJoiner stringJoiner;
	private static DateAndTime dAndt = new DateAndTime();
	 public static final String EMPTY_STRING = "";
	 public static final String SEPARATOR_COMMA = ",";

	/**
	 * Method to delete all files in a given folder.
	 * 
	 * @param path
	 * @throws IOException
	 */
	public void removeDirectoryFiles(String path) throws IOException {
		directory = new File(path);
		if (directory.isDirectory())
			if (directory.list().length > 0)
				for (File file : directory.listFiles()) {
					FileDeleteStrategy.FORCE.delete(file);
				}
	}

	/**
	 * Method to give concatenated string.
	 * 
	 * @param s
	 * @param metaDatas
	 * @return
	 */
	public String concatinateWith(String s, ArrayList<String> metaDatas) {
		stringJoiner = new StringJoiner(s);
		for (String metaData : metaDatas) {
			stringJoiner.add(metaData);
		}
		return stringJoiner.toString();
	}

	/**
	 * Method to get channel number in 3 digits.
	 * 
	 * @param channelNumber
	 * @return
	 */
	public String get3DigitChannelNumber(String channelNumber) {
		if (channelNumber.length() == 1)
			return "00" + channelNumber;
		else if (channelNumber.length() == 2)
			return "0" + channelNumber;
		return channelNumber;
	}
	
	public boolean isRecordableDanglingEpisode(long currTime, Episode episode) {
		if (episode != null && episode.getType().equals("program")) {
			long startTime = episode.getStart_time();
			long endTime = episode.getEnd_time();
			if (episode.is_recording_enabled() && ((isLive(startTime, endTime) && (currTime + 300) < endTime)|| isUpcoming(startTime, endTime))) {
				return true;
			}
		}
	
	return false;
	}

	public List<Assets> getListOfAllRecordableAssets(long currTime, List<Assets> allAssets) {
		List<Assets> allRecordableAssets = new ArrayList<>();
		long startTime = 0;
		long endTime = 0;
		if (!allAssets.isEmpty()) {
			for (Assets asset : allAssets) {
				if (!asset.getType().equalsIgnoreCase(Constants.REF_TYPE_VOD)) {
				startTime = asset.getStart_time();
				endTime = asset.getEnd_time();
				if (asset.is_recording_enabled() && ((isLive(startTime , endTime) && (currTime + 300) < endTime) || isUpcoming(startTime, endTime))) {
					allRecordableAssets.add(asset);
				}
			}
			}
		}
		return allRecordableAssets;
	}
	
	public boolean hasLiveAssets(Episode episode) {
		if (!episode.getAssets().isEmpty()) {
			for (Assets asset : episode.getAssets()) {
				if (isLiveAsset(asset)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean isUpcomingAsset(Assets asset) {
		if (asset.getRef_type().equalsIgnoreCase("VOD")) {
			return false;
		}
		return isUpcoming(asset.getStart_time(), asset.getEnd_time());
	}
	
	public boolean isLiveAsset(Assets asset) {
		if (asset.getRef_type().equalsIgnoreCase("VOD")) {
			return false;
		}
		return isLive(asset.getStart_time(), asset.getEnd_time());
	}
	/**
	 * Method to validate live time
	 * 
	 * @param start_time
	 * @param end_time
	 * @return
	 */
	public boolean isLive(Long start_time, Long end_time) {
		if (start_time == null || end_time == null) {
			return false;
		}
		return start_time <= dAndt.getCurrentEpochTime() && dAndt.getCurrentEpochTime() <= end_time;
	}
	
	/**
	 * Method to validate upcoming time
	 * 
	 * @param start_time
	 * @param end_time
	 * @return
	 */
	public boolean isUpcoming(Long start_time, Long end_time) {
		return start_time > dAndt.getCurrentEpochTime();
	}

	/**
	 * Method to validate catch up time
	 * 
	 * @param start_time
	 * @param end_time
	 * @return
	 */
	public boolean isCatchup(Long start_time, Long end_time) {
		return !isUpcoming(start_time, end_time) && !isLive(start_time, end_time);
	}
	
	/**
	 * 
	 * @param episodeId
	 * @return 'true' if Episode is of type 'shared_ref'
	 */
	public boolean isSharedEpisode(String id) {
		if (id != null && id.contains("tms:")) {
			return true;
		}
		return false;
	}
	
	public String getStringReplaced(String string, Map<String, String> keyMapping) {
     
        for (String key : keyMapping.keySet()) {
            string = string.replace(key, keyMapping.get(key));
        }

        return string;
	}

	public String collectionToString(Collection<?> list) {
		if (list.isEmpty()) {
			return EMPTY_STRING;
		}
        StringBuilder builder = new StringBuilder();
        for (Object item : list) {
            if(builder.length() != 0) {
                builder.append(SEPARATOR_COMMA);
            }
            builder.append(item == null ? EMPTY_STRING : item.toString());
        }
        return builder.toString();
    }
	
	public boolean isValidCollection(Collection<?> list) {
		return list != null && !list.isEmpty();
	}
}
