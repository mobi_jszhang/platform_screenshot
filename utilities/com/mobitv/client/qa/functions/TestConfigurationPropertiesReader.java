package com.mobitv.client.qa.functions;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.json.JSONException;
/**
 * 
 * @author MobiTV
 *
 */
public class TestConfigurationPropertiesReader implements TestConfigurationReader {
	private String propFileName = null;
	protected Properties properties = new Properties();
	private InputStream inputStream;
	private String host;

	/**
	 * Constructor of TestConfiguration reader, loads the property file based on
	 * carrier
	 * 
	 * @param carrier
	 * @param host
	 * @throws IOException
	 */
	public TestConfigurationPropertiesReader(String carrier, String host) throws IOException {
		this.host = host;
		inputStream = initProp(carrier);
		properties.load(inputStream);
	}

	/**
	 * Method to get appiumServer
	 * 
	 * @return String
	 */
	public String getAppiumServer() {
		return properties.getProperty("appiumServer");
	}

	/**
	 * Method to get product
	 * 
	 * @return String
	 */
	public String getProduct() {
		return properties.getProperty("product");
	}

	/**
	 * Method to get version
	 * 
	 * @return String
	 */
	public String getVersion() {
		return properties.getProperty("version");
	}

	/**
	 * Method to get appActivity
	 * 
	 * @return String
	 */
	public String getAppActivity() {
		return properties.getProperty("appActivity");
	}

	/**
	 * Method to get appPackage
	 * 
	 * @return String
	 */
	public String getAppPackage() {
		return properties.getProperty("appPackage");
	}

	/**
	 * Method to get ext_assertion_source
	 * 
	 * @return String
	 * 
	 */
	public String getExtAssertionSource() {
		return properties.getProperty("ext_assertion_source");
	}

	/**
	 * Method to get oauth_client_id
	 * 
	 * @return String
	 * 
	 */
	public String getOauthClientID() {
		return properties.getProperty("oauth_client_id");
	}

	/**
	 * Method to get oauth_client_secret
	 * 
	 * @return String
	 * 
	 */
	public String getOauthClientSecret() {
		return properties.getProperty("oauth_client_secret");
	}

	/**
	 * Method to get native_device_id
	 * 
	 * @return String
	 * 
	 */
	public String getNativeDeviceID() {
		return properties.getProperty("native_device_id");
	}

	/**
	 * Method for getting recording enabling status
	 * 
	 * @return boolean
	 * 
	 */
	public boolean isRecordEnabled() {
		return Boolean.parseBoolean(properties.getProperty("isRecordEnabled"));
	}

	/**
	 * Method for getting anonymous mode enabling status
	 * 
	 * @return boolean
	 * 
	 */
	public boolean isAnonymousModeEnabled() {
		return Boolean.parseBoolean(properties.getProperty("isAnonymousModeEnabled"));
	}

	/**
	 * Method for getting channle number enabling status
	 * 
	 * @return boolean
	 * @throws JSONException
	 */
	public boolean isChannelNumberEnabled() throws JSONException {
		return Boolean.parseBoolean(properties.getProperty("isChannelNumberEnabled"));
	}

	/**
	 * Method for getting catch up enabling status
	 * 
	 * @return boolean
	 * 
	 */
	public boolean isCatchUpEnabled() {
		return Boolean.parseBoolean(properties.getProperty("isCatchUpEnabled"));
	}

	/**
	 * Method for getting outOfHome status
	 * 
	 * @return boolean
	 * 
	 */
	public boolean isOutOfHomeEnabled() {
		return Boolean.parseBoolean(properties.getProperty("isOutOfHomeEnabled"));
	}

	/**
	 * Method to get vid
	 * 
	 * @return String
	 * 
	 */
	public String getVid() {
		return properties.getProperty("vid_" + host);
	}

	/**
	 * Method to get userName
	 * 
	 * @return String
	 * 
	 */
	public String getUserName() {
		return properties.getProperty("userName_" + host);
	}

	/**
	 * Method to get password
	 * 
	 * @return String
	 * 
	 */
	public String getPassword() {
		return properties.getProperty("password_" + host);
	}

	/**
	 * Method to get Authorization
	 * 
	 * @return String
	 * 
	 */
	public String getAuthorization() {
		return properties.getProperty("Authorization_" + host);
	}

	/**
	 * Method to get oauth_host
	 * 
	 * @return String
	 * 
	 */
	public String getOauthHost() {
		return properties.getProperty("oauth_host_" + host);
	}

	/**
	 * Method for getting respective property file
	 * 
	 * @param carrier
	 * @return property file as input stream
	 * @throws IOException
	 */
	private InputStream initProp(String carrier) throws IOException {
		propFileName = carrier.toLowerCase() + ".properties";
		return new FileInputStream(new File("testConfigurations/" + propFileName));
	}
}
