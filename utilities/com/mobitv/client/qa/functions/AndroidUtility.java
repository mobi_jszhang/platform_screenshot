package com.mobitv.client.qa.functions;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import com.mobitv.client.qa.constants.Constants;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
public class AndroidUtility {
	/**
	 * Method to turn off driver implicit waits.
	 * 
	 * @param androidDriver
	 */
	public void trunOfImplicitWait(AndroidDriver<MobileElement> androidDriver) {
		androidDriver.manage().timeouts().implicitlyWait(50, TimeUnit.MILLISECONDS);
	}

	/**
	 * Method to manage androidDriver ImplcitWait
	 * 
	 * @param androidDriver,
	 *            waitTime (in Seconds)
	 */
	public void manageImplicitWait(AndroidDriver<MobileElement> androidDriver, int waitTime) {
		androidDriver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);
	}

	/**
	 * Method to turn on driver implicit waits.
	 * 
	 * @param androidDriver
	 */
	public void turnOnImplicitWait(AndroidDriver<MobileElement> androidDriver) {
		androidDriver.manage().timeouts().implicitlyWait(Constants.DRIVER_IMPLICIT_WAIT, TimeUnit.SECONDS);
	}

	/**
	 * Method to enter text in a text field, by clearing the already existing text
	 * iside it.
	 * 
	 * @param mElement
	 * @param text
	 */
	public void clearAndInputText(MobileElement mElement, String text) {
		mElement.clear();
		mElement.sendKeys(text);
	}

	/**
	 * Method to get element state attribute
	 * 
	 * @param mElement
	 * @param attribute
	 * @return true/false
	 */
	public boolean getElementStateAttribute(MobileElement mElement, String attribute) {
		return Boolean.parseBoolean(getElementAttribute(mElement, attribute));
	}

	/**
	 * Method to get element attribute value.
	 * 
	 * @param mElement
	 * @param attribute
	 * @return String
	 */
	public String getElementAttribute(MobileElement mElement, String attribute) {
		return mElement.getAttribute(attribute);
	}

	/**
	 * Method to check whether given element is present or not using ID
	 * 
	 * @param androidDriver
	 * @param id
	 * @return boolean
	 */
	public boolean isElementPresentByID(AndroidDriver<MobileElement> androidDriver, String id, boolean useImplicitWait) {
		try {
			if (!useImplicitWait)
				trunOfImplicitWait(androidDriver);
			androidDriver.findElement(MobileBy.id(id));
			turnOnImplicitWait(androidDriver);
			return true;
		} catch (Exception e) {
			turnOnImplicitWait(androidDriver);
			return false;
		}
	}

	/**
	 * Method to check whether given element is present or not using Accessibility
	 * ID
	 * 
	 * @param androidDriver
	 * @param Accessibility
	 *            ID
	 * @return boolean
	 */
	public boolean isElementPresentByAcsID(AndroidDriver<MobileElement> androidDriver, String acsID, boolean useImplicitWait) {
		try {
			if (!useImplicitWait)
				trunOfImplicitWait(androidDriver);
			androidDriver.findElement(MobileBy.id(acsID));
			turnOnImplicitWait(androidDriver);
			return true;
		} catch (Exception e) {
			turnOnImplicitWait(androidDriver);
			return false;
		}
	}

	/**
	 * Method to check whether given elements are present or not using IDs
	 * 
	 * @param androidDriver
	 * @param ids
	 * @return boolean
	 */
	public boolean isElementPresentByID(AndroidDriver<MobileElement> androidDriver, ArrayList<String> ids, boolean useImplicitWait) {
		for (String id : ids) {
			if (isElementPresentByID(androidDriver, id, useImplicitWait))
				return true;
		}
		return false;
	}

	/**
	 * Method to check whether given element is present or not using class
	 * 
	 * @param androidDriver
	 * @param className
	 * @return boolean
	 */
	public boolean isElementPresentByClass(AndroidDriver<MobileElement> androidDriver, String className, boolean useImplicitWait) {
		try {
			if (!useImplicitWait)
				trunOfImplicitWait(androidDriver);
			androidDriver.findElement(MobileBy.className(className));
			turnOnImplicitWait(androidDriver);
			return true;
		} catch (Exception e) {
			turnOnImplicitWait(androidDriver);
			return false;
		}
	}

	/**
	 * Method to check whether given element is present or not using name
	 * 
	 * @param androidDriver
	 * @param name
	 * @return boolean
	 */
	public boolean isElementPresentByName(AndroidDriver<MobileElement> androidDriver, String name, boolean useImplicitWait) {
		try {
			if (!useImplicitWait)
				trunOfImplicitWait(androidDriver);
			androidDriver.findElement(MobileBy.name(name));
			turnOnImplicitWait(androidDriver);
			return true;
		} catch (Exception e) {
			turnOnImplicitWait(androidDriver);
			return false;
		}
	}

	/**
	 * Method to check whether given element is present or not using XPath
	 * 
	 * @param androidDriver
	 * @param xpath
	 * @return boolean
	 */
	public boolean isElementPresentByXPath(AndroidDriver<MobileElement> androidDriver, String xpath, boolean useImplicitWait) {
		try {
			if (!useImplicitWait)
				trunOfImplicitWait(androidDriver);
			androidDriver.findElement(MobileBy.xpath(xpath));
			turnOnImplicitWait(androidDriver);
			return true;
		} catch (Exception e) {
			turnOnImplicitWait(androidDriver);
			return false;
		}
	}

	/**
	 * Method to move focus to the required element.
	 * 
	 * @param element
	 * @param driver
	 */
	public void moveFocuseToElement(MobileElement mElement, AndroidDriver<MobileElement> androidDriver) {
		int x = mElement.getLocation().getX();
		int y = mElement.getLocation().getY();
		TouchAction action = new TouchAction(androidDriver);
		action.press(x, y).moveTo(x, y).release().perform();
	}
}