package com.mobitv.client.qa.functions;
/**
 * Wrapper class for TestConfiguration readers
 * 
 * @author MobiTV
 *
 */
public interface TestConfigurationReader {
	/**
	 * Method to get appiumServer
	 */
	public String getAppiumServer();

	/**
	 * Method to get product
	 */
	public String getProduct();

	/**
	 * Method to get version
	 */
	public String getVersion();

	/**
	 * Method to get appActivity
	 */
	public String getAppActivity();

	/**
	 * Method to get appPackage
	 */
	public String getAppPackage();

	/**
	 * Method to get ext_assertion_source
	 */
	public String getExtAssertionSource();

	/**
	 * Method to get oauth_client_id
	 */
	public String getOauthClientID();

	/**
	 * Method to get oauth_client_secret
	 */
	public String getOauthClientSecret();

	/**
	 * Method to get native_device_id
	 */
	public String getNativeDeviceID();

	/**
	 * Method for getting recording enabling status
	 */
	public boolean isRecordEnabled();

	/**
	 * Method for getting anonymous mode enabling status
	 */
	public boolean isAnonymousModeEnabled();

	/**
	 * Method for getting catch up enabling status
	 */
	public boolean isCatchUpEnabled();

	/**
	 * Method for getting Channel up enabling status
	 */
	public boolean isChannelNumberEnabled();

	/**
	 * Method for getting outOfHome status
	 */
	public boolean isOutOfHomeEnabled();

	/**
	 * Method to get vid
	 */
	public String getVid();

	/**
	 * Method to get userName
	 */
	public String getUserName();

	/**
	 * Method to get password
	 */
	public String getPassword();

	/**
	 * Method to get Authorization
	 */
	public String getAuthorization();

	/**
	 * Method to get oauth_host
	 */
	public String getOauthHost();
}
