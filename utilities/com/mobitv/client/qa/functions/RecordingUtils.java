package com.mobitv.client.qa.functions;
import com.mobitv.client.qa.models.Program;

public class RecordingUtils {
	
	public Boolean isUpcomingProgram(Program program) {
		if (program.getStart_time() > (System.currentTimeMillis() / 1000))
			return true;
		else
			return false;
	}
	
	public Boolean isLiveProgram(Program program) {
		if(program.getStart_time() < (System.currentTimeMillis() / 1000) && (program.getEnd_time() > (System.currentTimeMillis() / 1000)))
			return true;
		else
			return false;
	}
	        

}
