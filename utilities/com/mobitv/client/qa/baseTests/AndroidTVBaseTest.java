package com.mobitv.client.qa.baseTests;
import static org.testng.Assert.assertEquals;
import java.io.IOException;
import java.util.*;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import com.mobitv.client.qa.constants.AndroidKeyEvents;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.constants.XPaths;
import com.mobitv.client.qa.functions.DateAndTime;
import com.mobitv.client.qa.models.*;
import com.mobitv.client.qa.models.Recents.RecentsListItem;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
//import tests.mobitv.client.qa.nma.regression.tv.SeriesEpisodeInlineInfoModuleValidator;
//import tests.mobitv.client.qa.nma.regression.tv.TestRecordButton;
/**
 *
 * @author MobiTV
 *
 */
public class AndroidTVBaseTest extends AndroidBaseTest {
	private String buttonAction = null;
	private int catchupCount = 0;
	private int liveCount = 0;
	private int upcomingCount = 0;
	public boolean isRecordingset = false;
	public String buttonState = null;
	public List<Movie> recordedMovies = null;
	public static final long RECORDING_THRESHOLD_TIME = 300;
	JSONObject jsonObject;

	/**
	 * This method will be useful for initializing Android TV specific test
	 * environment.
	 */
	@BeforeSuite(alwaysRun = true)
	public void initializeAndroidTVTestEnvironment() {
		androidTVMenuItems = dictionaryReader.getAndroidTVMenuItems();
		androidTVCategoryFilters = dictionaryReader.getAndroidTVCategoryFilters();
		androidTVSearchResultSections = dictionaryReader.getAndroidTVSearchResultsSections();
		androidTVSettingsMenuItems = dictionaryReader.getAndroidTVSettingsMenuItems();
	}

	/**
	 * Method to validate whether application is already in logged in state or not.
	 * If not login.
	 */
	@BeforeClass(alwaysRun = true)
	public void signInVerification() {
		try {
			if (!verifyApplicationLogin()) {
				signIn();
			}
			selectProfile(false);
			setParentalControlsTurnedOff();
		} catch (Exception e) {}
	}

	/**
	 * Getting android driver before executing every test case. It will launch the
	 * app, based on the given device name, if multiple devices are connected to the
	 * host machine.
	 *
	 * @throws InterruptedException
	 * @
	 */
	@BeforeMethod(alwaysRun = true)
	public void initializeAndroidDriver() {
		for (int i = 0; i < 4; i++) {
			navigateBack(1);
		}
		for (int i = 0; i < 5; i++) {
			if (!androidUtility.isElementPresentByID(androidDriver, XPaths.menuTabsContainer, true)) {
				navigateBack(1);
			} else {
				navigateBack(1);
				break;
			}
		}
		waitForSomeTime(Constants.MEDIUM_SLEEP);
	}

	/**
	 * Currently this method is here to represent framework structure , in future it
	 * may get it's significance.
	 */
	@AfterMethod
	public void androidTVAfterMethod() {}

	/**
	 * Setting parental controls to off, before executing tests
	 */
	public void setParentalControlsTurnedOff() {
		goToParentalControlsScreen();
		mElement = androidDriver.findElement(MobileBy.id(XPaths.restrictedMatureContentStatus));
		if (mElement.getText().equals("ON")) {
			tap(1);
			waitForSomeTime(Constants.VERY_SMALL_SLEEP);
			// Enter four 1's
			for (int i = 0; i < 4; i++) {
				tap(1);
			}
			mElements = androidDriver.findElements(MobileBy.id(XPaths.alertTitle));
			if (mElements.size() > 0) {
				tap(1);
				// Enter four 0's
				for (int j = 0; j < 9; j++) {
					navigateRight(1);
				}
				for (int i = 0; i < 4; i++) {
					tap(1);
				}
			}
		}
	}

	/**
	 * Method to verify whether the application is already logged in or not.
	 * 
	 * @return login status
	 */
	protected boolean verifyApplicationLogin() {
		if (!ConfigurationConstants.isAnonymousModeEnabled) {
			return !androidUtility.isElementPresentByID(androidDriver, XPaths.userName, true);
		} else {
			clickMenu();
			/*
			 * mElement = androidDriver.findElement(MobileBy.id(XPaths.menuTabsContainer));
			 * mElements =
			 * mElement.findElements(MobileBy.className("android.widget.TextView")); if
			 * (mElements.size() > 3) {
			 */
			if (androidUtility.isElementPresentByID(androidDriver, "tv_signin_menu_tab", true))
				return false;
			else
				return true;
		}
	}

	/**
	 * Method for signIn to to the application.
	 */
	protected void signIn() {
		if (ConfigurationConstants.isAnonymousModeEnabled)
			clickSignIn();
		super.signIn(ConfigurationConstants.userName, ConfigurationConstants.password);
	}

	/**
	 * Method to click on SignIn button when user is anonymous (anonymous mode
	 * enabled)
	 */
	protected void clickSignIn() {
		try {
			androidUtility.manageImplicitWait(androidDriver, Constants.VERY_SMALL_SLEEP);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.menuTabsContainer));
			androidUtility.turnOnImplicitWait(androidDriver);
		} catch (Exception e) {
			clickMenu();
		}
		/*
		 * int signInTabPosition = 3; navigateRight(signInTabPosition); tap(1);
		 */
		mElement = androidDriver.findElement(MobileBy.id("tv_signin_menu_tab"));
		mElement.click();
	}

	/**
	 * Method to select a profile in the login screen, based on
	 * parameter(nonAdmin/Admin) nonAdmin = false -> select Admin profile.
	 * 
	 * @param nonAdmin
	 * @ @
	 */
	protected void selectProfile(boolean nonAdmin) {
		registeredProfilesCount = profilesJsonUtility.getRegisteredProfilesCount();
		System.out.println("PROFILE COUNT" + registeredProfilesCount);
		if (nonAdmin)
			if (!(registeredProfilesCount > 1))
				Assert.assertFalse(nonAdmin, "In current account there is only one profile registered");
		if (registeredProfilesCount > 1) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.profileMenuTitle));
			Assert.assertTrue(mElement.isDisplayed(), "Profile menu title is not displayed");
			Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("who_is_watching_text"), "Profile menu title is not according to the specifications");
			profilesNames = profilesJsonUtility.getAllProfilesNames();
			mElements = androidDriver.findElements(MobileBy.id(XPaths.profileName));
			for (int i = 0; i < mElements.size(); i++) {
				mElement = mElements.get(i);
				profileName = profilesNames.get(i);
				Assert.assertEquals(mElement.getText(), profileName, "Profile name is not matching/Didn't loggedinto intended account");
			}
			mElement = mElements.get(0);
			while (!androidUtility.getElementStateAttribute(mElement, "selected")) {
				navigateLeft(1);
				mElements = androidDriver.findElements(MobileBy.id(XPaths.profileName));
				mElement = mElements.get(0);
			}
			if (nonAdmin) {
				navigateRight(1);
				BaseTest.currentProfileName = profilesNames.get(1);
			} else {
				BaseTest.currentProfileName = profilesNames.get(0);
			}
			BaseTest.isAdmin = !nonAdmin;
			tap(1);
		}
		waitForSomeTime(Constants.DRIVER_IMPLICIT_WAIT);
	}

	/**
	 * Method to switch profile. Based current profile this method will switch to
	 * other profile. If current profile is Admin -> switch to non Admin. If current
	 * profile is non Admin -> switch to Admin.
	 * 
	 * @return true on success, false on failure
	 */
	protected boolean switchProfile() {
		if (!(profilesJsonUtility.getRegisteredProfilesCount() > 1))
			return false;
		if (goToSettings()) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.settingsWhosWatchingOption));
			Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("who_is_watching_text"), "Profile section is not displayed even though account has more than one profile");
			if (isAdmin)
				navigateRight(1);
			else
				navigateLeft(1);
			isAdmin = !isAdmin;
			tap(1);
			waitForSomeTime(Constants.MEDIUM_SLEEP);
			return true;
		}
		return false;
	}

	/**
	 * Method to go to parental controls.
	 *
	 * @return true on success, false on failure.
	 */
	protected boolean goToParentalControlsScreen() {
		if (goToSettings()) {
			if (profilesJsonUtility.getRegisteredProfilesCount() > 1) {
				navigateDown(1);
			}
			mElement = androidDriver.findElement(MobileBy.id(XPaths.settingsParentalOption));
			Assert.assertTrue(androidUtility.getElementStateAttribute(mElement, "focused"), "Parental control option is not having focus");
			tap(1);
			return true;
		}
		return false;
	}

	/**
	 * Method to go to Manage Profiles.
	 *
	 * @return true on success, false on failure.
	 */
	protected boolean goToManageProfilesScreen() {
		if (goToSettings()) {
			if (profilesJsonUtility.getRegisteredProfilesCount() > 1)
				navigateDown(1);
			navigateDown(1);
			if (profilesJsonUtility.getRegisteredProfilesCount() > 0) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.settingsProfilesOption));
				Assert.assertTrue(androidUtility.getElementStateAttribute(mElement, "focused"), "Manage Profiles option is not having focus");
				tap(1);
				return true;
			}
		}
		return false;
	}

	/**
	 * Method to go to Help.
	 *
	 * @return true on success, false on failure.
	 */
	protected boolean goToHelpScreen() {
		if (goToSettings()) {
			if (profilesJsonUtility.getRegisteredProfilesCount() > 1)
				navigateDown(1);
			navigateDown(1);
			if (profilesJsonUtility.getRegisteredProfilesCount() > 0)
				navigateDown(1);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.settingsHelpOption));
			Assert.assertTrue(androidUtility.getElementStateAttribute(mElement, "focused"), "Help option is not having focus");
			tap(1);
			return true;
		}
		return false;
	}

	/**
	 * Method to go to Terms Of Service.
	 *
	 * @return true on success, false on failure.
	 */
	protected boolean goToTermsOfServiceScreen() {
		if (goToSettings()) {
			if (profilesJsonUtility.getRegisteredProfilesCount() > 1)
				navigateDown(1);
			navigateDown(1);
			if (profilesJsonUtility.getRegisteredProfilesCount() > 0)
				navigateDown(1);
			navigateDown(1);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.settingsTermsOfServiceOption));
			Assert.assertTrue(androidUtility.getElementStateAttribute(mElement, "focused"), "Terms of service option is not having focus");
			tap(1);
			return true;
		}
		return false;
	}

	/**
	 * Method to do signOut from the application.
	 * 
	 * @throws IOException
	 */
	protected boolean signOut() throws IOException {
		navigateBack(1);
		if (androidUtility.isElementPresentByID(androidDriver, "tv_signin_menu_tab", true)) {
			System.out.println("true");
			return true;
		}
		waitForSomeTime(2);
		if (goToSettings()) {
			navigateDown(androidTVSettingsMenuItems.size());
			mElement = androidDriver.findElement(MobileBy.id(XPaths.settingsSignOutOption));
			if (androidUtility.getElementStateAttribute(mElement, "focused")) {
				tap(1);
				waitForSomeTime(Constants.SMALL_SLEEP);
				mElement = androidDriver.findElement(MobileBy.id(XPaths.alertTitle));
				uiMessage = mElement.getText();
				jsonMessage = dictionaryReader.getDictionaryValueForKey("are_you_sure_alert_title");
				assertEquals(uiMessage, jsonMessage, "signOut Alert title didn't matched");
				mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
				if (androidUtility.getElementStateAttribute(mElement, "focused")) {
					if (mElement.getText().equals(dictionaryReader.getDictionaryValueForKey("yes"))) {
						mElement.click();
					}
				}
			}
			waitForSomeTime(Constants.DRIVER_IMPLICIT_WAIT);
			return true;
		}
		return false;
	}

	/**
	 * Method to get to know whether the device supports dolby or not.
	 * 
	 * @return boolean
	 */
	protected boolean isDolbySupported() {
		return Boolean.parseBoolean(executeShellCommand("getprop ro.platform.support.dolby").get(0));
	}

	/**
	 * Method to get to know whether the device supports dts or not.
	 * 
	 * @return boolean
	 */
	protected boolean isDtsSupported() {
		return Boolean.parseBoolean(executeShellCommand("getprop ro.platform.support.dts").get(0));
	}

	/**
	 * Function to click on Menu.
	 */
	protected boolean clickMenu() {
		androidDriver.pressKeyCode(AndroidKeyEvents.NAVIGATE_BACK);
		androidUtility.manageImplicitWait(androidDriver, Constants.VERY_SMALL_SLEEP);
		try {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.menuTabsContainer));
			androidUtility.turnOnImplicitWait(androidDriver);
			return true;
		} catch (Exception e) {
			androidUtility.turnOnImplicitWait(androidDriver);
			return false;
		}
	}

	/**
	 * Method to select Guide Tab in Android TV app.
	 * 
	 * @return boolean
	 */
	protected boolean selectGuideTab() {
		if (selectTab(androidTVMenuItems.get(0))) {
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.fullViewHeaderTitle, true)) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.fullViewHeaderTitle));
				if (mElement.getText().equals(androidTVMenuItems.get(0)))
					return true;
			}
		}
		return false;
	}

	/**
	 * Method to select Shows Tab in Android TV app.
	 * 
	 * @return boolean
	 */
	protected boolean selectShowsTab() {
		if (selectTab(androidTVMenuItems.get(1)))
			return true;
		return false;
	}

	/**
	 * Method to select Movies Tab in Android TV app.
	 * 
	 * @return boolean
	 */
	protected boolean selectMoviesTab() {
		if (selectTab(androidTVMenuItems.get(2)))
			return true;
		return false;
	}

	/**
	 * Method to select Recordings Tab in Android TV app.
	 * 
	 * @return boolean
	 */
	protected boolean selectRecordingsTab() {
		if (selectTab(androidTVMenuItems.get(3))) {
			try {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.recordingsScreenTitle));
				if (mElement.getText().equals(dictionaryReader.getDictionaryValueForKey("recordings_title")))
					return true;
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

	/**
	 * Method to select Search Tab in Android TV app.
	 * 
	 * @return boolean
	 */
	protected boolean selectSearchTab() {
		if (selectTab(androidTVMenuItems.get(4)))
			return true;
		return false;
	}

	/**
	 * Method for navigating to settings page.
	 * 
	 * @return boolean
	 */
	protected boolean goToSettings() {
		if (selectRecordingsTab()) {
			navigateUp(1);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.settingsIcon));
			if (androidUtility.getElementStateAttribute(mElement, "focused")) {
				tap(1);
			}
		}
		try {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.settingsMenuTitle));
			if (mElement.getText().equals(dictionaryReader.getDictionaryValueForKey("settings_text")))
				return true;
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Method for navigating to All Shows page.
	 * 
	 * @return boolean
	 */
	protected boolean goToAllShows() {
		if (selectShowsTab()) {
			waitForSomeTime(Constants.MEDIUM_SLEEP);
			navigateUp(1);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.detailsSeeAllNavText));
			if (androidUtility.getElementStateAttribute(mElement, "focused")) {
				tap(1);
				mElement = androidDriver.findElement(MobileBy.id(XPaths.primaryPageTitle));
				return mElement.getText().equals(dictionaryReader.getDictionaryValueForKey("all_shows_text"));
			}
		}
		return false;
	}

	/**
	 * Method for navigating to All Movies page.
	 * 
	 * @return boolean
	 */
	protected boolean goToAllMovies() {
		if (selectMoviesTab()) {
			waitForSomeTime(Constants.SMALL_SLEEP);
			navigateUp(1);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.detailsSeeAllNavText));
			if (androidUtility.getElementStateAttribute(mElement, "focused")) {
				tap(1);
				mElement = androidDriver.findElement(MobileBy.id(XPaths.primaryPageTitle));
				return mElement.getText().equals(dictionaryReader.getDictionaryValueForKey("all_movies_text"));
			}
		}
		return false;
	}

	/**
	 * Method for navigating to replay screen.
	 * 
	 * @return boolean
	 */
	protected boolean goToReplay() {
		if (!ConfigurationConstants.isCatchUpEnabled || !ConfigurationConstants.isOutOfHomeEnabled)
			return false;
		selectGuideTab();
		navigateLeft(1);
		if(!androidUtility.isElementPresentByID(androidDriver,XPaths.buttonReplay,true)){
			navigateLeft(1);
			if(!androidUtility.isElementPresentByID(androidDriver,XPaths.buttonReplay,true)){
				navigateLeft(1);
			}
		}
		mElement = androidDriver.findElement(MobileBy.id(XPaths.buttonReplay));
		Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("catchup_title"), "Replay button text not matching");
		tap(1);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.fullViewHeaderTitle));
		if (mElement.getText().equals(dictionaryReader.getDictionaryValueForKey("catchup_title"))) {
			return true;
		}
		return false;
	}

	/**
	 * Method for navigating (put focus) to Subscription filter.
	 * 
	 * @return
	 */
	protected boolean goToSubscriptionFilter() {
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.subscriptionFilterText, true)) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.subscriptionFilterText));
			androidUtility.moveFocuseToElement(mElement, androidDriver);
			waitForSomeTime(Constants.SMALL_SLEEP);
			navigateRight(2);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.subscriptionFilterText));
			return androidUtility.getElementStateAttribute(mElement, "focused");
		}
		return false;
	}

	/**
	 * Method for navigating back to home.
	 */
	public void goToHomeScreen() {
		while (true) {
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.menuTabsContainer, true)) {
				navigateBack(1);
				break;
			} else {
				navigateBack(1);
			}
		}
	}

	/**
	 * Method for navigating to category filters screen.
	 * 
	 * @return
	 */
	protected boolean goToCategoryFilters() {
		if (!(ConfigurationConstants.isCatchUpEnabled) || !(ConfigurationConstants.isOutOfHomeEnabled))
			return false;
		if (selectGuideTab()) {
			navigateLeft(1);
			mElements = androidDriver.findElements(MobileBy.id(XPaths.buttonReplay));
			if (mElements.size() == 0) {
				navigateLeft(1);
			}
			navigateDown(1);
			tap(1);
			return true;
		}
		return false;
	}

	/**
	 * Method for navigating to category filters screen.
	 *
	 * @return
	 */
	protected boolean goToCategoryFiltersScreen() {
		if (!ConfigurationConstants.isCatchUpEnabled || !ConfigurationConstants.isOutOfHomeEnabled)
			return false;
		if (selectGuideTab()) {
			navigateLeft(1);
			mElements = androidDriver.findElements(MobileBy.id(XPaths.buttonReplay));
			if (mElements.size() == 0) {
				navigateLeft(1);
			}
			navigateDown(1);
			return true;
		}
		return false;
	}

	/**
	 * Navigating to top of EPG
	 */
	protected void navigateToTheTopOfTheEPGScreen() {
		if (!selectGuideTab()) {
			selectGuideTab();
		}
		ArrayList<Channel> channels = channelsJSONUtility.getAllChannels(-1);
		navigateUp(channels.size() + 1);
		System.out.println(channels.size() + 1);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.epgContentView));
		List<MobileElement> programRowRecycleViews = mElement.findElements(MobileBy.id(XPaths.programRowRecycleView));
		mElement = programRowRecycleViews.get(0).findElements(MobileBy.id(XPaths.epgProgramName)).get(1);
		if (androidUtility.getElementStateAttribute(mElement, "focused"))
			navigateLeft(1);
	}

	/**
	 * Method for applying a specified filter in category filters screen.
	 * 
	 * @param filter
	 * @return
	 */
	protected boolean applyCategoryFilter(String filter) {
		for (int i = 0; i < androidTVCategoryFilters.indexOf(filter); i++) {
			navigateDown(1);
		}
		if (androidUtility.isElementPresentByXPath(androidDriver, "//*[@text='" + filter + "']", true)) {
			mElement = androidDriver.findElement(MobileBy.xpath("//*[@text='" + filter + "']"));
			if (androidUtility.getElementStateAttribute(mElement, "focused")) {
				mElement.click();
				waitForSomeTime(Constants.SMALL_SLEEP);
				return true;
			}
		}
		return false;
	}

	/**
	 * Method to select Given tab MobileBy name in Android TV app.
	 * 
	 * @param tabName
	 */
	private boolean selectTab(String tabName) {
		int currentSelectionIndex = -1, selectionIndex = -1, movePositions = 0;
		selectionIndex = androidTVMenuItems.indexOf(tabName);
		androidDriver.pressKeyCode(AndroidKeyEvents.NAVIGATE_BACK);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.menuTabsContainer));
		if (mElement.isDisplayed()) {
			mElements = mElement.findElements(MobileBy.className("android.support.v7.app.ActionBar$Tab"));
			for (int i = 0; i < mElements.size(); i++) {
				mElement = mElements.get(i);
				if (androidUtility.getElementStateAttribute(mElement, "selected")) {
					currentSelectionIndex = i;
					break;
				}
			}
			if (currentSelectionIndex != -1) {
				movePositions = selectionIndex - currentSelectionIndex;
				if (movePositions > 0)
					navigateRight(Math.abs(movePositions));
				else
					navigateLeft(Math.abs(movePositions));
				tap(1);
				return true;
			}
		}
		return false;
	}

	/**
	 * Method to make sure that focus is on search box.
	 */
	protected void readySearchBox() {
		navigateRight(2);
		mElement = androidDriver.findElement(By.id(XPaths.searchTextEditor));
		mElement.click();
		waitForSomeTime(Constants.SMALL_SLEEP);
		hideKeyboard();
	}

	/**
	 * Method to execute a search query.
	 * 
	 * @param searchQuery
	 */
	protected void executeASearchQuery(String searchQuery) {
		System.out.println("searchQuery : " + searchQuery);
		readySearchBox();
		waitForSomeTime(Constants.SMALL_SLEEP);
		androidUtility.clearAndInputText(mElement, searchQuery);
		if (ConfigurationConstants.deviceName.equals("Amazon")) {
			System.out.println("Amazon");
			waitForSomeTime(Constants.SMALL_SLEEP);
			androidDriver.pressKeyCode(AndroidKeyEvents.PLAYPAUSE);
			waitForSomeTime(Constants.SMALL_SLEEP);
			// navigateDown(2);
		} else {
			// androidDriver.pressKeyCode(AndroidKeyEvents.MUTE);
			waitForSomeTime(Constants.VERY_SMALL_SLEEP);
			navigateRight(12);
			waitForSomeTime(2);
			tap(1);
			System.out.println("navigated back");
			waitForSomeTime(Constants.SMALL_SLEEP);
		}
	}

	/**
	 * Method to verify show info in more info overlay.
	 * 
	 * @param show
	 */
	protected void verifyShowMoreInfo(Show show) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.tvContentTitleText));
		uiName = mElement.getText();
		jsonName = show.getPrimaryMetaDataAccordingToLocalization();
		Assert.assertEquals(uiName, jsonName, "Show name didn't matched in in-line info module");
		if (show.getDescription() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.tvContentDescriptionText));
			uiDescription = mElement.getText();
			jsonDescription = show.getDescription();
			// Assert.assertEquals(uiDescription, jsonDescription, "Show description didn't
			// matched"); //TODO : Talk to lakshmi : show desc is compared to episode desc
			// in more details page
		}
		mElement = androidDriver.findElement(MobileBy.id(XPaths.tvContentCategoryInfoText));
		uiMetada = mElement.getText();
		jsonMetada = show.getMoreInfoSecondaryMetaDataAccordingToLocalization();
		// Assert.assertEquals(uiMetada, jsonMetada, "Show meta data didn't
		// matched");//Meta data is giving special characters.
		if (show.getActors_list() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.tvContentCastText));
			uiActors = mElement.getText();
			jsonActors = show.getActorsList();
			// Assert.assertEquals(uiActors, jsonActors, "Show actors list didn't matched");
			// //TODO : Talk to lakshmi : backend info has more actors list than UI - may be
			// space restrictions
		}
	}

	/**
	 * Method to validate movie in-line info. Validate MetaData and Buttons
	 *
	 * @param movie
	 */
	protected void validateMovieInLineInfo(Movie movie) {
		validateMovieInfo(movie);
		validateMovieButtonRow(movie);
		validateMovieInlineInfoButtonRow();
	}

	/**
	 * Method to validate movie details info.
	 * 
	 * @param movie
	 */
	protected void validateMovieDetailsInfo(Movie movie) {
		validateMovieInfo(movie);
		validateMovieButtonRow(movie);
	}

	/**
	 * Method to verify movie inline-info Metadata
	 *
	 * @param movie
	 */
	protected void validateMovieInfo(Movie movie) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleTitle));
		uiName = mElement.getText();
		jsonName = movie.getPrimaryMetaDataAccordingToLocalization();
		Assert.assertEquals(uiName, jsonName, "Movie name didn't matched in in-line info module");
		if (movie.getDescription() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleDescription));
			uiDescription = mElement.getText();
			jsonDescription = movie.getDescription();
			Assert.assertEquals(uiDescription, jsonDescription, "Movie description didn't match");
		}
		jsonData = movie.getInlineInfoSecondaryMetaDataAccordingToLocalization();
		String upComingAirDate = getUpcomingAirDate(movie);
		if (upComingAirDate != null) {
			jsonMetaData = new StringJoiner("  • ").add(jsonData).add(upComingAirDate).toString();
			jsonData = jsonMetaData;
		}
		if (isScheduledRecordingIconVisible(movie)) {
			jsonData = jsonData.concat(" <icon/>");
		}
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleMetaData));
		uiMetada = mElement.getText().replaceAll("[^a-zA-Z0-9]", "");
		jsonData = jsonData.replaceAll("[^a-zA-Z0-9]", "");
		Assert.assertEquals(uiMetada, jsonData, "Meta Data didn't match");
		System.out.println("MetaData : " + jsonData);
		/*
		 * mElement =
		 * androidDriver.findElement(MobileBy.id(XPaths.infoModuleMetaDatas));// TODO
		 * check this xpath is shared/thisone uiMetada = mElement.getText(); jsonMetada
		 * = movie.getInlineInfoSecondaryMetaDataAccordingToLocalization();
		 */
		// Assert.assertEquals(uiMetada, jsonMetada, "Movie meta data didn't
		// matched");//Meta data is giving special characters.
		if (movie.getActors_list() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleCrew));
			uiActors = mElement.getText();
			jsonActors = movie.getActorsList();
			Assert.assertEquals(uiActors, jsonActors, "Movie actors list didn't matched");
		}
	}

	/**
	 * Method to verify movie info in more info overlay.
	 * 
	 * @param movie
	 */
	protected void verifyMovieMoreInfo(Movie movie) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.tvContentTitleText));
		uiName = mElement.getText();
		jsonName = movie.getPrimaryMetaDataAccordingToLocalization();
		Assert.assertEquals(uiName, jsonName, "Movie name didn't matched in in-line info module");
		if (movie.getDescription() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.tvContentDescriptionText));
			uiDescription = mElement.getText();
			jsonDescription = movie.getDescription();
			Assert.assertEquals(uiDescription, jsonDescription, "Movie description didn't matched");
		}
		mElement = androidDriver.findElement(MobileBy.id(XPaths.tvContentCategoryInfoText));
		uiMetada = mElement.getText();
		jsonMetada = movie.getMoreInfoSecondaryMetaDataAccordingToLocalization();
		// Assert.assertEquals(uiMetada, jsonMetada, "Movie meta data didn't
		// matched");//Meta data is giving special characters.
		if (movie.getActors_list() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.tvContentCastText));
			uiActors = mElement.getText();
			jsonActors = movie.getActorsList();
			Assert.assertEquals(uiActors, jsonActors, "Movie actors list didn't matched");
		}
	}

	/**
	 * Method to verify movie info in Details page.
	 *
	 * @param movie
	 *
	 */
	protected void verifyMovieRecommendations(Movie movie, ArrayList<String> movieNames) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.rowHeader));
		uiName = mElement.getText();
		Assert.assertTrue(uiName.contains(dictionaryReader.getDictionaryValueForKey("you_might_like_title")), "YML section not displayed");
		for (int i = 0; i < 2; i++) {
			/*
			 * jsonName = movie.getPrimaryMetaDataAccordingToLocalization();
			 * System.out.println(jsonName);
			 */
			mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
			uiName = mElement.getText();
			// TODO Recommendations are not matching with the server
			// Assert.assertEquals(uiName, movie.getName(), "Movie name didn't matched");
			Assert.assertTrue(movieNames.contains(uiName), uiName + " is not in recommended list");
			Assert.assertTrue(androidDriver.findElement(MobileBy.id(XPaths.titleText)).isSelected());
			navigateRight(1);
		}
	}

	/**
	 * Method to verify movie info in Details page.
	 *
	 * @param movie
	 *
	 */
	protected void verifyShowRecommendations(Show show) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.rowHeader));
		uiName = mElement.getText();
		Assert.assertTrue(uiName.contains(dictionaryReader.getDictionaryValueForKey("you_might_like_title")), "YML section not displayed");
		for (int i = 0; i < 2; i++) {
			/*
			 * jsonName = movie.getPrimaryMetaDataAccordingToLocalization();
			 * System.out.println(jsonName);
			 */
			mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
			uiName = mElement.getText();
			// TODO Recommendations are not matching with the server
			// Assert.assertEquals(uiName, movie.getName(), "Movie name didn't matched");
			Assert.assertTrue(androidDriver.findElement(MobileBy.id(XPaths.titleText)).isSelected());
			navigateRight(1);
		}
	}

	/**
	 * Method to verify movie button row on info and details pages. 1.Check for
	 * buttons : Stop/Cancel/Delete Recording. 2.If Stop/Cancel/Delete not present,
	 * check for Record/Record to Watch Button 3.If neither of Case 1 or Case 2 is
	 * present then Assert Movie as Not Recordable. 3.Check for Play Button 4.Check
	 * for Full Details Button.
	 *
	 * @param movie
	 */
	protected void validateMovieButtonRow(Movie movie) {
		if (movie.getType().equalsIgnoreCase(Constants.REF_TYPE_PROGRAM) || movie.getType().equalsIgnoreCase(Constants.REF_TYPE_VOD)) {
			buttonAction = recordingsJsonUtility.checkRecordButtonAction(movie.getID());
		} else if (movie.getType().equalsIgnoreCase("shared_ref")) {
			List<Assets> assets = movie.getAssets();
			for (Assets asset : assets) {
				System.out.println(asset.getId() + "shared" + asset.getShared_ref_id());
				buttonAction = recordingsJsonUtility.checkRecordButtonAction(asset.getId());
				if (buttonAction != null)
					break;
				else {
					buttonAction = recordingsJsonUtility.checkRecordButtonActionBySharedRefID(asset.getShared_ref_id());
					if (buttonAction != null)
						break;
				}
			}
		}
		if (buttonAction != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
			switch (buttonAction) {
			case Constants.delete_record_button_title:
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("delete_record_button_title"));
				System.out.println("Button State is " + buttonAction);
				break;
			case Constants.cancel_record_button_title:
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("cancel_record_button_title"));
				System.out.println("Button State is " + buttonAction);
				break;
			case Constants.stop_record_button_title:
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("stop_record_button_title"));
				System.out.println("Button State is " + buttonAction);
				break;
			default:
				Assert.assertFalse(androidUtility.isElementPresentByID(androidDriver, XPaths.infoModuleBtnRecording, false), "Manage Recording button is not visible.");
				System.out.println("***----No Recording Available to Stop/Cancel/Delete----***");
			}
		} else if (ConfigurationConstants.isUserInHome) {
			if (isMovieUpcomingRecordingenabled(movie) || isMovieLiveRecordingEnabled(movie)) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("record_button_title"));
				System.out.println("Button State is " + Constants.record_button_title);
				buttonAction = Constants.record_button_title;
			} else if (isMovieLiveRecordingEnabled(movie) && !(ConfigurationConstants.isUserInHome)) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("record_to_watch_button_title"));
				System.out.println("Button State is " + Constants.record_to_watch_button_title);
			}
		} else {
			Assert.assertFalse(androidUtility.isElementPresentByID(androidDriver, XPaths.infoModuleBtnRecording, false), "Record button is not visible.");
			System.out.println("***----NOT A RECORDABLE MOVIE----***");
		}
		if (ConfigurationConstants.isUserInHome || isMovieOutOfHomePlayable(movie)) {
			if (getMovieCatchupAssetCount(movie) > 0 || getMovieLiveAssetCount(movie) > 0 || isMovieHasVodAssets(movie)) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnPlay));
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("play_button_title"));
				System.out.println("Button State is " + Constants.play_button_title);
			} else if (movie.getType().equalsIgnoreCase("vod")) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnPlay));
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("play_button_title"));
				System.out.println("Button State is " + Constants.play_button_title);
			}
		}
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnMoreInfo));
		Assert.assertTrue(mElement.isDisplayed(), dictionaryReader.getDictionaryValueForKey("full_details_button_title"));
		System.out.println("Button State is " + Constants.full_details_button_title);
	}

	/**
	 * Method to verify movie overlay screens buttons.
	 */
	protected void validateMovieOverlayButtons(Movie movie) {
		if (movie.getType().equalsIgnoreCase(Constants.REF_TYPE_PROGRAM) || movie.getType().equalsIgnoreCase(Constants.REF_TYPE_VOD)) {
			buttonAction = recordingsJsonUtility.checkOverlayButtonAction(movie.getID());
		}
		if (buttonAction != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.movieRecordingDetailsRecordButton));
			switch (buttonAction) {
			case Constants.delete_button_title:
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("delete_button_title"));
				System.out.println("Button State is " + buttonAction);
				break;
			case Constants.stop_button_title:
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("stop_recording_text"));
				System.out.println("Button State is " + buttonAction);
				break;
			}
		}
		if (ConfigurationConstants.isUserInHome || isMovieOutOfHomePlayable(movie)) {
			if (getMovieCatchupAssetCount(movie) > 0 || getMovieLiveAssetCount(movie) > 0 || isMovieHasVodAssets(movie)) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.movieRecordingDetailsPlayButton));
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("play_button_title"));
				System.out.println("Button State is " + Constants.play_button_title);
			} else if (movie.getType().equalsIgnoreCase("vod")) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.movieRecordingDetailsPlayButton));
				Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("play_button_title"));
				System.out.println("Button State is " + Constants.play_button_title);
			}
		}
		mElement = androidDriver.findElement(MobileBy.id(XPaths.movieRecordingDetailsInfoButton));
		final String message = "Actual Info button title not matching with expected.";
		Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("info_button_title"), message);
	}

	/**
	 * Method to verify movie button row on iInline info.
	 *
	 * @param movie
	 *
	 */
	protected void validateMovieInlineInfoButtonRow() {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnMoreInfo));
		Assert.assertEquals(mElement.getText(), "Full Details");
	}

	/**
	 * Method to verify movie button row on Details page .
	 *
	 * @param movie
	 *
	 */
	protected void validateMovieDetailsButtonRow() {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnMoreInfo));
		Assert.assertEquals(mElement.getText(), "More Info");
	}

	/**
	 * Method to get Live movie count
	 * 
	 * @param movie
	 * @return
	 */
	public int getMovieLiveAssetCount(Movie movie) {
		liveCount = 0;
		if (movie.getAssets() != null) {
			if (!movie.getAssets().isEmpty())
				for (int j = 0; j < movie.getAssets().size(); j++) {
					if (movie.getAssets().get(j).getRef_type() != null) {
						if (movie.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
							if (utility.isLive(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time())) {
								liveCount = liveCount + 1;
							}
						}
					} else if (movie.getAssets().get(j).getType().equalsIgnoreCase("program")) {
						if (utility.isLive(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time())) {
							liveCount = liveCount + 1;
						}
					}
				}
		}
		return liveCount;
	}

	/**
	 * Method to get Live movie assets
	 * 
	 * @param movie
	 * @return
	 */
	public ArrayList<Assets> getMovieLiveAssets(Movie movie) {
		ArrayList<Assets> assetsForGivenMovie = new ArrayList<>();
		if (!movie.getAssets().isEmpty())
			for (int j = 0; j < movie.getAssets().size(); j++) {
				if (movie.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isLive(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time())) {
						assetsForGivenMovie.add(movie.getAssets().get(j));
					}
				}
			}
		return assetsForGivenMovie;
	}

	/**
	 * Method to get Live Episode assets
	 * 
	 * @param episode
	 * @return
	 */
	public ArrayList<Assets> getEpisodeLiveAssets(Episode episode) {
		ArrayList<Assets> assetsForGivenEpisode = new ArrayList<>();
		if (!episode.getAssets().isEmpty())
			for (int j = 0; j < episode.getAssets().size(); j++) {
				if (episode.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isLive(episode.getAssets().get(j).getStart_time(), episode.getAssets().get(j).getEnd_time())) {
						assetsForGivenEpisode.add(episode.getAssets().get(j));
					}
				}
			}
		return assetsForGivenEpisode;
	}

	/**
	 * Method to return Upcoming movie count
	 * 
	 * @param movie
	 * @return
	 */
	public int getMovieUpcomingAssetCount(Movie movie) {
		upcomingCount = 0;
		if (!movie.getAssets().isEmpty())
			for (int j = 0; j < movie.getAssets().size(); j++) {
				if (movie.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isUpcoming(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time())) {
						upcomingCount = upcomingCount + 1;
					}
				}
			}
		return upcomingCount;
	}

	/**
	 * Method to return Upcoming movie Assets
	 * 
	 * @param movie
	 * @return
	 */
	public List<Assets> getMovieUpcomingAssets(Movie movie) {
		List<Assets> assets = new ArrayList<>();
		if (!movie.getAssets().isEmpty())
			for (int j = 0; j < movie.getAssets().size(); j++) {
				if (movie.getAssets().get(j).getType().equalsIgnoreCase("program")) {
					if (utility.isUpcoming(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time())) {
						assets.add(movie.getAssets().get(j));
					}
				}
			}
		return assets;
	}

	/**
	 * Method to get Catchup movie assets
	 * 
	 * @param movie
	 * @return
	 */
	public ArrayList<Assets> getMovieCatchupAssets(Movie movie) {
		ArrayList<Assets> assetsForGivenMovie = new ArrayList<>();
		if (!movie.getAssets().isEmpty())
			for (int j = 0; j < movie.getAssets().size(); j++) {
				if (movie.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isCatchup(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time())) {
						assetsForGivenMovie.add(movie.getAssets().get(j));
					}
				}
			}
		return assetsForGivenMovie;
	}

	/**
	 * Method to get Catchup movie assets
	 * 
	 * @param episode
	 * @return
	 */
	public ArrayList<Assets> getEpisodeCatchupAssets(Episode episode) {
		ArrayList<Assets> assetsForGivenEpisode = new ArrayList<>();
		if (!episode.getAssets().isEmpty())
			for (int j = 0; j < episode.getAssets().size(); j++) {
				if (episode.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isCatchup(episode.getAssets().get(j).getStart_time(), episode.getAssets().get(j).getEnd_time())) {
						assetsForGivenEpisode.add(episode.getAssets().get(j));
					}
				}
			}
		return assetsForGivenEpisode;
	}

	/**
	 * Method to get Catchup movie count
	 * 
	 * @param movie
	 * @return
	 */
	public int getMovieCatchupAssetCount(Movie movie) {
		catchupCount = 0;
		if (movie.getAssets() != null) {
			if (!movie.getAssets().isEmpty())
				for (int j = 0; j < movie.getAssets().size(); j++) {
					if (movie.getAssets().get(j).getRef_type() != null) {
						if (movie.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
							if (utility.isCatchup(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time())) {
								catchupCount = catchupCount + 1;
							}
						}
					} else if (movie.getAssets().get(j).getType().equalsIgnoreCase("program")) {
						if (utility.isCatchup(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time())) {
							catchupCount = catchupCount + 1;
						}
					}
				}
		}
		return catchupCount;
	}

	/**
	 * Method to return Recording available for upcoming movie
	 * 
	 * @param movie
	 * @return
	 */
	public boolean isMovieUpcomingRecordingenabled(Movie movie) {
		boolean flag = false;
		if (movie.getAssets() != null) {
			if (!movie.getAssets().isEmpty())
				for (int j = 0; j < movie.getAssets().size(); j++) {
					if (movie.getAssets().get(j).getRef_type() != null) {
						if (movie.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
							if (utility.isUpcoming(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time()) && (isRecordingEnabled(movie.getAssets().get(j))))
								return true;
						}
					} else if (movie.getAssets().get(j).getType().equalsIgnoreCase("program")) {
						if (utility.isUpcoming(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time()) && (isRecordingEnabled(movie.getAssets().get(j))))
							flag = true;
					}
				}
		}
		return flag;
	}

	/**
	 * @param movie
	 * @return
	 */
	public boolean isMovieLive(Movie movie) {
		boolean flag = false;
		if (!movie.getAssets().isEmpty())
			for (int j = 0; j < movie.getAssets().size(); j++) {
				if (movie.getAssets().get(j).getType().equalsIgnoreCase("program")) {
					if (utility.isLive(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time()))
						flag = true;
				}
			}
		return flag;
	}

	/**
	 * method to return Recording enabled for Live movie
	 * 
	 * @param movie
	 * @return
	 */
	public boolean isMovieLiveRecordingEnabled(Movie movie) {
		boolean flag = false;
		if (movie.getAssets() != null) {
			if (!movie.getAssets().isEmpty())
				for (int j = 0; j < movie.getAssets().size(); j++) {
					if (movie.getAssets().get(j).getRef_type() != null) {
						if (movie.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
							if (utility.isLive(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time()) && (isRecordingEnabled(movie.getAssets().get(j))))
								flag = true;
						}
					} else if (movie.getAssets().get(j).getType().equalsIgnoreCase("program")) {
						if (utility.isLive(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time()) && (isRecordingEnabled(movie.getAssets().get(j))))
							flag = true;
					}
				}
		}
		return flag;
	}

	/**
	 * Method to return vod assets for movie
	 * 
	 * @param movie
	 * @return
	 */
	public boolean isMovieHasVodAssets(Movie movie) {
		boolean flag = false;
		if (movie.getAssets() != null) {
			if (!movie.getAssets().isEmpty())
				for (int j = 0; j < movie.getAssets().size(); j++) {
					if (movie.getAssets().get(j).getRef_type() != null) {
						if (movie.getAssets().get(j).getRef_type().equalsIgnoreCase("vod"))
							flag = true;
					} else if (movie.getAssets().get(j).getType().equalsIgnoreCase("vod"))
						flag = true;
				}
		}
		return flag;
	}

	/**
	 * Method to get Live Episode count
	 * 
	 * @param movie
	 * @return
	 */
	public int getEpisodeLiveAssetCount(Episode episode) {
		liveCount = 0;
		if (!episode.getAssets().isEmpty())
			for (int j = 0; j < episode.getAssets().size(); j++) {
				if (episode.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isLive(episode.getAssets().get(j).getStart_time(), episode.getAssets().get(j).getEnd_time())) {
						liveCount = liveCount + 1;
					}
				}
			}
		return liveCount;
	}

	/**
	 * Method to return Upcoming episode count
	 * 
	 * @param movie
	 * @return
	 */
	public int getEpisodeUpcomingAssetCount(Episode episode) {
		upcomingCount = 0;
		if (!episode.getAssets().isEmpty())
			for (int j = 0; j < episode.getAssets().size(); j++) {
				if (episode.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isUpcoming(episode.getAssets().get(j).getStart_time(), episode.getAssets().get(j).getEnd_time())) {
						upcomingCount = upcomingCount + 1;
					}
				}
			}
		return upcomingCount;
	}

	/**
	 * Method to return Upcoming episode list
	 * 
	 * @param episode
	 * @return
	 */
	public ArrayList<Assets> getEpisodeUpcomingAssets(Episode episode) {
		ArrayList<Assets> assetsForGivenEpisode = new ArrayList<>();
		if (!episode.getAssets().isEmpty())
			for (int j = 0; j < episode.getAssets().size(); j++) {
				if (episode.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isUpcoming(episode.getAssets().get(j).getStart_time(), episode.getAssets().get(j).getEnd_time())) {
						assetsForGivenEpisode.add(episode.getAssets().get(j));
					}
				}
			}
		return assetsForGivenEpisode;
	}

	/**
	 * Method to return Upcoming episode list
	 * 
	 * @param movie
	 * @return
	 */
	public ArrayList<Assets> getMovieeUpcomingAssets(Movie movie) {
		ArrayList<Assets> assetsForGivenMovie = new ArrayList<>();
		if (!movie.getAssets().isEmpty())
			for (int j = 0; j < movie.getAssets().size(); j++) {
				if (movie.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isUpcoming(movie.getAssets().get(j).getStart_time(), movie.getAssets().get(j).getEnd_time())) {
						assetsForGivenMovie.add(movie.getAssets().get(j));
					}
				}
			}
		return assetsForGivenMovie;
	}

	/**
	 * Method to get Catchup episode count
	 * 
	 * @param movie
	 * @return
	 */
	public int getEpisodeCatchupAssetCount(Episode episode) {
		catchupCount = 0;
		if (!episode.getAssets().isEmpty())
			for (int j = 0; j < episode.getAssets().size(); j++) {
				if (episode.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isCatchup(episode.getAssets().get(j).getStart_time(), episode.getAssets().get(j).getEnd_time())) {
						catchupCount = catchupCount + 1;
					}
				}
			}
		return catchupCount;
	}

	/**
	 * Method to return Recording available for upcoming episode
	 * 
	 * @param movie
	 * @return
	 */
	public boolean isEpisodePlayable(Episode episode) {
		boolean flag = false;
		if (!episode.getAssets().isEmpty()) {
			if (getEpisodeCatchupAssetCount(episode) > 0)
				flag = true;
			if (getEpisodeLiveAssetCount(episode) > 0)
				flag = true;
			if (isEpisodeHasVodAssets(episode))
				flag = true;
		}
		return flag;
	}

	/**
	 * Method to return Recording available for upcoming episode
	 * 
	 * @param movie
	 * @return
	 */
	public boolean isEpisodeUpcomingRecordingenabled(Episode episode) {
		boolean flag = false;
		if (!episode.getAssets().isEmpty())
			for (int j = 0; j < episode.getAssets().size(); j++) {
				if (episode.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isUpcoming(episode.getAssets().get(j).getStart_time(), episode.getAssets().get(j).getEnd_time()) && (isRecordingEnabled(episode.getAssets().get(j))))
						flag = true;
					break;
				}
			}
		return flag;
	}

	/**
	 * method to return Recording enabled for Live episode
	 * 
	 * @param movie
	 * @return
	 */
	public boolean isEpisodeLiveRecordingEnabled(Episode episode) {
		boolean flag = false;
		if (!episode.getAssets().isEmpty())
			for (int j = 0; j < episode.getAssets().size(); j++) {
				if (episode.getAssets().get(j).getRef_type().equalsIgnoreCase("program")) {
					if (utility.isLive(episode.getAssets().get(j).getStart_time(), episode.getAssets().get(j).getEnd_time() - RECORDING_THRESHOLD_TIME) && (isRecordingEnabled(episode.getAssets().get(j))))
						flag = true;
				}
			}
		return flag;
	}

	/**
	 * Method to return vod assets for episode
	 * 
	 * @param movie
	 * @return
	 */
	public boolean isEpisodeHasVodAssets(Episode episode) {
		boolean flag = false;
		if (!episode.getAssets().isEmpty())
			for (int j = 0; j < episode.getAssets().size(); j++) {
				if (episode.getAssets().get(j).getRef_type().equalsIgnoreCase("vod"))
					flag = true;
			}
		return flag;
	}

	/**
	 * Method to test recording enabled or not
	 * 
	 * @param asset
	 * @return
	 */
	public boolean isRecordingEnabled(Assets asset) {
		return asset.is_recording_enabled();
	}

	/**
	 * Navigating to CatchUp section
	 */
	public void navigateToCatchUp() {
		selectGuideTab();
		navigateLeft(1);
		mElements = androidDriver.findElements(MobileBy.id(XPaths.buttonReplay));
		if (mElements.size() == 0) {
			navigateLeft(1);
		}
		tap(1);
		waitForSomeTime(Constants.MEDIUM_SLEEP);
	}

	/**
	 * Method to validate show in-line info.
	 * 
	 * @param show
	
	protected void validateShowInLineInfo(Show show) {
		new SeriesEpisodeInlineInfoModuleValidator(show);
	}
 */
	protected void validateShowInfoPlayButton(Show show, boolean hasPlayed) {
		if (mainJSONUtility.showHasPlayableEpisode(show)) {
			System.out.println("test play button " + hasPlayed);
			if (hasPlayed) {
				System.out.println("test play button PLAY");
				mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnPlay));
				Assert.assertEquals(mElement.getText(), "Play");
			} else {
				System.out.println("test play button PLAY MOST RECENT");
				mElement = androidDriver.findElement(MobileBy.id(XPaths.btnPlaySeries));
				Assert.assertEquals(mElement.getText(), "Play Most Recent");
			}
		}
	}

	protected void validateShowInlineAllEpisodeButton(boolean hasPlayed) {
		System.out.println("test series " + hasPlayed);
		if (hasPlayed) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnMoreInfo));
			Assert.assertEquals(mElement.getText(), "Series Info");
		} else {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.btnSeriesInfo));
			Assert.assertEquals(mElement.getText(), "View All Episodes");
		}
	}

	protected void validateShowDetailSeriesInfoButton(boolean hasPlayed) {
		System.out.println("test series detail page info button " + hasPlayed);
		if (hasPlayed) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnMoreInfo));
		} else {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.btnSeriesInfo));
		}
		Assert.assertEquals(mElement.getText(), "Series Info");
	}

	/**
	 * Method to validate show in-line info.
	 * 
	 * @param show,
	 *            hasPlayed
	 */
	protected void validateShowInfo(Show show, boolean hasPlayed) {
		if (!hasPlayed) {
			waitForSomeTime(2);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleTitle));
			uiName = mElement.getText();
			jsonName = show.getPrimaryMetaDataAccordingToLocalization();
			Assert.assertEquals(uiName, jsonName, "Show name didn't matched in in-line info module");
			if (show.getDescription() != null) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleDescription));
				uiDescription = mElement.getText();
				jsonDescription = show.getDescription();
				Assert.assertTrue(uiDescription.contains(jsonDescription), "Show description didn't matched");
			}
			mElement = androidDriver.findElement(MobileBy.id(XPaths.series_info_module_general_info));
			uiMetada = mElement.getText().replaceAll("[^a-zA-Z0-9]", "");
			jsonMetada = show.getInlineInfoSecondaryMetaDataAccordingToLocalization().replaceAll("[^a-zA-Z0-9]", "");
			// Assert.assertTrue(uiMetada.contains(jsonMetada), "Show meta data didn't
			// matched"); // has bugs on app not code
			if (show.getActors_list() != null) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleCrew));
				uiActors = mElement.getText();
				jsonActors = show.getActorsList();
				Assert.assertEquals(uiActors, jsonActors, "Show actors list didn't matched");
			}
		} // else TODO assert for recently played episode info
	}

	/**
	 * Method to validate show details page info
	 * 
	 * Includes: 1) validate show details page info 2) validate the more info button
	 * and click on it and verify the data on overlay 3) get all seasons data and
	 * verify the seasons row 4) validate everything related to episode
	 * 
	 * @param show
	 */
	protected void validateShowDetailPage(Show show) {
		boolean hasPlayed = mainJSONUtility.hasRecentForRefId(BaseTest.currentProfileID, false, "series", show.getID());
		validateShowInfo(show, hasPlayed); // meta data
		validateShowInfoPlayButton(show, hasPlayed);
		// TODO validateShowRecordButton(show);
		validateShowDetailSeriesInfoButton(hasPlayed);// moreinfo button
		navigateRight(3);
		tap(1);
		verifyShowMoreInfo(show);
		navigateBack(1);
		seasons = mainJSONUtility.getAllSeasons(show.getID());
		validateSeasonRow(seasons);
		if (seasons.size() > 1)
			navigateDown(1);
		navigateDown(1);
		validateEpisode(show, seasons);
	}

	/**
	 * Method to validate seasons row
	 * 
	 * Includes: 1) validate all seasons are displayed correctly (covered no
	 * season/only one season/ and multiple seasons)
	 * 
	 * @param seasons
	 */
	protected void validateSeasonRow(ArrayList<Seasons> seasons) {
		// two bugs filed so waiting the bugs get resolved and recheck this logic 17580
		// & 17581
		int size = seasons.size();
		if (size == 0) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.rowHeader));
			Assert.assertEquals(mElement.getText(),"Full Episodes");
		}
		if (size == 1) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.seasonTitle));
			Assert.assertEquals("SEASON " + seasons.get(0).getName(), mElement.getText());
		}
		if (size > 1) {
			mElements = androidDriver.findElements(MobileBy.id(XPaths.seasonTitle));
			for (int i = 0; i < size && i < mElements.size(); i++) {
				Assert.assertEquals("SEASON " + seasons.get(size - 1 - i).getName(), mElements.get(i).getText());
			}
		}
	}

	/**
	 * Method to validate episode (in-line and detail page)
	 * 
	 * Includes: 1) validate play and record button for episode 2) for record
	 * button( only assert the existence, TODO checking manage recording/ record) 3)
	 * click series info button under inline info module and go to detail page 4)
	 * verify play and record button and metadata on detail page 5) verify series
	 * info button and its related overlay on episode detail page 6) verify seasons
	 * row on episode detail page 7) check 1-6 for both upcoming and playable
	 * content(different row)
	 * 
	 * @param seasons
	 * @param show
	 */
	protected void validateEpisode(Show show, ArrayList<Seasons> seasons) {
		ArrayList<Episode> playableEpisodes = null;
		ArrayList<Episode> upcomingEpisodes = null;
		if (seasons.size() == 0) {
			playableEpisodes = mainJSONUtility.getAllPlayableEpisodes(show.getID(), "");
			upcomingEpisodes = mainJSONUtility.getAllUpcomingEpisodes(show.getID(), "");
		} else {
			playableEpisodes = mainJSONUtility.getAllPlayableEpisodes(show.getID(), Integer.toString(seasons.get(seasons.size() - 1).getName()));
			upcomingEpisodes = mainJSONUtility.getAllUpcomingEpisodes(show.getID(), Integer.toString(seasons.get(seasons.size() - 1).getName()));
		}
		HashSet<String> playableSet = new HashSet<String>();
		HashSet<String> recordableSet = new HashSet<String>();
		if (playableEpisodes.size() > 0) {
			for (int i = 0; i < playableEpisodes.size(); i++) {
				playableSet.add(playableEpisodes.get(i).getID());
				if (isEpisodeLiveRecordingEnabled(playableEpisodes.get(i)))
					recordableSet.add(playableEpisodes.get(i).getID());
			}
		}
		if (upcomingEpisodes.size() > 0) {
			for (int i = 0; i < upcomingEpisodes.size(); i++) {
				if (isEpisodeUpcomingRecordingenabled(upcomingEpisodes.get(i)))
					recordableSet.add(upcomingEpisodes.get(i).getID());
			}
		}
		for (int i = 0; i < 3 && i < playableEpisodes.size(); i++) {
			tap(1);
			validateEpisodeInfo(playableEpisodes.get(i)); // check inline
			validatePlayableEpisodeButton(playableEpisodes.get(i), recordableSet, "inline");// check inline
			navigateRight(2);// go to series Info button
			tap(1);// go to episode Detail Page
			validateEpisodeInfo(playableEpisodes.get(i)); // check detail page
			validatePlayableEpisodeButton(playableEpisodes.get(i), recordableSet, "detailsPage");// check detail page
			navigateRight(2);
			tap(1);
			// verifyShowMoreInfo(show); // TODO : talk to lakshmi
			navigateBack(1);
			validateSeasonRow(seasons);// validate season row on episode detail page
			// navigateBack(1); // back to all shows page with focus on the same show's view
			// all episodes button
			// tap(1);
			if (seasons.size() > 1)
				navigateDown(1);// to season row
			navigateDown(1);// to playable row
			// navigateUp(1);
			navigateRight(i + 1);
		}
		if (upcomingEpisodes.size() > 0 && playableEpisodes.size() > 0) {
			navigateDown(1);
			waitForSomeTime(1);
		}
		for (int i = 0; i < 3 && i < upcomingEpisodes.size(); i++) {
			tap(1);
			validateEpisodeInfo(upcomingEpisodes.get(i)); // check inline
			validateUpcomingEpisodeButton(upcomingEpisodes.get(i), playableSet, recordableSet, "inline");// check inline
			navigateRight(2);// go to series Info button
			tap(1);// go to episode Detail Page
			validateSeasonRow(seasons);// validate season row on episode detail page
			validateEpisodeInfo(upcomingEpisodes.get(i)); // check detail page
			validateUpcomingEpisodeButton(upcomingEpisodes.get(i), playableSet, recordableSet, "detailsPage");// check detail page
			navigateRight(2);
			tap(1);
			verifyShowMoreInfo(show);
			navigateBack(1);
			// navigateBack(1); // back to all shows page with focus on the same show's view
			// all episodes button
			// tap(1);
			if (seasons.size() > 1)
				navigateDown(1);// to season row
			navigateDown(1);
			if (upcomingEpisodes.size() > 0 && playableEpisodes.size() > 0)// to Upcoming row
				navigateDown(1);
			// navigateUp(1);
			navigateRight(i + 1);
		}
	}

	/**
	 * Method to validate episode meta data info includes: series name, episode
	 * season no episode no and name, episode description,genre, and original air
	 * date the last two not implemented b/c requirements/bugs
	 * 
	 * @param episode
	 */
	protected void validateEpisodeInfo(Episode episode) {
		waitForSomeTime(2);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleTitle));
		String uiShowName = mElement.getText();
		String jsonShowName = episode.getPrimaryMetaDataAccordingToLocalization();
		Assert.assertEquals(uiShowName, jsonShowName, "Show name didn't matched in in-line info module");
		if (episode.getDescription() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleDescription));
			uiDescription = mElement.getText();
			jsonDescription = episode.getDescription();
			if (episode.getOriginal_air_date() != null) {
				jsonMetada = dAndt.getDatefromEpoch(episode.getOriginal_air_date());
			}
			jsonDescription = jsonMetada + " " + jsonDescription;
		}
		Assert.assertTrue(jsonDescription.contains(uiDescription), "Episode description didn't matched");
		if (episode.getMetaDataAccordingToLocalization() != null) {// genre
			mElement = androidDriver.findElement(MobileBy.id(XPaths.episodeMetaData));
			uiMetada = mElement.getText().replaceAll("[^a-zA-Z0-9]", "");
			jsonMetada = episode.getMetaDataAccordingToLocalization().replaceAll("[^a-zA-Z0-9]", "");
			System.out.println("UI " + uiMetada);
			System.out.println("JSON " + jsonMetada);
			// Assert.assertTrue(uiMetada.contains(jsonMetada), "Episode meta data didn't
			// matched");//Meta data not showing genre sometimes.
		}
		/*
		 * System.out.println(episode.getOriginal_air_date()); if
		 * (episode.getOriginal_air_date() != null) { mElement =
		 * androidDriver.findElement(MobileBy.id(XPaths.infoModuleDate)); uiMetada =
		 * mElement.getText(); jsonMetada =
		 * dAndt.getDatefromEpoch(episode.getOriginal_air_date());
		 * Assert.assertEquals(uiMetada, jsonMetada,
		 * "Episode data: date didn't matched");//original date not showing bug ?
		 * requirements not clear }
		 */
		if (episode.getFormattedEpisodeName() != null) {
			// mElement = androidDriver.findElement(MobileBy.id(XPaths.seriesNOEpisodeNO));
			// uiShowName = mElement.getText();
			mElement = androidDriver.findElement(MobileBy.id(XPaths.episodeTitle));
			uiShowName = mElement.getText();
			jsonShowName = episode.getFormattedEpisodeName();
			Assert.assertEquals(uiShowName, jsonShowName, "Formatted episode Name didn't matched");
		}
	}

	/**
	 * Method to validate upcoming episode button row
	 * 
	 */
	protected void validateUpcomingEpisodeButton(Episode episode, HashSet<String> playableSet, HashSet<String> recordableSet, String InlineOrDetailsPage) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnMoreInfo));
		Assert.assertEquals("Series Info", mElement.getText(), "Series Info button should show up");
		if (recordableSet.contains(episode.getID())) {
			Assert.assertTrue(androidUtility.isElementPresentByID(androidDriver, XPaths.infoModuleBtnRecording, true));// only assert for existence
			if (InlineOrDetailsPage.equals("inline")) {
				validateEpisodeRecordButtonInlineInfo();
			} else {
				validateEpisodeRecordButtonDetailsPage();
			}
		}
		if (playableSet.contains(episode.getID())) {
			Assert.assertTrue(androidUtility.isElementPresentByID(androidDriver, XPaths.infoModuleBtnPlay, true));// only assert for existence
		}
	}

	/**
	 * Method to validate playable episode button row (diff from upcoming : play
	 * button should display for all playable)
	 * 
	 */
	protected void validatePlayableEpisodeButton(Episode episode, HashSet<String> recordableSet, String InlineOrDetailsPage) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnMoreInfo));
		Assert.assertEquals("Series Info", mElement.getText(), "Series Info button should show up");
		if (recordableSet.contains(episode.getID())) {
			Assert.assertTrue(androidUtility.isElementPresentByID(androidDriver, XPaths.infoModuleBtnRecording, true));// only assert for existence
			if (InlineOrDetailsPage.equals("inline")) {
				validateEpisodeRecordButtonInlineInfo();
			} else {
				validateEpisodeRecordButtonDetailsPage();
			}
		}
		Assert.assertTrue(androidUtility.isElementPresentByID(androidDriver, XPaths.infoModuleBtnPlay, true));// only assert for existence
	}

	protected void validateEpisodeRecordButtonInlineInfo() {
		boolean recordingBadgeSelected = false;
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
		if (!mElement.getText().equalsIgnoreCase("Record")) {
			navigateUp(1);
			mElements = androidDriver.findElements(MobileBy.id(XPaths.recording_badge));
			for (MobileElement element : mElements) {
				if (androidUtility.getElementStateAttribute(element, "selected")) {
					recordingBadgeSelected = true;
					break;
				}
			}
			// TODO : recording badge is displayed only for upcoming episodes only. Not for
			// playable episodes
			// Assert.assertTrue(recordingBadgeSelected, "Recording badge is not displayed
			// for episode which is set for recording");
			tap(1);
		}
	}

	protected void validateEpisodeRecordButtonDetailsPage() {
		boolean recordingBadgeSelected = false;
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
		if (!mElement.getText().equalsIgnoreCase("Record")) {
			navigateUp(1);
			mElements = androidDriver.findElements(MobileBy.id(XPaths.recording_badge));
			for (MobileElement element : mElements) {
				if (androidUtility.getElementStateAttribute(element, "selected")) {
					recordingBadgeSelected = true;
					break;
				}
			}
			// TODO : recording badge is displayed only for upcoming episodes only. Not for
			// playable episodes
			// Assert.assertTrue(recordingBadgeSelected, "Recording badge is not displayed
			// for episode which is set for recording");
		}
	}

	public void assertRecordingBadge() {
		boolean recordingBadgeSelected = false;
		navigateBack(1);
		mElements = androidDriver.findElements(MobileBy.id(XPaths.recording_badge));
		for (MobileElement element : mElements) {
			if (androidUtility.getElementStateAttribute(element, "selected")) {
				recordingBadgeSelected = true;
				break;
			}
		}
		Assert.assertTrue(recordingBadgeSelected, "Recording badge is not displayed for content which is set for recording");
	}

	/**
	 * Method to make parental control ON or ignore if it is already ON.
	 */
	public void enableOrIgnoreParentalControl() {
		if (verifyIsPinEnable() && !verifyIsParentalControlEnable()) {
			manageParentalControlOption();
		}
	}

	/**
	 * Method to make parental control OFF or ignore if it is already OFF.
	 */
	public void disableOrIgnoreParentalControl() {
		if (verifyIsPinEnable() && verifyIsParentalControlEnable()) {
			manageParentalControlOption();
		}
	}

	/**
	 * Method to manage enable and disable of parental control.
	 */
	public void manageParentalControlOption() {
		goToParentalControlsScreen();
		enableDisableParentelControl();
		goToHomeScreen();
	}

	/**
	 * Method to enable or disable parental control option base on boolean flag.
	 *
	 */
	public void enableDisableParentelControl() {
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.restrictedMatureContentTxt, true)) {
			tap(1);
		}
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.pinViewInputBtn, true)) {
			tap(4);
		}
	}

	/**
	 * Method to read text of specified element displaying on screen.
	 */
	public String getElementText(String elementId) {
		mElement = androidDriver.findElement(MobileBy.id(elementId));
		return mElement.getText();
	}

	/**
	 *
	 * Method to check for recording alloted time
	 *
	 * @return true on success, false on failure
	 */
	protected boolean isnDVRFlagIsOff() {
		try {
			int timeAlloted = recordingsJsonUtility.getTotalAllotedHours();
			if (timeAlloted == 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * @param tab
	 *
	 *            This method accepts tab value as "series" or "movies" based on tab
	 *            select the feature screen. navigate down to recommendation and
	 *            verify displayed contents as per server JSON
	 */
	protected void validateRecommendation(String tab) {
		waitForSomeTime(Constants.MEDIUM_SLEEP);
		ArrayList<String> jsonNames = new ArrayList<String>();
		if (tab.contains("series")) {
			selectShowsTab();
		} else {
			selectMoviesTab();
		}
		waitForSomeTime(Constants.MEDIUM_SLEEP);
		if (selectExpectedRow("Recommended For You")) {
			mElements = androidDriver.findElements(MobileBy.className("android.widget.LinearLayout"));
			List<RecommendListItem> recommendListItems = mainJSONUtility.getRecommend(tab, BaseTest.currentProfileID);
			for (int j = 0; j < recommendListItems.size(); j++) {
				String refID = recommendListItems.get(j).ref_id;
				String refType = recommendListItems.get(j).ref_type;
				if (refType.contains("series")) {
					jsonObject = mainJSONUtility.getseriesRecommendationDetails(refID);
					if (jsonObject.getInt("total") != 0) {
						jsonName = jsonObject.getJSONArray("inventories").getJSONObject(0).getString("name");
						jsonNames.add(jsonName);
					}
				} else if (refType.contains("vod")) {
					jsonObject = mainJSONUtility.getVODRecommendationDetailsJSON(refID);
					if (jsonObject.getInt("total") != 0) {
						jsonName = jsonObject.getJSONArray("inventories").getJSONObject(0).getString("name");
						jsonNames.add(jsonName);
					}
				}
			}
			for (int i = 0; i < mElements.size(); i++) {
				if (i != 0) {
					navigateRight(1);
				}
				if (androidUtility.isElementPresentByID(androidDriver, XPaths.titleText, true)) {
					mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
					uiName = mElement.getText();
					System.out.println("UI Name : " + uiName);
					if (jsonNames.contains(uiName)) {
						System.out.println("Matched UI name " + uiName + " with JSON name " + jsonNames + "");
						Assert.assertTrue(true);
					} else {
						System.out.println("Not matched UI name " + uiName + " with JSON name " + jsonNames + "");
						// Assert.assertTrue(false);
					}
				}
			}
		}
	}

	/**
	 * @param tab
	 *            This method accepts tab value as "series" or "movies" based on tab
	 *            select the feature screen. navigate down to resume watching and
	 *            verify displayed contents as per server JSON
	 */
	protected void validateResumeWatching(String tab) {
		waitForSomeTime(Constants.MEDIUM_SLEEP);
		ArrayList<String> jsonNames = new ArrayList<String>();
		if (tab.contains("series")) {
			selectShowsTab();
		} else {
			selectMoviesTab();
		}
		waitForSomeTime(Constants.MEDIUM_SLEEP);
		if (selectExpectedRow("Resume Watching")) {
			mElements = androidDriver.findElements(MobileBy.className("android.widget.LinearLayout"));
			List<RecentsListItem> recentsListItems = mainJSONUtility.getRecents(BaseTest.currentProfileID);
			for (int j = 0; j < recentsListItems.size(); j++) {
				String refID = recentsListItems.get(j).ref_id;
				String refType = recentsListItems.get(j).ref_type;
				if (refType.contains("series")) {
					jsonObject = mainJSONUtility.getseriesRecommendationDetails(refID);
					if (jsonObject.getInt("total") != 0) {
						jsonName = jsonObject.getJSONArray("inventories").getJSONObject(0).getString("name");
						jsonNames.add(jsonName);
					}
				} else if (refType.contains("shared_ref")) {
					jsonObject = mainJSONUtility.getSharedRefRecommendationDetailsJSON(refID);
					if (jsonObject.getInt("total") != 0) {
						if (jsonObject.getJSONArray("shared_refs").getJSONObject(0).has("name")) {
							jsonName = jsonObject.getJSONArray("shared_refs").getJSONObject(0).getString("name");
						} else {
							jsonName = jsonObject.getJSONArray("shared_refs").getJSONObject(0).getString("series_name");
						}
						jsonNames.add(jsonName);
					}
				} else if (refType.contains("program")) {
					jsonObject = mainJSONUtility.getProgramRecommendationDetailsJSON(refID);
					if (jsonObject.getInt("total") != 0) {
						jsonName = jsonObject.getJSONArray("programs").getJSONObject(0).getString("name");
						jsonNames.add(jsonName);
					}
				} else if (refType.contains("channel")) {
					jsonObject = mainJSONUtility.getChannelRecommendationDetailsJSON(refID);
					if (jsonObject.getInt("total") != 0) {
						jsonName = jsonObject.getJSONArray("programs").getJSONObject(0).getString("name");
						jsonNames.add(jsonName);
					}
				} else if (refType.contains("vod")) {
					jsonObject = mainJSONUtility.getVODRecommendationDetailsJSON(refID);
					if (jsonObject.getInt("total") != 0) {
						jsonName = jsonObject.getJSONArray("inventories").getJSONObject(0).getString("name");
						jsonNames.add(jsonName);
					}
				} else if (refType.contains("recording")) {
					jsonName = recordingsJsonUtility.getRecordingJsonName(refID);
					jsonNames.add(jsonName);
				}
			}
			for (int i = 0; i < mElements.size(); i++) {
				if (i != 0) {
					navigateRight(1);
				}
				if (androidUtility.isElementPresentByID(androidDriver, XPaths.titleText, true)) {
					mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
					uiName = mElement.getText();
					System.out.println("UI Name : " + uiName);
					if (jsonNames.contains(uiName)) {
						System.out.println("Matched UI name " + uiName + " with JSON name " + jsonNames + "");
						Assert.assertTrue(true);
					} else {
						System.out.println("Not matched UI name " + uiName + " with JSON name " + jsonNames + "");
						Assert.assertTrue(false);
					}
				}
			}
		}
	}

	protected boolean selectRecentlyWatched() {
		clickMenu();
		navigateDown(1);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.rowHeader));
		String recentlyWatched = mElement.getText();
		if (recentlyWatched.contains("Recently Watched")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param rowHeader
	 *            Displayed row header on feature screen (i.e. Recordings, Resume
	 *            Watching, Monday's Picks, Tuesday's Picks, Wednesday's Picks,
	 *            Thursday's Picks,Friday's Picks,Featured Networks, Recommended For
	 *            You etc.) and navigate down to that respective header
	 * @return true on success, false on failure.
	 */
	protected boolean selectExpectedRow(String headerTitleDicKey) {
		boolean isExpectedRowFound = false;
		String expectedRowHeader = dictionaryReader.getDictionaryValueForKey(headerTitleDicKey);
		if (expectedRowHeader.isEmpty())
			expectedRowHeader = headerTitleDicKey;
		mElements = androidDriver.findElements(MobileBy.id(XPaths.rowHeader));
		List<MobileElement> imgLayouts = androidDriver.findElements(MobileBy.id(XPaths.mainImageLayout));
		for (int rowIndex = 0; rowIndex < mElements.size(); rowIndex++) {
			MobileElement element = mElements.get(0);
			if ((isExpectedRowFound = element.getText().equals(expectedRowHeader))) {
				if (!imgLayouts.get(rowIndex).isSelected())
					navigateDown(1);
				break;
			}
			waitForSomeTime(Constants.SMALL_SLEEP);
			navigateDown(1);
			waitForSomeTime(Constants.SMALL_SLEEP);
		}
		return isExpectedRowFound;
	}

	/**
	 * Method to delete user created profile
	 *
	 * @return true on success, false on failure.
	 */
	protected boolean deleteProfile() {
		if (goToManageProfilesScreen()) {
			waitForSomeTime(Constants.SMALL_SLEEP);
			navigateDown(profileLimit);
			navigateRight(1);
			waitForSomeTime(Constants.SMALL_SLEEP);
			tap(1);
			Assert.assertTrue(androidUtility.isElementPresentByID(androidDriver, XPaths.alertTitle, true), "Remove Alert didn't show up");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
			Assert.assertEquals("Remove Forever", mElement.getText());
			tap(1);
			return true;
		}
		return false;
	}

	protected boolean selectUpcoming(String rowHeader) {
		navigateUp(1);
		waitForSomeTime(Constants.SMALL_SLEEP);
		navigateDown(1);
		String rowHeaderText;
		mElement = androidDriver.findElement(MobileBy.id(XPaths.rowHeader));
		rowHeaderText = mElement.getText();
		boolean flag = rowHeaderText.contains(rowHeader);
		while (!flag) {
			waitForSomeTime(Constants.SMALL_SLEEP);
			navigateDown(1);
			waitForSomeTime(Constants.SMALL_SLEEP);
			rowHeaderText = mElement.getText();
			flag = rowHeaderText.contains(rowHeader);
		}
		return flag;
	}

	public boolean navigateToVODMovie() {
		System.out.println("Select movies tab");
		selectMoviesTab();
		waitForSomeTime(Constants.VERY_SMALL_SLEEP);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.detailsSeeAllNavText));
		mElement.click();
		waitForSomeTime(Constants.MEDIUM_SLEEP);
		// navigating to full details page for first movie
		tap(1);
		waitForSomeTime(Constants.VERY_SMALL_SLEEP);
		navigateRight(2);
		Assert.assertTrue(androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnMoreInfo)).getText().equalsIgnoreCase("Full Details"), "Focused button is not Full Details ");
		tap(1);
		waitForSomeTime(Constants.SMALL_SLEEP);
		if (androidDriver.findElements(MobileBy.id(XPaths.rowHeader)).size() <= 0) {
			System.out.println("Recommendations are not found");
			return false;
		}
		navigateDown(1);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
		System.out.println("VOD movie : " + mElement.getText());
		tap(1);
		waitForSomeTime(Constants.SMALL_SLEEP);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnPlay));
		mElement.click();
		waitForSomeTime(Constants.MEDIUM_SLEEP);
		return true;
	}
//
//	protected void validateRecordButtonAction(Movie movie) {
//		recordedMovies = new ArrayList<>();
//		TestRecordButton btn = new TestRecordButton();
//		btn.testRecordButtonAlerts(movie);
//		System.out.println("Recording is Set");
//	}

	protected void validateMovieRecordingDetails(Movie movie) {
		if (selectRecordingsTab()) {
			waitForSomeTime(Constants.DRIVER_IMPLICIT_WAIT);
			navigateDown(2);
			mElement = androidDriver.findElement(MobileBy.AccessibilityId(XPaths.movieRecordingsRowAccessibilityID));
			Assert.assertTrue(mElement.isDisplayed(), "Movies section not displayed");
			mElements = mElement.findElements(MobileBy.id(XPaths.mainImageLayout));
			mElement = mElements.get(0);
			Assert.assertTrue(androidUtility.getElementStateAttribute(mElement, "selected"), "Focus is not on the first movie in movies section");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
			uiName = mElement.getText();
			System.out.println("Test Movie Name is " + movie.getName());
			System.out.println("Movie name shown up is : " + uiName);
			Assert.assertEquals(uiName, movie.getName(), "Movie name didn't match");
			System.out.println("***---Test Completed---***");
		}
	}

	/**
	 * Method to validate playable episode button row (diff from upcoming : play
	 * button should display for all playable)
	 *
	 * @param PoviderNetwork
	 *            List
	 */
	public void verifyNetworkLogo(List<String> providerNetworks) {
		mElements = androidDriver.findElements(MobileBy.id(XPaths.networkLogo));
		if (providerNetworks.size() == 1) {
			Assert.assertTrue(mElements != null, "Multiple provider network exists");
		} else if (providerNetworks.size() == 0) {
			Assert.assertTrue(mElements == null, "No provider network exists");
		}
	}
	/*
	 * public class RecordingComparator implements Comparator<Recording> {
	 * 
	 * @Override public int compare(Recording o1, Recording o2) { return (int)
	 * (o1.getStart_time() - o2.getStart_time()); } }
	 */
	Comparator<Recording> recordingDateComparator = (Recording rec1, Recording rec2) -> {
		return Long.compare(rec1.getStart_time(), rec2.getStart_time());
	};
	Comparator<Assets> assetDateComparator = (Assets a1, Assets a2) -> {
		return Long.compare(a1.getStart_time(), a2.getStart_time());
	};

	/**
	 * Method to get upcoming air date for movie Case 1. Return soonest Scheduled
	 * Recording Start-End Time and Date as upcoming air date if availbale. Case 2.
	 * Return soonest shared-ref program start-end time and date as upcoming air
	 * date if available.
	 */
	public String getUpcomingAirDate(Movie movie) {
		List<Recording> recordings = new ArrayList<>();
		Boolean hasLiveContent = isMovieLive(movie);
		List<Assets> assets = getMovieUpcomingAssets(movie);
		if (!hasLiveContent) {
			if (movie.getType().equalsIgnoreCase("shared_ref")) {
				for (Assets asset : assets) {
					if (recordingsJsonUtility.isScheduledProgram(asset.getId())) {
						recordings.add(recordingsJsonUtility.getScheduledRecordingforProgram(asset.getId()));
					}
				}
				if (recordings.size() != 0) {
					Collections.sort(recordings, recordingDateComparator);
					return getUpcomingDate(recordings.get(0).getStart_time(), recordings.get(0).getEnd_time());
				} else if (assets.size() != 0) {
					Collections.sort(assets, assetDateComparator);
					long startTimeSecs = 0L;
					long endTimeSecs = 0L;
					startTimeSecs = assets.get(0).getStart_time();
					endTimeSecs = assets.get(0).getEnd_time();
					if (startTimeSecs > (System.currentTimeMillis() / 1000)) {
						return getUpcomingDate(startTimeSecs, endTimeSecs);
					}
				}
			} else if (movie.getType().equalsIgnoreCase("program")) {
				if (utility.isUpcoming(movie.getStart_time(), movie.getEnd_time()) && recordingsJsonUtility.isScheduledProgram(movie.getID())) {
					return getUpcomingDate(movie.getStart_time(), movie.getEnd_time());
				}
			}
		}
		return null;
	}

	public boolean isMovieOutOfHomePlayable(Movie movie) {
		List<Assets> assets = movie.getAssets();
		for (Assets asset : assets) {
			Channel channel = channelsJSONUtility.getChannelsDetails(asset.getChannelId());
			if (channel.is_out_of_home_playback_enabled()) {
				return true;
			}
		}
		System.out.println("Not OOH Playable");
		return false;
	}

	public boolean isScheduledRecordingIconVisible(Movie movie) {
		List<Assets> assets = movie.getAssets();
		if (assets != null) {
			for (Assets asset : assets) {
				if (recordingsJsonUtility.isScheduledProgram(asset.getId())) {
					return true;
				}
			}
		}
		return false;
	}

	public String getUpcomingDate(Long startTimeSec, Long endTimeSec) {
		Date startTimeDate = new Date(startTimeSec * 1000);
		Date endTimeDate = new Date(endTimeSec * 1000);
		String date = DateAndTime.getDateInFormatMD(startTimeDate);
		String startTime = DateAndTime.getTimeInFormatHHMMwithTrim(startTimeDate, false);
		String endTime = DateAndTime.getTimeInFormatHHMMwithTrim(endTimeDate, true);
		return date + " " + startTime + " - " + endTime;
	}

	/**
	 * Method to validate title text of an element and expected title are equal.
	 * 
	 * @param elementId
	 *            - Element Id to verify.
	 * @param expectedTitleDicKey
	 *            - Dictionary key of expected title text.
	 * @param message
	 *            - message to display on assert failure.
	 */
	public void validateElementTitleEquals(String elementId, String expectedTitleDicKey, String message) {
		mElement = androidDriver.findElement(MobileBy.id(elementId));
		final String expectedBtnTitleText = dictionaryReader.getDictionaryValueForKey(expectedTitleDicKey);
		Assert.assertEquals(mElement.getText(), expectedBtnTitleText, message);
	}

	/**
	 * Method to validate title text of an element and expected title are not equal.
	 * 
	 * @param elementId
	 *            - Element Id to verify.
	 * @param expectedTitleDicKey
	 *            - Dictionary key of expected title text.
	 * @param message
	 *            - message to display on assert failure.
	 */
	public void validateElementTitleNotEquals(String elementId, String expectedTitleDicKey, String message) {
		mElement = androidDriver.findElement(MobileBy.id(elementId));
		final String expectedBtnTitleText = dictionaryReader.getDictionaryValueForKey(expectedTitleDicKey);
		Assert.assertNotEquals(mElement.getText(), expectedBtnTitleText, message);
	}

	/**
	 * Method to correct focus of tabs on manage recording screen.
	 */
	protected void correctTabFocus() {
		final boolean isRecordedTabFoucsed = Boolean.valueOf(androidDriver.findElementById(XPaths.recordedTab).getAttribute("focused"));
		if (!isRecordedTabFoucsed) {
			navigateDown(1);
			navigateUp(1);
		}
	}

	public void playLiveContentFromGuide() {
		selectGuideTab();
		navigateDown(2);
		tap(1);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnPlay));
		Assert.assertTrue(androidUtility.getElementStateAttribute(mElement, "focused"), "Play button is not having focus");
		tap(1);
		waitForSomeTime(Constants.SMALL_SLEEP);
		mElements = androidDriver.findElements(MobileBy.id(XPaths.playbackOptionTitle));
		if (mElements.size() > 0) {
			tap(1);
		}
		waitForSomeTime(Constants.MEDIUM_SLEEP);
	}

	/**
	 * Method to get selected series or movie meta-data from UI.
	 */
	protected String[] getSelectedProgramMetaData() {
		final String DELIMITER = ",";
		waitForSomeTime(Constants.VERY_SMALL_SLEEP);
		mElement = androidDriver.findElements(MobileBy.id(XPaths.mainImage)).parallelStream().filter(element -> element.isSelected()).findAny().orElse(null);
		final String programMetaData = androidUtility.getElementAttribute(mElement, "contentDescription");
		if (programMetaData == null) {
			return null;
		}
		return programMetaData.split(DELIMITER);
	}

	/**
	 * Method to verify confirmation alert dialog for recording deletion.
	 */
	protected void verifyAlertDialogDetails(final String expectedAlertDialogTitle) {
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.alertTitle, true)) {
			final String message = "Expected message not display on alert dialog.";
			waitForSomeTime(Constants.VERY_SMALL_SLEEP);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.alertTitle));
			final String actualAlertDialogTitle = mElement.getText();
			Assert.assertTrue(actualAlertDialogTitle.equals(expectedAlertDialogTitle), message);
		} else {
			final String message = "Alert dialog not display.";
			Assert.fail(message);
		}
	}

	/**
	 * Method to return a upcoming movie with recording enabled
	 * 
	 * @return
	 */
	public Movie getUpcomingRecordingenabledMovie() {
		Movie upcomingRecordingenabledMovie = null;
		movies = mainJSONUtility.getAllUpcomingMovies(200);
		for (Movie movie : movies) {
			if (movie.getType().equalsIgnoreCase("program") && movie.is_recording_enabled()) {
				upcomingRecordingenabledMovie = movie;
				break;
			} else if (isMovieUpcomingRecordingenabled(movie) && movie.getAssets().size() > 1 && movie.getAssets().size() < 5) {
				upcomingRecordingenabledMovie = movie;
				break;
			}
		}
		System.out.println("UpcomingRecordingEnabledMovieName : " + upcomingRecordingenabledMovie.getName());
		return upcomingRecordingenabledMovie;
	}

	/**
	 * Changing to record button for movie if it is already set for recording
	 * 
	 * @param movieRef
	 */
	public void setRecordButtonForGivenMovie(Movie movieRef) {
		navigateToSelectedMovie(movieRef.getName());
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
		if ((mElement.getText()).equalsIgnoreCase("Cancel Recording")) {
			mElement.click();
			waitForSomeTime(Constants.SMALL_SLEEP);
		}
		navigateUp(3);// making sure , screen displays from top
		System.out.println("button text : " + androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording)).getText());
	}

	/**
	 * Changing to record button for movie if it is already set for recording
	 * 
	 * @param movieRef
	 */
	public void setCancelRecordingButtonForGivenMovie(Movie movieRef) {
		navigateToSelectedMovie(movieRef.getName());
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording));
		if ((mElement.getText()).equalsIgnoreCase("Cancel Recording")) {
			return;
		} else {
			mElement.click();
			waitForSomeTime(Constants.SMALL_SLEEP);
			mElements = androidDriver.findElements(MobileBy.id(XPaths.recordAssetOptionTitle));
			if ((mElements.size()) > 0) {
				mElements.get(0).click();
			}
			mElements = androidDriver.findElements(MobileBy.id(XPaths.alertTitle));
			if (mElements.size() > 0) {
				if (mElements.get(0).getText().equalsIgnoreCase("Are you sure?")) {
					mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
					mElement.click();
				}
			}
		}
		navigateUp(3);// making sure , screen displays from top
		System.out.println("button text : " + androidDriver.findElement(MobileBy.id(XPaths.infoModuleBtnRecording)).getText());
	}

	/**
	 * Navigating to selected movie
	 * 
	 * @param movieTitle
	 */
	public void navigateToSelectedMovie(String movieTitle) {
		System.out.println("Search content : " + movieTitle);
		selectSearchTab();
		waitForSomeTime(Constants.SMALL_SLEEP);
		executeASearchQuery(movieTitle);
		if (androidUtility.isElementPresentByAcsID(androidDriver, XPaths.searchResultsShowsRowAccessibilityId, true))
			navigateDown(1);
		for (int i = 0; i < 7; i++) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.titleText));
			MobileElement sec_data = androidDriver.findElement(MobileBy.id(XPaths.titleText));
			if ((mElement.getText().equalsIgnoreCase(movieTitle) || sec_data.getText().contains(movieTitle))) {
				tap(1);
				break;
			} else {
				navigateRight(1);
			}
		}
		navigateRight(2);
		tap(1);
		waitForSomeTime(Constants.SMALL_SLEEP);
	}

	/**
	 * Method to create formatted name of an episode with its details.
	 */
	protected String getEpisodeFormattedName(String episodeName, String seasonNumber, String episodeNumber) {
		return StringUtils.join("S", seasonNumber, " : E", episodeNumber, " - ", episodeName);
	}

	/**
	 * Method to verify alert message in case no episode recordings available for a
	 * series.
	 */
	protected void verifyNoRecordingAlertMsg() {
		validateElementTitleEquals(XPaths.scheduledRecordingNoResults, "scheduled_episodes_empty_state_text", "No recording alert message is not same as expected.");
	}

	/**
	 * Method to navigate to EPG screen and verify is threshold alert displaying or
	 * not. If alert is displaying then navigate to manage recording screen else
	 * terminate test case.
	 */
	protected boolean checkAndNavigateWithRecordingThresholdAlertDialog() {
		mElement = androidDriver.findElements(MobileBy.id(XPaths.epgProgramName)).get(0);
		mElement.click();
		navigateUp(2);
		waitForSomeTime(Constants.SMALL_SLEEP);
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.episodeInforModuleBtnRecord, true)) {
			if ((mElement = androidDriver.findElement(MobileBy.id(XPaths.episodeInforModuleBtnRecord))).getText().equals(dictionaryReader.getDictionaryValueForKey("record_button_title"))) {
				mElement.click();
				final String expectedAlertDialogTitle = dictionaryReader.getDictionaryValueForKey("are_you_sure_alert_title");
				if (!androidUtility.isElementPresentByID(androidDriver, XPaths.alertTitle, true)) {
					return false;
				}
				mElement = androidDriver.findElement(MobileBy.id(XPaths.alertTitle));
				final String actualAlertDialogTitle = mElement.getText();
				if (!expectedAlertDialogTitle.equals(actualAlertDialogTitle)) {
					navigateBack(1);
					return false;
				}
				androidDriver.findElementById(XPaths.button2).click();
				return true;
			}
		}
		return false;
	}

	/**
	 * Replaces number of minutes with space
	 * 
	 * @param str
	 * @return
	 */
	public String methodReplacingMinutes(String str) {
		String[] data = str.split("\\.");
		data[0] = data[0].replaceAll("\\d", "");
		return String.join(".", data);
	}
}
