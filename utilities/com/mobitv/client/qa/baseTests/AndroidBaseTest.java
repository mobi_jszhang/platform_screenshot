package com.mobitv.client.qa.baseTests;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.MultiLineReceiver;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.mobitv.client.qa.constants.AndroidKeyEvents;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.constants.XPaths;
import com.mobitv.client.qa.functions.AndroidUtility;
import com.mobitv.client.qa.functions.DictionaryJSONReader;
import com.mobitv.client.qa.models.Channel;
import com.mobitv.client.qa.models.Program;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
/**
 * 
 * @author MobiTV
 *
 */
public class AndroidBaseTest extends BaseTest {
	protected static AndroidDriver<MobileElement> androidDriver;
	protected MobileElement mElement = null;
	protected List<MobileElement> mElements = null;
	protected AndroidDebugBridge adb;
	protected IDevice iDevice = null;
	protected AndroidUtility androidUtility = new AndroidUtility();
	private int adbConnectionTrails = 10;
	private String deviceName;
	private ArrayList<String> adbOutputLines;
	private SimpleDateFormat logTimeFormatter = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");
	protected static ArrayList<String> androidMobileMenuItems = null;
	protected static ArrayList<String> androidTVMenuItems = null;
	protected static ArrayList<String> androidTVCategoryFilters = null;
	protected static ArrayList<String> androidTVSearchResultSections = null;
	protected static ArrayList<String> androidTVSettingsMenuItems = null;

	/**
	 * This method will be useful for initializing Android specific test
	 * environment.
	 * 
	 * @throws InterruptedException
	 * @throws JSONException
	 * @throws IOException
	 */
	@BeforeSuite(alwaysRun = true)
	public void initializeAndroidTestEnvironment() throws InterruptedException, JSONException, IOException {
		ConfigurationConstants.appActivity = tReader.getAppActivity();
		ConfigurationConstants.appPackage = tReader.getAppPackage();
		createAndroidDriver();
		Thread.sleep(1000);// This is mandatory, no alternative solution is present at this moment.
		ConfigurationConstants.deviceLocale = getDeviceLocale();
		ConfigurationConstants.deviceFirmware = getDeviceOSVersion();
		clientConfiguration = mainJSONUtility.getClientConfiguration(ConfigurationConstants.deviceFirmware, ConfigurationConstants.deviceName, ConfigurationConstants.deviceLocale);
		ConfigurationConstants.childProtectionRatings = clientConfiguration.get_filter_by_child_protection_rating_list_asString();
		ConfigurationConstants.childProtectionEnabledRatings = clientConfiguration.getBlocked_child_protection_ratings();
		dictionaryReader = new DictionaryJSONReader(ConfigurationConstants.deviceLocale, clientConfiguration);
		Thread.sleep(3000);// This is mandatory, no alternative solution is present at this moment.
	}
	@BeforeClass(alwaysRun = true)
	public void createDriver(){
		try {
			System.out.println("beginning of class");
			createAndroidDriver();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}



	/**
	 * Quitting android driver after executing a test case. It will close the app. @
	 */
	@AfterMethod(alwaysRun = true)
	public void quitAndroidDriver() {
//		destroyAndroidDriver();
	}

	/**
	 * Method to initialize Android driver
	 * 
	 * @throws InterruptedException
	 */
	protected void createAndroidDriver() throws InterruptedException {
		try {
			AndroidDebugBridge.init(false);
		} catch (IllegalStateException e) {}
		adb = AndroidDebugBridge.createBridge();
		do {
			if (adb.hasInitialDeviceList() && adb != null)
				break;
			adbConnectionTrails--;
			Thread.sleep(3000);// This is mandatory, no alternative solution is present at this moment.
		} while (adbConnectionTrails > 0);
		for (IDevice iDevice : adb.getDevices()) {
			deviceName = iDevice.getName().toLowerCase();
			if (deviceName.contains(ConfigurationConstants.deviceName.toLowerCase())) {
				ConfigurationConstants.serialNumber = iDevice.getSerialNumber();
				this.iDevice = iDevice;
				break;
			}
		}
		if (iDevice == null && adb.getDevices().length != 0)
			iDevice = adb.getDevices()[0];
		try {
			androidDriver = driverFactory.getAndroidDriver();
			driver = androidDriver;
			wakeUp();
			waitForElementByID(Constants.DRIVER_IMPLICIT_WAIT, XPaths.profileMenuTitle);
		} catch (Exception e) {}
	}

	/**
	 * Method for quitting the Android driver.
	 */
	protected void destroyAndroidDriver() {
		try {
			AndroidDebugBridge.terminate();
		} catch (IllegalStateException e) {}
		if (null != androidDriver) {
			androidDriver.quit();
			androidDriver = null;
			driver = null;
		}
	}

	/**
	 * Method for signIn to to the application.
	 */
	protected void signIn(String userName, String password) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.userName));
		androidUtility.clearAndInputText(mElement, userName);
		hideKeyboard();
		mElement = androidDriver.findElement(MobileBy.id(XPaths.password));
		androidUtility.clearAndInputText(mElement, password);
		//hideKeyboard();
		if (ConfigurationConstants.deviceName.equals("Amazon")) {
			playPause(1);
		} else {
			hideKeyboard();
			mElement = androidDriver.findElement(MobileBy.id(XPaths.signInButton));
			mElement.click();
		}
		waitForElementByID(Constants.MEDIUM_SLEEP, XPaths.profileMenuTitle);
	}

	/**
	 * Method to get Device Manufacturer Name.
	 * 
	 * @return String
	 */
	protected String getDeviceManufacturerName() {
		return executeShellCommand("getprop ro.product.manufacturer").get(0);
	}

	/**
	 * Method to get Device Model.
	 * 
	 * @return String
	 */
	protected String getDeviceModel() {
		return executeShellCommand("getprop ro.product.model").get(0);
	}

	/**
	 * Method to get Device OS Version.
	 * 
	 * @return String
	 */
	protected String getDeviceOSVersion() {
		return executeShellCommand("getprop ro.build.version.release").get(0);
	}

	/**
	 * Method to get Device API Level.
	 * 
	 * @return String
	 */
	protected String getDeviceAPILevel() {
		return executeShellCommand("getprop ro.build.version.sdk").get(0);
	}

	/**
	 * Method to get Device Locale.
	 * 
	 * @return String
	 */
	protected String getDeviceLocale() {
		return executeShellCommand("getprop persist.sys.locale").get(0);
	}

	/**
	 * Method to get Device display size.
	 * 
	 * @return String
	 */
	protected String getDeviceDisplaySize() {
		return executeShellCommand("getprop sys.display-size").get(0);
	}

	/**
	 * Method to get Device Time Zone.
	 * 
	 * @return String
	 */
	protected String getDeviceTimeZone() {
		return executeShellCommand("getprop persist.sys.timezone").get(0);
	}

	/**
	 * Method to get Device iP address.
	 * 
	 * @return String
	 */
	protected String getDeviceIPAddress() {
		return executeShellCommand("getprop net.dns1").get(0);
	}

	/**
	 * Function to hide keyboard.
	 */
	protected void hideKeyboard() {
		if (null != androidDriver) {
			try {
				androidDriver.hideKeyboard();
			} catch (Exception e) {}
		}
	}

	/**
	 * Function to Navigate back. @ @
	 */
	protected void navigateBack(int count) {
		if (null != androidDriver) {
			for (int i = 0; i < count; i++) {
				androidDriver.pressKeyCode(AndroidKeyEvents.NAVIGATE_BACK);
				waitForSomeTime(Constants.VERY_SMALL_SLEEP);
			}
		}
	}

	/**
	 * Function to Navigate right.
	 */
	protected void navigateRight(int count) {
		if (null != androidDriver) {
			for (int i = 0; i < count; i++)
				androidDriver.pressKeyCode(AndroidKeyEvents.NAVIGATE_RIGHT);
		}
	}

	/**
	 * Function to Navigate left.
	 */
	protected void navigateLeft(int count) {
		if (null != androidDriver) {
			for (int i = 0; i < count; i++)
				androidDriver.pressKeyCode(AndroidKeyEvents.NAVIGATE_LEFT);
		}
	}

	/**
	 * Function to Navigate up.
	 */
	protected void navigateUp(int count) {
		if (null != androidDriver) {
			for (int i = 0; i < count; i++) {
				androidDriver.pressKeyCode(AndroidKeyEvents.NAVIGATE_UP);
			}
		}
	}

	/**
	 * Function to Navigate down.
	 */
	protected void navigateDown(int count) {
		if (null != androidDriver) {
			for (int i = 0; i < count; i++) {
				androidDriver.pressKeyCode(AndroidKeyEvents.NAVIGATE_DOWN);
			}
		}
	}

	/**
	 * Function to click Enter on mobile.
	 */
	protected void mobileEnter(int count) {
		if (null != androidDriver) {
			for (int i = 0; i < count; i++) {
				androidDriver.pressKeyCode(AndroidKeyEvents.ENTER);
			}
		}
	}

	/**
	 * Function to wake up the device from sleep mode
	 */
	protected void wakeUp() {
		if (null != androidDriver) {
			androidDriver.pressKeyCode(AndroidKeyEvents.WAKE_UP);
		}
	}

	/**
	 * Function to Tap on focused element. @ @
	 */
	protected void tap(int count) {
		if (null != androidDriver) {
			for (int i = 0; i < count; i++) {
				androidDriver.pressKeyCode(AndroidKeyEvents.CLICK);
				waitForSomeTime(Constants.SMALL_SLEEP);
			}
		}
	}

	/**
	 * Function to Forward Media. @ @
	 */
	protected void mediaForward(int count) {
		if (null != androidDriver) {
			for (int i = 0; i < count; i++) {
				androidDriver.pressKeyCode(AndroidKeyEvents.FORWARD);
				waitForSomeTime(Constants.SMALL_SLEEP);
			}
		}
	}

	/**
	 * Function to Rewind Media @ @
	 */
	protected void mediaRewind(int count) {
		if (null != androidDriver) {
			for (int i = 0; i < count; i++) {
				androidDriver.pressKeyCode(AndroidKeyEvents.REWIND);
				waitForSomeTime(Constants.SMALL_SLEEP);
			}
		}
	}

	/**
	 * Function to play or pause.
	 * 
	 */
	protected void playPause(int count) {
		if (null != androidDriver) {
			for (int i = 0; i < count; i++) {
				System.out.println("TESTING PLAY/PAUSE");
				androidDriver.pressKeyCode(AndroidKeyEvents.PLAYPAUSE);
				waitForSomeTime(Constants.SMALL_SLEEP);
			}
		}
	}
	
	

	
//	protected void longPress(int keycode, int second) {
//		if (null != androidDriver) {
//			try {
//				Runtime.getRuntime().exec("cmd /c adb shell input keyevent --longpress 5" + keycode);
//				Thread.sleep(second * 1000);
//				androidDriver.pressKeyCode(keycode);
//				Thread.sleep(1000); 
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			 
//		}
//	}

	/**
	 * Method check is network alert dialog display on screen.
	 * 
	 */
	protected boolean isNetworkAlertOnScreen() {
	
		return androidUtility.isElementPresentByID(androidDriver, XPaths.networkErrorAlertMsg, true);			
	}	
	
	
	/**
	 * Method for waiting for some time, during buffering.
	 * 
	 */
	protected void waitForBuffering() {

		int maxAttempts = 10;

		for (; maxAttempts >= 0; maxAttempts--) {
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.mediaBufferingSpinner, true)) {
				waitForSomeTime(Constants.VERY_SMALL_SLEEP);
			} else {
				break;
			}
		}
	}

	/**
	 * Method to switch to specific channel.
	 */
	protected void switchToChannel(int channelNum) {

		final String adbCmd = "input text";
		final String channelNumStr = String.join(StringUtils.SPACE, adbCmd, String.valueOf(channelNum).trim());

		executeShellCommand(channelNumStr);
	}

	/**
	 * Method for waiting some time, when needed.
	 * 
	 * @param waitTime
	 */
	protected void waitForSomeTime(int waitTime) {
		androidUtility.manageImplicitWait(androidDriver, waitTime);
		try {
			mElement = androidDriver.findElement(MobileBy.id(" "));
		} catch (Exception e) {}
		androidUtility.turnOnImplicitWait(androidDriver);
	}

	/**
	 * Method to wait for an element with specified amount of time using ID.
	 * 
	 * @param waitTime
	 * @param id
	 * @return
	 */
	protected boolean waitForElementByID(int waitTime, String id) {
		androidUtility.manageImplicitWait(androidDriver, waitTime);
		try {
			mElement = androidDriver.findElement(MobileBy.id(id));
			androidUtility.turnOnImplicitWait(androidDriver);
			return true;
		} catch (Exception e) {
			androidUtility.turnOnImplicitWait(androidDriver);
			return false;
		}
	}

	/**
	 * Method to wait for an element with specified amount of time using className.
	 * 
	 * @param waitTime
	 * @param id
	 * @return
	 */
	protected boolean waitForElementByClassName(int waitTime, String className) {
		androidUtility.manageImplicitWait(androidDriver, waitTime);
		try {
			mElement = androidDriver.findElement(MobileBy.className(className));
			androidUtility.turnOnImplicitWait(androidDriver);
			return true;
		} catch (Exception e) {
			androidUtility.turnOnImplicitWait(androidDriver);
			return false;
		}
	}

	/**
	 * Method to wait for an element with specified amount of time using name.
	 * 
	 * @param waitTime
	 * @param id
	 * @return
	 */
	protected boolean waitForElementByName(int waitTime, String name) {
		androidUtility.manageImplicitWait(androidDriver, waitTime);
		try {
			mElement = androidDriver.findElement(MobileBy.name(name));
			androidUtility.turnOnImplicitWait(androidDriver);
			return true;
		} catch (Exception e) {
			androidUtility.turnOnImplicitWait(androidDriver);
			return false;
		}
	}

	/**
	 * Method to execute give shell command.
	 * 
	 * @param Shell
	 *            command to execute
	 * @return shell command output lines.
	 */
	protected ArrayList<String> executeShellCommand(String cmd) {
		adbOutputLines = new ArrayList<>();
		try {
			if (iDevice == null) {
				adbOutputLines.add("No Device Found");
				return adbOutputLines;
			}
			iDevice.executeShellCommand(cmd, new MultiLineReceiver() {
				@Override
				public boolean isCancelled() {
					return false;
				}

				@Override
				public void processNewLines(String[] lines) {
					for (String line : lines) {
						adbOutputLines.add(line);
					}
				}
			});
		} catch (TimeoutException | AdbCommandRejectedException | ShellCommandUnresponsiveException | IOException e) {
			return null;
		}
		return adbOutputLines;
	}

	public ArrayList<Program> getListOfStartoverEnabledPrograms() {
		ArrayList<Program> listOfPrograms = new ArrayList<>();
		ArrayList<Channel> channels = channelsJSONUtility.getAllChannels(200);
		ArrayList<Channel> isStartoverEnabledChannels = new ArrayList<>();
		channels.forEach(channel -> {
			if (channel.isIs_startover_enabled()) {
				isStartoverEnabledChannels.add(channel);
			}
		});
		if (isStartoverEnabledChannels.size() < 1) {
			return null;
		} else {
			isStartoverEnabledChannels.forEach(channel -> {
				programs = channelsJSONUtility.getProgramsForChannel(10, channel.getID());
				listOfPrograms.addAll(programs);
			});
		}
		return listOfPrograms;
	}

	public ArrayList<String> getListOfStartoverEnabledProgramIds() {
		ArrayList<String> programIds = new ArrayList<>();
		programs = getListOfStartoverEnabledPrograms();
		programs.forEach(program -> {
			System.out.println("ProgramId : " + program.getID());
			programIds.add(program.getID());
		});
		return programIds;
	}

	public boolean isGivenProgramStartOverEnabled(String programId) {
		boolean isGivenProgramStartOverEnabled = false;
		List<String> programIds = new ArrayList<>();
		programIds.addAll(getListOfStartoverEnabledProgramIds());
		if (programIds.contains(programId)) {
			isGivenProgramStartOverEnabled = true;
		}
		return isGivenProgramStartOverEnabled;
	}

	// Util methods for catchup enabled
	public ArrayList<Program> getListOfCatchUpEnabledPrograms() {
		ArrayList<Program> listOfPrograms = new ArrayList<>();
		ArrayList<Channel> channels = channelsJSONUtility.getAllChannels(200);
		ArrayList<Channel> isCatchUpEnabledChannels = new ArrayList<>();
		channels.forEach(channel -> {
			if (channel.isIs_catchup_enabled()) {
				isCatchUpEnabledChannels.add(channel);
			}
		});
		if (isCatchUpEnabledChannels.size() < 1) {
			return null;
		} else {
			isCatchUpEnabledChannels.forEach(channel -> {
				programs = channelsJSONUtility.getProgramsForChannel(1, channel.getID());
				listOfPrograms.addAll(programs);
			});
		}
		return listOfPrograms;
	}

	public ArrayList<String> getListOfCatchUpEnabledProgramIds() {
		ArrayList<String> programIds = new ArrayList<>();
		programs = getListOfCatchUpEnabledPrograms();
		programs.forEach(program -> {
			programIds.add(program.getID());
		});
		return programIds;
	}

	public boolean isGivenProgramCatchUpEnabled(String programId) {
		boolean isGivenProgramCatchUpEnabled = false;
		List<String> programIds = new ArrayList<>();
		programIds.addAll(getListOfCatchUpEnabledProgramIds());
		if (programIds.contains(programId)) {
			isGivenProgramCatchUpEnabled = true;
		}
		return isGivenProgramCatchUpEnabled;
	}

	// Util methods for is_timeshift_enabled enabled
	public ArrayList<Program> getListOfTimeShiftEnabledPrograms() {
		ArrayList<Program> listOfPrograms = new ArrayList<>();
		ArrayList<Channel> channels = channelsJSONUtility.getAllChannels(200);
		ArrayList<Channel> isTimeShiftEnabledChannels = new ArrayList<>();
		channels.forEach(channel -> {
			if (channel.isIs_timeshift_enabled()) {
				isTimeShiftEnabledChannels.add(channel);
			}
		});
		if (isTimeShiftEnabledChannels.size() < 1) {
			return null;
		} else {
			isTimeShiftEnabledChannels.forEach(channel -> {
				programs = channelsJSONUtility.getProgramsForChannel(1, channel.getID());
				listOfPrograms.addAll(programs);
			});
		}
		return listOfPrograms;
	}

	public ArrayList<String> getListOfTimeShiftEnabledProgramIds() {
		ArrayList<String> programIds = new ArrayList<>();
		programs = getListOfTimeShiftEnabledPrograms();
		programs.forEach(program -> {
			programIds.add(program.getID());
		});
		return programIds;
	}

	public boolean isGivenProgramTimeShiftEnabled(String programId) {
		boolean isGivenProgramTimeShiftEnabled = false;
		List<String> programIds = new ArrayList<>();
		programIds.addAll(getListOfTimeShiftEnabledProgramIds());
		if (programIds.contains(programId)) {
			isGivenProgramTimeShiftEnabled = true;
		}
		return isGivenProgramTimeShiftEnabled;
	}

	// Util methods for Recording enabled
	public ArrayList<Program> getListOfRecordingEnabledPrograms() {
		ArrayList<Program> listOfPrograms = new ArrayList<>();
		ArrayList<Channel> channels = channelsJSONUtility.getAllChannels(200);
		ArrayList<Channel> isRecordingEnabledChannels = new ArrayList<>();
		channels.forEach(channel -> {
			if (channel.isIs_recording_enabled()) {
				isRecordingEnabledChannels.add(channel);
			}
		});
		if (isRecordingEnabledChannels.size() < 1) {
			return null;
		} else {
			isRecordingEnabledChannels.forEach(channel -> {
				programs = channelsJSONUtility.getProgramsForChannel(1, channel.getID());
				listOfPrograms.addAll(programs);
			});
		}
		return listOfPrograms;
	}

	public ArrayList<String> getListOfRecordingEnabledProgramIds() {
		ArrayList<String> programIds = new ArrayList<>();
		programs = getListOfRecordingEnabledPrograms();
		programs.forEach(program -> {
			programIds.add(program.getID());
		});
		return programIds;
	}

	public boolean isGivenProgramRecordingEnabled(String programId) {
		boolean isGivenProgramRecordingEnabled = false;
		List<String> programIds = new ArrayList<>();
		programIds.addAll(getListOfRecordingEnabledProgramIds());
		if (programIds.contains(programId)) {
			isGivenProgramRecordingEnabled = true;
		}
		return isGivenProgramRecordingEnabled;
	}

	/**
	 * This method will be called on finishing of test suite.
	 */
	@Override
	public void onFinish(ITestContext result) {}

	/**
	 * This method will be called on start of test suite.
	 */
	@Override
	public void onStart(ITestContext result) {}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {}

	/**
	 * This method will be called on Failure of a test case.
	 */
	@Override
	public void onTestFailure(ITestResult result) {
		/*
		 * try { testStartTime = testStartTime.replaceAll(":", "-"); testStartTime =
		 * testStartTime.replaceAll("\\s", "-");
		 * systemCommander.writeADBErrorLogs(ConfigurationConstants.serialNumber,
		 * "ErrorLog_" + testStartTime); extentTest.log(LogStatus.FAIL,
		 * "<a href='./Logs/" + testStartTime + ".log'>ADB Logs</a>"); } catch
		 * (IOException | InterruptedException e) {}
		 */
	}

	/**
	 * This method will be called on test case skip
	 */
	@Override
	public void onTestSkipped(ITestResult result) {}

	/**
	 * This method will be called at start of test case.
	 */
	@Override
	public void onTestStart(ITestResult result) {
		testStartTime = logTimeFormatter.format(new Date());
		try {
			systemCommander.clearADBLogs(ConfigurationConstants.serialNumber);
		} catch (IOException | InterruptedException e) {}
	}

	/**
	 * This method will be called once test case succeeded.
	 */
	@Override
	public void onTestSuccess(ITestResult result) {}

	/**
	 * Method to verify weather parental control is enable or disable.
	 *
	 * @return - true is enable else false.
	 */
	public boolean verifyIsParentalControlEnable() {
		return profilesJsonUtility.isParentalControlEnabled();
	}

	/**
	 * Method to verify weather pin is enable or disable.
	 *
	 * @return - true is enable else false.
	 */
	public boolean verifyIsPinEnable() {
		return profilesJsonUtility.isPinEnabled();
	}
}
