package com.mobitv.client.qa.baseTests;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.constants.URLs;
import com.mobitv.client.qa.drivers.DriverFactory;
import com.mobitv.client.qa.functions.DateAndTime;
import com.mobitv.client.qa.functions.DictionaryJSONReader;
import com.mobitv.client.qa.functions.ExecuteSystemCommands;
import com.mobitv.client.qa.functions.MessagesReader;
import com.mobitv.client.qa.functions.TestConfigurationPropertiesReader;
import com.mobitv.client.qa.functions.TestConfigurationReader;
import com.mobitv.client.qa.functions.Utility;
import com.mobitv.client.qa.json.CategoryFiltersJSONUtility;
import com.mobitv.client.qa.json.ChannelsJSONUtility;
import com.mobitv.client.qa.json.MainJSONUtility;
import com.mobitv.client.qa.json.OfferJSONUtility;
import com.mobitv.client.qa.json.ProfilesJSONUtility;
import com.mobitv.client.qa.json.PurchaseJSONUtility;
import com.mobitv.client.qa.json.RecordingsJSONUtility;
import com.mobitv.client.qa.json.SearchJSONUtility;
import com.mobitv.client.qa.models.Assets;
import com.mobitv.client.qa.models.Channel;
import com.mobitv.client.qa.models.ClientConfig;
import com.mobitv.client.qa.models.Episode;
import com.mobitv.client.qa.models.Movie;
import com.mobitv.client.qa.models.Program;
import com.mobitv.client.qa.models.Seasons;
import com.mobitv.client.qa.models.Show;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
/**
 * 
 * @author MobiTV
 *
 */
public class BaseTest implements ITestListener {
	protected TestConfigurationReader tReader;
	protected MessagesReader messageReader;
	protected static DictionaryJSONReader dictionaryReader;
	protected ClientConfig clientConfiguration;
	protected DriverFactory driverFactory = new DriverFactory();
	protected static WebDriver driver;
	protected Utility utility = new Utility();
	protected ProfilesJSONUtility profilesJsonUtility = new ProfilesJSONUtility();
	protected MainJSONUtility mainJSONUtility = new MainJSONUtility();
	protected SearchJSONUtility searchJSONUtility = new SearchJSONUtility();
	protected CategoryFiltersJSONUtility categoryFiltersJSONUtility = new CategoryFiltersJSONUtility();
	protected ChannelsJSONUtility channelsJSONUtility = new ChannelsJSONUtility();
	protected static RecordingsJSONUtility recordingsJsonUtility;
	protected OfferJSONUtility offerJSONUtility = new OfferJSONUtility();
	protected PurchaseJSONUtility purchaseJSONUtility = new PurchaseJSONUtility();
	protected DateAndTime dAndt = new DateAndTime();
	protected ArrayList<String> profilesNames;
	protected String profileName;
	private String osName;
	protected static ExecuteSystemCommands systemCommander;
	protected String uiName, jsonName, uiDescription, jsonDescription, uiMetada, uiActors, jsonActors, uiMessage, jsonMessage, jsonData, jsonMetaData, jsonMetada;
	private static String extentReportFile = null;
	private static ExtentReports extentReports = null;
	protected static ExtentTest extentTest = null;
	private File file = null;
	private String screenshot = null;
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH-mm-ss-SSS");
	protected String testStartTime = null;
	public static String currentProfileName;
	public static String currentProfileID;
	public static int profileLimit;
	public static String currentAccountRegion;
	public static int registeredProfilesCount;
	public static boolean isAdmin;
	public static String currentDevicelocale;
	public static String preferredlocale;
	protected ArrayList<Show> shows = null;
	protected Show show = null;
	protected ArrayList<Movie> movies = null, recommendations = null;
	protected HashMap<String, Movie> epgMovies;
	protected HashMap<String, Episode> epgShows;
	protected ArrayList<Show> show_recommendations = null;
	protected Movie movie = null;
	protected ArrayList<Episode> episodes = null;
	protected Episode episode = null;
	protected ArrayList<Channel> channels = null;
	protected Channel channel = null;
	protected ArrayList<Program> programs = null;
	protected Program program = null;
	protected ArrayList<Seasons> seasons = null;
	protected Seasons season = null;
	protected ArrayList<Assets> assets = null;
	protected Assets asset = null;
	public static ArrayList<String> purchasedSkuIds = null;
	public static ArrayList<String> freeSkuIds = null;
	public static ArrayList<String> purchasedOfferIds = null;
	public static ArrayList<String> freeOfferIds = null;
	public static ArrayList<String> subscriptionOfferIds = null;
	public static LinkedHashSet<Channel> allChannelsData = null;

	/**
	 * Method executes before running all the test cases/suite
	 * 
	 * @param deviceName
	 * @param carrier
	 * @param host
	 * @param buildVersion
	 * @throws IOException
	 */
	@BeforeSuite(alwaysRun = true)
	@Parameters({ "platformName", "deviceType", "deviceName", "carrier", "host", "buildVersion" })
	public void initializeTestEnvironment(@Optional("notSpecified") String platformName, @Optional("notSpecified") String deviceType, @Optional("notSpecified") String deviceName, @Optional("notSpecified") String carrier, @Optional("notSpecified") String host, @Optional("notSpecified") String buildVersion) throws IOException {
		ConfigurationConstants.platformName = platformName.equals("notSpecified") ? ConfigurationConstants.platformName : platformName;
		ConfigurationConstants.deviceType = deviceType.equals("notSpecified") ? ConfigurationConstants.deviceType : deviceType;
		switch (ConfigurationConstants.deviceType.toLowerCase()) {
		case "tv":
			ConfigurationConstants.device_Type = "stb";
			break;
		case "phone":
			ConfigurationConstants.device_Type = "phone-android";
			break;
		case "tablet":
			ConfigurationConstants.device_Type = "tablet-android";
		}
		ConfigurationConstants.deviceName = deviceName.equals("notSpecified") ? ConfigurationConstants.deviceName : deviceName;
		ConfigurationConstants.carrier = carrier.equals("notSpecified") ? ConfigurationConstants.carrier : carrier;
		ConfigurationConstants.host = host.equals("notSpecified") ? ConfigurationConstants.host : host;
		ConfigurationConstants.buildVersion = buildVersion.equals("notSpecified") ? ConfigurationConstants.buildVersion : buildVersion;
		// tReader = new TestConfigurationJsonReader(AutomationConstants.carrier,
		// AutomationConstants.host);//Using JSON
		tReader = new TestConfigurationPropertiesReader(ConfigurationConstants.carrier, ConfigurationConstants.host);// Using PropertiesFile
		ConfigurationConstants.product = tReader.getProduct();
		ConfigurationConstants.version = tReader.getVersion();
		ConfigurationConstants.extAssertionSource = tReader.getExtAssertionSource();
		ConfigurationConstants.oauthClientID = tReader.getOauthClientID();
		ConfigurationConstants.oauthClientSecret = tReader.getOauthClientSecret();
		ConfigurationConstants.nativeDeviceID = tReader.getNativeDeviceID();
		ConfigurationConstants.vid = tReader.getVid();
		ConfigurationConstants.userName = tReader.getUserName();
		ConfigurationConstants.password = tReader.getPassword();
		ConfigurationConstants.autherization = tReader.getAuthorization();
		ConfigurationConstants.oauthHost = tReader.getOauthHost();
		ConfigurationConstants.isRecordEnabled = tReader.isRecordEnabled();
		ConfigurationConstants.isAnonymousModeEnabled = tReader.isAnonymousModeEnabled();
		ConfigurationConstants.isCatchUpEnabled = tReader.isCatchUpEnabled();
		ConfigurationConstants.isOutOfHomeEnabled = tReader.isOutOfHomeEnabled();
		ConfigurationConstants.isChannelNumberEnabled = tReader.isChannelNumberEnabled();
		ConfigurationConstants.isUserInHome = isUserInHome(profilesJsonUtility.getAuthorization());
		extentReportFile = "./TestReport/Report.html";
		extentReports = new ExtentReports(extentReportFile, true).addSystemInfo("Host Name", "MobiTV Automation Machine").addSystemInfo("Environment", ConfigurationConstants.platformName).addSystemInfo("App Name", WordUtils.capitalizeFully(ConfigurationConstants.carrier)).addSystemInfo("App Version", ConfigurationConstants.buildVersion);
		extentReports.loadConfig(new File("./extent-config.xml"));
		utility.removeDirectoryFiles("./TestReport/Screenshots/");
		// utility.removeDirectoryFiles("./TestReport/Logs/");
		osName = System.getProperty("os.name").toLowerCase();
		if (osName.indexOf("win") >= 0)
			ConfigurationConstants.automationMachineOS = "win";
		else if (osName.indexOf("mac") >= 0)
			ConfigurationConstants.automationMachineOS = "mac";
		else if (osName.indexOf("nix") >= 0 || osName.indexOf("nux") >= 0 || osName.indexOf("aix") > 0)
			ConfigurationConstants.automationMachineOS = "ubu";
		systemCommander = new ExecuteSystemCommands(ConfigurationConstants.automationMachineOS);
		messageReader = new MessagesReader(ConfigurationConstants.carrier, ConfigurationConstants.host);
		currentProfileID = profilesJsonUtility.getProfileID(profilesJsonUtility.getAuthorization());
		currentAccountRegion = profilesJsonUtility.getAccountRegion();
		if (ConfigurationConstants.isRecordEnabled)
			recordingsJsonUtility = new RecordingsJSONUtility();
		profileLimit = profilesJsonUtility.getMaxProfilesRegistrationCount();
		allChannelsData = channelsJSONUtility.fetchAllChannels();
		purchasedSkuIds = purchaseJSONUtility.fetchPurchasedSkuId();
		freeSkuIds = offerJSONUtility.fetchFreeSkuIds();
		purchasedOfferIds = purchaseJSONUtility.fetchPurchasedOfferIds();
		freeOfferIds = offerJSONUtility.fetchFreeOfferIds();
		subscriptionOfferIds = offerJSONUtility.fetchSubscriptionOfferIds();
	}

	/**
	 * Method will execute once all the test cases in the suite are completed.
	 */
	@AfterSuite
	public void afterSuite() {
		extentReports.flush();
		extentReports.close();
	}

	/**
	 * This method will be called on finishing of test suite.
	 */
	@Override
	public void onFinish(ITestContext result) {}

	/**
	 * This method will be called on start of test suite.
	 */
	@Override
	public void onStart(ITestContext result) {}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {}

	/**
	 * This method will be called on Failure of a test case.
	 */
	@Override
	public void onTestFailure(ITestResult result) {
		try {
			captureScreenshot(screenshot = LocalTime.now().format(formatter));
		} catch (Exception e) {
			e.printStackTrace();
		}
		extentTest.log(LogStatus.FAIL, result.getMethod().getMethodName() + " Test Failed \n" + extentTest.addScreenCapture("./Screenshots/" + screenshot + ".JPEG") + "\n" + result.getThrowable() + "\n");
		extentReports.endTest(extentTest);
	}

	/**
	 * This method will be called on test case skip
	 */
	@Override
	public void onTestSkipped(ITestResult result) {
		extentTest.log(LogStatus.SKIP, result.getMethod().getMethodName() + " Test Skipped \n" + result.getThrowable() + "\n");
		extentReports.endTest(extentTest);
	}

	/**
	 * This method will be called at start of test case.
	 */
	@Override
	public void onTestStart(ITestResult result) {
		extentTest = extentReports.startTest(result.getMethod().getMethodName() + " Test");
		extentTest.log(LogStatus.INFO, "Test " + result.getInstanceName() + " " + result.getMethod().getMethodName() + " Started");
	}

	/**
	 * This method will be called once test case succeeded.
	 */
	@Override
	public void onTestSuccess(ITestResult result) {
		extentTest.log(LogStatus.PASS, result.getMethod().getMethodName() + "Test Succeeded");
		extentReports.endTest(extentTest);
	}

	/**
	 * Method to take screenshot.
	 * 
	 * @param fileName
	 * @return
	 */
	private boolean captureScreenshot(String fileName) {
		try {
			file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file, new File("./TestReport/Screenshots/" + fileName + ".JPEG"));
			return true;
		} catch (NullPointerException | IOException e) {
			return false;
		}
	}
	
	/**
	 * Method to check if user is in home.
	 */
	public boolean isUserInHome(String str_Authorization) {
		String profile_url = URLs.ProfileURL;
		try {
			URL url = new URL(profile_url);
			HttpURLConnection connection = mainJSONUtility.getSimpleConnection(profile_url, str_Authorization);
			connection.connect();
			JSONObject jsonObj = mainJSONUtility.getJSONFromConnection(connection);
			
			JSONArray jsnAry_ProfileList = jsonObj.getJSONArray("extended_property");
			for (int i = 0; i < jsnAry_ProfileList.length(); i++) {
				jsonObj = jsnAry_ProfileList.getJSONObject(i);
				if (jsonObj.getString("name").equals("home_indicator"))
					return jsonObj.getString("value").equalsIgnoreCase("yes") ? true : false;
			}
				return false;
		} catch (IOException e) {
			return false;
		}
	}
}
