package com.mobitv.client.qa.baseTests;

import java.util.ArrayList;
import java.util.HashSet;

import org.openqa.selenium.Dimension;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.constants.XPaths;
import com.mobitv.client.qa.models.Episode;
import com.mobitv.client.qa.models.Movie;
import com.mobitv.client.qa.models.Seasons;
import com.mobitv.client.qa.models.Show;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

/**
 * 
 * @author MobiTV
 *
 */
public class AndroidPhoneAndTabBaseTest extends AndroidBaseTest {
	/**
	 * This method will be useful for initializing Android Mobile specific test
	 * environment.
	 */
	@BeforeSuite
	public void initializeAndroidMobileTestEnvironment() {
		androidMobileMenuItems = dictionaryReader.getAndroidMobileMenuItems();
	}

	/**
	 * Method will execute once all the test cases in the suite are completed.
	 */
	@AfterSuite
	public void afterSuite() {
	}

	@BeforeMethod
	public void signInVerification() {
		try {
			if (!verifyApplicationLogin()) {
				signIn();
			}
			selectProfile(false);
		} catch (Exception e) {
		}
	}

	/**
	 * Method to verify whether the application is already logged in or not.
	 * 
	 * @return login status
	 */
	protected boolean verifyApplicationLogin() {
		if (!ConfigurationConstants.isAnonymousModeEnabled) {
			return !androidUtility.isElementPresentByID(androidDriver, XPaths.userName, true);
		} else {
			if (openNavigationDrawer())
				return !androidUtility.isElementPresentByID(androidDriver, XPaths.accountLayoutSignInButton, true);
			else
				return true;
		}
	}

	/**
	 * Method for signIn to to the application.
	 */
	protected void signIn() {
		if (ConfigurationConstants.isAnonymousModeEnabled)
			clickSignIn();
		super.signIn(ConfigurationConstants.userName, ConfigurationConstants.password);
	}

	/**
	 * Method to click on SignIn button when user is anonymous (anonymous mode
	 * enabled)
	 */
	protected void clickSignIn() {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.accountLayoutSignInButton));
		mElement.click();
	}

	/**
	 * Method to select a profile in the login screen, based on
	 * parameter(nonAdmin/Admin) nonAdmin = false -> select Admin profile.
	 * 
	 * @param nonAdmin
	 */
	protected void selectProfile(boolean nonAdmin) {
		registeredProfilesCount = profilesJsonUtility.getRegisteredProfilesCount();
		if (nonAdmin)
			if (!(registeredProfilesCount > 1))
				Assert.assertFalse(nonAdmin, "In current account there is only one profile registered");
		if (registeredProfilesCount > 1) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.profileSelectionTitle));
			Assert.assertTrue(mElement.isDisplayed(), "Profile menu title is not displayed");
			Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("who_is_watching_text"), "Profile menu title is not according to the specifications");
			profilesNames = profilesJsonUtility.getAllProfilesNames();
			mElements = androidDriver.findElements(MobileBy.id(XPaths.profileNameTextView));
			for (int i = 0; i < mElements.size(); i++) {
				mElement = mElements.get(i);
				profileName = profilesNames.get(i);
				Assert.assertEquals(mElement.getText(), profileName, "Profile name is not matching/Didn't loggedinto intended account");
			}
			if (nonAdmin) {
				mElement = mElements.get(1);
				BaseTest.currentProfileName = profilesNames.get(1);
			} else {
				mElement = mElements.get(0);
				BaseTest.currentProfileName = profilesNames.get(0);
			}
			BaseTest.isAdmin = !nonAdmin;
			mElement.click();
		}
		/*
		 * mElement = androidDriver.findElement(MobileBy.id(XPaths.profileAvtarView));
		 * mElement.click();
		 */
		waitForSomeTime(Constants.DRIVER_IMPLICIT_WAIT);
	}

	/**
	 * Method to open navigation drawer
	 * 
	 * @return status
	 */
	protected boolean openNavigationDrawer() {
		if (androidUtility.isElementPresentByAcsID(androidDriver, XPaths.navMenuAccessibilityId, true)) {
			mElement = androidDriver.findElement(MobileBy.AccessibilityId(XPaths.navMenuAccessibilityId));
			mElement.click();
			return androidUtility.isElementPresentByID(androidDriver, XPaths.accountLayout, true);
		}
		return false;
	}

	/**
	 * Method to switch profile. Based current profile this method will switch
	 * to other profile. If current profile is Admin -> switch to non Admin. If
	 * current profile is non Admin -> switch to Admin.
	 * 
	 * @return true on success, false on failure
	 */
	protected boolean switchProfile() {
		if (!(profilesJsonUtility.getRegisteredProfilesCount() > 1))
			return false;
		if (openNavigationDrawer()) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.accountLayoutSignInButton));
			mElement.click();
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.profileList, true)) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.profileList));
				mElements = mElement.findElements(MobileBy.id(XPaths.menuParent));
				if (isAdmin)
					mElement = mElements.get(1);
				else
					mElement = mElements.get(0);
				isAdmin = !isAdmin;
				mElement = mElement.findElement(MobileBy.id(XPaths.profileNameCell));
				mElement.click();
				waitForSomeTime(Constants.MEDIUM_SLEEP);
				return true;
			}
		}
		return false;
	}

	/**
	 * Method to select Manage Recordings in Android Mobile/Tablet app.
	 * 
	 * @return boolean
	 */
	protected boolean gotoManageRecordings() {
		if (ConfigurationConstants.isRecordEnabled)
			if (openNavigationDrawer()) {
				try {
					mElement = androidDriver.findElement(MobileBy.id(XPaths.menuList));
					mElements = mElement.findElements(MobileBy.id(XPaths.menuParent));
					for (int i = 0; i < mElements.size(); i++) {
						mElement = mElements.get(i);
						mElement = mElement.findElement(MobileBy.id(XPaths.cellEditScreen));
						if (mElement.getText().equals(dictionaryReader.getDictionaryValueForKey("manage_recordings_text"))) {
							mElement.click();
							break;
						}
					}
					return androidUtility.isElementPresentByID(androidDriver, XPaths.recordingViewPager, true);
				} catch (Exception e) {
					return false;
				}
			}
		return false;
	}

	/**
	 * Method to select Settings in Android Mobile/Tablet app.
	 * 
	 * @return boolean
	 */
	protected boolean gotoSettings() {
		if (openNavigationDrawer()) {
			try {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.menuList));
				mElements = mElement.findElements(MobileBy.id(XPaths.menuParent));
				for (int i = 0; i < mElements.size(); i++) {
					mElement = mElements.get(i);
					mElement = mElement.findElement(MobileBy.id(XPaths.cellEditScreen));
					if (mElement.getText().equals(dictionaryReader.getDictionaryValueForKey("settings_text"))) {
						mElement.click();
						break;
					}
				}
				return androidUtility.isElementPresentByID(androidDriver, XPaths.settingsPager, true);
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

	/**
	 * Method to select Featured Tab in Android Mobile/Tablet app.
	 * 
	 * @return boolean
	 */
	protected boolean selectFeaturedTab() {
		if (selectTab(androidMobileMenuItems.get(0)))
			return true;
		return false;
	}

	/**
	 * Method to select Live Tab in Android Mobile/Tablet app.
	 * 
	 * @return boolean
	 */
	protected boolean selectLiveTab() {
		if (selectTab(androidMobileMenuItems.get(1)))
			return true;
		return false;
	}

	/**
	 * Method to select Shows Tab in Android Mobile/Tablet app.
	 * 
	 * @return boolean
	 */
	protected boolean selectShowsTab() {
		if (selectTab(androidMobileMenuItems.get(2)))
			return true;
		return false;
	}

	/**
	 * Method to select Movies Tab in Android Mobile/Tablet app.
	 * 
	 * @return boolean
	 */
	protected boolean selectMoviesTab() {
		if (selectTab(androidMobileMenuItems.get(3)))
			return true;
		return false;
	}

	/**
	 * Method to select Search in Android Mobile/Tablet app.
	 * 
	 * @return boolean
	 */
	protected boolean gotoSearchScreen() {
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.actionSearch, true)) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.actionSearch));
			mElement.click();
			return androidUtility.isElementPresentByID(androidDriver, XPaths.searchSrcText, true);
		}
		return false;
	}

	/**
	 * Method to execute a search query.
	 * 
	 * @param searchQuery
	 */
	protected void executeASearchQuery(String searchQuery) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.search_plate));
		mElement.click();
		androidUtility.clearAndInputText(mElement, searchQuery);
		mobileEnter(1);
		// androidDriver.hideKeyboard();
		waitForSomeTime(Constants.SMALL_SLEEP);
	}

	/**
	 * Method to select Given tab MobileBy name in Android Mobile/Tablet app.
	 * 
	 * @param tabName
	 */
	private boolean selectTab(String tabName) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.tabs));
		if (mElement.isDisplayed()) {
			mElements = mElement.findElements(MobileBy.className("android.support.v7.app.ActionBar$Tab"));
			for (int i = 0; i < mElements.size(); i++) {
				mElement = mElements.get(i);
				mElement = mElement.findElement(MobileBy.className("android.widget.TextView"));
				if (mElement.getText().equalsIgnoreCase(tabName)) {
					mElement = mElements.get(i);
					mElement.click();
					waitForSomeTime(Constants.VERY_SMALL_SLEEP);
					mElement = androidDriver.findElement(MobileBy.id(XPaths.tabs));
					mElements = mElement.findElements(MobileBy.className("android.support.v7.app.ActionBar$Tab"));
					mElement = mElements.get(i);
					return androidUtility.getElementStateAttribute(mElement, "selected");
				}
			}
		}
		return false;
	}

	/**
	 * Method to verify show info in details page.
	 * 
	 * @param show
	 */
	protected void verifyShowInfo(Show show) {
		boolean hasPlayed = mainJSONUtility.hasRecentForRefId(BaseTest.currentProfileID, false, "series", show.getID());
		validateShowInfo(show, hasPlayed); // meta data
		validateShowInfoPlayButton(show, hasPlayed);
		// TODO validateShowRecordButton(show);
		if (androidUtility.isElementPresentByID(androidDriver, XPaths.info_module_see_more, true)) {
			System.out.println("see more");
			mElement = androidDriver.findElement(MobileBy.id(XPaths.info_module_see_more));
			Assert.assertTrue(mElement.getText().equalsIgnoreCase("See More"), "See More Button should show up.");
		}
		seasons = mainJSONUtility.getAllSeasons(show.getID());
		System.out.println(seasons.size());
		validateSeasonRow(seasons);
		validateEpisode(show, seasons);
	}

	/**
	 * Method to validate show info module data.
	 * 
	 * @param show,
	 *            hasPlayed
	 */
	protected void validateShowInfo(Show show, boolean hasPlayed) {
		if (!hasPlayed) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleTitle));
			uiName = mElement.getText();
			jsonName = show.getPrimaryMetaDataAccordingToLocalization();
			Assert.assertEquals(uiName, jsonName, "Show name didn't matched in in-line info module");
			if (show.getDescription() != null) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleDescription));
				uiDescription = mElement.getText();
				jsonDescription = show.getDescription();
				Assert.assertTrue(uiDescription.contains(jsonDescription), "Show description didn't matched");
			}
			mElement = androidDriver.findElement(MobileBy.id(XPaths.series_info_module_general_info));
			uiMetada = mElement.getText().replaceAll("[^a-zA-Z0-9]", "");
			jsonMetada = show.getInlineInfoSecondaryMetaDataAccordingToLocalization().replaceAll("[^a-zA-Z0-9]", "");
			Assert.assertTrue(uiMetada.contains(jsonMetada), "Show meta data didn't matched"); // has bugs on app not code
			if (show.getActors_list() != null) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleCrew));
				uiActors = mElement.getText();
				jsonActors = show.getActorsList();
				Assert.assertEquals(uiActors, jsonActors, "Show actors list didn't matched");
			}
		} // else TODO assert for recently played episode info
	}

	/**
	 * Method to validate show detail page play btn.
	 * 
	 * @param show,
	 *            hasPlayed
	 */
	protected void validateShowInfoPlayButton(Show show, boolean hasPlayed) {
		System.out.println("test play button " + hasPlayed);
		if (hasPlayed) {
			System.out.println("test play button PLAY");
			Assert.assertTrue(androidUtility.isElementPresentByID(androidDriver, XPaths.watchBtn, false), "Play button should display");
		} else {
			System.out.println("test play button NO PLAY BUTTON");
			Assert.assertFalse(androidUtility.isElementPresentByID(androidDriver, XPaths.watchBtn, false), "Play button should not display");
		}
	}

	/**
	 * Method to validate show detail page see more.
	 * 
	 * @param hasPlayed
	 */
	protected void validateSeriesDetailPageSeeMore(boolean hasPlayed) {
		if (!hasPlayed) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.contentTitleTxt));
			uiName = mElement.getText();
			jsonName = show.getPrimaryMetaDataAccordingToLocalization();
			Assert.assertEquals(uiName, jsonName, "Show name didn't matched in in-line info module");
			if (show.getDescription() != null) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.contentDescriptionTxt));
				uiDescription = mElement.getText();
				jsonDescription = show.getDescription();
				Assert.assertEquals(uiDescription, jsonDescription, "Show description didn't matched");
			}
			mElement = androidDriver.findElement(MobileBy.id(XPaths.contentCategoryInfoTxt));
			uiMetada = mElement.getText().replaceAll("[^a-zA-Z0-9]", "");
			jsonMetada = show.getMoreInfoSecondaryMetaDataAccordingToLocalization().replaceAll("[^a-zA-Z0-9]", "");
			// Assert.assertEquals(uiMetada, jsonMetada, "Show meta data didn't
			// matched");//Meta data is giving special characters.
			if (show.getActors_list() != null) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.contentCastTxt));
				uiActors = mElement.getText();
				jsonActors = show.getActorsList();
				Assert.assertEquals(uiActors, jsonActors, "Show actors list didn't matched");
			}
		} else {
			// validate episode TODO
		}
	}

	/**
	 * Method to validate seasons row
	 * 
	 * Includes: 1) validate all seasons are displayed correctly (covered no
	 * season/only one season/ and multiple seasons)
	 * 
	 * @param seasons
	 */
	protected void validateSeasonRow(ArrayList<Seasons> seasons) {
		int size = seasons.size();
		if (size == 0) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.seasonItemTitle));
			Assert.assertEquals("All Episodes", mElement.getText());
		}
		if (size == 1) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.seasonItemTitle));
			Assert.assertEquals("Season " + seasons.get(0).getName(), mElement.getText());
		}
		if (size > 1) {
			mElements = androidDriver.findElements(MobileBy.id(XPaths.seasonItemTitle));
			for (int i = 0; i < size && i < mElements.size(); i++) {
				Assert.assertEquals("Season " + seasons.get(size - 1 - i).getName(), mElements.get(i).getText());
			}
		}
	}

	/**
	 * Method to validate episode (in-line and detail page)
	 * 
	 * Includes: 1) validate play and record button for episode 2) for record
	 * button( only assert the existence, TODO checking manage recording/
	 * record) 3) click series info button under inline info module and go to
	 * detail page 4) verify play and record button and metadata on detail page
	 * 5) verify series info button and its related overlay on episode detail
	 * page 6) verify seasons row on episode detail page 7) check 1-6 for both
	 * upcoming and playable content(different row)
	 * 
	 * @param seasons
	 * @param show
	 */
	protected void validateEpisode(Show show, ArrayList<Seasons> seasons) {
		ArrayList<Episode> playableEpisodes = null;
		ArrayList<Episode> upcomingEpisodes = null;
		AndroidTVBaseTest tvTest = new AndroidTVBaseTest();
		if (seasons.size() == 0) {
			playableEpisodes = mainJSONUtility.getAllPlayableEpisodes(show.getID(), "");
			upcomingEpisodes = mainJSONUtility.getAllUpcomingEpisodes(show.getID(), "");
		} else {
			playableEpisodes = mainJSONUtility.getAllPlayableEpisodes(show.getID(), Integer.toString(seasons.get(seasons.size() - 1).getName()));
			upcomingEpisodes = mainJSONUtility.getAllUpcomingEpisodes(show.getID(), Integer.toString(seasons.get(seasons.size() - 1).getName()));
		}
		HashSet<String> playableSet = new HashSet<String>();
		HashSet<String> recordableSet = new HashSet<String>();
		if (playableEpisodes.size() > 0) {
			for (int i = 0; i < playableEpisodes.size(); i++) {
				playableSet.add(playableEpisodes.get(i).getID());
				if (playableEpisodes.get(i).getType().equalsIgnoreCase("shared_ref")) {
					if (tvTest.isEpisodeLiveRecordingEnabled(playableEpisodes.get(i)))
						recordableSet.add(playableEpisodes.get(i).getID());
				} else if (playableEpisodes.get(i).getType().equalsIgnoreCase("program")) {
					if (playableEpisodes.get(i).is_recording_enabled())
						recordableSet.add(playableEpisodes.get(i).getID());
				}
			}
		}
		if (upcomingEpisodes.size() > 0) {
			for (int i = 0; i < upcomingEpisodes.size(); i++) {
				if (upcomingEpisodes.get(i).getType().equalsIgnoreCase("shared_ref")) {
					if (tvTest.isEpisodeUpcomingRecordingenabled(upcomingEpisodes.get(i)))
						recordableSet.add(upcomingEpisodes.get(i).getID());
				} else if (upcomingEpisodes.get(i).getType().equalsIgnoreCase("program")) {
					if (upcomingEpisodes.get(i).is_recording_enabled())
						recordableSet.add(upcomingEpisodes.get(i).getID());
				}
			}
		}
		if (playableEpisodes.size() > 0) {
			System.out.println("TEST PLAYABLE");
			Episode firstPlayable = playableEpisodes.get(0);
			mElement = androidDriver.findElement(MobileBy.id(XPaths.episodePrimaryMetadata));
			uiName = mElement.getText();
			jsonName = firstPlayable.getEpisode_number() == 0 ? firstPlayable.getName() : firstPlayable.getEpisode_number() + ". " + firstPlayable.getName();
			System.out.println(jsonName);
			System.out.println(uiName);
			// Assert.assertEquals(jsonName, uiName, "Episode Name is not showing
			// correctly");
			mElement.click();
			validateEpisodeInfo(firstPlayable); // check detail page
			validatePlayableEpisodeButton(firstPlayable, recordableSet);// check detail page
			validateSeasonRow(seasons);// validate season row on episode detail page
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.info_module_see_more, true)) {
				System.out.println("see more");
				mElement = androidDriver.findElement(MobileBy.id(XPaths.info_module_see_more));
				Assert.assertEquals(mElement.getText(), "See More", "See More Button should show up.");
			}
			clickAppBackBtn();
		}
		if (upcomingEpisodes.size() > 0) {
			scrollToText("Upcoming", XPaths.details_recycler_view);
			String xpathID = ConfigurationConstants.deviceType.equalsIgnoreCase("phone") ? "cell_txt_title" : "module_title";
			//Assert.assertTrue(androidUtility.isElementPresentByID(androidDriver, xpathID, true));
			System.out.println("TEST UPCOMING");
			/*
			 * //Comment out due to TODO scrolltotext Episode firstUpcoming =
			 * upcomingEpisodes.get(0); mElement =
			 * androidDriver.findElement(MobileBy.id(XPaths.episodePrimaryMetadata)); uiName
			 * = mElement.getText(); jsonName = firstUpcoming.getEpisode_number() + ". "+
			 * firstUpcoming.getName(); scrollToText(jsonName,
			 * XPaths.details_recycler_view); System.out.println(jsonName);
			 * System.out.println(uiName); //Assert.assertEquals(jsonName, uiName,
			 * "Episode Name is not showing correctly"); mElement.click();
			 * 
			 * validateEpisodeInfo(firstUpcoming); // check detail page
			 * validateUpcomingEpisodeButton(firstUpcoming, playableSet, recordableSet);//
			 * check detail page validateSeasonRow(seasons);// validate season row on
			 * episode detail page if(androidUtility.isElementPresentByID(androidDriver,
			 * XPaths.info_module_see_more, true)) { System.out.println("see more");
			 * mElement =
			 * androidDriver.findElement(MobileBy.id(XPaths.info_module_see_more));
			 * Assert.assertEquals(mElement.getText(), "See More",
			 * "See More Button should show up." ); } clickAppBackBtn();
			 */
		}
	}

	/**
	 * Method to validate episode detail page info.
	 * 
	 * @param episode
	 */
	protected void validateEpisodeInfo(Episode episode) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleTitle));
		String uiShowName = mElement.getText();
		String jsonShowName = episode.getPrimaryMetaDataAccordingToLocalization();
		Assert.assertEquals(uiShowName, jsonShowName, "Show name didn't matched on detail page");
		if (episode.getDescription() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleDescription));
			uiDescription = mElement.getText();
			jsonDescription = episode.getDescription();
			if (episode.getOriginal_air_date() != null) {
				jsonMetada = dAndt.getDatefromEpoch(episode.getOriginal_air_date());
			}
			jsonDescription = jsonMetada + " " + jsonDescription;
		}
		// Assert.assertEquals(uiDescription, jsonDescription, "Episode description
		// didn't matched");
		if (episode.getMetaDataAccordingToLocalization() != null) {// genre
			mElement = androidDriver.findElement(MobileBy.id(XPaths.episodeMetaData));
			uiMetada = mElement.getText().replaceAll("[^a-zA-Z0-9]", "");
			jsonMetada = episode.getMetaDataAccordingToLocalization().replaceAll("[^a-zA-Z0-9]", "");
			System.out.println("UI " + uiMetada);
			System.out.println("JSON " + jsonMetada);
			// Assert.assertTrue(uiMetada.contains(jsonMetada), "Episode meta data didn't
			// matched");//Meta data not showing genre sometimes.
		}
		if (episode.getFormattedEpisodeName() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.episodeTitle));
			uiShowName = mElement.getText();
			jsonShowName = episode.getFormattedEpisodeName();
			Assert.assertEquals(uiShowName, jsonShowName, "Formatted episode Name didn't matched");
		}
	}

	/**
	 * Method to validate playable episode button row (diff from upcoming : play
	 * button should display for all playable)
	 * 
	 */
	protected void validatePlayableEpisodeButton(Episode episode, HashSet<String> recordableSet) {
		if (recordableSet.contains(episode.getID())) {
			Assert.assertTrue(androidUtility.isElementPresentByID(androidDriver, XPaths.recordBtn, true));// only assert for existence
		}
		Assert.assertTrue(androidUtility.isElementPresentByID(androidDriver, XPaths.watchBtn, true));// only assert for existence
	}

	/**
	 * Method to validate upcoming episode button row
	 * 
	 */
	protected void validateUpcomingEpisodeButton(Episode episode, HashSet<String> playableSet, HashSet<String> recordableSet) {
		if (recordableSet.contains(episode.getID())) {
			Assert.assertTrue(androidUtility.isElementPresentByID(androidDriver, XPaths.recordBtn, true));// only assert for existence
		}
		if (playableSet.contains(episode.getID())) {
			Assert.assertTrue(androidUtility.isElementPresentByID(androidDriver, XPaths.watchBtn, true));// only assert for existence
		}
	}

	/**
	 * Method to validate episode detail page see more.
	 * 
	 * @param episode
	 */
	protected void validateEpisodeDetailPageSeeMore(Episode episode) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.contentTitleTxt));
		uiName = mElement.getText();
		jsonName = episode.getPrimaryMetaDataAccordingToLocalization();
		Assert.assertEquals(uiName, jsonName, "Show name didn't matched in in-line info module");
		if (episode.getDescription() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.contentDescriptionTxt));
			uiDescription = mElement.getText();
			jsonDescription = episode.getDescription();
			Assert.assertEquals(uiDescription, jsonDescription, "Show description didn't matched");
		}
		mElement = androidDriver.findElement(MobileBy.id(XPaths.contentCategoryInfoTxt));
		uiMetada = mElement.getText().replaceAll("[^a-zA-Z0-9]", "");
		jsonMetada = episode.getMetaDataAccordingToLocalization().replaceAll("[^a-zA-Z0-9]", "");
		// Assert.assertEquals(uiMetada, jsonMetada, "Show meta data didn't
		// matched");//Meta data is giving special characters.
	}

	/**
	 * Method to verify movie info in details page.
	 * 
	 * @param movie
	 */
	protected void verifyMovieInfo(Movie movie) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.movie_title));
		uiName = mElement.getText();
		jsonName = movie.getPrimaryMetaDataAccordingToLocalization();
		Assert.assertEquals(uiName, jsonName, "Movie name didn't matched in in-line info module");
		System.out.println(movie.getDescription());
		if (movie.getDescription() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleDescription));
			uiDescription = mElement.getText();
			jsonDescription = movie.getDescription();
			Assert.assertEquals(uiDescription, jsonDescription, "Movie description didn't matched");
		}
		mElement = androidDriver.findElement(MobileBy.id(XPaths.movie_metadata));
		uiMetada = mElement.getText().replaceAll("�", " . ");
		jsonMetada = movie.getMoreInfoSecondaryMetaDataAccordingToLocalization();
		Assert.assertEquals(uiMetada, jsonMetada, "Meta Data didn't match");
		System.out.println(jsonData);
		// Assert.assertEquals(uiMetada, jsonMetada, "Movie meta data didn't
		// matched");//Meta data is giving special characters.
		if (movie.getActors_list() != null) {
			mElement = androidDriver.findElement(MobileBy.id(XPaths.infoModuleCrew));
			uiActors = mElement.getText();
			jsonActors = movie.getActorsList();
			Assert.assertEquals(uiActors, jsonActors, "Movie actors list didn't matched");
		}
	}

	/**
	 * Method to verify movie Recommendations in it's details page.
	 * 
	 * @param movie
	 * 
	 */
	protected void verifyMovieRecommendations(Movie movie) {
		mElement = androidDriver.findElement(MobileBy.id(XPaths.module_title));
		uiName = mElement.getText();
		Assert.assertTrue(uiName.toLowerCase().contains("you might also like"), "YML section not displayed");
		scrollToText(movie.getName(), XPaths.details_recycler_view);
	}

	/**
	 * Method to scroll to particular text
	 * 
	 * @param searchTxt
	 * @param scrollableList
	 */
	public void scrollToText(String searchTxt, String scrollableList) {
		
			androidDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().resourceId(\"" + ConfigurationConstants.appPackage + ":id/" + scrollableList + "\")).getChildByText(" + "new UiSelector().className(\"android.widget.TextView\"), \"" + searchTxt + "\")");
			waitForSomeTime(Constants.MEDIUM_SLEEP);
		
	}

	public static void scrollDown() {
		Dimension dimensions = androidDriver.manage().window().getSize();
		Double screenHeightStart = dimensions.getHeight() * 0.5;
		int scrollStart = screenHeightStart.intValue();
		Double screenHeightEnd = dimensions.getHeight() * 0.2;
		int scrollEnd = screenHeightEnd.intValue();
		androidDriver.swipe(0, scrollStart, 0, scrollEnd, 2000);
	}

	public static void scrollUp() {
		Dimension dimensions = androidDriver.manage().window().getSize();
		Double screenHeightStart = dimensions.getHeight() * 0.5;
		int scrollStart = screenHeightStart.intValue();
		Double screenHeightEnd = dimensions.getHeight() * 0.2;
		int scrollEnd = screenHeightEnd.intValue();
		androidDriver.swipe(0, scrollEnd, 0, scrollStart, 2000);
	}

	/**
	 * method to swipe left on mobile screen
	 */
	public static void swipeLeft() {
		Dimension size = driver.manage().window().getSize();
		int startx = (int) (size.width * 0.80);
		int endx = (int) (size.width * 0.20);
		int starty = size.height / 2;
		androidDriver.swipe(startx, starty, endx, starty, 2000);
	}

	/**
	 * method to swipe right on mobile screen This is basic method. To enhance
	 * this method , like swipe from left edge to right , change startx to 0,
	 * etc
	 */
	public static void swipeRight() {
		Dimension size = driver.manage().window().getSize();
		int startx = (int) (size.width * 0.20);
		int endx = (int) (size.width * 0.80);
		int starty = size.height / 2;
		androidDriver.swipe(startx, starty, endx, starty, 2000);
	}

	/**
	 * Method to do signOut from the application.
	 * 
	 * @return boolean
	 */
	protected boolean signOut() {
		if (openNavigationDrawer()) {
			if (androidUtility.isElementPresentByID(androidDriver, XPaths.switchUserButton, true)) {
				mElement = androidDriver.findElement(MobileBy.id(XPaths.switchUserButton));
				if (mElement.getText().equals(dictionaryReader.getDictionaryValueForKey("sign_out_button_title"))) {
					mElement.click();
					if (androidUtility.isElementPresentByID(androidDriver, XPaths.button1, true)) {
						mElement = androidDriver.findElement(MobileBy.id(XPaths.button1));
						mElement.click();
						waitForSomeTime(Constants.DRIVER_IMPLICIT_WAIT);
						return true;
					}
				}
			}
		}
		return false;
	}

	protected void clickAppBackBtn() {
		MobileElement backElement = androidDriver.findElement(MobileBy.id(XPaths.navMenu));
		backElement.click();
	}
}
