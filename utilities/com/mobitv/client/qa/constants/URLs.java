package com.mobitv.client.qa.constants;
import com.mobitv.client.qa.baseTests.BaseTest;
/**
 * @author MobiTV
 */
import com.mobitv.client.qa.functions.DateAndTime;
public class URLs {
	private static DateAndTime dAndt = new DateAndTime();
	private static String vid = ConfigurationConstants.vid;
	private static String carrier = ConfigurationConstants.carrier;
	private static String product = ConfigurationConstants.product;
	private static String version = ConfigurationConstants.version;
	public static String oauthHost = ConfigurationConstants.oauthHost;
	private static String deviceType = ConfigurationConstants.device_Type;
	private static String appVersion = ConfigurationConstants.buildVersion;
	private static String drmType = ConfigurationConstants.drm_Type;
	private static String region = BaseTest.currentAccountRegion;
	private static String profileId = BaseTest.currentProfileID;
	// URLs related to EPG
	public static String searchEPGLiveMovieURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/details.json?ref_types=program&sort_by=linked_channel_number&sort_direction=asc&offset=0&length=30&start_time=%s&category=movies&timeslice=1800&regions=national,%s", vid, carrier, product, version, dAndt.getCurrentEpochTime(), "%s");
	public static String searchEPGLiveMovieURLForPast24Hrs = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/details.json?ref_types=program&sort_by=linked_channel_number&sort_direction=asc&offset=0&length=30&start_time=%s&category=movies&timeslice=1800&regions=national,%s", vid, carrier, product, version, dAndt.getCurrentEpochTime() - 86400, "%s");
	public static String searchEPGLiveShowURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/details.json?ref_types=program&sort_by=linked_channel_number&sort_direction=asc&offset=0&length=30&start_time=%s&category=tv&timeslice=1800&regions=national,%s", vid, carrier, product, version, dAndt.getCurrentEpochTime(), "%s");
	public static String searchNewsURL = "https://" + vid + "/guide/v5.1/search/" + carrier + "/" + product + "/" + version + "/search/details.json?ref_types=program&sort_by=linked_channel_number&sort_direction=asc&offset=0&length=30&start_time=%s&genre_list=news&category=tv,news&timeslice=3600&regions=national,%s";
	public static String searchEPGLiveAndUpcomingShowURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/details.json?ref_types=program&sort_by=linked_channel_number&sort_direction=asc&offset=0&length=100&start_time=%s&category=tv&timeslice=5400&regions=national,%s", vid, carrier, product, version, dAndt.getCurrentEpochTime(), "%s");
	// URLs related to movies
	public static String AllMoviesURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?category=movies&drm_rights=play::%s&facets=child_protection_rating,genre_list&include_suspended=false&ref_types=program,vod&sort_by=year,availability_time&sort_direction=desc,desc&timeslice=604800&start_time=%s&regions=national,", vid, carrier, product, version, drmType, dAndt
		.getOneWeekBeforeEpochTime());
	public static String AllUpcomingMoviesURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?category=movies&drm_rights=play::%s&facets=child_protection_rating,genre_list&include_suspended=false&ref_types=program,vod&sort_by=year,availability_time&sort_direction=desc,desc&timeslice=18000&start_time=%s&regions=national,", vid, carrier, product, version, drmType,
		dAndt.getCurrentEpochTime());
	public static String moviesYMLURL = String.format("https://%s/guide/v5/recommendations/%s/%s/%s/default/content/recommendation.json?ref_type=vod&ref_id=#ref_id&recommendation_type=movie&length=15&regions=national,", vid, carrier, product, version);
	public static String inventoriesURL = String.format("https://%s/guide/v5/lineup/%s/%s/%s/default/ondemand/inventories.json?inventory_ids=", vid, carrier, product, version);
	public static String movieDetailsURL = String.format("https://%s/guide/v5.1/lineup/%s/%s/%s/shared_ref/details.json?regions=national,%s&shared_ref_ids=", vid, carrier, product, version, "%s");
	// URLs related to shows
	public static String AllShowsURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=series&sort_by=original_air_date,start_of_availability&sort_direction=desc,desc&offset=0&regions=national,", vid, carrier, product, version);
	public static String episodeURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=vod,program&sort_by=episode_number,original_air_date,availability_time&sort_direction=desc,desc,desc&offset=0&length=200&start_time=%s&drm_rights=play::%s&series_id=#series_id&timeslice=604800&season_number=#season_number&regions=national,%s", vid, carrier, product,
		version, dAndt.getOneWeekBeforeEpochTime(), drmType, "%s");
	public static String episodePlayableURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=vod,program&sort_by=season_number,original_air_date,availability_time&sort_direction=desc,desc,desc&offset=0&length=200&start_time=%s&drm_rights=play::%s&series_id=#series_id&timeslice=604800&season_number=#season_number&regions=national,%s", vid, carrier,
		product, version, dAndt.getOneWeekBeforeEpochTime(), drmType, "%s");
	public static String episodeUpcomingURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=program&sort_by=episode_number,original_air_date,availability_time&sort_direction=desc,desc,desc&offset=0&length=200&start_time=%s&drm_rights=play::%s&series_id=#series_id&timeslice=604800&season_number=#season_number&regions=national,%s", vid, carrier, product,
		version, dAndt.getCurrentEpochTime(), drmType, "%s");
	public static String episodeURLWithSeriesId = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=vod,program&sort_by=episode_number,original_air_date,availability_time&sort_direction=desc,desc,desc&offset=0&length=200&start_time=%s&drm_rights=play::%s&series_id=#series_id&timeslice=604800&regions=national,%s", vid, carrier, product, version, dAndt
		.getCurrentEpochTime(), drmType, "%s");
	public static String seasonsURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search.json?ref_types=program,vod&offset=0&length=0&start_time=%s&facets=season_number&series_id=#seriesId&timeslice=1209600&regions=national,%s", vid, carrier, product, version, dAndt.getOneWeekBeforeEpochTime(), "%s");
	public static String ymlrelatedSeriesURL = String.format("https://%s/guide/v5/lineup/%s/%s/%s/default/ondemand/series.json?series_ids=", vid, carrier, product, version);
	public static String seriesYMLURL = String.format("https://%s/guide/v5/recommendations/%s/%s/%s/default/content/recommendation.json?ref_type=series&ref_id=#ref_id&recommendation_type=series&length=15", vid, carrier, product, version);
	public static String episodeDetailsURL = String.format("https://%s/guide/v5.1/lineup/%s/%s/%s/shared_ref/details.json?regions=national,jackson&shared_ref_ids=", vid, carrier, product, version);
	public static String seriesYMLURLv1 = String.format("https://%s/guide/v5/recommendations/%s/%s/%s/default/profile/%s/recommendation.json?recommendation_type=series&offset=0&length=50&regions=national,%s", vid, carrier, product, version, "%s", "%s");
	// URLs related to Mobile shows
	public static String MobileShowsURL = String.format("https://%S/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=series&sort_by=latest_start_of_availability,name&sort_direction=desc,asc&offset=0&regions=national,", vid, carrier, product, version);
	// URLs related to Mobile movies
	public static String MobileMoviesURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=vod,program&sort_by=year&sort_direction=desc&offset=0&timeslice=604800&start_time=%s&category=movies&regions=national,", vid, carrier, product, version, dAndt.getOneWeekBeforeEpochTime());
	public static String MobileUpcomingMoviesURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=vod,program&sort_by=year&sort_direction=desc&offset=0&timeslice=604800&start_time=%s&category=movies&regions=national,", vid, carrier, product, version, dAndt.getCurrentEpochTime());
	public static String MobileMoviesUpcomingAndLiveURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=vod,program&sort_by=year&sort_direction=desc&offset=0&timeslice=1036800&start_time=%s&category=movies&regions=national,", vid, carrier, product, version, dAndt.getCurrentEpochTime() - 7200);
	// URLs related to search
	public static String SearchResultsShowsURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=series&sort_by=year,availability_time&sort_direction=desc,desc&offset=0&drm_rights=play::%s&regions=national,", vid, carrier, product, version, drmType);
	public static String SearchResultsMoviesURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=vod,program&sort_by=year,availability_time&sort_direction=desc,desc&offset=0&timeslice=604800&start_time=%s&drm_rights=play::%s&category=movies&regions=national,", vid, carrier, product, version, dAndt.getOneWeekBeforeEpochTime(), drmType);
	public static String SearchResultsEpisodesURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?ref_types=vod,program&sort_by=year,availability_time&sort_direction=desc,desc&offset=0&timeslice=604800&start_time=%s&drm_rights=play::%s&category=tv,sports,news,music&regions=national,", vid, carrier, product, version, dAndt.getOneWeekBeforeEpochTime(), drmType);
	public static String SearchResultsChannelsURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/shared/details.json?&ref_types=channel&sort_by=year,availability_time&sort_direction=desc,desc&offset=0&drm_rights=play::%s&regions=national,", vid, carrier, product, version, drmType);
	public static String SearchResultsUpcomingURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/details.json?&ref_types=program&sort_by=year,availability_time&sort_direction=desc,desc&offset=0&timeslice=604800&drm_rights=play::%s&start_time=%s&regions=national,", vid, carrier, product, version, drmType, dAndt.getCurrentEpochTime());
	public static String SearchResultsUpcomingURLMovie = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/details.json?&ref_types=vod,program&sort_by=year,availability_time&sort_direction=desc,desc&offset=0&timeslice=604800&drm_rights=play::%s&start_time=%s&regions=national,", vid, carrier, product, version, drmType, dAndt.getCurrentEpochTime());
	// URLs related to TV Network child page
	public static String NetworkChildFeatureURL = String.format("https://" + vid + "/aggregator/v5/managedlist/" + carrier + "/" + product + "/" + version + "/tile-list-v2/featured-network-screen.json?drm_rights=play::%s&config_params=%s,&regions=national,", drmType, (!(ConfigurationConstants.autherization == null)) ? ConfigurationConstants.autherization.substring(6) : " ");
	public static String MoviesNetworkFeatureURL = String.format("https://" + vid + "/aggregator/v5/managedlist/" + carrier + "/" + product + "/" + version + "/tile-list-v2/featured-movies-screen.json?drm_rights=play::stb-other&config_params=%s,&regions=national,", (!(ConfigurationConstants.autherization == null)) ? ConfigurationConstants.autherization.substring(6) : " ");
	public static String SeriesNetworkFeatureURL = String.format("https://" + vid + "/aggregator/v5/managedlist/" + carrier + "/" + product + "/" + version + "/tile-list-v2/featured-series-screen.json?drm_rights=play::stb-other&config_params=%s,&regions=national,", (!(ConfigurationConstants.autherization == null)) ? ConfigurationConstants.autherization.substring(6) : " ");
	public static String AllFeaturedNetworkURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search.json?ref_types=vod,program&offset=0&length=100&facets=provider_networks&regions=national,%s&category=tv,sports,news,music", vid, carrier, product, version, region);
	public static String MobileFeatureURL = String.format("https://" + vid + "/aggregator/v5/managedlist/" + carrier + "/" + product + "/" + version + "/tile-list-v2/featured-screen.json?child_protection_rating=%s&drm_rights=play::%s&config_params=%s,&regions=national,", "%s", drmType, (!(ConfigurationConstants.autherization == null)) ? ConfigurationConstants.autherization.substring(6) : " ");

	// URLs related to TV Network child page
		public static String NetworkChildFeatureURL_pop = String.format("https://" + vid + "/aggregator/v5/managedlist/" + carrier + "/" + product + "/" + version + "/tile-list-v2/featured-network-screen.json?drm_rights=play::%s&timezone=%s&config_params=%s,&regions=national,", deviceType, dAndt.getCurrentTimeZoneOffsetFromGMT(), (!(ConfigurationConstants.autherization == null)) ? ConfigurationConstants.autherization.substring(6) : " ");
		public static String MoviesFeatureURL = String.format("https://" + vid + "/aggregator/v5/managedlist/" + carrier + "/" + product + "/" + version + "/tile-list-v2/featured-movies-screen-v2.json?drm_rights=play::%s&timezone=%s&config_params=%s,&regions=national,", deviceType, dAndt.getCurrentTimeZoneOffsetFromGMT(), (!(ConfigurationConstants.autherization == null)) ? ConfigurationConstants.autherization.substring(6) : " ");
		public static String SeriesFeatureURL = String.format("https://" + vid + "/aggregator/v5/managedlist/" + carrier + "/" + product + "/" + version + "/tile-list-v2/featured-series-screen-v2.json?drm_rights=play::%s&timezone=%s&config_params=%s,&regions=national,", deviceType, dAndt.getCurrentTimeZoneOffsetFromGMT(), (!(ConfigurationConstants.autherization == null)) ? ConfigurationConstants.autherization.substring(6) : " ");
		public static String AllFeaturedNetworkURL_pop = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search.json?ref_types=vod,program&offset=0&length=100&facets=provider_networks&regions=national,%s&category=tv,sports,news,music", vid, carrier, product, version, region);
	// URLs related to category filters
	public static String CategoryFilterSportsOnNowURL = "https://" + vid + "/guide/v5.1/search/" + carrier + "/" + product + "/" + version + "/search/details.json?ref_types=program&sort_by=start_time&sort_direction=asc&offset=0&length=%s&timeslice=14400&start_time=%s&genre_list=sports&offer_ids=%s&regions=national,%s";
	public static String CategoryFilterNewsOnNowURL = "https://" + vid + "/guide/v5.1/search/" + carrier + "/" + product + "/" + version + "/search/details.json?ref_types=program&sort_by=start_time&sort_direction=asc&offset=0&length=%s&timeslice=14400&start_time=%s&genre_list=news&offer_ids=%s&regions=national,%s";
	public static String CategoryFilterMoviesOnNowURL = "https://" + vid + "/guide/v5.1/search/" + carrier + "/" + product + "/" + version + "/search/details.json?ref_types=program&sort_by=start_time&sort_direction=asc&offset=0&length=%s&timeslice=14400&start_time=%s&category=movies&offer_ids=%s&regions=national,%s";
	public static String CategoryFilterKidsOnNowURL = "https://" + vid + "/guide/v5.1/search/" + carrier + "/" + product + "/" + version + "/search/details.json?ref_types=program&sort_by=start_time&sort_direction=asc&offset=0&length=%s&timeslice=14400&start_time=%s&genre_list=kids&family&offer_ids=%s&regions=national,%s";
	// URLs related to channels
	public static String AllChannelsURL = "https://" + vid + "/guide/v5.1/search/" + carrier + "/" + product + "/" + version + "/search/details.json?ref_types=channel&sort_by=channel_number&sort_direction=asc&regions=national,%s&offset=%s";
	public static String ChannelProgramsURL = String.format("https://%s/guide/v5/program/%s/%s/%s/default/programs.json?start-time=%s&timeslice=18000&channels=", vid, carrier, product, version, "%s");
	public static String ChannelProgramsURLForLast24Hrs = String.format("https://%s/guide/v5/program/%s/%s/%s/default/programs.json?start-time=%s&timeslice=86400&sort_by=start_time&sort_direction=desc&channels=", vid, carrier, product, version, dAndt.getCurrentEpochTime() - 86400);
	// URLs related to authorization
	public static String TokenURLForArvig = "https://my.arvig.com/oauth/token";
	public static String ArvigIntegrationOmniaIDURL = "https://my.arvig.com/api/mobi_id?access_token=";
	public static String TokensURL = String.format("https://%s/identity/v5/oauth2/%s/%s/%s/tokens.json", vid, carrier, product, version);
	public static String TokenURL = String.format("https://%s/identity/v5/idam/%s/%s/%s/tokens.json", vid, carrier, product, version);
	public static String BillingIDURL = String.format("https://%s/identity/v5/idam/%s/%s/%s/token/validate.json?token=", vid, carrier, product, version);
	public static String CSpireIntegrationOmniaIDURL = String.format("https://%s/%s/tokeninfo/user?access_token=", oauthHost, carrier);
	public static String CSpireOmniaIDURL = String.format("https://%s/oauth2/v1/tokeninfo/user?access_token=", oauthHost);
	public static String CSpireIntegrationTokenURL = String.format("https://%s/%s/token", oauthHost, carrier);
	public static String CSpireTokenURL = String.format("https://%s/oauth2/v1/token", oauthHost);
	// URLs related to profiles
	public static String ProfileURL = String.format("https://%s/core/v5/session/%s/%s/%s/current/profile.json", vid, carrier, product, version);
	public static String ProfilesURL = String.format("https://%s/core/v5/accounts/%s/%s/%s/", vid, carrier, product, version);
	public static String PurchasesURL = String.format("https://%s/core/v5/purchase/%s/%s/%s/", vid, carrier, product, version);
	public static String SwitchTokenURL = String.format("https://%s/identity/v5/oauth2/%s/%s/%s/switch/", vid, carrier, product, version);
	// URLs related to recordings
	public static String UsageSummaryURL = String.format("https://%s/guide/v5/recording/%s/%s/%s/default/", vid, carrier, product, version);
	public static String RecordingSummaryURL = String.format("https://%s/guide/v5/recording/%s/%s/%s/default", vid, carrier, product, version);
	public static String RecordedTabSeriesURL = String.format("https://%s/guide/v5/recording/%s/%s/%s/default/%s/all.json?status=all&seriesIds=%s&shallowEntitiesOnly=false", vid, carrier, product, version, "%s", "%s");
	public static String RecordedTabSinglesURL = String.format("https://%s/guide/v5/recording/%s/%s/%s/default/%s/single.json?status=all&shallowEntitiesOnly=false", vid, carrier, product, version, "%s");
	// URLs related to Dictionary
	public static String DictionaryURL = String.format("https://%s/core/v5/resource/%s/%s/%s/android/dictionary_", vid, carrier, product, version);
	// URL related to Client Configuration
	public static String ClientConfigURL = String.format("https://%s/core/v5/config/%s/%s/%s/client_config.json?device_type=%s&cell_cap=WIFI&app_version=%s.&platform=android&", vid, carrier, product, version, deviceType, appVersion);
	// URL related to shared ref
	public static String SharedRefURL = String.format("https://%s/guide/v5.1/lineup/%s/%s/%s/shared_ref/details.json?regions=national,", vid, carrier, product, version);
	// For recent
	public static String RecentsURL = "https://" + vid + "/core/v5.1/prefs/" + carrier + "/" + product + "/" + version + "/%s/recents.json?category=&completed=%b&ref_type=%s&length=100";
	public static String RecentsAll = "https://" + vid + "/core/v5.1/prefs/" + carrier + "/" + product + "/" + version + "/%s/recents.json?&length=100";
	public static String programURL = String.format("https://%s/guide/v5/program/%s/%s/%s/default/programs.json?programs=", vid, carrier, product, version);
	// Get channels on series is airing on (from current time to next seven days)
	public static String SeriesAiringOnChannelsURL = String.format("https://%s/guide/v5.1/search/%s/%s/%s/search/details.json?drm_rights=play::%s&facets=channel_id&ref_types=program&sort_by=start_time&sort_direction=asc&start_time=%s&timeslice=604800&regions=national,%s&series_id=", vid, carrier, product, version, drmType, dAndt.getCurrentEpochTime(), "%s");
	// Get series detail base on series id.
	public static String SeriesDetailURL = "https://data-production.cspire.tv/guide/v5.1/lineup/series.json?series_ids=%s";
	// Get movie detail base on movie id.
	public static String MovieDetailURL = "https://data-production.cspire.tv/guide/v5/recording/cspire/paytv/1.0/default/programs.json?programIds=%s";
	public static String ProgramsByChannelId = "https://" + vid + "/guide/v5.1/search/" + carrier + "/" + product + "/" + version + "/search/details.json?ref_types=program&sort_by=start_time&sort_direction=asc&offset=0&length=%s&timeslice=1209600&start_time=%s&channel_id=%s&series_id=%s";
	// Offers
	public static String FreeOffers = "https://" + vid + "/core/v5/offer/" + carrier + "/" + product + "/" + version + "/offers/free.json?purchase_type=content";
	public static String SubscrtptionOffers = "https://" + vid + "/core/v5/offer/" + carrier + "/" + product + "/" + version + "/offers/subscription.json?purchase_type=content";
	public static String OffersData = "https://" + vid + "/core/v5/offer/" + carrier + "/" + product + "/" + version + "/offers.json?offer_ids=%s";
	public static String PurchasedOffers = "https://" + vid + "/core/v5/purchase/" + carrier + "/" + product + "/" + version + "/%s/purchases.json?purchase_type=content";
	// Popularity URls
	// ----------------------------------------------------------------------------------------
	public static String BrowseAllSeriesURL = String.format("https://%s/guide/v5.1/recommendations/%s/%s/%s/popular.json?device=%s&parental_rating=G,TV-Y,TVY,TV-Y7,TVY7,TV-G,TVG,PG,TV-PG,TVPG,PG-13,PG13,TV-14,TV14,NR&availability=playable&region=national,%s&length=50&start_index=0&recommendation_type=series", vid, carrier, product, version, deviceType, "%s");
	public static String SeriesDetailURL_pop = String.format("https://%s/guide/v5.2/lineup/%s/%s/%s/series/details.json?metadata_root_ids=", vid, carrier, product, version);
	public static String BrowseAllMoviesURL = String.format("https://%s/guide/v5.1/recommendations/%s/%s/%s/popular.json?device=%s&parental_rating=G,TV-Y,TVY,TV-Y7,TVY7,TV-G,TVG,PG,TV-PG,TVPG,PG-13,PG13,TV-14,TV14,NR&availability=playable&region=national,%s&length=50&start_index=0&recommendation_type=movie", vid, carrier, product, version, deviceType, "%s");
	public static String MoviesDetailURL = String.format("https://%s/guide/v5.2/lineup/%s/%s/%s/shared_ref/details.json?metadata_root_ids=", vid, carrier, product, version);
	public static String MoviesRecommendedForYouURL = "https://" + vid + "/guide/v5/recommendations/" + carrier + "/" + product + "/" + version + "/default/profile/%s/recommendation.json?recommendation_type=movie&offset=0&length=50&regions=national,%s";
	public static String SeriesRecommendedForYouURL = "https://" + vid + "/guide/v5/recommendations/" + carrier + "/" + product + "/" + version + "/default/profile/%s/recommendation.json?recommendation_type=series&offset=0&length=50&regions=national,%s";
	public static String MoviesRecommendedURL = String.format("https://" + vid + "/guide/v5.1/recommendations/" + carrier + "/" + product + "/" + version + "/profile/%s/recommendation.json?device=%s&parental_rating=G,TV-Y,TVY,TV-Y7,TVY7,TV-G,TVG,PG,TV-PG,TVPG,PG-13,PG13,TV-14,TV14,NR&length=50&start_index=0&region=national,%s&recommendation_type=movie", "%s", deviceType, "%s");
	public static String SeriesRecommendedURL = String.format("https://" + vid + "/guide/v5.1/recommendations/" + carrier + "/" + product + "/" + version + "/profile/%s/recommendation.json?device=%s&parental_rating=G,TV-Y,TVY,TV-Y7,TVY7,TV-G,TVG,PG,TV-PG,TVPG,PG-13,PG13,TV-14,TV14,NR&length=50&start_index=0&region=national,%s&recommendation_type=series", "%s", deviceType, "%s");
	public static String MoviePopularURL = String.format("https://%s/guide/v5.1/recommendations/%s/%s/%s/popular.json?device=%s&parental_rating=G,TV-Y,TVY,TV-Y7,TVY7,TV-G,TVG,PG,TV-PG,TVPG,PG-13,PG13,TV-14,TV14,NR&region=national,%s&length=50&start_index=0&recommendation_type=movie", vid, carrier, product, version, deviceType, "%s");;
	public static String SeriesPopularURL = String.format("https://%s/guide/v5.1/recommendations/%s/%s/%s/popular.json?device=%s&parental_rating=G,TV-Y,TVY,TV-Y7,TVY7,TV-G,TVG,PG,TV-PG,TVPG,PG-13,PG13,TV-14,TV14,NR&region=national,%s&length=50&start_index=0&recommendation_type=series", vid, carrier, product, version, deviceType, "%s");;
 }
