package com.mobitv.client.qa.constants;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author MobiTV
 *
 */
public class ConfigurationConstants {
	/**
	 * This constant used for differentiating some commands based on OS. 1. This
	 * constant will get assigned with value when running tests. 2. By default it's
	 * "win".
	 * 
	 */
	public static String automationMachineOS = "win";
	/**
	 * This constant used for getting client configuration data 1. This constant
	 * will get assigned with value when running tests. 2. By default it's "stb".
	 * But it could be phone-android or tablet-android as well.
	 * 
	 */
	public static String device_Type = "tablet-android";

	/**
	 * This constant used for getting correct json file 1. This constant will get
	 * assigned with value when running tests. 2. By default it's "stb-other". But
	 * it could be "phone-android" or "tablet-android" as well.
	 * 
	 */
	public static String drm_Type = "tablet-android";
	/**
	 * This constant used for getting client configuration data 1. This constant
	 * will get assigned with value when running tests. 2. By default it's "en_US".
	 * 
	 */
	public static String deviceLocale = "en_US";
	/**
	 * This constant used for getting client configuration data 1. This constant
	 * will get assigned with value when running tests. 2. By default it's
	 * "6.0.1".[Android M]
	 * 
	 */
	public static String deviceFirmware = "6.0.1";
	/**
	 * This constant used for setting capability while getting driver. 1. This
	 * constant will get assigned with value when running tests. 2. This constant
	 * will be useful for Appium/Selenium. 3. By default it's Android, values could
	 * be Android/Web
	 * 
	 */
	public static String platformName = "Android";
	/**
	 * This constant used for setting capability while getting driver. 1. This
	 * constant never changes, it's always "true" only 2. This constant will be
	 * useful for Appium. To make sure that app is not resetting every time when
	 * executing suite of tests.
	 * 
	 */
	public static boolean noReset = true;
	/**
	 * This constant used for getting driver. 1. This constant will get assigned
	 * with value when running tests. 2. By default it will be
	 * "http://127.0.0.1:4723/wd/hub". 3. This will be overridden by the
	 * appiumServer value from testConfiguration
	 * 
	 */
	public static String appiumServer = "http://127.0.0.1:4723/wd/hub";
	/**
	 * These constants are common for all flavors. 1. All these constants will get
	 * assigned with values when running tests. 2. No need to modify or give any
	 * value to these constants at any time. 3. After initialization in beforeSuite
	 * method in base test class, the values will be accessed where ever required.
	 */
	public static String product;
	public static String version;
	public static String appActivity;
	/**
	 * These constants are flavor specific. 1. All these constants will get assigned
	 * with values when running tests. 2. Based on the flavor we are running, we
	 * need to modify carrier value. But this change is only when we are running
	 * individual test cases while developing automation scripts. carrier could be
	 * "cspire"/"connect"/"metronet"/"directlink"/"ebox" etc. This will be
	 * overridden by the carrier value given from command line, when running form
	 * the command. By default it will be "cspire". 3. No other constants are need
	 * to be modified.
	 * 
	 */
	public static String carrier = "cspire";
	public static String appPackage;
	public static String extAssertionSource;
	public static String oauthClientID;
	public static String oauthClientSecret;
	public static String nativeDeviceID;
	/**
	 * These constants are environment specific 1. All these constants will get
	 * assigned with values when running tests. 2. Based on the environment we are
	 * running we need to modify host value. But this change is only when we are
	 * running individual test cases while developing automation scripts. host could
	 * be "integration"/"staging"/"production" This will be overridden by the host
	 * value given from command line, when running form the command. By default it
	 * will be "production" 3. No other constants re need to be modified.
	 * 
	 */
	public static String host = "production";
	public static String vid;
	public static String userName;
	public static String password;
	public static String autherization;
	public static String oauthHost;
	/**
	 * This constant is to identify the device 1. This constant will get assigned
	 * with value when running tests. As some test cases need extra steps based on
	 * the device we are running,we may need to use this parameter to differentiate
	 * the code. 2. Based on the device we are running we need to modify deviceName
	 * value. But this change is only when we are running individual test cases
	 * while developing automation scripts. deviceName could be
	 * "Amazon"/"MIBox"/"EBox"/"Samsung Galaxy 2 phone"/"Google nexus Tab"/"Browser"
	 * etc. This will be overridden by the deviceName value given from command line,
	 * when running from the command. By default it will be "MIBox"
	 */
	public static String deviceName = "MIBox";
	/**
	 * This constant used for setting capability while getting driver. 1. This
	 * constant will get assigned with value when running tests. 2. This constant
	 * will be useful for Appium. 3. This is very useful when there are multiple
	 * devices connected to a single system.
	 * 
	 */
	public static String serialNumber;
	/**
	 * This constant is to identify the device 1. This constant will get assigned
	 * with value when running tests. As some test cases need extra steps based on
	 * the device we are running,we may need to use this parameter to differentiate
	 * the code. 2. Based on the device we are running we need to modify deviceType
	 * value. But this change is only when we are running individual test cases
	 * while developing automation scripts. deviceType could be
	 * "TV/"/"Phone"/"Tablet"/"PC". This will be overridden by the deviceType value
	 * given from command line, when running from the command. By default it will be
	 * "TV"
	 */
	public static String deviceType = "Tablet";
	/**
	 * This constant is to identify the buildVersion 1. This constant will get
	 * assigned with value when running tests. 2. By default it will be "1.1.1". 3.
	 * This will be overridden by the buildVersion value given from command line,
	 * when running from the command. 4. This parameter will be useful at the time
	 * of creating reports.
	 * 
	 */
	public static String buildVersion = "1.9.28";
	/**
	 * These constants are feature specific, as some features may not be available
	 * in some flavors. 1. This constant will get assigned with value when running
	 * tests based on the flavor 2. No need to modify or give any value to these
	 * constants at any time. 3. After initialization in beforeSuite method in base
	 * test class, the values will be accessed where ever required.
	 */
	public static boolean isRecordEnabled;
	public static boolean isAnonymousModeEnabled;
	public static boolean isCatchUpEnabled;
	public static boolean isOutOfHomeEnabled;
	public static boolean isChannelNumberEnabled;
	/**
	 * This constant will be use full for constructing URLs.
	 */
	public static String childProtectionRatings = "";
	public static String childProtectionEnabledRatings = "";

	/**
	 * This constant will be used to determine user in-home check.
	 * The check is useful across many test cases where the in-home check is required.
	 * Initializing it here will reduce the multiple network calls to determine its value.
	 */
	public static boolean isUserInHome = false;
	/**
	 * This constant will be used to determine screenshot interval in seconds
	 */
	public static int delaySecond = 3;
	/**
	 * This constant will be used to determine screenshot prokect drm right check;
	 */
	public static boolean onDRM = true;
}
