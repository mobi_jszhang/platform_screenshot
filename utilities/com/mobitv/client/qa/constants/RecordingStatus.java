package com.mobitv.client.qa.constants;

public enum RecordingStatus {

	COMPLETED("completed"),
	ONGOING("ongoing"),
	SCHEDULED("scheduled"),
	PARTIALLYRECORDED("partially_recorded"),
	PARTIAL("partial"),
	ALL("all"),
	ERROR("error"),
	CANCELLED("pending_recording_cancelled");
	
	private String value;
	
	public String getValue(){
		return value;
	}
	
	RecordingStatus(String value){
		this.value = value;
	}
}