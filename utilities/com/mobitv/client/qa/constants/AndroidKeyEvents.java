package com.mobitv.client.qa.constants;
import io.appium.java_client.android.AndroidKeyCode;
/**
 * 
 * @author MobiTV
 *
 */
public interface AndroidKeyEvents {
	public static final int NAVIGATE_BACK = AndroidKeyCode.KEYCODE_BACK;
	public static final int ENTER = AndroidKeyCode.ENTER;
	public static final int NAVIGATE_UP = AndroidKeyCode.KEYCODE_DPAD_UP;
	public static final int NAVIGATE_DOWN = AndroidKeyCode.KEYCODE_DPAD_DOWN;
	public static final int NAVIGATE_RIGHT = AndroidKeyCode.KEYCODE_DPAD_RIGHT;
	public static final int NAVIGATE_LEFT = AndroidKeyCode.KEYCODE_DPAD_LEFT;
	public static final int CLICK = AndroidKeyCode.KEYCODE_DPAD_CENTER;
	public static final int MENU = AndroidKeyCode.KEYCODE_MENU;
	public static final int HOME = AndroidKeyCode.KEYCODE_HOME;
	public static final int SEARCH = AndroidKeyCode.KEYCODE_SEARCH;
	public static final int WAKE_UP = AndroidKeyCode.KEYCODE_WAKEUP;
	public static final int POWER = AndroidKeyCode.KEYCODE_POWER;
	public static final int REWIND = AndroidKeyCode.KEYCODE_MEDIA_REWIND;
	public static final int FORWARD = AndroidKeyCode.KEYCODE_MEDIA_FAST_FORWARD;
	public static final int PLAYPAUSE = AndroidKeyCode.KEYCODE_MEDIA_PLAY_PAUSE;
	public static final int MUTE = AndroidKeyCode.KEYCODE_VOLUME_MUTE;
}
