package com.mobitv.client.qa.constants;
/**
 * 
 * @author MobiTV
 *
 */
public interface Constants {
	// Sleep constants.
	public static final int VERY_SMALL_SLEEP = 1;// one second
	public static final int SMALL_SLEEP = 3;// three seconds
	public static final int MEDIUM_SLEEP = 6;// six seconds
	public static final int DRIVER_IMPLICIT_WAIT = 10;// ten seconds
	public static final long RECORDING_THRESHOLD_TIME = 300;
	
	//Content Type
	public static final String REF_TYPE_NETWORK = "network"; 
    public static final String REF_TYPE_CHANNEL = "channel";
    public static final String REF_TYPE_RECORDING = "recording";
    public static final String REF_TYPE_PROGRAM = "program";
    public static final String REF_TYPE_VOD = "vod";
    public static final String REF_TYPE_SERIES = "series";
    public static final String REF_TYPE_SHARED_REF = "shared_ref";
    public static final String REF_TYPE_MOVIE = "movie";

    //Category Type
    public static final String CATEGORY_TYPE_TV = "tv";
    public static final String CATEGORY_TYPE_MOVIE = "movie";

    //Recording
	public static final String record_button_title = "Record";
	public static final String play_button_title = "Play";
	public static final String stop_button_title = "Stop";
	public static final String delete_button_title = "Delete";

	public static final String record_to_watch_button_title = "Record to Watch";
	public static final String delete_record_button_title = "Delete Recording";
	public static final String stop_record_button_title = "Stop Recording";
	public static final String cancel_record_button_title = "Cancel Recording";
	public static final String full_details_button_title = "Full Details";
	
	//Purchase
	public static final String ACTIVE = "active";
	public static final String CANCELED = "canceled";

}
