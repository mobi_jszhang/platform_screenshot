package com.mobitv.client.qa.json;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONObject;

import com.mobitv.client.qa.constants.URLs;
import com.mobitv.client.qa.models.Offers;

public class ProfilesJSONUtility extends MainJSONUtility {
	/**
	 * Method for getting current profile ID
	 * 
	 * @param str_Authorization
	 * @return profile_ID
	 */
	public String getProfileID(String str_Authorization) {
		profile_url = URLs.ProfileURL;
		try {
			url = new URL(profile_url);
			connection = getSimpleConnection(profile_url, str_Authorization);
			connection.connect();
			return profile_ID = getJSONFromConnection(connection).getString("profile_id");
		} catch (IOException e) {
			return null;
		}
	}
	
	
	public boolean isUserInHome(String str_Authorization) {
		profile_url = URLs.ProfileURL;
		try {
			url = new URL(profile_url);
			connection = getSimpleConnection(profile_url, str_Authorization);
			connection.connect();
			jsonObj = getJSONFromConnection(connection);
			
			jsnAry_ProfileList = jsonObj.getJSONArray("extended_property");
			for (int i = 0; i < jsnAry_ProfileList.length(); i++) {
				jsonObj = jsnAry_ProfileList.getJSONObject(i);
				if (jsonObj.getString("name").equals("home_indicator"))
					return jsonObj.getString("value").equalsIgnoreCase("yes") ? true : false;
			}
			
			return false;
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * Method for getting Account profiles JSON object
	 * 
	 * @param void
	 * @return JSONObject
	 */
	public JSONObject getAccountProfilesJSON() {
		str_Authorization = getAuthorization();
		profiles_url = URLs.ProfilesURL;
		profiles_url = String.format(profiles_url + "%s/profiles.json", getProfileID(str_Authorization));
		return getJSONFromConnection(getSimpleConnection(profiles_url, str_Authorization));
	}

	/**
	 * Method to get profiles registration status JSON object
	 * 
	 * @return JSONObject
	 */
	public JSONObject getProfilesStattusJSON() {
		return getAccountProfilesJSON().getJSONObject("profile_stat");
	}

	/**
	 * Method to get JSON object of logged in account.
	 */
	public JSONObject getAccountJSON() {
		jsonObj = getAccountProfilesJSON();
		jsnAry_ProfileList = jsonObj.getJSONArray("profiles");
		jsonObj = jsnAry_ProfileList.getJSONObject(0).getJSONObject("account");
		return jsonObj;
	}

	/**
	 * Method to verify is pin enable for account.
	 */
	public boolean isPinEnabled() {
		return getAccountJSON().getBoolean("pin_enabled");
	}

	/**
	 * Method to verify is parental control flag is already enable or not.
	 */
	public boolean isParentalControlEnabled() {
		return getAccountJSON().getBoolean("parental_control_enabled");
	}

	/**
	 * Method to get already registered profiles count.
	 * 
	 */
	public int getRegisteredProfilesCount() {
		return getProfilesStattusJSON().getInt("registered");
	}

	/**
	 * Method to get number of profiles can be registered for a account.
	 * 
	 * @return int
	 */
	public int getMaxProfilesRegistrationCount() {
		return getProfilesStattusJSON().getInt("max");
	}

	/**
	 * Method to get number of profiles still can be registered for the current
	 * account.
	 * 
	 * @return
	 */
	public int getRemainingProfilesCount() {
		return getProfilesStattusJSON().getInt("remaining");
	}

	/**
	 * Method to get all the registered profiles Names.
	 */
	public ArrayList<String> getAllProfilesNames() {
		profilesNames = new ArrayList<>();
		jsonObj = getAccountProfilesJSON();
		jsnAry_ProfileList = jsonObj.getJSONArray("profiles");
		for (int i = 0; i < jsnAry_ProfileList.length(); i++) {
			jsonObj = jsnAry_ProfileList.getJSONObject(i);
			profilesNames.add(jsonObj.getString("user_nick_name"));
		}
		return profilesNames;
	}

	/**
	 * Method to get current region of the user
	 * 
	 * @return region
	 */
	public String getAccountRegion() {
		str_Authorization = getAuthorization();
		profile_url = URLs.ProfileURL;
		try {
			url = new URL(profile_url);
			connection = getSimpleConnection(profile_url, str_Authorization);
			connection.connect();
			jsonObj = getJSONFromConnection(connection);
			jsnAry_ProfileList = jsonObj.getJSONArray("extended_property");
			for (int i = 0; i < jsnAry_ProfileList.length(); i++) {
				jsonObj = jsnAry_ProfileList.getJSONObject(i);
				if (jsonObj.getString("name").equals("region"))
					return jsonObj.getString("value");
			}
		} catch (IOException e) {
			return "";
		}
		return "";
	}

	/**
	 * Method for getting Account Purchased data JSON
	 * 
	 * @return JSONObject
	 */
	public JSONObject getPurchaseJSON() {
		str_Authorization = getAuthorization();
		profile_ID = getProfileID(str_Authorization);
		url_Purchases = URLs.PurchasesURL;
		url_Purchases = String.format(url_Purchases + "%s/purchases.json", profile_ID);
		try {
			url = new URL(url_Purchases);
			connection = getSimpleConnection(url_Purchases, str_Authorization);
			connection.connect();
			return getJSONFromConnection(connection);
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Method for getting SwitchToken JSON object
	 * 
	 * @param admin_nicname
	 * @param user_nicname
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getSwitchToken(String admin_nicname, String user_nicname) throws Exception {
		jsnAry_ProfileList = getAccountProfilesJSON().getJSONArray("profiles");
		if (jsnAry_ProfileList.length() > 1) {
			for (int i = 0; i < jsnAry_ProfileList.length(); i++) {
				jsonObj = jsnAry_ProfileList.getJSONObject(i);
				boolean bFlag = jsonObj.getString("user_nick_name").equalsIgnoreCase(admin_nicname);
				if (bFlag) {
					str_AdminProfileID = jsnAry_ProfileList.getJSONObject(i).getString("profile_id");
				}
				bFlag = jsonObj.getString("user_nick_name").equalsIgnoreCase(user_nicname);
				if (bFlag) {
					str_UserProfileID = jsnAry_ProfileList.getJSONObject(i).getString("profile_id");
				}
			}
		} else {
			throw new Exception("Default admin profile only available.Please add second profile to execute this scenario.");
		}
		url_SwitchTokens = URLs.SwitchTokenURL;
		url_SwitchTokens = String.format(url_SwitchTokens + "%s/%s/tokens.json", str_AdminProfileID, str_UserProfileID);
		jsonObj = getToken();
		str_AccessToken = jsonObj.getString("access_token");
		str_Authorization = jsonObj.getString("token_type") + " " + str_AccessToken;
		urlParameters = "{\"access_token\" : \"" + str_AccessToken + "\"}";
		url = new URL(url_SwitchTokens);
		connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Accept", "application/json");
		connection.setRequestProperty("Authorization", str_Authorization);
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.connect();
		dout = new DataOutputStream(connection.getOutputStream());
		dout.write(urlParameters.getBytes("UTF8"));
		dout.flush();
		dout.close();
		return getJSONFromConnection(connection);
	}
}
