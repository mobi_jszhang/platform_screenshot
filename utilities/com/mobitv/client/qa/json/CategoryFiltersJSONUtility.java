package com.mobitv.client.qa.json;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.json.JSONObject;
import com.mobitv.client.qa.baseTests.BaseTest;
import com.mobitv.client.qa.constants.URLs;
import com.mobitv.client.qa.functions.DateAndTime;
import com.mobitv.client.qa.functions.Utility;
import com.mobitv.client.qa.models.Assets;
import com.mobitv.client.qa.models.Movie;
import com.mobitv.client.qa.models.Program;
/**
 * 
 * @author MobiTV
 *
 */
public class CategoryFiltersJSONUtility extends MainJSONUtility {
	private String categoryFilterURL = null;
	private String startTime = null;

	/**
	 * Method to get programs of On Now section of category filter results.
	 * 
	 * @param count
	 * @param filter
	 * @return
	 */
	public ArrayList<Program> getCategoyFilterResultsOnNowPrograms(int count, String filter, HashSet<String> allOffers) {
		programs = new ArrayList<>();
		jsonObj = getCategoyFilterResultsOnNowProgramsJSON(count, filter, allOffers);
		hits = jsonObj.getJSONArray("hits");
		for (int i = 0; i < hits.length() && i < count; i++) {
			result = hits.getJSONObject(i).getJSONObject("result");
			keys = JSONObject.getNames(result);
			switch (keys[0]) {
			case "shared_ref":
				jsonObj = result.getJSONObject("shared_ref");
				break;
			case "program":
				jsonObj = result.getJSONObject("program");	
				break;
	            }
			program = gson.fromJson(jsonObj.toString(), Program.class);
			programs.add(program);
		}
		return programs;
	}

	/**
	 * Method to get programs of On Now section of category filter results.
	 * 
	 * @param count
	 * @param filter
	 * @return
	 */
	public ArrayList<Program> getCategoyFilterResultsNextRoundedPrograms(int count, String filter, HashSet<String> allOffers) {
		programs = new ArrayList<>();
		jsonObj = getCategoyFilterResultsNextRoundedProgramsJSON(count, filter, allOffers);
		hits = jsonObj.getJSONArray("hits");
		for (int i = 0; i < hits.length() && i < count; i++) {
			result = hits.getJSONObject(i).getJSONObject("result");
			keys = JSONObject.getNames(result);
			switch (keys[0]) {
			case "shared_ref":
				jsonObj = result.getJSONObject("shared_ref");
				break;
			case "program":
				jsonObj = result.getJSONObject("program");
				break;
			}
			program = gson.fromJson(jsonObj.toString(), Program.class);
			programs.add(program);
		}
		return programs;
	}

	/**
	 * Method to get programs json of on now section in category filters results
	 * section.
	 * 
	 * @param count
	 * @param filter
	 * @return
	 */
	public JSONObject getCategoyFilterResultsOnNowProgramsJSON(int count, String filter, HashSet<String> allOffers) {
		Utility utility = new Utility();
		String offersIds = utility.collectionToString(allOffers);
		long currTime = new DateAndTime().getCurrentEpochTime();
		switch (filter) {
		case "Sports":
			categoryFilterURL = String.format(URLs.CategoryFilterSportsOnNowURL, count, currTime, offersIds, BaseTest.currentAccountRegion);
			break;
		case "News":
			categoryFilterURL = String.format(URLs.CategoryFilterNewsOnNowURL, count, currTime, offersIds, BaseTest.currentAccountRegion);
			break;
		case "Movies":
			categoryFilterURL = String.format(URLs.CategoryFilterMoviesOnNowURL, count, currTime, offersIds, BaseTest.currentAccountRegion);
			break;
		case "Kids":
			categoryFilterURL = (String.format(URLs.CategoryFilterKidsOnNowURL, count, currTime, offersIds,  BaseTest.currentAccountRegion)).replace("kids&family","kids%20%26%20family");
			break;
		}
		System.out.println("URL for " + filter + " : " + categoryFilterURL);
		return getJSONFromConnection(getSimpleConnection(categoryFilterURL, ""));
	}
	
	/**
	 * Method to get programs json of on first complete 30 minutes section in
	 * category filters results section.
	 * 
	 * @param count
	 * @param filter
	 * @return
	 */
	public JSONObject getCategoyFilterResultsNextRoundedProgramsJSON(int count, String filter, HashSet<String> allOffers) {
		Utility utility = new Utility();
		String offersIds = utility.collectionToString(allOffers);
		String nextRoundedHalfAnHourEpochTime = (new DateAndTime()).getNextRoundedHalfAnHourEpochTime();
		System.out.println("\n Next Rounded Off time : " + nextRoundedHalfAnHourEpochTime);
		switch (filter) {
		case "Sports":
			categoryFilterURL = String.format(URLs.CategoryFilterSportsOnNowURL, count, nextRoundedHalfAnHourEpochTime, offersIds, BaseTest.currentAccountRegion);
			break;
		case "News":
			categoryFilterURL = String.format(URLs.CategoryFilterNewsOnNowURL, count, nextRoundedHalfAnHourEpochTime, offersIds, BaseTest.currentAccountRegion);
			break;
		case "Movies":
			categoryFilterURL = String.format(URLs.CategoryFilterMoviesOnNowURL, count, nextRoundedHalfAnHourEpochTime, offersIds, BaseTest.currentAccountRegion);
			break;
		case "Kids":
			categoryFilterURL = String.format(URLs.CategoryFilterKidsOnNowURL, count, nextRoundedHalfAnHourEpochTime, offersIds,  BaseTest.currentAccountRegion);
			break;
		}
		System.out.println("URL for " + filter + " : " + categoryFilterURL);
		return getJSONFromConnection(getSimpleConnection(categoryFilterURL, ""));
	}

}
