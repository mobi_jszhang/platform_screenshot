package com.mobitv.client.qa.json;

import java.util.ArrayList;

import org.json.JSONObject;

import com.mobitv.client.qa.constants.URLs;
import com.mobitv.client.qa.functions.Utility;
import com.mobitv.client.qa.models.Offers;

public class OfferJSONUtility extends MainJSONUtility{
	
	private ArrayList<String> freeOfferIds = new ArrayList<>();
	private Utility utility = new Utility();
	private int total;
	ArrayList<Offers> allFreeOffersData= null;
	
	/**
	 * 
	 * @return ArrayList<String> of the free sku_Ids from the free offers
	 */
	public ArrayList<String> fetchFreeSkuIds() {
		ArrayList<String> freeSkuIds = new ArrayList<>();
		
		jsonObj = getFreeOffersIdsJSONObject();
		if (jsonObj != null) {
			total = jsonObj.getInt("total");

			if (total > 0) {
				offer_ids = jsonObj.getJSONArray("offer_ids");
				for (int i = 0; i < offer_ids.length(); i++) {
					freeOfferIds.add(offer_ids.getString(i));
				}

				if (utility.isValidCollection(freeOfferIds)) {
					System.out.println("Free offer id : " + freeOfferIds.size());
					allFreeOffersData = generateFreeOfferData();
					if (utility.isValidCollection(allFreeOffersData)) {
						System.out.println("All offer data not null at 36");
						for (Offers offer : allFreeOffersData) {
							freeSkuIds.addAll(offer.sku);
						}
						System.out.println("Free sku ids size : " + freeSkuIds.size());
						return freeSkuIds;
					} System.out.println("All offer data null at 42");
				}
			}
		} else {
			System.out.println("null at line 44");
		}
		return null;
	}
	
	/**
	 * 
	 * @return ArrayList<String> of offer Id's of free Offers associated with the VID
	 */
	public ArrayList<String> fetchFreeOfferIds() {
		if (utility.isValidCollection(freeOfferIds)) {
			return freeOfferIds;
		}
		return null;
	}
	
	/**
	 * 
	 * @return ArrayList<String> of offer Id's of all the offers associated with the VID
	 */
	public ArrayList<String> fetchSubscriptionOfferIds() {
		ArrayList<String> subscriptionOffersIds = new ArrayList<>();
		jsonObj = getSubscriptionOffersIdsJSONObject();
		
		if (jsonObj != null) {
			total = jsonObj.getInt("total");

			if (total > 0) {
				offer_ids = jsonObj.getJSONArray("offer_ids");
				for (int i = 0; i < offer_ids.length(); i++) {
					subscriptionOffersIds.add(offer_ids.getString(i));
				}
				return subscriptionOffersIds;
			}
		}
		
		return null;
	}
	
	/**
	 * 
	 * @return ArrayList<Offers> of Offer data
	 */
	private ArrayList<Offers> generateFreeOfferData() {
		ArrayList<Offers> allOffersData = new ArrayList();
		jsonObj = getOfferDataForFreeOffers();
		
		if (jsonObj != null) {
			offers = jsonObj.getJSONArray("offers");
			System.out.println("Offers data length from json : " + offers.length());
			for (int i = 0; i < offers.length(); i++) {
				jsonObj = offers.getJSONObject(i);
				offer = gson.fromJson(jsonObj.toString(), Offers.class);
				allOffersData.add(offer);
			}
			System.out.println("ALl offers data : " + allOffersData.size());
			return allOffersData;
		} else{
			System.out.println("null at line 61");
		}
		return null;
	}
	
	private JSONObject getFreeOffersIdsJSONObject() {
		String URL = URLs.FreeOffers;
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}
	
	private JSONObject getSubscriptionOffersIdsJSONObject() {
		String URL = URLs.SubscrtptionOffers;
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}
	
	private JSONObject getOfferDataForFreeOffers() {
		String URL = String.format(URLs.OffersData, utility.collectionToString(freeOfferIds));
		System.out.println("Offer URL " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}
}
