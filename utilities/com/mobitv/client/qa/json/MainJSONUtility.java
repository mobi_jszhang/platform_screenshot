package com.mobitv.client.qa.json;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mobitv.client.qa.baseTests.BaseTest;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.constants.URLs;
import com.mobitv.client.qa.functions.DateAndTime;
import com.mobitv.client.qa.functions.Utility;
import com.mobitv.client.qa.models.Assets;
import com.mobitv.client.qa.models.Channel;
import com.mobitv.client.qa.models.ClientConfig;
import com.mobitv.client.qa.models.Episode;
import com.mobitv.client.qa.models.Movie;
import com.mobitv.client.qa.models.Offers;
import com.mobitv.client.qa.models.Program;
import com.mobitv.client.qa.models.PurchaseData;
import com.mobitv.client.qa.models.Recents;
import com.mobitv.client.qa.models.Recents.RecentsListItem;
import com.mobitv.client.qa.models.RecommendListItem;
import com.mobitv.client.qa.models.Seasons;
import com.mobitv.client.qa.models.SeriesRecording;
import com.mobitv.client.qa.models.SharedRef;
import com.mobitv.client.qa.models.Show;
/**
 * 
 * @author MobiTV
 *
 */
public class MainJSONUtility {
	protected JSONObject jsonObj = null, result = null, assetObj = null, seasonObj = null, sharedRefObj = null, refTypeObj = null;
	protected JSONArray jsnAry_ProfileList = null, hits = null, inventories = null, jsnAry_programs = null, category = null, assetArray = null, seasonArray = null, sharedRefArray = null, movieAssets = null;
	protected JSONArray purchase_infos = null;
	protected JSONArray offer_ids = null;
	protected JSONArray offers = null;
	protected String ext_assertion = null;
	protected String tokens_url = null;
	protected String token_url = null;
	protected String ominiaid_url = null;
	protected String billingID_url = null;
	protected String profile_url = null;
	protected String sharedRefID = null;
	protected ArrayList<String> profilesNames = null;
	protected String profiles_url = null;
	protected String url_SwitchTokens = null;
	protected String urlParameters = null;
	protected String url_Purchases = null;
	protected String ominiaid = null;
	protected String billingID = null;
	protected String str_Authorization = null;
	protected String str_AccessToken = null;
	protected String profile_ID = null;
	protected String str_AdminProfileID = null;
	protected String str_UserProfileID = null;
	protected BufferedReader br = null;
	protected String inputLine = null;
	protected DataOutputStream dout = null;
	protected HttpURLConnection connection = null;
	protected URL url = null;
	protected GsonBuilder gsonBuilder = new GsonBuilder();
	protected Gson gson = gsonBuilder.create();
	protected String[] keys = null;
	protected byte[] stringBytes = null;
	protected ArrayList<Show> shows = null;
	protected Show show = null;
	protected Seasons season = null;
	protected ArrayList<Movie> movies = null;
	protected ArrayList<Assets> assets = null;
	protected ArrayList<Seasons> seasons = null;
	protected HashMap<String, Movie> epgMovies;
	protected HashMap<String, Episode> epgShows;
	protected Movie movie = null;
	protected Assets asset = null;
	protected ArrayList<Episode> episodes = null;
	protected Episode episode = null;
	protected ArrayList<Channel> channels = null;
	protected Channel channel = null;
	protected ArrayList<Program> programs = null;
	protected Program program = null;
	protected String URL, URL1 = null;
	private StringBuffer response = null;
	protected String ref_id = null;
	protected int total = 0;
	protected SharedRef sharedRef = null;
	protected List<SharedRef> sharedRefObjects;
	protected Offers offer = null;
	protected PurchaseData purchaseData = null;
	protected LinkedHashMap<String, ArrayList<Movie>> featureMovies;
	protected LinkedHashMap<String, ArrayList<Show>> featureShows;
	protected ArrayList<String> networkNames;
	protected Utility utility = new Utility();
	
	/**
	 * Method for getting JSON object from a json file
	 * 
	 * @param FileReader
	 * @return jsonObj
	 */
	public JSONObject getJSONFromFile(FileReader fileReader) {
		try {
			br = new BufferedReader(fileReader);
			response = new StringBuffer();
			while ((inputLine = br.readLine()) != null) {
				response.append(inputLine);
				response.append('\r');
			}
			br.close();
			stringBytes = response.toString().getBytes();
			return new JSONObject(new String(stringBytes, "UTF-8"));
		} catch (IOException | JSONException e) {
			return null;
		}
	}

	/**
	 * Method for getting JSON object from the connection
	 * 
	 * @param connection
	 * @return jsonObj
	 */
	public JSONObject getJSONFromConnection(HttpURLConnection connection) {
		try {
			if (connection.getResponseCode() != 200)
				return null;
			br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			response = new StringBuffer();
			while ((inputLine = br.readLine()) != null) {
				response.append(inputLine);
				response.append('\r');
			}
			br.close();
			stringBytes = response.toString().getBytes();
			jsonObj = new JSONObject(new String(stringBytes, "UTF-8"));
		} catch (IOException | JSONException e) {
			return null;
		}
		return jsonObj;
	}

	/**
	 * Method for getting HttpURLConnection with request type GET and
	 * str_Authorization
	 * 
	 * @param String,String
	 * @return HttpURLConnection
	 */
	public HttpURLConnection getSimpleConnection(String str_url, String str_Authorization) {
		try {
			url = new URL(str_url);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-Type", "application/json");
			if (str_Authorization.length() > 1)
				connection.setRequestProperty("Authorization", str_Authorization);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			return connection;
		} catch (MalformedURLException | ProtocolException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Method for getting Authorization token
	 * 
	 * @return str_Authorization
	 */
	public String getAuthorization() {
		jsonObj = getToken();
		str_Authorization = jsonObj.getString("token_type") + " " + jsonObj.getString("access_token");
		return str_Authorization;
	}

	/**
	 * Method for getting AccessToken JSON object
	 * 
	 * @return JSONObject
	 */
	public JSONObject getToken() {
		switch (ConfigurationConstants.carrier) {
		case "cspire":
			ext_assertion = getExtAssertionForCSpire();
			ominiaid = getOminiaidForCSpire(ext_assertion);
			urlParameters = "{\"grant_type\" :\"ext_assertion\",\"ext_assertion\" : \"" + ext_assertion + "\",\"ext_assertion_source\" : \"" + ConfigurationConstants.extAssertionSource + "\",\"ext_credential\" : \"" + ominiaid + "\",\"scope\" : \"role_verified_identity\"}";
			break;
		case "arvigwifitv":
			ext_assertion = getExtAssertionForArvig();
			ominiaid = getOminiaidForArvig(ext_assertion);
			urlParameters = "{\"grant_type\" :\"ext_assertion\",\"ext_assertion\" : \"" + ext_assertion + "\",\"ext_assertion_source\" : \"arvigauthservice\",\"ext_credential\" : \"" + ominiaid + "\",\"scope\" : \"role_verified_identity\"}";
			break;
		default:
			ext_assertion = getExtAssertionForIdamService();
			billingID = getBillingIDForIdamService(ext_assertion);
			urlParameters = "{\"grant_type\" :\"ext_assertion\",\"ext_assertion\" : \"" + ext_assertion + "\",\"ext_assertion_source\" :\"" + ConfigurationConstants.extAssertionSource + "\",\"ext_credential\":\"" + billingID + "\",\"scope\" : \"role_verified_identity\"}";
		}
		tokens_url = URLs.TokensURL;
		try {
			url = new URL(tokens_url);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			dout = new DataOutputStream(connection.getOutputStream());
			dout.write(urlParameters.getBytes("UTF8"));
			dout.flush();
			dout.close();
			return getJSONFromConnection(connection);
		} catch (Exception e) {
			return null;
		}
	}

	public String getExtAssertionForArvig() {
		try {
			switch (ConfigurationConstants.host) {
			case "integration":
				token_url = URLs.CSpireIntegrationTokenURL;
				break;
			default:
				token_url = URLs.TokenURLForArvig;
			}
			url = new URL(token_url);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Authorization", ConfigurationConstants.autherization);
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			dout = new DataOutputStream(connection.getOutputStream());
			urlParameters = "{\"username\":\"" + ConfigurationConstants.userName + "\",\"password\":\"" + ConfigurationConstants.password + "\",\"grant_type\":\"password\",\"scope\":\"mobitv\",\"client_id\":\"" + ConfigurationConstants.oauthClientID + "\",\"client_secret\":\"" + ConfigurationConstants.oauthClientSecret + "\"}";
			dout.write(urlParameters.getBytes("UTF8"));
			dout.flush();
			dout.close();
			JSONObject obj = getJSONFromConnection(connection);
			return obj.getString("access_token");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getOminiaidForArvig(String arvig_Acess_Token) {
		switch (ConfigurationConstants.host) {
		case "integration":
			ominiaid_url = URLs.ArvigIntegrationOmniaIDURL;
			break;
		default:
			ominiaid_url = URLs.ArvigIntegrationOmniaIDURL;
		}
		ominiaid_url = String.format(ominiaid_url + arvig_Acess_Token);
		try {
			url = new URL(ominiaid_url);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("Authorization", "Bearer " + arvig_Acess_Token);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.connect();
			JSONObject obj = getJSONFromConnection(connection);
			ominiaid = Integer.toString(obj.getInt("mobi_id"));
			return ominiaid;
		} catch (IOException | JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Method to get all EPGMovies
	 * 
	 * @param count
	 * @return
	 */
	public HashMap<String, Movie> getAllEPGMovies() {
		jsonObj = getAllEPGMoviesJSON();
		epgMovies = getEPGMoviesForJsonObject(jsonObj);
		return epgMovies;
	}

	public HashMap<String, Movie> getAllEPGMoviesForPast24Hrs() {
		jsonObj = getAllEPGMoviesJSONForPast24Hrs();
		epgMovies = getEPGMoviesForJsonObject(jsonObj);
		return epgMovies;
	}

	/**
	 * Method to get all EPGMovies
	 * 
	 * @param count
	 * @return
	 */
	public HashMap<String, Episode> getAllEPGShows() {
		jsonObj = getAllEPGEpisodesJSON();
		epgShows = getEPGShowsForJsonObject(jsonObj);
		return epgShows;
	}

	public HashMap<String, Episode> getLiveAndUpcomingEPGShows() {
		jsonObj = getLiveAndUpcomingEPGEpisodesJSON();
		epgShows = getLiveAndUpcomingEPGShowsForJsonObject(jsonObj);
		return epgShows;
	}

	/**
	 * Method to get all Movies (All Movies screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public ArrayList<Movie> getAllMovies(int count) {
		jsonObj = getAllMoviesJSON(count);
		movies = getMoviesForJsonObject(jsonObj, count);
		return movies;
	}

	/**
	 * @param count
	 * @return
	 */
	public ArrayList<Movie> getAllChildProtectionEnabledMovies(int count) {
		jsonObj = getAllChildProtectionEnabledMoviesJSON(count);
		movies = getMoviesForJsonObject(jsonObj, count);
		return movies;
	}

	public ArrayList<Movie> getAllUpcomingMovies(int count) {
		jsonObj = getAllUpcomingMoviesJSON(count);
		movies = getMoviesForJsonObject(jsonObj, count);
		return movies;
	}

	public ArrayList<Movie> getMoviesForJsonObject(JSONObject jsonOb, int count) {
		JSONObject movieData = null;
		movies = new ArrayList<>();
		total = jsonOb.getInt("total");
		if (total > 0) {
			hits = jsonOb.getJSONArray("hits");
			for (int i = 0; i < hits.length() && i < count; i++) {
				List<Assets> assetArrayList = new ArrayList<>();
				result = hits.getJSONObject(i).getJSONObject("result");
				String[] keyValues = JSONObject.getNames(result);
				switch (keyValues[0]) {
				case "vod":
					movieData = result.getJSONObject("vod");
					break;
				case "shared_ref":
					movieData = result.getJSONObject("shared_ref");
					String sharedRefId = movieData.getString("id");
					JSONObject jsonObject = fetchSharedAssets(sharedRefId);
					List<Assets> assetArray = getAssetromJSON(jsonObject);
					assetArrayList.addAll(assetArray);
					break;
				case "program":
					movieData = result.getJSONObject("program");
					break;
				}
				movie = gson.fromJson(movieData.toString(), Movie.class);
				movie.setAssets(assetArrayList);
				movies.add(movie);
			}
		}
		return movies;
	}

	/**
	 * @param jsonObj
	 * @return
	 */
	public HashMap<String, Movie> getEPGMoviesForJsonObject(JSONObject jsonObj) {
		System.out.println(URLs.searchEPGLiveMovieURL);
		epgMovies = new HashMap<>();
		int total = jsonObj.getInt("total");
		if (total > 0) {
			JSONArray hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				List<Assets> assetArrayList = new ArrayList<>();
				jsonObj = hits.getJSONObject(i).getJSONObject("result").getJSONObject("program");
				String sharedRefId = jsonObj.getString("shared_ref_id");
				JSONObject jsonObject = fetchSharedAssets(sharedRefId);
				List<Assets> assetArray = getAssetromJSON(jsonObject);
				assetArrayList.addAll(assetArray);
				GsonBuilder gsonBuilder = new GsonBuilder();
				Gson gson = gsonBuilder.create();
				Movie movie = gson.fromJson(jsonObj.toString(), Movie.class);
				movie.setAssets(assetArrayList);
				String linkedCh = String.valueOf(jsonObj.getInt("linked_channel_number"));
				epgMovies.put(movie.getName(), movie);
				System.out.println(linkedCh + movie.getName());
			}
		}
		return epgMovies;
	}

	/**
	 * @param jsonObj
	 * @return
	 */
	public HashMap<String, Episode> getEPGShowsForJsonObject(JSONObject jsonObj) {
		System.out.println(URLs.searchEPGLiveShowURL);
		epgShows = new HashMap<>();
		int total = jsonObj.getInt("total");
		if (total > 0) {
			JSONArray hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				jsonObj = hits.getJSONObject(i).getJSONObject("result").getJSONObject("program");
				GsonBuilder gsonBuilder = new GsonBuilder();
				Gson gson = gsonBuilder.create();
				Episode episode = gson.fromJson(jsonObj.toString(), Episode.class);
				String linkedCh = String.valueOf(jsonObj.getInt("linked_channel_number"));
				epgShows.put(episode.getSeries_name(), episode);
				System.out.println(linkedCh + episode.getName());
			}
		}
		return epgShows;
	}

	public HashMap<String, Episode> getLiveAndUpcomingEPGShowsForJsonObject(JSONObject jsonObj) {
		System.out.println(URLs.searchEPGLiveAndUpcomingShowURL);
		epgShows = new HashMap<>();
		int total = jsonObj.getInt("total");
		if (total > 0) {
			JSONArray hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				jsonObj = hits.getJSONObject(i).getJSONObject("result").getJSONObject("program");
				GsonBuilder gsonBuilder = new GsonBuilder();
				Gson gson = gsonBuilder.create();
				Episode episode = gson.fromJson(jsonObj.toString(), Episode.class);
				String linkedCh = String.valueOf(jsonObj.getInt("linked_channel_number"));
				epgShows.put(episode.getSeries_name(), episode);
				System.out.println(linkedCh + episode.getName());
			}
		}
		return epgShows;
	}

	/**
	 * Remove below method and replace it with method written by Lakshmi
	 * 
	 * @param movieNAME
	 * @return
	 */
	public ArrayList<Assets> getAllAssets(String movieNAME) {
		movies = new ArrayList<>();
		assets = new ArrayList<>();
		jsonObj = getAllMoviesJSON(200);
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length() && i < 200; i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "vod":
					jsonObj = result.getJSONObject("vod");
					break;
				case "shared_ref":
					jsonObj = result.getJSONObject("shared_ref");
					String movieName = ((String) (jsonObj.get("name"))).replaceAll("\\s+", "");
					String givenMovieName = movieNAME.replaceAll("\\s+", "");
					if (movieName.equals(givenMovieName)) {
						System.out.println("check -2 ");
						assetArray = jsonObj.getJSONArray("assets");
						for (int j = 0; j < assetArray.length(); j++) {
							assetObj = assetArray.getJSONObject(j);
							// System.out.println("Recording"+assetArray.getJSONObject(0).getBoolean("is_recording_enabled"));
							asset = gson.fromJson(assetObj.toString(), Assets.class);
							assets.add(asset);
							// System.out.println("Final" +
							// assets.get(0).is_recording_enabled());
						}
					}
					break;
				case "program":
					jsonObj = result.getJSONObject("program");
					break;
				}
				movie = gson.fromJson(jsonObj.toString(), Movie.class);
				movie.setAssets(assets);
				movies.add(movie);
			}
		}
		return assets;
	}

	/**
	 * Method to get All recommendations
	 * 
	 * @param count
	 * @return
	 */
	public JSONObject getAllMoviesJSON(int count) {
		if (ConfigurationConstants.deviceType.equalsIgnoreCase("phone") || ConfigurationConstants.deviceType.equalsIgnoreCase("tablet")) {
			URL = String.format(URLs.MobileMoviesURL + "%s&child_protection_rating=%s&length=" + count, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionRatings);
		} else {
			URL = String.format(URLs.AllMoviesURL + "%s&child_protection_rating=%s&length=" + count, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionRatings);
//			System.out.println("allMoviesUrl : " + URL);
		}
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public JSONObject fetchSharedAssets(String sharedRefId) {
		if (ConfigurationConstants.deviceType.equalsIgnoreCase("phone") || ConfigurationConstants.deviceType.equalsIgnoreCase("tablet")) {
			URL = String.format(URLs.MobileMoviesURL, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionRatings);
		} else {
			URL = String.format(URLs.SharedRefURL + "%s&shared_ref_ids=%s", BaseTest.currentAccountRegion, sharedRefId);
//			System.out.println("allMoviesUrl : " + URL);
		}
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * @return
	 */
	public JSONObject getAllEPGMoviesJSON() {
		URL = String.format(URLs.searchEPGLiveMovieURL, BaseTest.currentAccountRegion);
		System.out.println("allEPGMoviesUrl : " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public JSONObject getAllEPGMoviesJSONForPast24Hrs() {
		URL = String.format(URLs.searchEPGLiveMovieURLForPast24Hrs, BaseTest.currentAccountRegion);
		System.out.println("allEPGMoviesUrl : " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * @return
	 */
	public JSONObject getAllEPGEpisodesJSON() {
		URL = String.format(URLs.searchEPGLiveShowURL, BaseTest.currentAccountRegion);
		System.out.println("allEPGShowsUrl : " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public JSONObject getLiveAndUpcomingEPGEpisodesJSON() {
		URL = String.format(URLs.searchEPGLiveAndUpcomingShowURL, BaseTest.currentAccountRegion);
		System.out.println("allLiveandUpcomingEPGShowsUrl : " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * @return
	 */
	public JSONObject getMoviesNetworkJSON() {
		URL = String.format(URLs.MoviesNetworkFeatureURL);
		System.out.println("MoviesNetwork : " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * @return
	 */
	public JSONObject getShowsNetworkJSON() {
		URL = String.format(URLs.SeriesNetworkFeatureURL);
		System.out.println("SeriesNetwork : " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * @param count
	 * @return
	 */
	public JSONObject getAllChildProtectionEnabledMoviesJSON(int count) {
		if (ConfigurationConstants.deviceType.equalsIgnoreCase("phone")) {
			URL = String.format(URLs.MobileUpcomingMoviesURL + "%s&child_protection_rating=%s&length=" + count, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionEnabledRatings);
		} else {
			URL = String.format(URLs.AllUpcomingMoviesURL + "%s&child_protection_rating=%s&length=" + count, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionEnabledRatings);
		}
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * @param movieTmsId
	 * @return
	 */
	public JSONObject getAllAssetsJSON(String movieTmsId) {
		if (ConfigurationConstants.deviceType.equalsIgnoreCase("phone")) {
			URL = String.format(URLs.MobileMoviesURL + "%s&child_protection_rating=%s&length=" + movieTmsId, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionRatings);
		} else {
			URL = String.format(URLs.movieDetailsURL + movieTmsId,BaseTest.currentAccountRegion);
		}
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public JSONObject getAllAssetsJSONForEpisode(String episodeTmsId) {
		if (ConfigurationConstants.deviceType.equalsIgnoreCase("phone")) {
			URL = String.format(URLs.MobileShowsURL + "%s&child_protection_rating=%s&length=" + episodeTmsId, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionRatings);
			// TODO : fix above URL for mobile
		} else {
			URL = String.format(URLs.episodeDetailsURL + episodeTmsId);
			System.out.println("getAllAssetsJSONForEpisode URL : " + URL);
		}
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public ArrayList<JSONObject> getAllAssetsForMovie(String movieTmsId) {
		ArrayList<JSONObject> movieAssets = new ArrayList<>();
		jsonObj = getAllAssetsJSON(movieTmsId);
		sharedRefArray = jsonObj.getJSONArray("shared_refs");
		if(sharedRefArray.getJSONObject(0).has("assets")) {
			assetArray = sharedRefArray.getJSONObject(0).getJSONArray("assets");
			assetArray.forEach(asset -> {
				movieAssets.add((JSONObject) ((JSONObject) asset).get("asset"));	
			});
		} else
			return null;
		return movieAssets;
	}

	public ArrayList<JSONObject> getAllAssetsForEpisode(String episodeTmsId) {
		ArrayList<JSONObject> episodeAssets = new ArrayList<>();
		jsonObj = getAllAssetsJSONForEpisode(episodeTmsId);
		sharedRefArray = jsonObj.getJSONArray("shared_refs");
		assetArray = sharedRefArray.getJSONObject(0).getJSONArray("assets");
		assetArray.forEach(asset -> {
			episodeAssets.add((JSONObject) ((JSONObject) asset).get("asset"));
		});
		return episodeAssets;
	}

	public Movie getMovieWithTmsId(String movieTmsId) {
		jsonObj = getAllAssetsJSON(movieTmsId);
		System.out.println("JSOnObject for movie details page : " + jsonObj);
		sharedRefArray = jsonObj.getJSONArray("shared_refs");
		Movie movieToReturn = gson.fromJson(sharedRefArray.get(0).toString(), Movie.class);
		return movieToReturn;
	}

	public Episode getEpisodeWithTmsId(String episodeTmsId) {
		jsonObj = getAllAssetsJSONForEpisode(episodeTmsId);
		// System.out.println("JSOnObject for episode details page : "+
		// jsonObj);
		sharedRefArray = jsonObj.getJSONArray("shared_refs");
		Episode episodeToReturn = gson.fromJson(sharedRefArray.get(0).toString(), Episode.class);
		return episodeToReturn;
	}

	/**
	 * Method to get All upcoming movie recommendations
	 * 
	 * @param count
	 * @return
	 */
	public JSONObject getAllUpcomingMoviesJSON(int count) {
		if (ConfigurationConstants.deviceType.equalsIgnoreCase("phone")) {
			URL = String.format(URLs.MobileUpcomingMoviesURL + "%s&child_protection_rating=%s&length=" + count, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionRatings);
		} else {
			URL = String.format(URLs.AllUpcomingMoviesURL + "%s&child_protection_rating=%s&length=" + count, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionRatings);
			System.out.println("allUpcomingMoviesUrl : " + URL);
		}
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * @param movieid
	 * @return
	 */
	public ArrayList<Movie> getMovieRecommendations(String movieid) {
		movies = new ArrayList<>();
		jsonObj = getMovieRecommendatiosJSON(movieid);
		System.out.println("jsonObj : " + jsonObj);
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				ref_id = hits.getJSONObject(i).getString("ref_id");
				jsonObj = getRecommendationDetails(ref_id);
				inventories = jsonObj.getJSONArray("inventories");
				jsonObj = inventories.getJSONObject(0);
				movie = gson.fromJson(jsonObj.toString(), Movie.class);
				movies.add(movie);
			}
		}
		return movies;
	}

	/**
	 * Method to get recommendations movie details
	 * 
	 * @param id
	 * @return
	 */
	public JSONObject getRecommendationDetails(String id) {
		URL = String.format(URLs.inventoriesURL + id);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get all 15 movie recommendations
	 * 
	 * @param movieid
	 * @return
	 */
	public JSONObject getMovieRecommendatiosJSON(String movieid) {
		URL = String.format(URLs.moviesYMLURL.replaceFirst("#ref_id", movieid) + BaseTest.currentAccountRegion);
		URL = URL.replace("tms:", "tms%3A").replace(",", "%2C");
		System.out.println("YML url : " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public Show getShowDetailsByID(String showid) {
		jsonObj = getseriesRecommendationDetails(showid);
		inventories = jsonObj.getJSONArray("inventories");
		jsonObj = inventories.getJSONObject(0);
		show = gson.fromJson(jsonObj.toString(), Show.class);
		return show;
	}

	/**
	 * Method to get all Movie Recommendation (All Movies screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public ArrayList<Show> getSeriesRecommendations(String showid) {
		shows = new ArrayList<>();
		jsonObj = getSeriesRecommendatiosJSON(showid);
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				ref_id = hits.getJSONObject(i).getString("ref_id");
				jsonObj = getseriesRecommendationDetails(ref_id);
				inventories = jsonObj.getJSONArray("inventories");
				jsonObj = inventories.getJSONObject(0);
				show = gson.fromJson(jsonObj.toString(), Show.class);
				shows.add(show);
			}
		}
		return shows;
	}

	/**
	 * 
	 * @param showid
	 * @return
	 */
	public JSONObject getSeriesRecommendatiosJSON(String showid) {
		URL = String.format(URLs.seriesYMLURL.replaceFirst("#ref_id", showid));
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public JSONObject getseriesRecommendationDetails(String id) {
		URL = String.format(URLs.ymlrelatedSeriesURL + id);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get all Shows (All Shows screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public ArrayList<Show> getAllShows(int count) {
		shows = new ArrayList<>();
		jsonObj = getAllShowsJSON(count);
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length() && i < count; i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "series":
					jsonObj = result.getJSONObject("series");
					break;
				}
				show = gson.fromJson(jsonObj.toString(), Show.class);
				shows.add(show);
			}
		}
		return shows;
	}

	/**
	 * Method to get all Shows JSON Object (All Shows screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public JSONObject getAllShowsJSON(int count) {
		if (ConfigurationConstants.deviceType.equalsIgnoreCase("phone") || ConfigurationConstants.deviceType.equalsIgnoreCase("tablet")) {
			URL = String.format(URLs.MobileShowsURL + "%s&child_protection_rating=%s&length=" + count, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionRatings);
		} else {
			URL = String.format(URLs.AllShowsURL + "%s&child_protection_rating=%s&length=" + count, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionRatings);
			System.out.println("Shows Url : "+URL);
		}
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get all Episodes for series (All Shows screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public ArrayList<Episode> getAllEpisodes(String series_id, String season_number) {
		episodes = new ArrayList<>();
		jsonObj = getAllEpisodesJSON(series_id, season_number);
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "vod":
					jsonObj = result.getJSONObject("vod");
					break;
				case "shared_ref":
					jsonObj = result.getJSONObject("shared_ref");
					assetArray = jsonObj.getJSONArray("assets");
					assets = new ArrayList<>();
					for (int j = 0; j < assetArray.length(); j++) {
						assetObj = assetArray.getJSONObject(j);
						asset = gson.fromJson(assetObj.toString(), Assets.class);
						assets.add(asset);
					}
					break;
				case "program":
					jsonObj = result.getJSONObject("program");
					break;
				}
				episode = gson.fromJson(jsonObj.toString(), Episode.class);
				episode.setAssets(assets);
				episodes.add(episode);
			}
		}
		return episodes;
	}

	/**
	 * Getting all episodes for a given show
	 * 
	 * @param series_id
	 * @return
	 */
	public ArrayList<Episode> getAllEpisodes(String series_id) {
		episodes = new ArrayList<>();
		jsonObj = getAllEpisodesJSON(series_id);
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "vod":
					jsonObj = result.getJSONObject("vod");
					break;
				case "shared_ref":
					jsonObj = result.getJSONObject("shared_ref");
					assetArray = jsonObj.getJSONArray("assets");
					assets = new ArrayList<>();
					for (int j = 0; j < assetArray.length(); j++) {
						assetObj = assetArray.getJSONObject(j);
						asset = gson.fromJson(assetObj.toString(), Assets.class);
						assets.add(asset);
					}
					break;
				case "program":
					jsonObj = result.getJSONObject("program");
					break;
				}
				episode = gson.fromJson(jsonObj.toString(), Episode.class);
				episode.setAssets(assets);
				episodes.add(episode);
			}
		}
		return episodes;
	}

	/**
	 * Method to get all Episodes for series (All Shows screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public ArrayList<Seasons> getAllSeasons(String series_id) {
		seasons = new ArrayList<>();
		jsonObj = getAllSeasonsJSON(series_id);
		total = jsonObj.getInt("total");
		if (total > 0){
			hits = jsonObj.getJSONArray("navigators");
			int count = hits.getJSONObject(0).getInt("count");
			if (count > 0) {
				inventories = hits.getJSONObject(0).getJSONArray("entries");
				for (int i = 0; i < inventories.length(); i++) {
					seasonObj = inventories.getJSONObject(i);
					season = gson.fromJson(seasonObj.toString(), Seasons.class);
					seasons.add(season);
				}
			}
		}else{
			System.out.println("No seasons for given show");
		}
		return seasons;
	}

	/**
	 * Method to get all episodes JSON Object (All Shows screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public JSONObject getAllPlayableEpisodesJSON(String series_id, String season_number) {
		if (season_number.equals("")) {
			URL = String.format(URLs.episodePlayableURL, BaseTest.currentAccountRegion).replace("#series_id", series_id).replace("season_number=#season_number", season_number);
		} else {
			URL = String.format(URLs.episodePlayableURL, BaseTest.currentAccountRegion).replace("#series_id", series_id).replace("#season_number", season_number);
		}
		System.out.println(URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public JSONObject getAllUpcomingEpisodesJSON(String series_id, String season_number) {
		if (season_number.equals("")) {
			URL = String.format(URLs.episodeUpcomingURL, BaseTest.currentAccountRegion).replace("#series_id", series_id).replace("season_number=#season_number", season_number);
		} else {
			URL = String.format(URLs.episodeUpcomingURL, BaseTest.currentAccountRegion).replace("#series_id", series_id).replace("#season_number", season_number);
		}
		System.out.println(URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get all episodes JSON Object (All Shows screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public JSONObject getAllEpisodesJSON(String series_id, String season_number) {
		URL = String.format(URLs.episodeURL, BaseTest.currentAccountRegion).replace("#series_id", series_id).replace("#season_number", season_number);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get all episodes JSON Object (All Shows screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public JSONObject getAllEpisodesJSON(String series_id) {
		URL = String.format(URLs.episodeURLWithSeriesId, BaseTest.currentAccountRegion).replace("#series_id", series_id);
		// System.out.println(URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public ArrayList<Episode> getAllPlayableEpisodes(String series_id, String season_number) {
		episodes = new ArrayList<>();
		jsonObj = getAllPlayableEpisodesJSON(series_id, season_number);
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0].toString()) {
				case "vod":
					jsonObj = result.getJSONObject("vod");
					break;
				case "shared_ref":
					jsonObj = result.getJSONObject("shared_ref");
					assetArray = jsonObj.getJSONArray("assets");
					assets = new ArrayList<>();
					for (int j = 0; j < assetArray.length(); j++) {
						assetObj = assetArray.getJSONObject(j);
						asset = gson.fromJson(assetObj.toString(), Assets.class);
						assets.add(asset);
					}
					break;
				case "program":
					jsonObj = result.getJSONObject("program");
					break;
				}
				episode = gson.fromJson(jsonObj.toString(), Episode.class);
				episode.setAssets(assets);
				episodes.add(episode);
			}
		}
		return episodes;
	}

	/**
	 * Method to get all Episodes for series (All Shows screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public ArrayList<Episode> getAllUpcomingEpisodes(String series_id, String season_number) {
		episodes = new ArrayList<>();
		jsonObj = getAllUpcomingEpisodesJSON(series_id, season_number);
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "vod":
					jsonObj = result.getJSONObject("vod");
					break;
				case "shared_ref":
					jsonObj = result.getJSONObject("shared_ref");
					assetArray = jsonObj.getJSONArray("assets");
					assets = new ArrayList<>();
					for (int j = 0; j < assetArray.length(); j++) {
						assetObj = assetArray.getJSONObject(j);
						asset = gson.fromJson(assetObj.toString(), Assets.class);
						assets.add(asset);
					}
					break;
				case "program":
					jsonObj = result.getJSONObject("program");
					break;
				}
				episode = gson.fromJson(jsonObj.toString(), Episode.class);
				episode.setAssets(assets);
				episodes.add(episode);
			}
		}
		return episodes;
	}

	/**
	 * Method to get all episodes JSON Object (All Shows screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public JSONObject getAllSeasonsJSON(String series_id) {
		URL = String.format(URLs.seasonsURL, BaseTest.currentAccountRegion).replaceFirst("#seriesId", series_id);
		System.out.println("getAllSeasonsJSON : "+URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get all live and upcoming Movies (All movies screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public ArrayList<Movie> getAllLiveAndUpcomingMovies(int count) {
		jsonObj = getAllLiveAndUpcomingMoviesJSON(count);
		movies = getMoviesForJsonObject(jsonObj, count);
		return movies;
	}

	/**
	 * Method to get all live and upcoming Movie JSON (All movies screen contents)
	 * 
	 * @param count
	 * @return
	 */
	public JSONObject getAllLiveAndUpcomingMoviesJSON(int count) {
		if (ConfigurationConstants.deviceType.equalsIgnoreCase("phone")) {
			URL = String.format(URLs.MobileMoviesUpcomingAndLiveURL + "%s&child_protection_rating=%s&length=" + count, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionRatings);
			System.out.println("allLiveAndUpcomingMoviesUrl : " + URL);
		} else {
			URL = String.format(URLs.AllMoviesURL + "%s&child_protection_rating=%s&length=" + count, BaseTest.currentAccountRegion, ConfigurationConstants.childProtectionRatings);
			// System.out.println("allMoviesUrl : "+ URL);
		}
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get client configuration.
	 * 
	 * @param deviceFirmware
	 * @param device
	 * @param deviceLocale
	 * @return
	 */
	public ClientConfig getClientConfiguration(String deviceFirmware, String device, String deviceLocale) {
		jsonObj = getClientConfigJSONJSON(deviceFirmware, device, deviceLocale);
		return gson.fromJson(jsonObj.toString(), ClientConfig.class);
	}

	/**
	 * Method to get client configurationJson
	 * 
	 * @param deviceFirmware,device,deviceLocale
	 * @return
	 */
	public JSONObject getClientConfigJSONJSON(String deviceFirmware, String device, String deviceLocale) {
		URL = String.format(URLs.ClientConfigURL + "firmware=%s&device=%s&locale=%s", deviceFirmware, device, deviceLocale);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	// ************************** PayTV related Flavors
	// ********************************\\
	/**
	 * Method for getting ext_asserion for other than CSpire flavor applications
	 * only
	 * 
	 * @return access_token
	 */
	public String getExtAssertionForIdamService() {
		token_url = URLs.TokenURL;
		try {
			url = new URL(token_url);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			dout = new DataOutputStream(connection.getOutputStream());
			urlParameters = "{\"grant_type\" :\"password\",\"email\" : \"" + ConfigurationConstants.userName + "\",\"native_device_id\" :\"" + ConfigurationConstants.nativeDeviceID + "\",\"oauth_client_id\" :\"" + ConfigurationConstants.oauthClientID + "\",\"oauth_client_secret\" :\"" + ConfigurationConstants.oauthClientSecret + "\",\"password\":\"" + ConfigurationConstants.password + "\"}";
			dout.write(urlParameters.getBytes("UTF8"));
			dout.flush();
			dout.close();
			return getJSONFromConnection(connection).getString("access_token");
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * This method is for getting BillingID
	 * 
	 * @param token
	 * @return billingID
	 */
	public String getBillingIDForIdamService(String token) {
		try {
			billingID_url = URLs.BillingIDURL;
			billingID_url = String.format(billingID_url + "%s", token);
			url = new URL(billingID_url);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			return getJSONFromConnection(connection).getString("billing_id");
		} catch (IOException | JSONException e) {
			return null;
		}
	}

	// ************************** CSpire related Only
	// ********************************\\
	/**
	 * Method for getting Account ominiaid, applicable only for CSpire
	 * 
	 * @param cspire_Acess_Token
	 * @return ominiaid
	 */
	public String getOminiaidForCSpire(String cspire_Acess_Token) {
		switch (ConfigurationConstants.host) {
		case "integration":
			ominiaid_url = URLs.CSpireIntegrationOmniaIDURL;
			break;
		default:
			ominiaid_url = URLs.CSpireOmniaIDURL;
		}
		ominiaid_url = String.format(ominiaid_url + "%s", cspire_Acess_Token);
		try {
			url = new URL(ominiaid_url);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("Request Method", "POST");
			connection.setRequestProperty("Authorization", ConfigurationConstants.autherization);
			connection.connect();
			ominiaid = getJSONFromConnection(connection).getJSONObject("principal").getJSONObject("attributes").getString("omnia_id");
			return ominiaid;
		} catch (IOException | JSONException e) {
			return null;
		}
	}

	/**
	 * Method for getting ext_asserion for CSpire flavor application only
	 * 
	 * @return access_token
	 */
	public String getExtAssertionForCSpire() {
		try {
			switch (ConfigurationConstants.host) {
			case "integration":
				token_url = URLs.CSpireIntegrationTokenURL;
				break;
			default:
				token_url = URLs.CSpireTokenURL;
			}
			url = new URL(token_url);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Authorization", ConfigurationConstants.autherization);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			dout = new DataOutputStream(connection.getOutputStream());
			inputLine = "username=" + URLEncoder.encode(ConfigurationConstants.userName, "UTF-8") + "&" + "password=" + URLEncoder.encode(ConfigurationConstants.password, "UTF-8") + "&" + "grant_type=password";
			dout.writeBytes(inputLine);
			dout.flush();
			dout.close();
			return getJSONFromConnection(connection).getString("access_token");
		} catch (IOException | JSONException e) {
			System.out.println(e);
			return null;
		}
	}

	/**
	 * This method retrieves shows. Takes each show and checks it's count in recents
	 * object. If count matches recentItemsCount. Then that Show object is returned.
	 * 
	 * @param profileId
	 *            current active profile id
	 * @param isCompleted
	 *            if content should be completed or partially watched
	 * @param refType
	 *            i.e. program, series, shared ref etc
	 * @param recentItemsCount
	 *            search for no of watched episodes
	 * @return Show
	 */
	public Show getSeriesForNoOfEpisodesWatched(String profileId, boolean isCompleted, String refType, int recentItemsCount) {
		URL = String.format(URLs.RecentsURL, profileId, isCompleted, refType);
		jsonObj = getJSONFromConnection(getSimpleConnection(URL, getAuthorization()));
		Recents recents = jsonObj != null ? gson.fromJson(jsonObj.toString(), Recents.class) : null;
		if (recents != null && recents.recentlist_items != null) {
			List<RecentsListItem> recentItems = recents.recentlist_items;
			shows = getAllShows(200);
			if (shows.size() <= 0)
				return null;
			int watchedEpisodes = 0;
			for (int i = 0; i < shows.size(); i++) {
				watchedEpisodes = 0;
				Show show = shows.get(i);
				for (int j = 0; j < recentItems.size(); j++) {
					if (recentItems.get(j).ref_id.equals(show.getID())) {
						watchedEpisodes += 1;
					}
				}
				if (watchedEpisodes == recentItemsCount) {
					if(showHasPlayableEpisode(show)){
						return show;
					}
				}
			}
		}
		return null;
	}

	/**
	 * This method retrieves shows. Takes each show and checks it's count in recents
	 * object. If count matches recentItemsCount. Then that Show object is returned.
	 * 
	 * @param profileId
	 *            current active profile id
	 * @param isCompleted
	 *            if content should be completed or partially watched
	 * @param refType
	 *            i.e. program, series, shared ref etc
	 * @param recentItemsCount
	 *            search for no of watched episodes
	 * @return Show
	 */
	public ArrayList<Show> getSeriesListForNoEpisodesWatched(String profileId, boolean isCompleted, String refType, int recentItemsCount) {
		ArrayList<Show> noWatchedShows = new ArrayList<Show>();
		show = new Show();
		URL = String.format(URLs.RecentsURL, profileId, isCompleted, refType);
		jsonObj = getJSONFromConnection(getSimpleConnection(URL, getAuthorization()));
		Recents recents = jsonObj != null ? gson.fromJson(jsonObj.toString(), Recents.class) : null;
		if (recents != null && recents.recentlist_items != null) {
			List<RecentsListItem> recentItems = recents.recentlist_items;
			shows = getAllShows(200);
			if (shows.size() <= 0)
				return null;
			int watchedEpisodes = 0;
			for (int i = 0; i < shows.size(); i++) {
				watchedEpisodes = 0;
				Show show = shows.get(i);
				for (int j = 0; j < recentItems.size(); j++) {
					if (recentItems.get(j).ref_id.equals(show.getID())) {
						watchedEpisodes += 1;
					}
				}
				System.out.println("Watched episodes" + watchedEpisodes);
				if (watchedEpisodes == recentItemsCount) {
					noWatchedShows.add(show);
				}
			}
		}
		return noWatchedShows;
	}

	/**
	 * Retrieves recent data for given ref type and ref id.
	 * 
	 * @param authorization
	 * @param profileId
	 *            current active profile id
	 * @param isCompleted
	 *            if content should be completed or partially watched
	 * @param refType
	 *            i.e. program, series, shared ref etc. refId i.e. series id,
	 *            program id etc.
	 * @return recent list items for given ref id.
	 */
	public List<RecentsListItem> getRecentsForRefId(String profileId, boolean isCompleted, String refType, String refId) {
		String authorization = getAuthorization();
		URL = String.format(URLs.RecentsURL, profileId, isCompleted, refType);
		jsonObj = getJSONFromConnection(getSimpleConnection(URL, authorization));
		Recents recents = jsonObj != null ? gson.fromJson(jsonObj.toString(), Recents.class) : null;
		List<RecentsListItem> recentsListItems = new ArrayList<RecentsListItem>();
		if (recents != null && recents.recentlist_items != null) {
			List<RecentsListItem> recentItems = recents.recentlist_items;
			for (int i = 0; i < recentItems.size(); i++) {
				if (recentItems.get(i).ref_id.equals(refId)) {
					recentsListItems.add(recentItems.get(i));
				}
			}
		}
		return recentsListItems;
	}

	/**
	 * Retrieves true if given ref type and ref id has recents data, false
	 * otherwise.
	 * 
	 * @param authorization
	 * @param profileId
	 *            current active profile id
	 * @param isCompleted
	 *            if content should be completed or partially watched
	 * @param refType
	 *            i.e. program, series, shared ref etc.
	 * @param refId
	 *            i.e. series id, program id etc.
	 * @return
	 */
	public boolean hasRecentForRefId(String profileId, boolean isCompleted, String refType, String refId) {
		List<RecentsListItem> recentsListItems = getRecentsForRefId(profileId, isCompleted, refType, refId);
		return recentsListItems.size() > 0 ? true : false;
	}

	/**
	 * 
	 * @param show
	 * @return true if show has any playable episode
	 */
	public boolean showHasPlayableEpisode(Show show) {
		List<Episode> episodes = new ArrayList<>();
		episodes = getPlayableEpisodesForSeries(show);
		if (!episodes.isEmpty()) {
			return true;
		} else {
			SeriesRecording recording = new RecordingsJSONUtility().getSeriesRecordingById(show.getID());
			if (recording != null) {
				return true;
			}
		}
		return false;
	}

	public List<Episode> getPlayableEpisodesForSeries(Show show) {
		return getAllPlayableEpisodes(show.getID(), "");
	}

	/**
	 * 
	 * @param show
	 * @return true if show has Live OOH playable episode
	 */
	public boolean showHasLiveOOHPlayableEpisode(Show show) {
		List<Episode> episodes = new ArrayList<>();
		episodes = getPlayableEpisodesForSeries(show);
		for (Episode episode : episodes) {
			if (new Utility().isLive(episode.getStart_time(), episode.getEnd_time()) && isChannelOOHPlayable(episode)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param episode
	 * @return 'true' if the channel is OOH playable
	 */
	public boolean isChannelOOHPlayable(Episode episode) {
		ChannelsJSONUtility channelsJSONUtility = new ChannelsJSONUtility();
		List<Assets> episodeAssets = episode.getAssets();
		if (!episodeAssets.isEmpty()) {
			for (Assets asset : episodeAssets) {
				Channel channel = channelsJSONUtility.getChannelsDetails(asset.getChannelId());
				if (channel.is_out_of_home_playback_enabled()) {
					return true;
				}
			}
		} else {
			Channel channel = channelsJSONUtility.getChannelsDetails(episode.getChannelId());
			if (channel.is_out_of_home_playback_enabled()) {
				return true;
			}
		}
		return false;
	}

	public List<RecentsListItem> getRecents(String profileId) {
		String authorization = getAuthorization();
		URL = String.format(URLs.RecentsAll, profileId);
		System.out.println(URL);
		jsonObj = getJSONFromConnection(getSimpleConnection(URL, authorization));
		System.out.println(jsonObj);
		Recents recents = jsonObj != null ? gson.fromJson(jsonObj.toString(), Recents.class) : null;
		List<RecentsListItem> recentsListItems = new ArrayList<RecentsListItem>();
		if (recents != null && recents.recentlist_items != null) {
			List<RecentsListItem> recentItems = recents.recentlist_items;
			for (int i = 0; i < recentItems.size(); i++) {
				recentsListItems.add(recentItems.get(i));
			}
		}
		return recentsListItems;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public JSONObject getProgramRecommendationDetailsJSON(String id) {
		URL = String.format(URLs.programURL + id);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public JSONObject getSharedRefRecommendationDetailsJSON(String id) {
		URL = String.format(URLs.movieDetailsURL + id);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public JSONObject getChannelRecommendationDetailsJSON(String id) {
		URL = String.format(URLs.ChannelProgramsURL + id,new DateAndTime().getCurrentEpochTime());
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public JSONObject getVODRecommendationDetailsJSON(String id) {
		URL = String.format(URLs.inventoriesURL + id);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method for getting all series recordings data in the form of list of objects
	 * 
	 * @return List<RecommendListItem>
	 */
	public ArrayList<RecommendListItem> getRecommend(String tab, String profileId) {
		ArrayList<RecommendListItem> recommendListItems;
		Type listType = new TypeToken<List<RecommendListItem>>() {}.getType();
		try {
			String authorization = getAuthorization();
			System.out.println("token : " + authorization);
			if (tab.contains("series")) {
				URL = String.format(URLs.seriesYMLURLv1, profileId, BaseTest.currentAccountRegion);
			} else {
				URL = String.format(URLs.moviesYMLURL, profileId);
			}
			System.out.println(URL);
			jsonObj = getJSONFromConnection(getSimpleConnection(URL, authorization));
			System.out.println(jsonObj);
			recommendListItems = gson.fromJson(jsonObj.getJSONArray("hits").toString(), listType);
			if (jsonObj.getInt("total") != 0) {
				return recommendListItems;
			}
		} catch (NullPointerException | JSONException e) {
			return null;
		}
		return null;
	}

	public ArrayList<SharedRef> getSharedRefIds(JSONObject jsonObj) {
		ArrayList<SharedRef> sharedRefIds = new ArrayList<>();
		total = jsonObj.getInt("total");
		if (total > 0) {
			try {
				hits = jsonObj.getJSONArray("hits");
				for (int i = 0; i < hits.length() && i < 5; i++) {
					result = hits.getJSONObject(i).getJSONObject("result");
					jsonObj = result.getJSONObject("shared_ref");
					sharedRef = gson.fromJson(jsonObj.toString(), SharedRef.class);
					sharedRefIds.add(sharedRef);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sharedRefIds;
	}

	public List<Assets> getAssetromJSON(JSONObject jsonObject) {
		total = jsonObject.getInt("total");
		Assets assetData = null;
		List<Assets> assetList = new ArrayList<>();
		try{
			if (total > 0) {
				JSONArray array = jsonObject.getJSONArray("shared_refs");
				for (int i = 0; i < array.length(); i++) {
					JSONObject obj = array.getJSONObject(i);
					JSONArray assetArray = obj.getJSONArray("assets");
					for (int j = 0; j < assetArray.length(); j++) {
						JSONObject assetObject = assetArray.getJSONObject(j);
						JSONObject object = assetObject.getJSONObject("asset");
						keys = JSONObject.getNames(object);
						switch (keys[0]) {
						case "vod":
							refTypeObj = object.getJSONObject("vod");
							break;
						case "program":
							refTypeObj = object.getJSONObject("program");
							break;
						}
						assetData = gson.fromJson(refTypeObj.toString(), Assets.class);
						assetList.add(assetData);
					}
				}
			}
		}catch(Exception e){}
		
		return assetList;
	}

	/**
	 * 
	 * @return ArrayList<Channel> that are part of purchased or free offer.
	 */
	public ArrayList<Channel> getListOfPurchasedChannels() {
		Utility utility = new Utility();
		LinkedHashSet<Channel> allchannel = BaseTest.allChannelsData;
		ArrayList<Channel> allPurchasedChannel = new ArrayList<>();
		List<String> skuIds = null;
		HashSet<String> allSkuIds = getAllPlayableSkuIds();
		if (utility.isValidCollection(allchannel) && utility.isValidCollection(allSkuIds)) {
			for (Channel channel : allchannel) {
				skuIds = channel.getSku_ids();
				if (utility.isValidCollection(skuIds)) {
					for (String skuId : skuIds) {
						if (allSkuIds.contains(skuId)) {
							allPurchasedChannel.add(channel);
							break;
						}
					}
				}
			}
		}
		return allPurchasedChannel;
	}

	/**
	 * 
	 * @param channel
	 * @return true if the channel has sku_ids that are part of 'purchased offers'
	 *         or 'free offers'
	 */
	public boolean isChannelPurchased(Channel channel) {
		Utility utility = new Utility();
		List<String> skuIds = channel.getSku_ids();
		HashSet<String> allSkuIds = getAllPlayableSkuIds();
		if (utility.isValidCollection(skuIds) && utility.isValidCollection(allSkuIds)) {
			for (String skuId : skuIds) {
				if (allSkuIds.contains(skuId)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @param channel
	 * @return true if the channel has the recording rights. The recording rights
	 *         are decided on the basis that if the "sku_ids" associated with the
	 *         channel are part of the offers that are purchased for that particular
	 *         user account
	 */
	public boolean isChannelPurchasedForRecordingRights(Channel channel) {
		Utility utility = new Utility();
		List<String> skuIds = channel.getSku_ids();
		ArrayList<String> purchasedSkuIds = getAllRecordableSkuIds();
		if (utility.isValidCollection(skuIds) && utility.isValidCollection(purchasedSkuIds)) {
			for (String skuId : skuIds) {
				if (purchasedSkuIds.contains(skuId)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @return HashSet<String> of skuIds. These skuIds are associated with the
	 *         channels that are available for play-back for that particular account
	 */
	public HashSet<String> getAllPlayableSkuIds() {
		Utility utility = new Utility();
		HashSet<String> allSkusIds = new HashSet<>();
		if (utility.isValidCollection(BaseTest.freeSkuIds)) {
			allSkusIds.addAll(BaseTest.freeSkuIds);
		}
		if (utility.isValidCollection(BaseTest.purchasedSkuIds)) {
			allSkusIds.addAll(BaseTest.purchasedSkuIds);
		}
		return allSkusIds;
	}

	/**
	 * 
	 * @return ArrayList<String> of all the Recordable skuIds. In case of a "free
	 *         offer", the channels associated with this offer are "Playable" but
	 *         they are not "Recordable" until and unless that offer is purchased
	 *         for that particular user account. So to check if channel is
	 *         recordable, we will do the comparison with the purchased skuid's.
	 */
	public ArrayList<String> getAllRecordableSkuIds() {
		return BaseTest.purchasedSkuIds;
	}

	/**
	 * 
	 * @return HashSet<String> of all the offerId's
	 */
	// TODO When we complete the upcoming implementation of showing 'only
	// subscribed' contents in the app, need to stop adding "subscriptionOfferIds"
	// to "allOfferIds".
	public HashSet<String> getAllOfferIds() {
		HashSet<String> allOfferIds = new HashSet<>();
		Utility utility = new Utility();
		if (utility.isValidCollection(BaseTest.freeOfferIds)) {
			allOfferIds.addAll(BaseTest.freeOfferIds);
		}
		if (utility.isValidCollection(BaseTest.subscriptionOfferIds)) {
			allOfferIds.addAll(BaseTest.subscriptionOfferIds);
		}
		if (utility.isValidCollection(BaseTest.purchasedOfferIds)) {
			allOfferIds.addAll(BaseTest.purchasedOfferIds);
		}
		return allOfferIds;
	}

	public List<Movie> getMovieData(ArrayList<Program> programs) {
		DateAndTime dateTime = new DateAndTime();
		List<Movie> movies = new ArrayList<>();
		for (Program program : programs) {
			Movie movie = new Movie();
			if (program.getCategory().get(0).equalsIgnoreCase("movies") && program.getStart_time() > dateTime.getCurrentEpochTime()) {
				if (program.getType().equalsIgnoreCase("shared_ref")) {
					String sharedRefId = program.getID();
					JSONObject jsonObject = fetchSharedAssets(sharedRefId);
					List<Assets> assetArray = getAssetromJSON(jsonObject);
					movie.setAssets(assetArray);
					movie.setChild_protection_rating(assetArray.get(0).getchild_protection_rating());
				} else {
					movie.setChild_protection_rating(program.getChild_protection_rating());
					movie.setStart_time(program.getStart_time());
					movie.setEnd_time(program.getEnd_time());
				}
				movie.setActors_list(program.getActors_list());
				movie.setDuration(String.valueOf(program.getDuration()));
				movie.setName(program.getName());
				movie.setType(program.getType());
				movie.setYear(program.getYear());
				movie.setDescription(program.getDescription());
				movie.setProvider_networks(program.getProvider_networks());
				movie.setGenre_list(program.getGenre_list());
				movies.add(movie);
			}
		}
		return movies;
	}

	/**
	 * Method to get all feature row Movies and feature row name map
	 *
	 * @param
	 * @return
	 */
	public LinkedHashMap<String, ArrayList<Movie>> getAllFeatureRowMovies() {
		jsonObj = getMoviesFeaturePageJSON();
		featureMovies = getFeaturedRowMoviesForJsonObject(jsonObj);
		return featureMovies;
	}

	/**
	 * @param jsonObj
	 * @return
	 */
	public LinkedHashMap<String, ArrayList<Movie>> getFeaturedRowMoviesForJsonObject(JSONObject jsonObj) {
		featureMovies = new LinkedHashMap<>();
		JSONArray jsonarray = jsonObj.getJSONArray("tiles");
		for (int i = 0; i < jsonarray.length(); i++) {
			String name = jsonarray.getJSONObject(i).getString("tile_name");
			System.out.println("Row Name : " + name);
			if (!jsonarray.getJSONObject(i).has("tile_items") || name.toLowerCase().contains("network")) {
				if (name.toLowerCase().contains("network") || name.toLowerCase().contains("record") || name.toLowerCase().contains("resume") || name.toLowerCase().contains("relate") || name.toLowerCase().contains("recommend")) {
					// TODO always check the latest requirement for local rows
					featureMovies.put(name, null);
				}
			} else {
				ArrayList<Movie> featureRowMovies = new ArrayList<>();
				for (int j = 0; j < jsonarray.getJSONObject(i).getJSONArray("tile_items").length(); j++) {
					jsonObj = jsonarray.getJSONObject(i).getJSONArray("tile_items").getJSONObject(j);
					GsonBuilder gsonBuilder = new GsonBuilder();
					Gson gson = gsonBuilder.create();
					Movie movie = gson.fromJson(jsonObj.toString(), Movie.class);
					featureRowMovies.add(movie);
				}
				featureMovies.put(name, featureRowMovies);
			}
		}
		return featureMovies;
	}

	/**
	 * @param jsonObj
	 * @return
	 */
	public ArrayList<String> getNetworkRowFromJSONobj(JSONObject jsonObj) {
		networkNames = new ArrayList<>();
		JSONArray jsonarray = jsonObj.getJSONArray("tiles");
		for (int i = 0; i < jsonarray.length(); i++) {
			String name = jsonarray.getJSONObject(i).getString("tile_name");
			System.out.println("Row Name : " + name);
			if (name.toLowerCase().contains("network")) {
				JSONArray jsonAry = jsonarray.getJSONObject(i).getJSONArray("tile_items");
				System.out.println("no of network " + jsonAry.length());
				for (int j = 0; j < jsonAry.length(); j++)
					networkNames.add(jsonAry.getJSONObject(j).getString("name").trim().replaceAll("\\s+", "+"));
				break;
			}
		}
		return networkNames;
	}

	/**
	 * @return networkNames
	 */
	public ArrayList<String> getMoviesPageNetworkNames() {
		jsonObj = getMoviesFeaturePageJSON();
		networkNames = getNetworkRowFromJSONobj(jsonObj);
		return networkNames;
	}

	/**
	 * @return networkNames
	 */
	public ArrayList<String> getShowsPageNetworkNames() {
		jsonObj = getShowsFeaturePageJSON();
		networkNames = getNetworkRowFromJSONobj(jsonObj);
		for (String name : networkNames)
			System.out.println(name);
		return networkNames;
	}

	/**
	 * @return networkNames
	 */
	public ArrayList<String> getAllFeaturedNetworkNames() {
		jsonObj = getAllFeaturedNetworkJSON();
		networkNames = getFeaturedNetworkNamesFromJSONobj(jsonObj);
		return networkNames;
	}

	/**
	 * @param jsonObj
	 * @return
	 */
	public ArrayList<String> getFeaturedNetworkNamesFromJSONobj(JSONObject jsonObj) {
		networkNames = new ArrayList<>();
		JSONArray jsonarray = jsonObj.getJSONArray("navigators").getJSONObject(0).getJSONArray("entries");
		for (int i = 0; i < jsonarray.length(); i++) {
			String name = jsonarray.getJSONObject(i).getString("name").toString();
			// System.out.println("Row Name : " + name);
			networkNames.add(name);
		}
		return networkNames;
	}

	/**
	 * @return
	 */
	public JSONObject getAllFeaturedNetworkJSON() {
		URL = String.format(URLs.AllFeaturedNetworkURL);
		System.out.println("FeaturedNetwork : " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * @return
	 */
	public JSONObject getMoviesFeaturePageJSON() {
		URL = String.format(URLs.MoviesFeatureURL + BaseTest.currentAccountRegion);
		System.out.println("Movies Feature Page : " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * @return
	 */
	public JSONObject getShowsFeaturePageJSON() {
		URL = String.format(URLs.SeriesFeatureURL + BaseTest.currentAccountRegion);
		System.out.println("Shows Feature Page : " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get all Shows (All Shows screen contents)
	 *
	 * @param count
	 * @return
	 */
	public ArrayList<Show> getAllShowsDetail() {
		ArrayList<String> metadataRootIDs = getAllShowsRootID();
		jsonObj = getShowsDetailJSON(metadataRootIDs);
		shows = new ArrayList<>();
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("inventories");
			for (int i = 0; i < hits.length(); i++) {
				jsonObj = hits.getJSONObject(i);
				show = gson.fromJson(jsonObj.toString(), Show.class);
				shows.add(show);
			}
		}
		return shows;
	}

	/**
	 * Method to get all Shows (All Shows screen contents)
	 *
	 * @param count
	 * @return
	 */
	public ArrayList<String> getAllShowsRootID() {
		ArrayList<String> metadataRootIDs = new ArrayList<>();
		jsonObj = getBrowseAllShowsJSON();
		total = jsonObj.getInt("next_index");
		if (total > 0) {
			hits = jsonObj.getJSONArray("recommendation");
			for (int i = 0; i < hits.length(); i++)
				metadataRootIDs.add(hits.getJSONObject(i).getString("metadata_root_id"));
			// show = gson.fromJson(jsonObj.toString(), Show.class);
			// shows.add(show);
		}
		return metadataRootIDs;
	}

	/**
	 * Method to get all Shows JSON Object (All Shows screen contents)
	 *
	 * @param count
	 * @return
	 */
	public JSONObject getBrowseAllShowsJSON() {
		URL = String.format(URLs.BrowseAllSeriesURL, BaseTest.currentAccountRegion);
		System.out.println(URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get all Shows detail JSON Object (All Shows screen contents)
	 *
	 * @param count
	 * @return
	 */
	public JSONObject getShowsDetailJSON(ArrayList<String> metadataRootIDs) {
		String required = "";
		for (String s : metadataRootIDs)
			required = required + s + ",";
		URL = URLs.SeriesDetailURL_pop + required;
		System.out.println(URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public ArrayList<Movie> getMoviesPageRecommended() {
		ArrayList<String> metadataRootIDs = new ArrayList<>();
		jsonObj = getMoviesPageRecommendedJSON();
		total = jsonObj.getInt("next_index");
		if (total > 0) {
			hits = jsonObj.getJSONArray("recommendation");
			for (int i = 0; i < hits.length(); i++)
				metadataRootIDs.add(hits.getJSONObject(i).getString("metadata_root_id"));
		}
		jsonObj = getMoviesDetailJSON(metadataRootIDs);
		movies = new ArrayList<>();
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("shared_refs");
			for (int i = 0; i < hits.length(); i++) {
				jsonObj = hits.getJSONObject(i);
				movie = gson.fromJson(jsonObj.toString(), Movie.class);
				movies.add(movie);
			}
		}
		return movies;
	}

	public JSONObject getMoviesPageRecommendedJSON() {
		URL = String.format(URLs.MoviesRecommendedURL, BaseTest.currentProfileID, BaseTest.currentAccountRegion);
		System.out.println(URL);
		System.out.println(BaseTest.currentAccountRegion);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get all Shows detail JSON Object (All Shows screen contents)
	 *
	 * @param count
	 * @return
	 */
	public JSONObject getMoviesDetailJSON(ArrayList<String> metadataRootIDs) {
		String required = "";
		for (String s : metadataRootIDs)
			required = required + s + ",";
		URL = URLs.MoviesDetailURL + required;
		System.out.println(URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public HashMap<String, Show> getSeriesPageRecommendationForYou() {
		HashMap<String, Show> showsMap = new HashMap<String, Show>();
		JSONObject jsonObj = getSeriesPageRecommendedForYouJSON();
		System.out.println(jsonObj.toString());
		ArrayList<String> refIDs = new ArrayList<String>();
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++)
				refIDs.add(hits.getJSONObject(i).getString("ref_id"));
			System.out.println(refIDs);
			for (int i = 0; i < hits.length(); i++) {
				jsonObj = getseriesRecommendationDetails(refIDs.get(i));
				inventories = jsonObj.getJSONArray("inventories");
				jsonObj = inventories.getJSONObject(0);
				show = gson.fromJson(jsonObj.toString(), Show.class);
				System.out.println(show.getPrimaryMetaDataAccordingToLocalization());
				showsMap.put(show.getPrimaryMetaDataAccordingToLocalization(), show);
			}
		}
		return showsMap;
	}

	public JSONObject getSeriesPageRecommendedForYouJSON() {
		URL = String.format(URLs.SeriesRecommendedForYouURL, BaseTest.currentProfileID, BaseTest.currentAccountRegion);
		System.out.println(URL);
		System.out.println(BaseTest.currentAccountRegion);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public ArrayList<Show> getSeriesPageRecommended() {
		ArrayList<String> metadataRootIDs = new ArrayList<>();
		jsonObj = getSeriesPageRecommendedJSON();
		total = jsonObj.getInt("next_index");
		if (total > 0) {
			hits = jsonObj.getJSONArray("recommendation");
			for (int i = 0; i < hits.length(); i++)
				metadataRootIDs.add(hits.getJSONObject(i).getString("metadata_root_id"));
		}
		jsonObj = getShowsDetailJSON(metadataRootIDs);
		shows = new ArrayList<>();
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("inventories");
			for (int i = 0; i < hits.length(); i++) {
				jsonObj = hits.getJSONObject(i);
				show = gson.fromJson(jsonObj.toString(), Show.class);
				shows.add(show);
			}
		}
		return shows;
	}

	public JSONObject getSeriesPageRecommendedJSON() {
		URL = String.format(URLs.SeriesRecommendedURL, BaseTest.currentProfileID, BaseTest.currentAccountRegion);
		System.out.println(URL);
		System.out.println(BaseTest.currentAccountRegion);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	public ArrayList<Show> getSeriesPagePopular() {
		ArrayList<String> metadataRootIDs = new ArrayList<>();
		jsonObj = getSeriesPagePopularJSON();
		total = jsonObj.getInt("next_index");
		if (total > 0) {
			hits = jsonObj.getJSONArray("recommendation");
			for (int i = 0; i < hits.length(); i++)
				metadataRootIDs.add(hits.getJSONObject(i).getString("metadata_root_id"));
		}
		jsonObj = getShowsDetailJSON(metadataRootIDs);
		shows = new ArrayList<>();
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("inventories");
			for (int i = 0; i < hits.length(); i++) {
				jsonObj = hits.getJSONObject(i);
				show = gson.fromJson(jsonObj.toString(), Show.class);
				shows.add(show);
			}
		}
		return shows;
	}

	public JSONObject getSeriesPagePopularJSON() {
		URL = String.format(URLs.SeriesPopularURL, BaseTest.currentAccountRegion);
		System.out.println(URL);
		System.out.println(BaseTest.currentAccountRegion);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}
}
