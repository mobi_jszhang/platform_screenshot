package com.mobitv.client.qa.json;

import java.util.ArrayList;

import org.json.JSONObject;

import com.mobitv.client.qa.baseTests.BaseTest;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.constants.URLs;
import com.mobitv.client.qa.functions.Utility;
import com.mobitv.client.qa.models.PurchaseData;

public class PurchaseJSONUtility extends MainJSONUtility {
	private ArrayList<PurchaseData> allPurchaseData = null;
	private Utility utility = new Utility();
	
	/**
	 * 
	 * @return ArrayList<String> of sku_Ids associated with the purchased offers for the account with which user has login
	 */
	public ArrayList<String> fetchPurchasedSkuId() {
		ArrayList<String> purchasedSkuIds = new ArrayList<>();
		
		allPurchaseData = generatePurchasedData();
		
		if (utility.isValidCollection(allPurchaseData)) {
			for (PurchaseData purchaseData : allPurchaseData) {
				if (purchaseData.status.equalsIgnoreCase(Constants.ACTIVE) || purchaseData.status.equalsIgnoreCase(Constants.CANCELED)) {
					purchasedSkuIds.addAll(purchaseData.sku_ids);
				}
			}
			return purchasedSkuIds;
		}
		return null;
	}
	
	/**
	 * 
	 * @return ArrayList<String> of purchased offer Id's
	 */
	public ArrayList<String> fetchPurchasedOfferIds() {
		ArrayList<String> purchasedOffersIds = new ArrayList<>();
		
		if (allPurchaseData == null || allPurchaseData.isEmpty()) {
			allPurchaseData = generatePurchasedData();
		}
		
		if (utility.isValidCollection(allPurchaseData)) {
			for (PurchaseData purchaseData : allPurchaseData) {
				if (purchaseData.status.equalsIgnoreCase(Constants.ACTIVE) || purchaseData.status.equalsIgnoreCase(Constants.CANCELED)) {
					purchasedOffersIds.add(purchaseData.offer_id);
				}
			}
			return purchasedOffersIds;
		}
		return null;
		
	}
	
	/**
	 * 
	 * @return ArrayList<PurchaseData> of purchased offer data
	 */
	private ArrayList<PurchaseData> generatePurchasedData() {
		ArrayList<PurchaseData> allPurchaseData = new ArrayList<>();
		jsonObj = getPurchaseDataJSONObject();
		
		if (jsonObj != null) {
			purchase_infos = jsonObj.getJSONArray("purchase_infos");
			for (int i = 0; i < purchase_infos.length(); i++) {
				jsonObj = purchase_infos.getJSONObject(i);
				purchaseData = gson.fromJson(jsonObj.toString(), PurchaseData.class);
				allPurchaseData.add(purchaseData);
			}
			return allPurchaseData;
		}
		return null;
	}
	
	
	
	private JSONObject getPurchaseDataJSONObject() {
		String URL = String.format(URLs.PurchasedOffers, BaseTest.currentProfileID);
//		System.out.println("Purchase URL " + URL);
		return getJSONFromConnection(getSimpleConnection(URL, getAuthorization()));
	}
}
