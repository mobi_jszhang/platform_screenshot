package com.mobitv.client.qa.json;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.reflect.TypeToken;
import com.mobitv.client.qa.Comparators.AssetsStartTimeComparator;
import com.mobitv.client.qa.baseTests.BaseTest;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.constants.RecordingStatus;
import com.mobitv.client.qa.constants.URLs;
import com.mobitv.client.qa.functions.Utility;
import com.mobitv.client.qa.models.*;
import com.mobitv.client.qa.models.SeriesMovies.ImagesMetadata;

;

/**
 * 
 * @author MobiTV
 *
 */
public class RecordingsJSONUtility extends MainJSONUtility {
	private String usageSummaryAPI = null, recordingSummaryAPI = null;
	private JSONObject usageSummaryJSON;
	private JSONObject recordingsSummaryJSON = null;
	private HashSet<String> recordedTabSeriesNames = null;
	private HashSet<String> scheduledTabSeriesNames = null;
	private String programID, channelID, sharedRefID;
	private List<String> channelIDs;
	private SharedRef sharedRef;
	private ChannelsJSONUtility channelsJSONUtility = new ChannelsJSONUtility();
	private Recording record = null;
	private SeriesRecording seriesRecord = null;
	private static ArrayList<String> sharedRefprogramIDs, onGoingProgramIDs, completedProgramIDs, scheduledProgramIDs;
	private static HashMap<String, Recording> onGoingRecordings, completedRecordings, scheduledRecordings;
	private static HashMap<String, Recording> onGoingSharedRef, completedSharedRef, scheduledSharedRef;
	private static ArrayList<Recording> recordings = null;
	private static ArrayList<SeriesRecording> seriesRecordings = null;
	private static HashMap<String, List<Recording>> seriesRecordingsMap;
	ArrayList<SeriesInfo> seriesList;
	private static HashMap<String, List<String>> seriesChannelMap;

	/**
	 * Constructor
	 */
	public RecordingsJSONUtility() {
		parseAllRecordingsData();
	}

	/**
	 * Method for getting Usage summary JSON object
	 * 
	 * @param void
	 * @return JSONObject
	 */
	private JSONObject getUsageSummaryJSON() {
		try {
			str_Authorization = getAuthorization();
			usageSummaryAPI = URLs.UsageSummaryURL;
			usageSummaryAPI = String.format(usageSummaryAPI + "%s/usage_summary", BaseTest.currentProfileID);
			connection = getSimpleConnection(usageSummaryAPI, str_Authorization);
			connection.connect();
			return getJSONFromConnection(connection);
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Method for getting All Recordings summary JSON object
	 * 
	 * @param void
	 * @return JSONObject
	 */
	private JSONObject getRecordingsSummaryJSON() {
		try {
			str_Authorization = getAuthorization();
			recordingSummaryAPI = URLs.RecordingSummaryURL + "/%s/all?status=all";
			recordingSummaryAPI = String.format(recordingSummaryAPI, BaseTest.currentProfileID);
			connection = getSimpleConnection(recordingSummaryAPI, str_Authorization);
			connection.connect();
			return getJSONFromConnection(connection);
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * Method for getting all recordings data in the form of list of objects
	 * 
	 * @return List<Recording>
	 */
	public ArrayList<Recording> getAllRecordingsData() {
		Type listType = new TypeToken<List<Recording>>() {
		}.getType();
		try {

			if(recordingsSummaryJSON == null)
				recordingsSummaryJSON = getRecordingsSummaryJSON();

			return gson.fromJson(recordingsSummaryJSON.getJSONObject("recordings").getJSONArray("recording").toString(), listType);
		} catch (NullPointerException | JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Method for getting recordings name based on recording Id
	 * 
	 * @return String recording name
	 */
	public String getRecordingJsonName(String recordingId) {
		try {
			ArrayList<Recording> recordingItemList = getAllRecordingsData();
			for (int i = 0; i < recordingItemList.size(); i++) {
				String jsonId = recordingItemList.get(i).getID();
				if (jsonId.equals(recordingId)) {
					String recordingName = recordingItemList.get(i).getName();
					return recordingName;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return null;

	}

	/**
	 * Method for getting all series recordings data in the form of list of
	 * objects
	 * 
	 * @return List<SeriesRecording>
	 */
	private ArrayList<SeriesRecording> getAllSeriesRecordings() {
		Type listType = new TypeToken<List<SeriesRecording>>() {
		}.getType();
		try {

			if(recordingsSummaryJSON == null)
					recordingsSummaryJSON = getRecordingsSummaryJSON();

			return gson.fromJson(recordingsSummaryJSON.getJSONObject("seriesRecordings").getJSONArray("series_recording").toString(), listType);
		} catch (NullPointerException | JSONException e) {
			return null;
		}
	}
	
	/**
	 * Method for getting All Recordings summary JSON object
	 * 
	 * @param void
	 * @return JSONObject
	 */
	private JSONObject getRecordedSeriesJSON(String seriesID) {
		try {
			str_Authorization = getAuthorization();
			recordingSummaryAPI = String.format(URLs.RecordedTabSeriesURL, BaseTest.currentProfileID, seriesID);
			connection = getSimpleConnection(recordingSummaryAPI, str_Authorization);
			System.out.println("Recorded series URL " + recordingSummaryAPI);
			connection.connect();
			return getJSONFromConnection(connection);
		} catch (IOException e) {
			return null;
		}
	}
	
	/**
	 * Method for getting All single Recordings summary JSON object
	 * 
	 * @param void
	 * @return JSONObject
	 */
	private JSONObject getRecordedSingleJSON() {
		try {
			str_Authorization = getAuthorization();
			String recordingSingleAPI = String.format(URLs.RecordedTabSinglesURL , BaseTest.currentProfileID);
			connection = getSimpleConnection(recordingSingleAPI, str_Authorization);
			System.out.println("recordingSingleAPI " + recordingSingleAPI);
			connection.connect();
			return getJSONFromConnection(connection);
		} catch (IOException e) {
			return null;
		}
	}
	
	
	/**
	 * Method for getting all Single recordings data in the form of list of
	 * objects
	 * 
	 * @return List<Recording>
	 */
	private ArrayList<Recording> getAllSingleRecordings() {
		Type listType = new TypeToken<List<Recording>>() {
		}.getType();
		try {
			recordingsSummaryJSON = getRecordedSingleJSON();
			return gson.fromJson(recordingsSummaryJSON.getJSONArray("recording").toString(), listType);
		} catch (NullPointerException | JSONException e) {
			return null;
		}
	}
	
	/**
	 * Method to get all recorded series data in the form of list of objects.
	 *
	 */
	public ArrayList<Recording> getAllRecordedMovies() {
		ArrayList<Recording> singles = getAllSingleRecordings();
		ArrayList<Recording> recordedMovies = new ArrayList<Recording>();
		Recording single = new Recording();
		for(int i = 0; i < singles.size(); i++) {
			single = singles.get(i);
			if(single.getCategory() != null && single.getCategory().contains("movies")) {
				if(single.getRecording_status() != null && !single.getRecording_status().equals(RecordingStatus.SCHEDULED))  {
					//System.out.println(i + " all singles data" + single.getName());
					recordedMovies.add(single);
				}	
			}
		}
		
		return recordedMovies;
	}
	/**
	 * Method for getting all series recordings data in the form of list of objects
	 * 
	 * @return List<SeriesRecording>
	 */
	public ArrayList<Recording> getAllRecordingsforSeries(String seriesID) {
		Type listType = new TypeToken<List<Recording>>() {}.getType();
		try {
			JSONObject recordingsJSON = getRecordedSeriesJSON(seriesID).getJSONObject("seriesRecordings").getJSONArray("series_recording").getJSONObject(0);
			return gson.fromJson(recordingsJSON.getJSONArray("recording").toString(), listType);
		} catch (NullPointerException | JSONException e) {
			return null;
		}
	}

	public SeriesRecording getSeriesRecordingById(String seriesId) {
		ArrayList<SeriesRecording> allSeriesRecordings = getAllSeriesRecordings();
		for (SeriesRecording seriesRecording : allSeriesRecordings) {
			if (seriesRecording.getSeries_id().equals(seriesId)) {
				return seriesRecording;
			}
		}
		return null;
	}

	/**
	 * Method to get all recorded series data in the form of list of objects.
	 *
	 */
	public List<SeriesRecording> getAllRecordedSeries() {

		List<SeriesRecording> allSeriesRecordingList = getAllSeriesRecordings().stream().filter(seriesRecording -> seriesRecording.getRecordings() != null && seriesRecording.getRecordings().parallelStream().allMatch(recording -> recording != null && (!recording.getRecording_status().equals(RecordingStatus.SCHEDULED))))
				.collect(Collectors.toList());

		return allSeriesRecordingList;
	}

	/**
	 * Method to bind series recording object with thumbnail id into map.
	 */
	public Map<String, SeriesRecording> getAllRecordedSeriesInMap() {

		List<SeriesRecording> allRecordedSeriesList = getAllRecordedSeries();
		Map<String, SeriesRecording> seriesRecordingMap = new HashMap<>();
		allRecordedSeriesList.parallelStream().forEach(series -> seriesRecordingMap.put(series.getRecordings().get(0).getThumbnailId(), series));

		return seriesRecordingMap;
	}

	/**
	 * Get list of all series data.
	 * 
	 * @return ArrayList<SeriesInfo>
	 */
	public ArrayList<SeriesInfo> getAllSeriesInfo() {
		Type listType = new TypeToken<List<SeriesInfo>>() {
		}.getType();
		try {
			recordingsSummaryJSON = getRecordingsSummaryJSON();
			return gson.fromJson(recordingsSummaryJSON.getJSONObject("seriesInfoList").getJSONArray("series_info").toString(), listType);
		} catch (NullPointerException | JSONException e) {
			return null;
		}
	}

	/**
	 * 
	 * /** Method to check recording status of the given ID/Program.
	 * 
	 * @param sharedRefID
	 * @param recordingStatus
	 * @return
	 */
	private boolean checkForRecordingStatus(String sharedRefID, String recordingStatus) {
		URL = String.format(URLs.SharedRefURL + "%s&shared_ref_ids=tms:%s", BaseTest.currentAccountRegion, sharedRefID);
		connection = getSimpleConnection(URL, "");
		jsonObj = getJSONFromConnection(connection).getJSONArray("shared_refs").getJSONObject(0);
		assetArray = jsonObj.getJSONArray("assets");
		sharedRefprogramIDs = new ArrayList<>();
		for (int i = 0; i < assetArray.length(); i++) {
			try {
				jsonObj = assetArray.getJSONObject(i).getJSONObject("asset").getJSONObject("program");
				if (jsonObj.getBoolean("is_recording_enabled"))
					sharedRefprogramIDs.add(jsonObj.getString("id"));
			} catch (JSONException e) {
			}
		}
		switch (recordingStatus) {
		case "recordable":
			return isRecordable(sharedRefprogramIDs);
		case "recorded":
			return isRecorded(sharedRefprogramIDs);
		case "recording":
			return isRecording(sharedRefprogramIDs);
		case "scheduled":
			return isScheduled(sharedRefprogramIDs);
		}
		return false;
	}

	/**
	 * Method to know, whether given sharedref is suitable for recording.
	 * 
	 * @param sharedRefprogramIDs
	 * @return boolean
	 */
	public boolean isRecordable(ArrayList<String> sharedRefprogramIDs) {
		for (int i = 0; i < sharedRefprogramIDs.size(); i++) {
			programID = sharedRefprogramIDs.get(i);
			if (onGoingProgramIDs.contains(programID) || completedProgramIDs.contains(programID) || scheduledProgramIDs.contains(programID))
				return false;
		}
		return true;
	}

	/**
	 * Method to know, whether given sharedref contains only recorded content
	 * 
	 * @param sharedRefprogramIDs
	 * @return boolean
	 */
	public boolean isRecorded(ArrayList<String> sharedRefprogramIDs) {
		for (int i = 0; i < sharedRefprogramIDs.size(); i++) {
			programID = sharedRefprogramIDs.get(i);
			if (completedProgramIDs.contains(programID))
				return true;
		}
		return false;
	}

	/**
	 * Method to know, whether given sharedref contains only ongoing recording
	 * 
	 * @param sharedRefprogramIDs
	 * @return boolean
	 */
	public boolean isRecording(ArrayList<String> sharedRefprogramIDs) {
		for (int i = 0; i < sharedRefprogramIDs.size(); i++) {
			programID = sharedRefprogramIDs.get(i);
			if (completedProgramIDs.contains(programID))
				return false;
		}
		for (int i = 0; i < sharedRefprogramIDs.size(); i++) {
			programID = sharedRefprogramIDs.get(i);
			if (onGoingProgramIDs.contains(programID))
				return true;
		}
		return false;
	}

	/**
	 * Method to know, whether given sharedref contains only scheduled recording
	 * 
	 * @param sharedRefprogramIDs
	 * @return boolean
	 */
	public boolean isScheduled(ArrayList<String> sharedRefprogramIDs) {
		for (int i = 0; i < sharedRefprogramIDs.size(); i++) {
			programID = sharedRefprogramIDs.get(i);
			if (completedProgramIDs.contains(programID))
				return false;
		}
		for (int i = 0; i < sharedRefprogramIDs.size(); i++) {
			programID = sharedRefprogramIDs.get(i);
			if (onGoingProgramIDs.contains(programID))
				return false;
		}
		for (int i = 0; i < sharedRefprogramIDs.size(); i++) {
			programID = sharedRefprogramIDs.get(i);
			if (scheduledProgramIDs.contains(programID))
				return true;
		}
		return false;
	}

	/**
	 * Method for parsing all recordings data in the form of list of objects
	 * 
	 */
	public void parseAllRecordingsData() {
		recordings = new ArrayList<Recording>();
		recordings = getAllRecordingsData();
		onGoingProgramIDs = new ArrayList<>();
		completedProgramIDs = new ArrayList<>();
		scheduledProgramIDs = new ArrayList<>();
		onGoingRecordings = new HashMap<>();
		completedRecordings = new HashMap<>();
		scheduledRecordings = new HashMap<>();
		onGoingSharedRef = new HashMap<>();
		completedSharedRef = new HashMap<>();
		scheduledSharedRef = new HashMap<>();
		for (Recording record : recordings) {
			programID = record.getProgram_id();
			sharedRefID = record.getShared_ref_id();
			if ((record.getRecording_status() != null && (record.getRecording_status().contains("ongoing")))) {
				onGoingProgramIDs.add(programID);
				onGoingRecordings.put(programID, record);
				if(sharedRefID != null)
					onGoingSharedRef.put(sharedRefID, record);
			} else if ((record.getRecording_status() != null && (record.getRecording_status().contains("completed")))) {
				completedProgramIDs.add(programID);
				completedRecordings.put(programID, record);
				if(sharedRefID != null)
					completedSharedRef.put(sharedRefID, record);
			} else if ((record.getRecording_status() != null && (record.getRecording_status().contains("scheduled")))) {
				scheduledProgramIDs.add(programID);
				scheduledRecordings.put(programID, record);
				if(sharedRefID != null)
					scheduledSharedRef.put(sharedRefID, record);
			}
		}
		seriesRecordings = new ArrayList<>();
		seriesRecordings = getAllSeriesRecordings();
		seriesRecordingsMap = new HashMap<>();
		for (SeriesRecording srecord : seriesRecordings) {
			seriesRecordingsMap.put(srecord.getSeries_id(), srecord.getRecordings());
		}
		seriesList = new ArrayList<>();
		seriesList = getAllSeriesInfo();
		seriesChannelMap = new HashMap<>();
		for (SeriesInfo seriesInfo : seriesList) {
			if (seriesChannelMap.containsKey(seriesInfo.series_id)) {
				seriesChannelMap.get(seriesInfo.series_id).add(seriesInfo.channel_id);
			} else {
				List<String> channelList = new ArrayList<>();
				channelList.add(seriesInfo.channel_id);
				seriesChannelMap.put(seriesInfo.series_id, channelList);
			}
		}
	}

	/**
	 * Method for getting Usage summary data. (time in minute)
	 * 
	 * @param
	 * @return HashMap<String,Integer>
	 */
	private HashMap<String, Integer> getUsageSummaryData() {
		HashMap<String, Integer> usageSummaryData = new HashMap<String, Integer>();
		try {
			usageSummaryJSON = getUsageSummaryJSON();
			System.out.println("usageSummaryJSON : "+ usageSummaryJSON);
			usageSummaryData.put("timeUsed", usageSummaryJSON.getInt("time_used"));
			usageSummaryData.put("timeAvailable", usageSummaryJSON.getInt("time_available"));
			usageSummaryData.put("timeAlloted", usageSummaryJSON.getInt("time_alloted"));
		} catch (NullPointerException | JSONException e) {
			return null;
		}
		return usageSummaryData;
	}

	/**
	 * Method to get number of hours utilized for recordings.
	 * 
	 * @return
	 */
	public int getNumberOfHoursUtilized() {
		return getUsageSummaryData().get("timeUsed") / 60;
	}

	/**
	 * Method to get number of hours alloted for recordings.
	 * 
	 * @return
	 */
	public int getTotalAllotedHours() {
		return getUsageSummaryData().get("timeAlloted") / 60;
	}

	/**
	 * Method to get remaining hour, to be utilized for recordings.
	 * 
	 * @return
	 */
	public double getRemainingHours() {
		return getUsageSummaryData().get("timeAvailable") / 60;
	}

	/**
	 * Method to get remaining minutes, to be utilized for recordings.
	 *
	 * @return
	 */
	public Long getRemainingMinutes() {
		return Long.valueOf(getUsageSummaryData().get("timeAvailable"));
	}

	/**
	 * Method to get first recordable shared ref movie from all the available
	 * programs in the available channels.
	 * 
	 * @return
	 */
	public SharedRef getARecordableSharedRefMovie() {
		if (!ConfigurationConstants.isRecordEnabled)
			return null;
		channelIDs = channelsJSONUtility.getAllChannelsIDs();
		for (int i = channelIDs.size() - 1; i >= 0; i--) {
			channelID = channelIDs.get(i);
			jsonObj = channelsJSONUtility.getProgramsJSONForChannel(channelID);
			try {
				jsnAry_programs = jsonObj.getJSONArray("programs");
				for (int j = 0; j < jsnAry_programs.length(); j++) {
					jsonObj = jsnAry_programs.getJSONObject(j);
					category = jsonObj.getJSONArray("category");
					if (category.getString(0).equals("movies")) {
						if (jsonObj.getBoolean("is_recording_enabled")) {
							sharedRefID = jsonObj.getString("shared_ref_id").split(":")[1];
							if (checkForRecordingStatus(sharedRefID, "recordable")) {
								URL = String.format(URLs.SharedRefURL + "%s&shared_ref_ids=tms:%s", BaseTest.currentAccountRegion, sharedRefID);
								sharedRef = new SharedRef();
								sharedRef.id = sharedRefID;
								connection = getSimpleConnection(URL, "");
								jsonObj = getJSONFromConnection(connection).getJSONArray("shared_refs").getJSONObject(0);
								sharedRef.name = jsonObj.getString("name");
								return sharedRef;
							}
						}
					}
				}
			} catch (JSONException e) {
			}
		}
		return null;
	}

	/**
	 * Method to get first recordable shared ref episode from all the available
	 * programs in the available channels.
	 * 
	 * @return
	 */
	public SharedRef getARecordableSharedRefEpisode() {
		if (!ConfigurationConstants.isRecordEnabled)
			return null;
		channelIDs = channelsJSONUtility.getAllChannelsIDs();
		for (int i = 0; i < channelIDs.size(); i++) {
			channelID = channelIDs.get(i);
			jsonObj = channelsJSONUtility.getProgramsJSONForChannel(channelID);
			try {
				jsnAry_programs = jsonObj.getJSONArray("programs");
				for (int j = 0; j < jsnAry_programs.length(); j++) {
					jsonObj = jsnAry_programs.getJSONObject(j);
					category = jsonObj.getJSONArray("category");
					if (category.getString(0).equals("tv")) {
						if (jsonObj.getBoolean("is_recording_enabled")) {
							sharedRefID = jsonObj.getString("shared_ref_id").split(":")[1];
							if (checkForRecordingStatus(sharedRefID, "recordable")) {
								URL = String.format(URLs.SharedRefURL + "%s&shared_ref_ids=tms:%s", BaseTest.currentAccountRegion, sharedRefID);
								sharedRef = new SharedRef();
								sharedRef.id = sharedRefID;
								connection = getSimpleConnection(URL, "");
								jsonObj = getJSONFromConnection(connection).getJSONArray("shared_refs").getJSONObject(0);
								sharedRef.name = jsonObj.getString("name");
								return sharedRef;
							}
						}
					}
				}
			} catch (JSONException e) {
			}
		}
		return null;
	}

	/**
	 * Method to get all recordings under a given series ID
	 * 
	 * @param seriesID
	 * @return
	 */
	public List<Recording> getRecordingsUnderSeries(String seriesID) {
		return seriesRecordingsMap.get(seriesID);
	}

	public Recording getCompletelyRecordedEpisode(String episodeId) {
		return completedRecordings.get(episodeId);
	}

	public Recording getOngoingRecordingForEpisode(String episodeId) {
		return onGoingRecordings.get(episodeId);
	}

	public Recording getScheduledRecordingForEpisode(String episodeId) {
		return scheduledRecordings.get(episodeId);
	}

	/**
	 * Method to get first completed recording(movie) in the list.
	 * 
	 * @return
	 */
	public Recording getARecordedMovie() {
		for (int i = 0; i < completedProgramIDs.size(); i++) {
			record = completedRecordings.get(completedProgramIDs.get(i));
			if (record.getCategory().get(0).equals("movies"))
				return record;
		}
		return null;
	}

	/**
	 * Method to get first completed recording(episode) in the list.
	 * 
	 * @return
	 */
	public Recording getARecordedEpisode() {
		for (int i = 0; i < completedProgramIDs.size(); i++) {
			record = completedRecordings.get(completedProgramIDs.get(i));
			if (record.getCategory().get(0).equals("tv"))
				return record;
		}
		return null;
	}

	/**
	 * Method to get first ongoing recording(movie) in the list.
	 * 
	 * @return
	 */
	public Recording getAOngoingRecordingMovie() {
		for (int i = 0; i < onGoingProgramIDs.size(); i++) {
			record = onGoingRecordings.get(onGoingProgramIDs.get(i));
			if (record.getCategory().get(0).equals("movies"))
				return record;
		}
		return null;
	}

	/**
	 * Method to get first ongoing recording(episode) in the list.
	 * 
	 * @return
	 */
	public Recording getAOngoingRecordingEpisode() {
		for (int i = 0; i < onGoingProgramIDs.size(); i++) {
			record = onGoingRecordings.get(onGoingProgramIDs.get(i));
			if (record.getCategory().get(0).equals("tv"))
				return record;
		}
		return null;
	}

	/**
	 * Method to get first scheduled recording(movie) in the list.
	 * 
	 * @return
	 */
	public Recording getAScheduledRecordingMovie() {
		for (int i = 0; i < scheduledProgramIDs.size(); i++) {
			record = scheduledRecordings.get(scheduledProgramIDs.get(i));
			if (record.getCategory().get(0).equals("movies"))
				return record;
		}
		return null;
	}

	/**
	 * Method to get first scheduled recording(episode) in the list.
	 * 
	 * @return
	 */
	public Recording getAScheduledRecordingEpisode() {
		for (int i = 0; i < scheduledProgramIDs.size(); i++) {
			record = scheduledRecordings.get(scheduledProgramIDs.get(i));
			if (record.getCategory().get(0).equals("tv"))
				return record;
		}
		return null;
	}

	/**
	 * Method to get list of program name and its type.
	 */
	public Map<String, String> getScheduledRecordingProgramNameAndType() {

		Map<String, String> scheduledProgram = new HashMap();
		Set<String> programKeySet = scheduledRecordings.keySet();

		for (String key : programKeySet) {
			Recording recording = scheduledRecordings.get(key);
			final String programName = recording.getName() != null ? recording.getName() : recording.getSeries_name();
			scheduledProgram.put(programName, recording.getCategory().get(0));
		}

		return scheduledProgram;
	}

	/**
	 * Method for getting all recorded tab series names
	 * 
	 * @return HashSet<String>
	 */
	public HashSet<String> getRecordedTabSeriesNames() {
		recordedTabSeriesNames = new HashSet<String>();
		for (int i = 0; i < recordings.size(); i++) {
			record = recordings.get(i);
			if ("tv".equalsIgnoreCase(record.getCategory().get(0))) {
				if (record.getRecording_status() != null && (record.getRecording_status().contains("ongoing") || record.getRecording_status().contains("completed"))) {
					if (record.getSeries_name() != null) {
						recordedTabSeriesNames.add(record.getSeries_name());
					}
				}
			}
		}
		return recordedTabSeriesNames;
	}

	/**
	 * Method for getting all Recorded Tab Movies objects
	 * 
	 * @return ArrayList<Recording>
	 */
	public ArrayList<Recording> getRecordedMovies() {
		ArrayList<Recording> recordedTabMovies = new ArrayList<Recording>();
		for (int i = 0; i < recordings.size(); i++) {
			record = recordings.get(i);
			if (record.getCategory() != null) {
				if ("movies".equalsIgnoreCase(record.getCategory().get(0))) {
					if (record.getRecording_status() != null) {
						if (record.getRecording_status().contains("ongoing")
								|| record.getRecording_status().contains("completed")) {
							if (record.getName() != null) {
								recordedTabMovies.add(record);
							}
						}
					}
				}
			}
		}
		return recordedTabMovies;
	}

	/**
	 * Method to bind series recording object with thumbnail id into map.
	 */
	public Map<String, Recording> getAllRecordedMoviesInMap() {

		List<Recording> allRecordedMoviesList = getRecordedMovies();
		Map<String, Recording> moviesRecordingMap = new HashMap<>();
		allRecordedMoviesList.parallelStream().forEach(movie -> moviesRecordingMap.put(movie.getThumbnailId(), movie));

		return moviesRecordingMap;
	}

	/**
	 * Method for getting all recorded tab movies
	 * 
	 * @return HashSet<String>
	 */
	public ArrayList<String> getRecordedTabMoviesNames() {
		ArrayList<String> recordedTabMoviesNames = new ArrayList<String>();
		ArrayList<Recording> movieRecordings = getRecordedMovies();
		for (int i = 0; i < movieRecordings.size(); i++) {
			record = movieRecordings.get(i);
			if (record.getName() != null) {
				recordedTabMoviesNames.add(record.getName());
			}
		}
		return recordedTabMoviesNames;
	}

	/**
	 * Method for getting all scheduled tab series names
	 * 
	 * @return HashSet<String>
	 */
	//	public HashSet<String> getScheduledTabSeriesNames() {
	//		scheduledTabSeriesNames = new HashSet<String>();
	//		for (int i = 0; i < seriesRecordings.size(); i++) {
	//			seriesRecord = seriesRecordings.get(i);
	//			if (null != seriesRecord.getName())
	//				scheduledTabSeriesNames.add(seriesRecord.getName());
	//		}
	//		return scheduledTabSeriesNames;
	//	}

	/**
	 * Returns list of shows available for recording.
	 * 
	 * @return ArrayList<Show>
	 */
	public ArrayList<Show> getSeriesAvailableForRecording() {
		ArrayList<Show> seriesAvailableForRecording = new ArrayList<>();
		shows = getAllShows(100);
		for (Show show : shows) {
			if (!seriesChannelMap.containsKey(show.getID())) {
				System.out.println(show.getName());
				seriesAvailableForRecording.add(show);
			}
		}
		return seriesAvailableForRecording;
	}

	/**
	 * This method retrieves and returns list of channels on which the given
	 * series is airing.
	 * 
	 * @param seriesId
	 * @return ArrayList<Channel>
	 */
	public ArrayList<Channel> getChannelListForSeries(String seriesId, ArrayList<Channel> allChannels) {
		Set<String> channelIds = new HashSet<>();
		JSONObject jsonObj = getJSONFromConnection(getSimpleConnection(String.format(URLs.SeriesAiringOnChannelsURL + seriesId, BaseTest.currentAccountRegion), ""));
		try {
			JSONArray hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				JSONObject result = hits.getJSONObject(i).getJSONObject("result");
				JSONObject programJSONObject = result.getJSONObject("program");
				Program program = gson.fromJson(programJSONObject.toString(), Program.class);
				channelIds.add(program.getChannel_id());
			}
		} catch (Exception e) {
		}
		ArrayList<Channel> seriesChannels = new ArrayList<>();
		for (Channel channel : allChannels) {
			if (channelIds.contains(channel.getID())) {
				seriesChannels.add(channel);
			}
		}
		return seriesChannels;
	}

	/**
	 * Returns series airing on multiple channels.
	 * 
	 * @return series object airing on channels
	 */
	public HashMap<Show, ArrayList<Channel>> getRecordableSeries(boolean isAiringOnMultipleCnannels) {
		ArrayList<Show> seriesAvailableForRecording = getSeriesAvailableForRecording();
		// Get all channels
		ArrayList<Channel> allChannels = channelsJSONUtility.getAllChannels(-1);
		HashMap<Show, ArrayList<Channel>> seriesAiringOnChannels = new HashMap<>();
		ArrayList<Channel> channels = null;
		for (int i = 0; i < seriesAvailableForRecording.size(); i++) {
			Show show = seriesAvailableForRecording.get(i);
			channels = getChannelListForSeries(show.getID(), allChannels);
			System.out.println("size" + channels.size());
			if (isAiringOnMultipleCnannels && channels.size() > 1) {
				seriesAiringOnChannels = new HashMap<>();
				seriesAiringOnChannels.put(show, channels);
				return seriesAiringOnChannels;
			} else if (!isAiringOnMultipleCnannels && channels.size() == 1) {
				seriesAiringOnChannels = new HashMap<>();
				seriesAiringOnChannels.put(show, channels);
				return seriesAiringOnChannels;
			}
		}
		return seriesAiringOnChannels;
	}

	/**
	 * Method to initialized required JSON object from series API response.
	 */
	private void initSeriesMoviesDetailJSON(String programId, boolean isSeries) {
		try {
			final String requestURL = isSeries ? URLs.SeriesDetailURL : URLs.MovieDetailURL;
			final String jsonArrayKey = isSeries ? "inventories" : "programs";
			final String seriesDetailAPI = String.format(requestURL, programId);
			connection = getSimpleConnection(seriesDetailAPI, "");
			connection.connect();
			jsonObj = getJSONFromConnection(connection);
			JSONArray inventories = jsonObj.getJSONArray(jsonArrayKey);
			jsonObj = inventories.getJSONObject(0);
		} catch (IOException e) {
			jsonObj = null;
		}catch (Exception e) {
			jsonObj = null;
		}
	}

	/**
	 * Method to get series detail based on series id.
	 */
	public SeriesMovies getSeriesDetailWithId(String seriesId) {

		SeriesMovies seriesDetails = new SeriesMovies();

		initSeriesMoviesDetailJSON(seriesId, true);

		if (jsonObj != null) {
			seriesDetails = gson.fromJson(jsonObj.toString(), SeriesMovies.class);
		}

		return seriesDetails;
	}

	/**
	 * Method to get required movie object using movie id.
	 */
	public Movie getRequiredMovieWithId(String movieId) {

		Movie movie = null;
		initSeriesMoviesDetailJSON(movieId, false);

		if (jsonObj != null) {
			movie = gson.fromJson(jsonObj.toString(), Movie.class);
		}

		return movie;
	}

	/**
	 * Method to get movie detail based on movie id.
	 */
	public SeriesMovies getMovieDetailWithId(String movieId) {

		SeriesMovies movieDetails = new SeriesMovies();

		initSeriesMoviesDetailJSON(movieId, false);

		if (jsonObj != null) {
			movieDetails = gson.fromJson(jsonObj.toString(), SeriesMovies.class);
		}

		return movieDetails;
	}

	/**
	 * Method to get list of assets for a movie using shared ref id.
	 */
	public List<Assets> getAssetsForMovie(String sharedRefId) {

		final JSONObject movieAssetJson = fetchSharedAssets(sharedRefId);
		List<Assets> assetArray = getAssetromJSON(movieAssetJson);

		return assetArray;
	}

	/**
	 * Method to get thumbnail id of series poster image fromAPI.
	 */
	public String getSeriesPosterThumbnailId(SeriesMovies seriesDetails) {
		final String thumbnailId = getProgramPosterThumbnailId(seriesDetails);
		return thumbnailId;
	}

	/**
	 * Method to get thumbnail id of movies poster image fromAPI.
	 */
	public String getMoviePosterThumbnailId(SeriesMovies movieDetails) {
		final String thumbnailId = getProgramPosterThumbnailId(movieDetails);
		return thumbnailId;
	}

	/**
	 * Method to extract thumbnail id of series poster image from series
	 * details.
	 */
	private String getProgramPosterThumbnailId(SeriesMovies seriesMovieDetails) {

		final String requiredImageName = "poster";
		String thumbnailId = "";

		List<ImagesMetadata> images = seriesMovieDetails.getImages();

		for (ImagesMetadata imgMData : images) {

			if (imgMData.getName().equalsIgnoreCase(requiredImageName)) {
				thumbnailId = imgMData.getId();
				break;
			}

		}

		return thumbnailId;
	}

	/**
	 * Method for getting all scheduled Tab Series objects here all records are
	 * captured - because scheduled status will be present only the content is
	 * to be aired in 5 mins
	 * 
	 * @return ArrayList<Recording>
	 */
	public ArrayList<SeriesRecording> getScheduledSeries() {
		ArrayList<SeriesRecording> scheduledTabSeries = new ArrayList<>();
		for (int i = 0; i < seriesRecordings.size(); i++) {
			scheduledTabSeries.add(seriesRecordings.get(i));
			//			seriesRecord = seriesRecordings.get(i);
			//			if (seriesRecord.getRecordings() != null) {
			//				if (!seriesRecord.getRecordings().isEmpty()) {
			//					recordings = seriesRecord.getRecordings();
			//				System.out.println(" series name : "+ seriesRecord.getName() + "has episodes : "+recordings.size() );
			//					for (int j = 0; j < recordings.size(); j++) {
			//						record = recordings.get(j);
			//						System.out.println("record - series name : "+ record.getSeries_name() + " status : "+ record.getRecording_status());
			//						if (record.getRecording_status().contains("scheduled")) {
			//							scheduledTabSeries.add(seriesRecord);
			//							break;
			//						}
			//					}
			//				}
			//			}else {
			//				scheduledTabSeries.add(seriesRecord);
			//			}
		}
		return scheduledTabSeries;
	}

	public List<SeriesRecording> getAllCompletedSeries() {

		List<SeriesRecording> allSeriesRecordingList = getAllSeriesRecordings().stream().filter(seriesRecording -> seriesRecording.getRecordings() != null && seriesRecording.getRecordings().parallelStream().allMatch(recording -> recording != null && (recording.getRecording_status().equals("completed"))))
				.collect(Collectors.toList());
		return allSeriesRecordingList;
	}

	/**
	 * Method for getting all scheduled Tab Series names
	 *
	 * @return HashSet<String>
	 */
	public ArrayList<String> getScheduledTabSeriesNames() {
		ArrayList<String> scheduledTabSeriesNames = new ArrayList<>();
		ArrayList<SeriesRecording> scheduledTabSeries = (ArrayList<SeriesRecording>) getScheduledSeries();
		System.out.println("scheduledTabSeries size expected : " + scheduledTabSeries.size());
		for (int i = 0; i < scheduledTabSeries.size(); i++) {
			seriesRecord = scheduledTabSeries.get(i);
			if (seriesRecord.getName() != null) {
				System.out.println(seriesRecord.getName());
				scheduledTabSeriesNames.add(seriesRecord.getName());
			}
		}
		return scheduledTabSeriesNames;
	}

	/**
	 * Method for getting all individually scheduled Tab objects
	 *
	 * @return ArrayList<Recording>
	 */
	public ArrayList<Recording> getScheduledContent() {
		ArrayList<Recording> scheduledTabContent = new ArrayList<Recording>();
		for (int i = 0; i < recordings.size(); i++) {
			record = recordings.get(i);
			if ((record.getRecording_status() != null && (record.getRecording_status().contains("scheduled")))) {
				if (record.getName() != null) {
					scheduledTabContent.add(record);
				}
			}
		}
		return scheduledTabContent;
	}

	/**
	 * 
	 * @param episode
	 * @return First Upcoming Recording for any available asset of episode
	 */
	public Recording getFirstUpcomingRecordingForEpisode(Episode episode) {
		Recording recording = null;
		if (isSharedEpisode(episode.getID())) {
			List<Assets> allProgramAssets = new ArrayList<>();
			List<Assets> allAssets = episode.getAssets();
			for (Assets asset : allAssets) {
				if (asset.getRef_type().equals(Constants.REF_TYPE_PROGRAM)) {
					allProgramAssets.add(asset);
				}
			}
			if (allProgramAssets != null) {
				if (allProgramAssets.size() > 1) {
					Collections.sort(allProgramAssets, new AssetsStartTimeComparator());
				}
				recording = !allProgramAssets.isEmpty() ? getScheduledRecordingForEpisode(allProgramAssets.get(0).getId()) : null;
			}
		}

		if (recording == null) {
			recording = getScheduledRecordingForEpisode(episode.getID());
			if (recording != null) {
				return recording;
			}
		}
		return null;
	}

	public boolean isSharedEpisode(String episodeId) {
		if (episodeId.contains("tms:")) {
			return true;
		}
		return false;
	}

	public boolean isSeriesRecordable(Show show) {
		Utility utility = new Utility();
		MainJSONUtility mainJSONUtility = new MainJSONUtility();
		ArrayList<Channel> allChannels = channelsJSONUtility.getAllChannels(-1);
		ArrayList<Channel> channels = getChannelListForSeries(show.getID(), allChannels);
		if (utility.isValidCollection(channels)) {
			for (Channel channel : channels) {
				if (mainJSONUtility.isChannelPurchasedForRecordingRights(channel)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isScheduledProgram(String ref_id) {
		if (scheduledProgramIDs.contains(ref_id))
			return true;
		else
			return false;
	}

	public boolean isOngoingProgram(String ref_id) {
		if (onGoingProgramIDs.contains(ref_id))
			return true;
		else
			return false;
	}

	public boolean isRecordedProgram(String ref_id) {
		if (completedProgramIDs.contains(ref_id))
			return true;
		else
			return false;
	}

	public Recording getScheduledRecordingforProgram(String id) {
		if (scheduledProgramIDs.contains(id))
			for (Recording recording : recordings) {
				if (recording.getProgram_id().equals(id))
					return recording;
			}
		return null;
	}

	public String checkRecordButtonAction(String ref_id) {
		if (scheduledRecordings.containsKey(ref_id))
			return Constants.cancel_record_button_title;
		else if (completedRecordings.containsKey(ref_id))
			return Constants.delete_record_button_title;
		else if (onGoingRecordings.containsKey(ref_id))
			return Constants.stop_record_button_title;
		else
			return null;
	}
	
	public String checkRecordButtonActionBySharedRefID(String ref_id) {
		if (scheduledSharedRef.containsKey(ref_id))
			return Constants.cancel_record_button_title;
		else if (completedSharedRef.containsKey(ref_id))
			return Constants.delete_record_button_title;
		else if (onGoingSharedRef.containsKey(ref_id))
			return Constants.stop_record_button_title;
		else
			return null;
	}

	/**
	 * Method to check overlay action buttons.
	 */
	public String checkOverlayButtonAction(String ref_id) {
		if (completedRecordings.containsKey(ref_id))
			return Constants.delete_button_title;
		else if (onGoingRecordings.containsKey(ref_id))
			return Constants.stop_button_title;
		else
			return null;
	}

	/**
	 *
	 * @param recordingId
	 * @return episode id of that recording object
	 */
	public String getEpisodeIdFromRecording(String recordingId) {
		ArrayList<Recording> allRecordings = getAllRecordingsData();
		for (Recording recording : allRecordings) {
			if (recording.getID().equals(recordingId)) {
				return recording.getProgram_id();
			}
		}
		return null;
	}

	/**
	 *
	 * @param episodeId
	 * @return 'true' if there is any 'Completed', 'Ongoing' or 'Scheduled'
	 *         recording for the given 'episodeId'
	 */
	public boolean isRecordingSetForEpisode(String episodeId) {
		if (getCompletelyRecordedEpisode(episodeId) != null || getOngoingRecordingForEpisode(episodeId) != null || getScheduledRecordingForEpisode(episodeId) != null) {
			return true;
		}
		return false;
	}

	public List<Recording> getCompletedIndividualRecordingsForSeries(String seriesId) {
		List<Recording> allCompletedIndividaulRecordingUnderSeries = new ArrayList<>();

		if (completedRecordings != null && !completedRecordings.isEmpty()) {
			for (Map.Entry<String, Recording> entry : completedRecordings.entrySet()) {
				if (seriesId.equals(entry.getValue().getSeries_id())) {
					allCompletedIndividaulRecordingUnderSeries.add(entry.getValue());
				}
			}
		}

		return allCompletedIndividaulRecordingUnderSeries;
	}

	public List<Recording> getOngoingIndividualRecordingsForSeries(String seriesId) {
		List<Recording> allOngoingIndividaulRecordingUnderSeries = new ArrayList<>();

		if (onGoingRecordings != null && !onGoingRecordings.isEmpty()) {
			for (Map.Entry<String, Recording> entry : onGoingRecordings.entrySet()) {
				if (seriesId.equals(entry.getValue().getSeries_id())) {
					allOngoingIndividaulRecordingUnderSeries.add(entry.getValue());
				}
			}
		}
		return allOngoingIndividaulRecordingUnderSeries;
	}

	public List<Recording> getScheduledIndividualRecordingsForSeries(String seriesId) {
		List<Recording> allScheduledIndividaulRecordingUnderSeries = new ArrayList<>();

		if (scheduledRecordings != null && !scheduledRecordings.isEmpty()) {
			for (Map.Entry<String, Recording> entry : scheduledRecordings.entrySet()) {
				if (seriesId.equals(entry.getValue().getSeries_id())) {
					allScheduledIndividaulRecordingUnderSeries.add(entry.getValue());
				}
			}
		}
		return allScheduledIndividaulRecordingUnderSeries;
	}

	
	/**
	 * Method to get list of recorded and ongoing episodes list of a series id.
	 * @param seriesId
	 * @return
	 */
	public List<Recording> getAllOnGoingAndCompletedRecordingsForSeriesId(String seriesId){
		List<Recording> seriesRecordings = new ArrayList<>();
		List<Recording> allOngoingIndividualRecordings = getOngoingIndividualRecordingsForSeries(seriesId);
		List<Recording> allCompletedIndividualRecordings = getCompletedIndividualRecordingsForSeries(seriesId);
		for(Recording program : allOngoingIndividualRecordings)
		{
			if(program.getCategory().get(0).equals(Constants.CATEGORY_TYPE_TV)){				
				seriesRecordings.add(program);
			}
		}
		for(Recording program : allCompletedIndividualRecordings)
		{
			if(program.getCategory().get(0).equals(Constants.CATEGORY_TYPE_TV)){			
				seriesRecordings.add(program);
			}
		}
		return seriesRecordings;
	}
	
	/**
	 * Check if no completed recordings programs.
	 */
	public boolean hasNoCompletedRecordings() {
		return completedProgramIDs.isEmpty();
	}

	/**
	 * Check if no ongoing recordings programs.
	 */
	public boolean hasNoOngoingRecordings() {
		return onGoingProgramIDs.isEmpty();
	}

	/**
	 * Check if no scheduled programs.
	 */
	public boolean hasNoScheduledRecordings() {
		return scheduledProgramIDs.isEmpty();
	}

}
