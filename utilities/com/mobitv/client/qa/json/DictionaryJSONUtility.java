package com.mobitv.client.qa.json;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import com.mobitv.client.qa.constants.URLs;
import com.mobitv.client.qa.models.ClientConfig;
import com.mobitv.client.qa.models.Language;
/**
 * 
 * @author MobiTV
 *
 */
public class DictionaryJSONUtility extends MainJSONUtility {
	private ArrayList<String> appSupportedLanguageCodes = null;
	private String currentDeviceLanguageCode = null;
	private List<Language> appSupportedLanguages = null;

	/**
	 * Method to get dictionaryJSON object
	 * 
	 * @param locale
	 * @return
	 */
	public JSONObject getDictionaryFromServer(String locale) {
		URL = String.format(URLs.DictionaryURL + "%s.json", locale);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to give preferred locale based on the device locale support and app
	 * locale support.
	 * 
	 * @param currentDevicelocale
	 * @param clientConfiguration
	 * @return
	 */
	public String getPreferredLocale(String currentDevicelocale, ClientConfig clientConfiguration) {
		if (currentDevicelocale.length() >= 5 && currentDevicelocale != null) {
			currentDeviceLanguageCode = currentDevicelocale.split("-")[0];
			appSupportedLanguageCodes = getAppSupportedLanguagesCodes(clientConfiguration);
			if (appSupportedLanguageCodes.contains(currentDeviceLanguageCode))
				return currentDeviceLanguageCode;
			else
				return appSupportedLanguageCodes.get(0);
		}
		return "en";
	}

	/**
	 * Method to get all languages codes supported by the app.
	 * 
	 * @param clientConfiguration
	 * @return
	 */
	public ArrayList<String> getAppSupportedLanguagesCodes(ClientConfig clientConfiguration) {
		appSupportedLanguageCodes = new ArrayList<>();
		appSupportedLanguages = clientConfiguration.getSupported_languages();
		if (null != appSupportedLanguages)
			for (int i = 0; i < appSupportedLanguages.size(); i++)
				appSupportedLanguageCodes.add(appSupportedLanguages.get(i).getCode());
		else
			appSupportedLanguageCodes.add("en");
		return appSupportedLanguageCodes;
	}
}
