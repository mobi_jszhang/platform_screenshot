package com.mobitv.client.qa.json;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import com.mobitv.client.qa.baseTests.BaseTest;
import com.mobitv.client.qa.constants.URLs;
import com.mobitv.client.qa.functions.DateAndTime;
import com.mobitv.client.qa.models.Assets;
import com.mobitv.client.qa.models.Channel;
import com.mobitv.client.qa.models.Episode;
import com.mobitv.client.qa.models.Movie;
import com.mobitv.client.qa.models.Program;
import com.mobitv.client.qa.models.Show;
/**
 * 
 * @author MobiTV
 *
 */
public class SearchJSONUtility extends MainJSONUtility {
	private String searchResultURL = null;
	private String searchUpcominResultURL = null;

	/**
	 * Method to get Shows of search results for a given query.
	 * 
	 * @param count
	 * @param searchQuery
	 * @return
	 */
	public ArrayList<Show> getSearchResultShows(int count, String searchQuery) {
		shows = new ArrayList<>();
		jsonObj = getSearchResultShowsJSONObject(searchQuery, count);
		try {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length() && i < count; i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "series":
					jsonObj = result.getJSONObject("series");
					break;
				}
				show = gson.fromJson(jsonObj.toString(), Show.class);
				shows.add(show);
			}
		} catch (Exception e) {}
		return shows;
	}

	/**
	 * Method to get Movies of search results for a given query.
	 * 
	 * @param count
	 * @param searchQuery
	 * @return
	 */
	public ArrayList<Movie> getSearchResultMovies(int count, String searchQuery) {
		movies = new ArrayList<>();
		JSONObject movieData = null;
		jsonObj = getSearchResultsMoviesJSONObject(searchQuery, count);
		try {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length() && i < count; i++) {
				List<Assets> assetArrayList = new ArrayList<>();
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "vod":
					jsonObj = result.getJSONObject("vod");
					break;
				case "shared_ref":
					movieData = result.getJSONObject("shared_ref");
					String sharedRefId = movieData.getString("id");
					JSONObject jsonObject = fetchSharedAssets(sharedRefId);
					List<Assets> assetArray = getAssetromJSON(jsonObject);
					assetArrayList.addAll(assetArray);
					jsonObj = result.getJSONObject("shared_ref");
					
					break;
				case "program":
					jsonObj = result.getJSONObject("program");
					break;
				}
				movie = gson.fromJson(jsonObj.toString(), Movie.class);
				movie.setAssets(assetArrayList);
				movies.add(movie);
			}
		} catch (Exception e) {}
		return movies;
	}

	/**
	 * Method to get Episodes of search results for a given query.
	 * 
	 * @param count
	 * @param searchQuery
	 * @return
	 */
	public ArrayList<Episode> getSearchResultEpisodes(int count, String searchQuery) {
		episodes = new ArrayList<>();
		jsonObj = getSearchResultsEpisodesJSONObject(searchQuery, count);
		try {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length() && i < count; i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "vod":
					jsonObj = result.getJSONObject("vod");
					break;
				case "shared_ref":
					jsonObj = result.getJSONObject("shared_ref");
					break;
				case "program":
					jsonObj = result.getJSONObject("program");
					break;
				}
				episode = gson.fromJson(jsonObj.toString(), Episode.class);
				episodes.add(episode);
			}
		} catch (Exception e) {}
		return episodes;
	}

	/**
	 * Method to get Channels of search results for a given query.
	 * 
	 * @param count
	 * @param searchQuery
	 * @return
	 */
	public ArrayList<Channel> getSearchResultChannels(int count, String searchQuery) {
		channels = new ArrayList<>();
		jsonObj = getSearchResultsChannelsJSONObject(searchQuery, count);
		try {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length() && i < count; i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "channel":
					jsonObj = result.getJSONObject("channel");
					break;
				}
				channel = gson.fromJson(jsonObj.toString(), Channel.class);
				channels.add(channel);
			}
		} catch (Exception e) {}
		return channels;
	}
	
	public ArrayList<Program> getSearchNewsResult() {
		programs = new ArrayList<>();
		jsonObj = getSearchNewsResultsNJSONObject();
		try {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "program":
					jsonObj = result.getJSONObject("program");
					break;
				}
				program = gson.fromJson(jsonObj.toString(), Program.class);
				programs.add(program);
			}
		} catch (Exception e) {}
		return programs;
	}

	/**
	 * Method to get Upcoming programs of search results for a given query.
	 * 
	 * @param count
	 * @param searchQuery
	 * @return
	 */
	public ArrayList<Program> getSearchResultUpcomingPrograms(int count, String searchQuery) {
		programs = new ArrayList<>();
		jsonObj = getSearchResultsUpcomingJSONObject(searchQuery, count);
		try {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length() && i < count; i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "program":
					jsonObj = result.getJSONObject("program");
					break;
				}
				program = gson.fromJson(jsonObj.toString(), Program.class);
				programs.add(program);
			}
		} catch (Exception e) {}
		return programs;
	}

	/**
	 * Method to get shows search results JSON object for a given query.
	 * 
	 * @param searchQuery
	 * @return
	 */
	public JSONObject getSearchResultShowsJSONObject(String searchQuery, int count) {
		searchResultURL = String.format(URLs.SearchResultsShowsURL + "%s&q=%s&length=" + count, BaseTest.currentAccountRegion, searchQuery.replaceAll("\\s", "%20"));
		return getJSONFromConnection(getSimpleConnection(searchResultURL, ""));
	}

	/**
	 * Method to get movies search results JSON object for a given query.
	 * 
	 * @param searchQuery
	 * @return
	 */
	public JSONObject getSearchResultsMoviesJSONObject(String searchQuery, int count) {
		searchResultURL = String.format(URLs.SearchResultsMoviesURL + "%s&q=%s&length=" + count, BaseTest.currentAccountRegion, searchQuery.replaceAll("\\s", "%20"));
		System.out.println(searchResultURL);
		return getJSONFromConnection(getSimpleConnection(searchResultURL, ""));
	}

	/**
	 * Method to get episodes search results JSON object for a given query.
	 * 
	 * @param searchQuery
	 * @return
	 */
	public JSONObject getSearchResultsEpisodesJSONObject(String searchQuery, int count) {
		searchResultURL = String.format(URLs.SearchResultsEpisodesURL + "%s&q=%s&length=" + count, BaseTest.currentAccountRegion, searchQuery.replaceAll("\\s", "%20"));
		return getJSONFromConnection(getSimpleConnection(searchResultURL, ""));
	}

	/**
	 * Method to get channels search results JSON object for a given query.
	 * 
	 * @param searchQuery
	 * @return
	 */
	public JSONObject getSearchResultsChannelsJSONObject(String searchQuery, int count) {
		searchResultURL = String.format(URLs.SearchResultsChannelsURL + "%s&q=%s&length=" + count, BaseTest.currentAccountRegion, searchQuery.replaceAll("\\s", "%20"));
		return getJSONFromConnection(getSimpleConnection(searchResultURL, ""));
	}
	
	
	public JSONObject getSearchNewsResultsNJSONObject() {
		long cuurTime = new DateAndTime().getCurrentEpochTime();
		searchResultURL = String.format(URLs.searchNewsURL, String.valueOf(cuurTime), BaseTest.currentAccountRegion);
		return getJSONFromConnection(getSimpleConnection(searchResultURL, ""));
	}

	/**
	 * Method to get upcoming search results JSON object for a given query.
	 * 
	 * @param searchQuery
	 * @return
	 */
	public JSONObject getSearchResultsUpcomingJSONObject(String searchQuery, int count) {
		searchResultURL = String.format(URLs.SearchResultsUpcomingURL + "%s&q=%s&length=" + count, BaseTest.currentAccountRegion, searchQuery.replaceAll("\\s", "%20"));
		System.out.println(searchResultURL);
		return getJSONFromConnection(getSimpleConnection(searchResultURL, ""));
	}
	
	
	
	public List<Program> getProgramByChannelId(String channelId, String seriesId, int length) {		
		programs = new ArrayList<>();
		jsonObj = getProgramJSONObjectByChannelId(channelId, seriesId, length);
		try {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length(); i++) {
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "program":
					jsonObj = result.getJSONObject("program");
					break;
				}
				program = gson.fromJson(jsonObj.toString(), Program.class);
				programs.add(program);
			}
		} catch (Exception e) {}
		return programs;
				
	}
	
	public JSONObject getProgramJSONObjectByChannelId(String channelId, String seriesId, int length) {
		Long currTime = new DateAndTime().getCurrentEpochTime();
		searchResultURL = String.format(URLs.ProgramsByChannelId, length, currTime, channelId, seriesId);
		return getJSONFromConnection(getSimpleConnection(searchResultURL, ""));
	}
	
	public ArrayList<Movie> getSearchUpcominResultMovies(int count, String searchQuery) {
		movies = new ArrayList<>();
		JSONObject movieData = null;
		jsonObj = getSearchResultsUpcomingMoviesJSONObject(searchQuery, count);
		try {
			hits = jsonObj.getJSONArray("hits");
			for (int i = 0; i < hits.length() && i < count; i++) {
				List<Assets> assetArrayList = new ArrayList<>();
				result = hits.getJSONObject(i).getJSONObject("result");
				keys = JSONObject.getNames(result);
				switch (keys[0]) {
				case "vod":
					jsonObj = result.getJSONObject("vod");
					break;
				case "shared_ref":
					movieData = result.getJSONObject("shared_ref");
					String sharedRefId = movieData.getString("id");
					JSONObject jsonObject = fetchSharedAssets(sharedRefId);
					List<Assets> assetArray = getAssetromJSON(jsonObject);
					assetArrayList.addAll(assetArray);
					jsonObj = result.getJSONObject("shared_ref");
					
					break;
				case "program":
					jsonObj = result.getJSONObject("program");
					break;
				}
				movie = gson.fromJson(jsonObj.toString(), Movie.class);
				movie.setAssets(assetArrayList);
				movies.add(movie);
			}
		} catch (Exception e) {}
		return movies;
	}
	
	public JSONObject getSearchResultsUpcomingMoviesJSONObject(String searchQuery, int count) {
		searchUpcominResultURL = String.format(URLs.SearchResultsUpcomingURLMovie + "%s&q=%s&length=" + count, BaseTest.currentAccountRegion, searchQuery.replaceAll("\\s", "%20"));
		System.out.println(searchUpcominResultURL);
		return getJSONFromConnection(getSimpleConnection(searchResultURL, ""));
	}
}
