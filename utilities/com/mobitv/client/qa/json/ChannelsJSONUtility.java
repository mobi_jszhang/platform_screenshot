package com.mobitv.client.qa.json;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import com.mobitv.client.qa.functions.DateAndTime;
import org.json.JSONObject;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.mobitv.client.qa.baseTests.BaseTest;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.constants.URLs;
import com.mobitv.client.qa.functions.Utility;
import com.mobitv.client.qa.models.Channel;
import com.mobitv.client.qa.models.Program;
public class ChannelsJSONUtility extends MainJSONUtility {
	private ArrayList<String> channelIDs;

	/**
	 * Method to get particular number of channels as per the input parameter "count"
	 * 
	 * @param count
	 * @return
	 */
	public ArrayList<Channel> getAllChannels(int count) {
		Utility utility = new Utility();
		LinkedHashSet<Channel> allChannels = BaseTest.allChannelsData;
		if (!utility.isValidCollection(allChannels)) {
			allChannels = fetchAllChannels();
			if (!utility.isValidCollection(allChannels)) {
				System.out.println("Channel Data is empty");
				return null;
			}
		}
		channels = new ArrayList<>();
		List<Channel> requiredChannels = null;

		if (count > 0) {
			requiredChannels = ImmutableList.copyOf(Iterables.limit(allChannels, count));
			channels.addAll(requiredChannels);
		} else {
			channels.addAll(allChannels);
		}
		return channels;
	}

	/**
	 * Method to return all the channels
	 * @return LinkedHashSet<Channel> of all the channels
	 * Try to avoid this method as much as possible since all channel data is already stored in "BaseTest.allChannelsData" at beginning.
	 */
	public LinkedHashSet<Channel> fetchAllChannels() {
		LinkedHashSet<Channel> channels = new LinkedHashSet<>();
		boolean hasMoreData = true;
		int offset = 0;
		int offsetMultiplier = 1;
		while (hasMoreData) {
			jsonObj = getAllChannelsJSON(String.valueOf(offset));
			total = jsonObj.getInt("total");
			offset = 200 * offsetMultiplier;
			offsetMultiplier++;
			hasMoreData = total > offset ? true : false;
			if (total > 0) {
				hits = jsonObj.getJSONArray("hits");
				for (int i = 0; i < hits.length(); i++) {
					result = hits.getJSONObject(i).getJSONObject("result");
					keys = JSONObject.getNames(result);
					switch (keys[0]) {
					case "channel":
						jsonObj = result.getJSONObject("channel");
						break;
					}
					channel = gson.fromJson(jsonObj.toString(), Channel.class);
					channels.add(channel);
				}
			}
		}
		return channels;
	}

	/**
	 * Method to get JSONObject of all channels.
	 *
	 * @return
	 */
	public JSONObject getAllChannelsJSON(String offset) {
		URL = String.format(URLs.AllChannelsURL, BaseTest.currentAccountRegion, offset);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get JSONObject programs of a given channel from current time.
	 * 
	 * @param channelID
	 * @return
	 */
	public JSONObject getProgramsJSONForChannel(String channelID) {
		URL = String.format(URLs.ChannelProgramsURL + "%s&regions=national,%s",new DateAndTime().getCurrentEpochTime(), channelID, BaseTest.currentAccountRegion);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get JSONObject programs of a given channel from current time.
	 * 
	 * @param channelID
	 * @param count
	 * @return
	 */
	public JSONObject getProgramsJSONForChannel(String channelID, int count) {
		URL = String.format(URLs.ChannelProgramsURL + "%s&regions=national,%s&length=" + count,new DateAndTime().getCurrentEpochTime(), channelID, BaseTest.currentAccountRegion);
		System.out.println("getProgramsJSONForChannel : "+ URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}
	public JSONObject getProgramsJSONForChannelForLast24Hrs(String channelID, int count) {
		URL = String.format(URLs.ChannelProgramsURLForLast24Hrs + "%s&regions=national,%s&length=" + count, channelID, BaseTest.currentAccountRegion);
		System.out.println("getProgramsJSONForChannelForLast24Hrs : "+ URL);
		return getJSONFromConnection(getSimpleConnection(URL, ""));
	}

	/**
	 * Method to get programs of a given channel from current time.
	 * 
	 * @param count
	 * @param channelId
	 * @return
	 */
	public ArrayList<Program> getProgramsForChannel(int count, String channelID) {
		programs = new ArrayList<>();
		jsonObj = getProgramsJSONForChannel(channelID, count);
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("programs");
			for (int i = 0; i < hits.length() && i < count; i++) {
				jsonObj = hits.getJSONObject(i);
				program = gson.fromJson(jsonObj.toString(), Program.class);
				programs.add(program);
			}
		}
		return programs;
	}
	public ArrayList<Program> getProgramsForChannelForLast24Hrs(int count, String channelID) {
		programs = new ArrayList<>();
		jsonObj = getProgramsJSONForChannelForLast24Hrs(channelID, count);
		total = jsonObj.getInt("total");
		if (total > 0) {
			hits = jsonObj.getJSONArray("programs");
			for (int i = 0; i < hits.length() && i < count; i++) {
				jsonObj = hits.getJSONObject(i);
				program = gson.fromJson(jsonObj.toString(), Program.class);
				programs.add(program);
			}
		}
		return programs;
	}

	/**
	 * Method to get all channels ID's for the current account
	 * 
	 * @return channelIDs
	 */
	public ArrayList<String> getAllChannelsIDs() {
		Utility utility = new Utility();
		channelIDs = new ArrayList<>();
		LinkedHashSet<Channel> allChannels = BaseTest.allChannelsData;

		if (!utility.isValidCollection(allChannels)) {
			allChannels = fetchAllChannels();
			if (!utility.isValidCollection(allChannels)) {
				System.out.println("Channel Data is empty");
				return null;
			}
		}
		for (Channel channel : allChannels) {
			channelIDs.add(channel.getID());
		}
		return channelIDs;
	}

	/**
	 * Method to get running program on channel.
	 */
	public Program getProgramForChannel(String channelId) {
		final int programCount = 2;
		List<Program> programListFromAPI = getProgramsForChannel(programCount, channelId);
		programListFromAPI.removeIf(program -> !utility.isLive(program.getStart_time(), program.getEnd_time()));
		Program reqProgram = programListFromAPI.get(0);
		return reqProgram;

	}

	/**
	 * Method return program from channel if its having non-restricted program
	 * ratings.
	 *
	 * @param channel
	 *            - to verify non-restricted program.
	 * @return - non-restricted type program.
	 */
	public Program getProgramForChannelFromAPIIfNotRestricted(Channel channel) {
		Program program = getProgramForChannel(channel.getID());
		return ConfigurationConstants.childProtectionEnabledRatings.contains(program.getChild_protection_rating())
				? null : program;
	}

	public Channel getChannelsDetails(String channelId) {
		Utility utility = new Utility();
		LinkedHashSet<Channel> allChannels = BaseTest.allChannelsData;
		if (!utility.isValidCollection(allChannels)) {
			allChannels = fetchAllChannels();
			if (!utility.isValidCollection(allChannels)) {
				System.out.println("Channel Data is empty");
				return null;
			}
		}

		for (Channel channel : allChannels) {
			if (channel.getID().equals(channelId)) {
				return channel;
			}
		}
		return null;
	}
}