package com.mobitv.client.qa.models;
import java.util.List;
import com.google.gson.annotations.SerializedName;
/**
 * 
 * @author MobiTV
 *
 */
public class Channel {
	@SerializedName("id")
	private String ID = null;
	private String name = null;
	private String description = null;
	private String type = null;
	private List<String> drm_rights = null;
	private String media_class = null;
	private String media_id = null;
	private String media_aspect_ratio = null;
	private String vendor_station_id = null;
	private String network = null;
	private List<String> provider_networks = null;
	private List<String> audio_languages_list = null;
	private List<String> category = null;
	private boolean is_catchup_enabled = false;
	private int catchup_duration = 0;
	private boolean is_startover_enabled = false;
	private boolean is_timeshift_enabled = false;
	private int timeshift_duration = 0;
	private boolean is_out_of_home_playback_enabled = false;
	private boolean is_recording_enabled = false;
	private boolean has_program_data = false;
	private int channel_number = 0;
	private String recording_copy_method = null;
	private List<String> sku_ids = null;

	public Channel() {}

	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @param iD
	 *            the iD to set
	 */
	public void setID(String iD) {
		ID = iD;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the drm_rights
	 */
	public List<String> getDrm_rights() {
		return drm_rights;
	}

	/**
	 * @param drm_rights
	 *            the drm_rights to set
	 */
	public void setDrm_rights(List<String> drm_rights) {
		this.drm_rights = drm_rights;
	}

	/**
	 * @return the media_class
	 */
	public String getMedia_class() {
		return media_class;
	}

	/**
	 * @param media_class
	 *            the media_class to set
	 */
	public void setMedia_class(String media_class) {
		this.media_class = media_class;
	}

	/**
	 * @return the media_id
	 */
	public String getMedia_id() {
		return media_id;
	}

	/**
	 * @param media_id
	 *            the media_id to set
	 */
	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	/**
	 * @return the media_aspect_ratio
	 */
	public String getMedia_aspect_ratio() {
		return media_aspect_ratio;
	}

	/**
	 * @param media_aspect_ratio
	 *            the media_aspect_ratio to set
	 */
	public void setMedia_aspect_ratio(String media_aspect_ratio) {
		this.media_aspect_ratio = media_aspect_ratio;
	}

	/**
	 * @return the vendor_station_id
	 */
	public String getVendor_station_id() {
		return vendor_station_id;
	}

	/**
	 * @param vendor_station_id
	 *            the vendor_station_id to set
	 */
	public void setVendor_station_id(String vendor_station_id) {
		this.vendor_station_id = vendor_station_id;
	}

	/**
	 * @return the network
	 */
	public String getNetwork() {
		return network;
	}

	/**
	 * @param network
	 *            the network to set
	 */
	public void setNetwork(String network) {
		this.network = network;
	}

	/**
	 * @return the provider_networks
	 */
	public List<String> getProvider_networks() {
		return provider_networks;
	}

	/**
	 * @param provider_networks
	 *            the provider_networks to set
	 */
	public void setProvider_networks(List<String> provider_networks) {
		this.provider_networks = provider_networks;
	}

	/**
	 * @return the audio_languages_list
	 */
	public List<String> getAudio_languages_list() {
		return audio_languages_list;
	}

	/**
	 * @param audio_languages_list
	 *            the audio_languages_list to set
	 */
	public void setAudio_languages_list(List<String> audio_languages_list) {
		this.audio_languages_list = audio_languages_list;
	}

	/**
	 * @return the category
	 */
	public List<String> getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(List<String> category) {
		this.category = category;
	}

	/**
	 * @return the is_catchup_enabled
	 */
	public boolean isIs_catchup_enabled() {
		return is_catchup_enabled;
	}

	/**
	 * @param is_catchup_enabled
	 *            the is_catchup_enabled to set
	 */
	public void setIs_catchup_enabled(boolean is_catchup_enabled) {
		this.is_catchup_enabled = is_catchup_enabled;
	}

	/**
	 * @return the catchup_duration
	 */
	public int getCatchup_duration() {
		return catchup_duration;
	}

	/**
	 * @param catchup_duration
	 *            the catchup_duration to set
	 */
	public void setCatchup_duration(int catchup_duration) {
		this.catchup_duration = catchup_duration;
	}

	/**
	 * @return the is_startover_enabled
	 */
	public boolean isIs_startover_enabled() {
		return is_startover_enabled;
	}

	/**
	 * @param is_startover_enabled
	 *            the is_startover_enabled to set
	 */
	public void setIs_startover_enabled(boolean is_startover_enabled) {
		this.is_startover_enabled = is_startover_enabled;
	}

	/**
	 * @return the is_timeshift_enabled
	 */
	public boolean isIs_timeshift_enabled() {
		return is_timeshift_enabled;
	}

	/**
	 * @param is_timeshift_enabled
	 *            the is_timeshift_enabled to set
	 */
	public void setIs_timeshift_enabled(boolean is_timeshift_enabled) {
		this.is_timeshift_enabled = is_timeshift_enabled;
	}

	/**
	 * @return the timeshift_duration
	 */
	public int getTimeshift_duration() {
		return timeshift_duration;
	}

	/**
	 * @param timeshift_duration
	 *            the timeshift_duration to set
	 */
	public void setTimeshift_duration(int timeshift_duration) {
		this.timeshift_duration = timeshift_duration;
	}

	/**
	 * @return the is_out_of_home_playback_enabled
	 */
	public boolean is_out_of_home_playback_enabled() {
		return is_out_of_home_playback_enabled;
	}

	/**
	 * @param is_out_of_home_playback_enabled
	 *            the is_out_of_home_playback_enabled to set
	 */
	public void setIs_out_of_home_playback_enabled(boolean is_out_of_home_playback_enabled) {
		this.is_out_of_home_playback_enabled = is_out_of_home_playback_enabled;
	}

	/**
	 * @return the is_recording_enabled
	 */
	public boolean isIs_recording_enabled() {
		return is_recording_enabled;
	}

	/**
	 * @param is_recording_enabled
	 *            the is_recording_enabled to set
	 */
	public void setIs_recording_enabled(boolean is_recording_enabled) {
		this.is_recording_enabled = is_recording_enabled;
	}

	/**
	 * @return the has_program_data
	 */
	public boolean isHas_program_data() {
		return has_program_data;
	}

	/**
	 * @param has_program_data
	 *            the has_program_data to set
	 */
	public void setHas_program_data(boolean has_program_data) {
		this.has_program_data = has_program_data;
	}

	/**
	 * @return the channel_number
	 */
	public int getChannel_number() {
		return channel_number;
	}

	/**
	 * @param channel_number
	 *            the channel_number to set
	 */
	public void setChannel_number(int channel_number) {
		this.channel_number = channel_number;
	}

	/**
	 * @return the recording_copy_method
	 */
	public String getRecording_copy_method() {
		return recording_copy_method;
	}

	/**
	 * @param recording_copy_method
	 *            the recording_copy_method to set
	 */
	public void setRecording_copy_method(String recording_copy_method) {
		this.recording_copy_method = recording_copy_method;
	}

	/**
	 * @return - List of Sku_ids in a channel.
	 */
	public List<String> getSku_ids() {
		return sku_ids;
	}

	/**
	 * @param sku_ids - List of Sku_ids for a channel.
	 */
	public void setSku_ids(List<String> sku_ids) {
		this.sku_ids = sku_ids;
	}
}