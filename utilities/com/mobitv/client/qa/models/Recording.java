package com.mobitv.client.qa.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author MobiTV
 *
 */
public class Recording {
	@SerializedName("id")
	private String ID = null;
	private String name = null;
	private String description = null;
	private long recording_start_time = 0;
	private long start_time = 0;
	private String series_recording_id = null;
	private String carrier = null;
	private String product = null;
	private String version = null;
	private String account_id = null;
	private String profile_id = null;
	private String program_id = null;
	private long end_time = 0;
	private long recording_end_time = 0;
	private String channel_id = null;
	private String backend_channel_id = null;
	private String recording_status = null;
	private String error_code = null;
	private String error_message = null;
	private String child_protection_rating = null;
	private String series_id = null;
	private String series_name = null;
	private boolean is_series = false;
	private String post_roll_duration = null;
	private List<String> category = null;
	private String shared_ref_id = null;
	private String thumbnail_id = null;
	private long expiry_time = 0;

	public Recording() {
	}

	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the recording_start_time
	 */
	public Long getRecording_start_time() {
		return recording_start_time;
	}

	/**
	 * @return the start_time
	 */
	public long getStart_time() {
		return start_time;
	}

	/**
	 * @return the series_recording_id
	 */
	public String getSeries_recording_id() {
		return series_recording_id;
	}

	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}

	/**
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @return the account_id
	 */
	public String getAccount_id() {
		return account_id;
	}

	/**
	 * @return the profile_id
	 */
	public String getProfile_id() {
		return profile_id;
	}

	/**
	 * @return the program_id
	 */
	public String getProgram_id() {
		return program_id;
	}

	/**
	 * @return the end_time
	 */
	public long getEnd_time() {
		return end_time;
	}

	/**
	 * @return the recording_end_time
	 */
	public long getRecording_end_time() {
		return recording_end_time;
	}

	/**
	 * @return the channel_id
	 */
	public String getChannel_id() {
		return channel_id;
	}

	/**
	 * @return the backend_channel_id
	 */
	public String getBackend_channel_id() {
		return backend_channel_id;
	}

	/**
	 * @return the recording_status
	 */
	public String getRecording_status() {
		return recording_status;
	}

	/**
	 * @return the error_code
	 */
	public String getError_code() {
		return error_code;
	}

	/**
	 * @return the error_message
	 */
	public String getError_message() {
		return error_message;
	}

	/**
	 * @return the child_protection_rating
	 */
	public String getChild_protection_rating() {
		return child_protection_rating;
	}

	/**
	 * @return the series_id
	 */
	public String getSeries_id() {
		return series_id;
	}

	/**
	 * @return the series_name
	 */
	public String getSeries_name() {
		return series_name;
	}

	/**
	 * @return the is_series
	 */
	public boolean isIs_series() {
		return is_series;
	}

	/**
	 * @return the post_roll_duration
	 */
	public String getPost_roll_duration() {
		return post_roll_duration;
	}

	/**
	 * @return the category
	 */
	public List<String> getCategory() {
		return category;
	}

	/**
	 * @return the shared_ref_id
	 */
	public String getShared_ref_id() {
		return shared_ref_id;
	}

	/**
	 * @return - thumbnail id value.
	 */
	public String getThumbnailId() {
		return thumbnail_id;
	}

	/**
	 * @param set thumbnail id value.
	 */
	public void setThumbnailId(String thumbnail_id) {
		this.thumbnail_id = thumbnail_id;
	}	
	/**
	 * @return the expiry_time
	 */
	public long getExpiry_time() {
		return expiry_time;
	}
}
