package com.mobitv.client.qa.models;
import java.util.List;
public class SharedRef {
	public String id = null;
	public String name = null;
	public String sub_title = null;
	public String description = null;
	public String type = null;
	public String thumbnail_id = null;
	public List<String> thumbnail_formats = null;
	public List<String> category = null;
	public List<String> genre_list = null;
	public String child_protection_rating = null;
	public List<String> advisory_list = null;
	public List<String> actors_list = null;
	public List<String> directors_list = null;
	public String series_id = null;
	public String series_name = null;
	public String season_number = null;
	public String episode_number = null;
	public long original_air_date = 0L;
	public String year = null;
	public String network = null;
	public List<String> provider_networks = null;
	public List<String> countries_of_production_list = null;
	public List<String> keyword_list = null;
	public String audio_lang = null;
	public String metadata_root_id = null;
	public List<Assets> assets = null;

	public SharedRef() {}
	
	public List<Assets> getAssets() {
		return assets;
	}

	public void setAssets(List<Assets> assets) {
		this.assets = assets;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


}