package com.mobitv.client.qa.models;

import java.util.ArrayList;
import java.util.List;

public class PurchaseData {

	public String id;
	public String offer_id;
	public String type;
	public String offer_type;
	public String purchase_type;
	public List<String> sku_ids;
	public String status;
	
	public PurchaseData() {
		id = "";
		offer_id = "";
		type = "";
		offer_type = "";
		purchase_type = "";
		sku_ids = new ArrayList<>();
		status = "";
	}
	
}
