package com.mobitv.client.qa.models;

public class RecommendListItem {

	public String ref_type; // Type of media asset (i.e. program, channel, vod, series, recording, catchup).
	public String type; // Type for media search 
	public String ref_id; // Identifier for media asset. When used with \'ref_type\', uniquely identifies asset.

	/**
	 * @return the iD
	 */
	public String getRef_id() {
		return ref_id;
	}

	/**
	 * @param iD
	 *            the iD to set
	 */
	public void setRef_id(String ref_id) {
		this.ref_id = ref_id;
	}

	/**
	 * @return the ref_type
	 */
	public String getRef_type() {
		return ref_type;
	}

	/**
	 * @param ref_type
	 *            the ref_type to set
	 */
	public void setRef_type(String ref_type) {
		this.ref_type = ref_type;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
