package com.mobitv.client.qa.models;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.functions.DateAndTime;
import com.mobitv.client.qa.functions.Utility;
/**
 * 
 * @author MobiTV
 *
 */
public class Show {
	@SerializedName("id")
	private String ID = null;
	private String name = null;
	private String description = null;
	private String type = null;
	private String audio_lang = null;
	private String title_lang = null;
	private String description_lang = null;
	private List<String> genre_list = null;
	private List<String> actors_list = null;
	private String vendor_station_id = null;
	private String network = null;
	private List<String> provider_networks = null;
	private long expires = 0;
	private int start_of_availability = 0;
	private int upload_date = 0;
	private String child_protection_rating = null;
	private List<String> category = null;
	private long suspension_time = 0;
	private int earliest_original_air_date = 0;
	private int latest_original_air_date = 0;
	private int latest_duration = 0;
	private int latest_start_of_availability = 0;
	private ArrayList<String> jsonMetaData = null;
	private static Utility utility = new Utility();
	private String metadata_root_id = null;

	public Show() {}

	/**
	 * @return the metadata_root_id
	 */
	public String getRootID() {
		return metadata_root_id;
	}

	/**
	 * @param rootID
	 *            the metadata_root_id to set
	 */
	public void setRootID(String rootID) {
		metadata_root_id = rootID;
	}
	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @param iD
	 *            the iD to set
	 */
	public void setID(String iD) {
		ID = iD;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the audio_lang
	 */
	public String getAudio_lang() {
		if (this.audio_lang != null && this.audio_lang.contains("-")) {
			return this.audio_lang.split("-")[0];
		}
		return audio_lang;
	}

	/**
	 * @param audio_lang
	 *            the audio_lang to set
	 */
	public void setAudio_lang(String audio_lang) {
		this.audio_lang = audio_lang;
	}

	/**
	 * @return the title_lang
	 */
	public String getTitle_lang() {
		if (this.title_lang != null && this.title_lang.contains("-")) {
			return this.title_lang.split("-")[0];
		}
		return title_lang;
	}

	/**
	 * @param title_lang
	 *            the title_lang to set
	 */
	public void setTitle_lang(String title_lang) {
		this.title_lang = title_lang;
	}

	/**
	 * @return the description_lang
	 */
	public String getDescription_lang() {
		if (this.description_lang != null && this.description_lang.contains("-")) {
			return this.description_lang.split("-")[0];
		}
		return description_lang;
	}

	/**
	 * @param description_lang
	 *            the description_lang to set
	 */
	public void setDescription_lang(String description_lang) {
		this.description_lang = description_lang;
	}

	/**
	 * @return the genre_list
	 */
	public List<String> getGenre_list() {
		return genre_list;
	}

	/**
	 * @param genre_list
	 *            the genre_list to set
	 */
	public void setGenre_list(List<String> genre_list) {
		this.genre_list = genre_list;
	}

	/**
	 * @return the actors_list
	 */
	public List<String> getActors_list() {
		return actors_list;
	}

	/**
	 * @param actors_list
	 *            the actors_list to set
	 */
	public void setActors_list(List<String> actors_list) {
		this.actors_list = actors_list;
	}

	/**
	 * @return the vendor_station_id
	 */
	public String getVendor_station_id() {
		return vendor_station_id;
	}

	/**
	 * @param vendor_station_id
	 *            the vendor_station_id to set
	 */
	public void setVendor_station_id(String vendor_station_id) {
		this.vendor_station_id = vendor_station_id;
	}

	/**
	 * @return the network
	 */
	public String getNetwork() {
		return network;
	}

	/**
	 * @param network
	 *            the network to set
	 */
	public void setNetwork(String network) {
		this.network = network;
	}

	/**
	 * @return the provider_networks
	 */
	public List<String> getProvider_networks() {
		return provider_networks;
	}

	/**
	 * @param provider_networks
	 *            the provider_networks to set
	 */
	public void setProvider_networks(List<String> provider_networks) {
		this.provider_networks = provider_networks;
	}

	/**
	 * @return the expires
	 */
	public long getExpires() {
		return expires;
	}

	/**
	 * @param expires
	 *            the expires to set
	 */
	public void setExpires(int expires) {
		this.expires = expires;
	}

	/**
	 * @return the start_of_availability
	 */
	public int getStart_of_availability() {
		return start_of_availability;
	}

	/**
	 * @param start_of_availability
	 *            the start_of_availability to set
	 */
	public void setStart_of_availability(int start_of_availability) {
		this.start_of_availability = start_of_availability;
	}

	/**
	 * @return the upload_date
	 */
	public int getUpload_date() {
		return upload_date;
	}

	/**
	 * @param upload_date
	 *            the upload_date to set
	 */
	public void setUpload_date(int upload_date) {
		this.upload_date = upload_date;
	}

	/**
	 * @return the child_protection_rating
	 */
	public String getChild_protection_rating() {
		return child_protection_rating;
	}

	/**
	 * @param child_protection_rating
	 *            the child_protection_rating to set
	 */
	public void setChild_protection_rating(String child_protection_rating) {
		this.child_protection_rating = child_protection_rating;
	}

	/**
	 * @return the category
	 */
	public List<String> getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(List<String> category) {
		this.category = category;
	}

	/**
	 * @return the suspension_time
	 */
	public long getSuspension_time() {
		return suspension_time;
	}

	/**
	 * @param suspension_time
	 *            the suspension_time to set
	 */
	public void setSuspension_time(int suspension_time) {
		this.suspension_time = suspension_time;
	}

	/**
	 * @return the earliest_original_air_date
	 */
	public int getEarliest_original_air_date() {
		return earliest_original_air_date;
	}

	/**
	 * @param earliest_original_air_date
	 *            the earliest_original_air_date to set
	 */
	public void setEarliest_original_air_date(int earliest_original_air_date) {
		this.earliest_original_air_date = earliest_original_air_date;
	}

	/**
	 * @return the latest_original_air_date
	 */
	public int getLatest_original_air_date() {
		return latest_original_air_date;
	}

	/**
	 * @param latest_original_air_date
	 *            the latest_original_air_date to set
	 */
	public void setLatest_original_air_date(int latest_original_air_date) {
		this.latest_original_air_date = latest_original_air_date;
	}

	/**
	 * @return the latest_duration
	 */
	public int getLatest_duration() {
		return latest_duration;
	}

	/**
	 * @param latest_duration
	 *            the latest_duration to set
	 */
	public void setLatest_duration(int latest_duration) {
		this.latest_duration = latest_duration;
	}

	/**
	 * @return the latest_start_of_availability
	 */
	public int getLatest_start_of_availability() {
		return latest_start_of_availability;
	}

	/**
	 * @param latest_start_of_availability
	 *            the latest_start_of_availability to set
	 */
	public void setLatest_start_of_availability(int latest_start_of_availability) {
		this.latest_start_of_availability = latest_start_of_availability;
	}

	/**
	 * Method to get primary meta data according to audio language support.
	 * 
	 * @return
	 */
	public String getPrimaryMetaDataAccordingToLocalization() {
		if (this.getAudio_lang() != null) {
			switch (this.getAudio_lang()) {
			case "fr":
				return this.name + " (FR)";
			default:
				return this.name;
			}
		} else
			return this.name;
	}

	/**
	 * Method to get meta data of a show in in-line info module.
	 * 
	 * @return formatted meta data.
	 */
	public String getInlineInfoSecondaryMetaDataAccordingToLocalization() {
		return getInlineInfoSecondaryMetaDataAccordingToLocalization(null, null);
	}

	public String getInlineInfoSecondaryMetaDataAccordingToLocalization(Long startTime, Long endTime) {
		jsonMetaData = new ArrayList<>();
		if (this.getAudio_lang() != null)
			switch (this.getAudio_lang()) {
			case "fr":
				jsonMetaData.add("French");
			default:
				;
			}
		// if (getNetwork() != null)
		// jsonMetaData.add(this.getNetwork());
		if (getGenre_list() != null)
			jsonMetaData.add(this.getGenre_list().get(0));
		if (getChild_protection_rating() != null)
			jsonMetaData.add(this.getChild_protection_rating());
		
		if ((startTime != null && endTime != null) && utility.isUpcoming(startTime, endTime)) {
			jsonMetaData.add(getFormatedAirDate(startTime));
			jsonMetaData.add(getFormatedStartTime(startTime));
		}
		
		switch (ConfigurationConstants.deviceType) {
		case "TV":
			return utility.concatinateWith(" . ", jsonMetaData);
		case "Phone":
		case "Tablet":
			return utility.concatinateWith(" . ", jsonMetaData);
		default:
			return "Device not supported";
		}
	}
	
	/**
	 * Method to get meta data of a movie in more-info screen.
	 * 
	 * @param movie
	 * @return formatted meta data.
	 */
	public String getMoreInfoSecondaryMetaDataAccordingToLocalization() {
		jsonMetaData = new ArrayList<>();
		if (this.getAudio_lang() != null)
			switch (this.getAudio_lang()) {
			case "fr":
				jsonMetaData.add("French");
			default:
				;
			}
		if (getGenre_list() != null)
			jsonMetaData.add(this.getGenre_list().get(0));
		if (getChild_protection_rating() != null)
			jsonMetaData.add(this.getChild_protection_rating());
		switch (ConfigurationConstants.deviceType) {
		case "TV":
			return utility.concatinateWith(" . ", jsonMetaData);
		case "Phone":
		case "Tablet":
			return utility.concatinateWith(" . ", jsonMetaData);
		default:
			return "Device not supported";
		}
	}

	/**
	 * Method to get actors list.
	 * 
	 * @param movie
	 * @return formatted meta data.
	 */
	public String getActorsList() {
		jsonMetaData = new ArrayList<>();
		if (getActors_list() != null)
			for (String actor : this.getActors_list()) {
				jsonMetaData.add(actor);
			}
		switch (ConfigurationConstants.deviceType) {
		case "TV":
			return utility.concatinateWith(", ", jsonMetaData);
		case "Phone":
			return utility.concatinateWith(", ", jsonMetaData);
		case "Tablet":
			return utility.concatinateWith(", ", jsonMetaData);
		default:
			return "Device not supported";
		}
	}
	
	private String getFormatedAirDate(Long startTime) {
		Date date = new Date(startTime * 1000);
		return DateAndTime.getDateInFormatMD(date);
	}
	
	private String getFormatedStartTime(Long startTime) {
		Date date = new Date(startTime * 1000);
		return new DateAndTime().getDateInFormathhmma(date);
	}
}
