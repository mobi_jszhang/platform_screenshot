package com.mobitv.client.qa.models;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.functions.DateAndTime;
import com.mobitv.client.qa.functions.Utility;
/**
 * 
 * @author MobiTV
 *
 */
public class Episode {
	@SerializedName("id")
	private String ID = null;
	private String name = null;
	private String description = null;
	private String type = null;
	private String audio_lang = null;
	private String title_lang = null;
	private String description_lang = null;
	private List<String> category = null;
	private List<String> genre_list = null;
	private String child_protection_rating = null;
	private String series_id = null;
	private String series_name = null;
	private int season_number = 0;
	private int episode_number = 0;
	private Long original_air_date = 0L;
	private String year = null;
	private List<String> provider_networks = null;
	private List<Assets> assets = null;
	private Long start_time = 0L;
	private Long end_time = 0L;
	private boolean is_recording_enabled = false;
	private ArrayList<String> jsonMetaData = null;
	private static Utility utility = new Utility();
	private StringBuffer formattedEpisodeName = null;
	private String channel_id = null;

	public Episode() {}

	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @param iD
	 *            the iD to set
	 */
	public void setID(String iD) {
		ID = iD;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		if (this.description_lang != null && this.description_lang.contains("-")) {
			return this.description_lang.split("-")[0];
		}
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the audio_lang
	 */
	public String getAudio_lang() {
		if (this.audio_lang != null && this.audio_lang.contains("-")) {
			return this.audio_lang.split("-")[0];
		}
		return audio_lang;
	}

	/**
	 * @param audio_lang
	 *            the audio_lang to set
	 */
	public void setAudio_lang(String audio_lang) {
		this.audio_lang = audio_lang;
	}

	/**
	 * @return the title_lang
	 */
	public String getTitle_lang() {
		if (this.title_lang != null && this.title_lang.contains("-")) {
			return this.title_lang.split("-")[0];
		}
		return title_lang;
	}

	/**
	 * @param title_lang
	 *            the title_lang to set
	 */
	public void setTitle_lang(String title_lang) {
		this.title_lang = title_lang;
	}

	/**
	 * @return the description_lang
	 */
	public String getDescription_lang() {
		return description_lang;
	}

	/**
	 * @param description_lang
	 *            the description_lang to set
	 */
	public void setDescription_lang(String description_lang) {
		this.description_lang = description_lang;
	}

	/**
	 * @return the category
	 */
	public List<String> getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(List<String> category) {
		this.category = category;
	}

	/**
	 * @return the genre_list
	 */
	public List<String> getGenre_list() {
		return genre_list;
	}

	/**
	 * @param genre_list
	 *            the genre_list to set
	 */
	public void setGenre_list(List<String> genre_list) {
		this.genre_list = genre_list;
	}

	/**
	 * @return the child_protection_rating
	 */
	public String getChild_protection_rating() {
		return child_protection_rating;
	}

	/**
	 * @param child_protection_rating
	 *            the child_protection_rating to set
	 */
	public void setChild_protection_rating(String child_protection_rating) {
		this.child_protection_rating = child_protection_rating;
	}

	/**
	 * @return the series_id
	 */
	public String getSeries_id() {
		return series_id;
	}

	/**
	 * @param series_id
	 *            the series_id to set
	 */
	public void setSeries_id(String series_id) {
		this.series_id = series_id;
	}

	/**
	 * @return the series_name
	 */
	public String getSeries_name() {
		return series_name;
	}

	/**
	 * @param series_name
	 *            the series_name to set
	 */
	public void setSeries_name(String series_name) {
		this.series_name = series_name;
	}

	/**
	 * @return the season_number
	 */
	public int getSeason_number() {
		return season_number;
	}

	/**
	 * @param season_number
	 *            the season_number to set
	 */
	public void setSeason_number(int season_number) {
		this.season_number = season_number;
	}

	/**
	 * @return the episode_number
	 */
	public int getEpisode_number() {
		return episode_number;
	}

	/**
	 * @param episode_number
	 *            the episode_number to set
	 */
	public void setEpisode_number(int episode_number) {
		this.episode_number = episode_number;
	}

	/**
	 * @return the original_air_date
	 */
	public Long getOriginal_air_date() {
		return original_air_date;
	}

	/**
	 * @param original_air_date
	 *            the original_air_date to set
	 */
	public void setOriginal_air_date(Long original_air_date) {
		this.original_air_date = original_air_date;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the provider_networks
	 */
	public List<String> getProvider_networks() {
		return provider_networks;
	}

	/**
	 * @param provider_networks
	 *            the provider_networks to set
	 */
	public void setProvider_networks(List<String> provider_networks) {
		this.provider_networks = provider_networks;
	}

	/**
	 * @return the Assets
	 */
	public List<Assets> getAssets() {
		return assets;
	}

	/**
	 * @param Shared
	 *            Assets the Shared Asset to set
	 */
	public void setAssets(List<Assets> assets) {
		this.assets = assets;
	}

	/**
	 * @return
	 */
	public Long getStart_time() {
		return start_time;
	}

	/**
	 * @param start_time
	 */
	public void setStart_time(Long start_time) {
		this.start_time = start_time;
	}

	/**
	 * @return
	 */
	public Long getEnd_time() {
		return end_time;
	}

	/**
	 * @param end_time
	 */
	public void setEnd_time(Long end_time) {
		this.end_time = end_time;
	}

	/**
	 * @return
	 */
	public boolean is_recording_enabled() {
		return is_recording_enabled;
	}

	public String getChannelId() {
		return this.channel_id;
	}

	public void setChannelId(String channel_id) {
		this.channel_id = channel_id;
	}
	
	/**
	 * Method to get primary meta data according to audio language support.
	 * 
	 * @return
	 */
	public String getPrimaryMetaDataAccordingToLocalization() {
		if (this.getAudio_lang() != null) {
			switch (this.getAudio_lang()) {
			case "fr":
				return this.name + " (FR)";
			default:
				return this.series_name;
			}
		} else
			return this.series_name;
	}

	/**
	 * Method to get meta data of a episode.
	 * 
	 * @return formatted meta data.
	 */
	public String getMetaDataAccordingToLocalization() {
		return getMetaDataAccordingToLocalization(null);
	}

	public String getMetaDataAccordingToLocalization(Long startTime) {
		jsonMetaData = new ArrayList<>();
		if (this.getAudio_lang() != null)
			switch (this.getAudio_lang()) {
			case "fr":
				jsonMetaData.add("French");
			default:
				;
			}
		if (getGenre_list() != null)
			jsonMetaData.add(this.getGenre_list().get(0));
		if (getChild_protection_rating() != null) {
			jsonMetaData.add(this.getChild_protection_rating());
		} else {
			jsonMetaData.add("Not Rated");
		}
		
		if (startTime != null) {
			jsonMetaData.add(getFormatedAirDate(startTime));
			jsonMetaData.add(getFormatedStartTime(startTime));
		}
		switch (ConfigurationConstants.deviceType) {
		case "TV":
			return utility.concatinateWith(" . ", jsonMetaData);
		case "Phone":
		case "Tablet":
			return utility.concatinateWith(" . ", jsonMetaData);
		default:
			return "Device not supported";
		}
	}
	/**
	 * Method to formatted episode name.
	 * 
	 * @return formatted meta data.
	 */
	public String getFormattedEpisodeName() {
		if (getSeason_number() == 0 && getName() == null) {
				for (Assets assets : this.getAssets()) {
					if(assets.getType().equalsIgnoreCase("program")){
						String episodeName = assets.getName();
						return episodeName;
					}
				}
			return getPrimaryMetaDataAccordingToLocalization();
		}
		formattedEpisodeName = new StringBuffer();
		if (getSeason_number() != 0) {
			formattedEpisodeName.append("Season " + getSeason_number() + " ");
			if (getEpisode_number() != 0) {
				formattedEpisodeName.replace(1, 7, "");
				formattedEpisodeName.append(": E" + getEpisode_number());
			}
			if (!formattedEpisodeName.equals("")) {
				formattedEpisodeName.append(" - ");
			}
		}	
		if (getName() != null)
			formattedEpisodeName.append(getName());
		return formattedEpisodeName.toString();
	}
	
	private String getFormatedAirDate(Long startTime) {
		Date date = new Date(startTime * 1000);
		return DateAndTime.getDateInFormatMD(date);
	}
	
	private String getFormatedStartTime(Long startTime) {
		Date date = new Date(startTime * 1000);
		return new DateAndTime().getTimeInFormatHHMMwithTrim(date, true);
	}
	
}
