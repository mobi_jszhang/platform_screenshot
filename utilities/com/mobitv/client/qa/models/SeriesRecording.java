package com.mobitv.client.qa.models;
import java.util.List;
import com.google.gson.annotations.SerializedName;
public class SeriesRecording {
	@SerializedName("id")
	private String ID = null;
	private String name = null;
	private long recording_start_time = 0;
	@SerializedName("recording")
	private List<Recording> recordings;
	private String account_id = null;
	private String profile_id = null;
	private String series_id = null;
	private String channel_id = null;
	private String carrier = null;
	private String product = null;
	private String version = null;
	private String post_roll_duration = null;
	private List<String> category = null;
	private boolean is_series_across_all_channels = false;

	public SeriesRecording() {}

	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the recording_start_time
	 */
	public long getRecording_start_time() {
		return recording_start_time;
	}

	/**
	 * @return the recordings
	 */
	public List<Recording> getRecordings() {
		return recordings;
	}

	/**
	 * @return the account_id
	 */
	public String getAccount_id() {
		return account_id;
	}

	/**
	 * @return the profile_id
	 */
	public String getProfile_id() {
		return profile_id;
	}

	/**
	 * @return the series_id
	 */
	public String getSeries_id() {
		return series_id;
	}

	/**
	 * @return the channel_id
	 */
	public String getChannel_id() {
		return channel_id;
	}

	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}

	/**
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @return the post_roll_duration
	 */
	public String getPost_roll_duration() {
		return post_roll_duration;
	}

	/**
	 * @return the category
	 */
	public List<String> getCategory() {
		return category;
	}

	/**
	 * @return the is_series_across_all_channels
	 */
	public boolean isIs_series_across_all_channels() {
		return is_series_across_all_channels;
	}

	/**
	 * @param is_series_across_all_channels
	 *            the is_series_across_all_channels to set
	 */
	public void setIs_series_across_all_channels(boolean is_series_across_all_channels) {
		this.is_series_across_all_channels = is_series_across_all_channels;
	}
}
