package com.mobitv.client.qa.models;
/**
 * 
 * @author MobiTV
 *
 */
public class Seasons {
	private int count;
	private int name;

	public Seasons() {}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getName() {
		return name;
	}

	public void setName(int name) {
		this.name = name;
	}
}
