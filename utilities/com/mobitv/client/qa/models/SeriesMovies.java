package com.mobitv.client.qa.models;

import java.util.List;

public class SeriesMovies {

	private String id;
	private String name;
	private String description;
	private String type;
	private String title_lang;
	private String description_lang;
	private String thumbnail_id;
	private String network;
	private String upload_date;
	private String child_protection_rating;
	private String subcategory;
	private List<ImagesMetadata> images;	
	
	public static class ImagesMetadata{
		
		private List<String> formats;
		private String id;
		private String name;
		
		
		public List<String> getFormats() {
			return formats;
		}
		public void setFormats(List<String> formats) {
			this.formats = formats;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle_lang() {
		return title_lang;
	}

	public void setTitle_lang(String title_lang) {
		this.title_lang = title_lang;
	}

	public String getDescription_lang() {
		return description_lang;
	}

	public void setDescription_lang(String description_lang) {
		this.description_lang = description_lang;
	}

	public String getThumbnail_id() {
		return thumbnail_id;
	}

	public void setThumbnail_id(String thumbnail_id) {
		this.thumbnail_id = thumbnail_id;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getUpload_date() {
		return upload_date;
	}

	public void setUpload_date(String upload_date) {
		this.upload_date = upload_date;
	}

	public String getChild_protection_rating() {
		return child_protection_rating;
	}

	public void setChild_protection_rating(String child_protection_rating) {
		this.child_protection_rating = child_protection_rating;
	}

	public String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	public List<ImagesMetadata> getImages() {
		return images;
	}

	public void setImages(List<ImagesMetadata> images) {
		this.images = images;
	}
}