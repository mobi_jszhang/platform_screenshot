package com.mobitv.client.qa.models;
import java.util.List;
/**
 * 
 * @author MobiTV
 *
 */
public class Assets {
	private String id = null;
	private String ref_type = null;
	private String type = null;
	private String name = null;
	private Long start_time;
	private Long end_time;
	private boolean is_recording_enabled;
	private boolean is_catchup_enabled;
	private boolean is_startover_enabled;
	private List<String> provider_networks = null;
	private String duration = null;
	private String shared_ref_id;

    private String channel_id;
    private String child_protection_rating = null;
	public Assets() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRef_type() {
		return ref_type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRef_type(String ref_type) {
		this.ref_type = ref_type;
	}

	public Long getStart_time() {
		return start_time;
	}

	public void setStart_time(Long start_time) {
		this.start_time = start_time;
	}

	public Long getEnd_time() {
		return end_time;
	}

	public void setEnd_time(Long end_time) {
		this.end_time = end_time;
	}

	public String getChannelId() {
		return this.channel_id;
	}

	public void setChannelId(String channel_id) {
		this.channel_id = channel_id;
	}

	public List<String> getProvider_networks() {
		return provider_networks;
	}

	public void setProvider_networks(List<String> provider_networks) {
		this.provider_networks = provider_networks;
	}

	public boolean is_recording_enabled() {
		return is_recording_enabled;
	}

	public boolean is_catchup_enabled() {
		return is_catchup_enabled;
	}

	public boolean is_startover_enabled() {
		return is_startover_enabled;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getShared_ref_id() {
		return shared_ref_id;
	}

	public void setShared_ref_id(String shared_ref_id) {
		this.shared_ref_id = shared_ref_id;
	}


	public String getchild_protection_rating() {
		return child_protection_rating;
	}

	public void setchild_protection_rating(String childProtectionRating) {
		this.child_protection_rating = childProtectionRating;
	}
}
