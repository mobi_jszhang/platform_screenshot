package com.mobitv.client.qa.models;

import java.util.ArrayList;
import java.util.List;

public class Offers {
	public String offer_id;
	public String offer_type;
	public String purchase_type;
	public List<String> sku;

	public Offers() {
		offer_id = "";
		offer_type = "";
		purchase_type = "";
		sku = new ArrayList<>();
	}
}
