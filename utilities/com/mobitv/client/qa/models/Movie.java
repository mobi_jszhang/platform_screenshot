package com.mobitv.client.qa.models;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.mobitv.client.qa.baseTests.AndroidTVBaseTest;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.functions.DateAndTime;
import com.mobitv.client.qa.functions.Utility;
/**
 * 
 * @author MobiTV
 *
 */
public class Movie {
	@SerializedName("id")
	private String ID = null;
	private String name = null;
	private String description = null;
	private String type = null;
	private String title_lang = null;
	private String audio_lang = null;
	private String description_lang = null;
	private List<String> category = null;
	private List<String> genre_list = null;
	private String child_protection_rating = null;
	private List<String> provider_networks = null;
	private List<String> actors_list = null;
	private List<Assets> assets = null;
	private String year = null;
	private boolean is_recording_enabled = false;
	private boolean isCatchupEnabled = false;
	private String duration = null;
	private ArrayList<String> jsonMetaData = null;
	private static Utility utility = new Utility();
	private long start_time = 0L;
	private long end_time = 0L;
	private String shared_ref_id = null;
	private String metadata_root_id = null;


	public Movie() {}

	/**
	 * @return the metadata_root_id
	 */
	public String getRootID() {
		return metadata_root_id;
	}

	/**
	 * @param rootID
	 *            the metadata_root_id to set
	 */
	public void setRootID(String rootID) {
		metadata_root_id = rootID;
	}


	/**
	 * Method to get shared ref id.
	 */
	public String getShared_ref_id() {
		return shared_ref_id;
	}

	/**
	 * Method to set shared ref id.
	 */
	public void setShared_ref_id(String shared_ref_id) {
		this.shared_ref_id = shared_ref_id;
	}

	/**
	 * @return the actors_list
	 */
	public List<String> getActors_list() {
		return actors_list;
	}

	/**
	 * @param actors_list
	 *            the actors_list to set
	 */
	public void setActors_list(List<String> actors_list) {
		this.actors_list = actors_list;
	}

	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @param iD
	 *            the iD to set
	 */
	public void setID(String iD) {
		ID = iD;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the title_lang
	 */
	public String getTitle_lang() {
		if (this.title_lang != null && this.title_lang.contains("-")) {
			return this.title_lang.split("-")[0];
		}
		return title_lang;
	}

	/**
	 * @param title_lang
	 *            the title_lang to set
	 */
	public void setTitle_lang(String title_lang) {
		this.title_lang = title_lang;
	}

	/**
	 * @return the audio_lang
	 */
	public String getAudio_lang() {
		if (this.audio_lang != null && this.audio_lang.contains("-")) {
			return this.audio_lang.split("-")[0];
		}
		return audio_lang;
	}

	/**
	 * @param audio_lang
	 *            the audio_lang to set
	 */
	public void setAudio_lang(String audio_lang) {
		this.audio_lang = audio_lang;
	}

	/**
	 * @return the description_lang
	 */
	public String getDescription_lang() {
		if (this.description_lang != null && this.description_lang.contains("-")) {
			return this.description_lang.split("-")[0];
		}
		return description_lang;
	}

	/**
	 * @param description_lang
	 *            the description_lang to set
	 */
	public void setDescription_lang(String description_lang) {
		this.description_lang = description_lang;
	}

	/**
	 * @return the category
	 */
	public List<String> getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(List<String> category) {
		this.category = category;
	}

	/**
	 * @return the genre_list
	 */
	public List<String> getGenre_list() {
		return genre_list;
	}

	/**
	 * @param genre_list
	 *            the genre_list to set
	 */
	public void setGenre_list(List<String> genre_list) {
		this.genre_list = genre_list;
	}

	/**
	 * @return the child_protection_rating
	 */
	public String getChild_protection_rating() {
		return child_protection_rating;
	}

	/**
	 * @param child_protection_rating
	 *            the child_protection_rating to set
	 */
	public void setChild_protection_rating(String child_protection_rating) {
		this.child_protection_rating = child_protection_rating;
	}

	/**
	 * @return the provider_networks
	 */
	public List<String> getProvider_networks() {
		return provider_networks;
	}

	/**
	 * @param provider_networks
	 *            the provider_networks to set
	 */
	public void setProvider_networks(List<String> provider_networks) {
		this.provider_networks = provider_networks;
	}

	public List<Assets> getAssets() {
		return assets;
	}

	public void setAssets(List<Assets> assets) {
		this.assets = assets;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return
	 */
	public boolean is_recording_enabled() {
		return is_recording_enabled;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public boolean isCatchupEnabled() {
		return isCatchupEnabled;
	}

	public void setCatchupEnabled(boolean isCatchupEnabled) {
		this.isCatchupEnabled = isCatchupEnabled;
	}

	public long getStart_time() {
		return start_time;
	}

	public void setStart_time(long start_time) {
		this.start_time = start_time;
	}

	public long getEnd_time() {
		return end_time;
	}

	public void setEnd_time(long end_time) {
		this.end_time = end_time;
	}

	/**
	 * Method to get primary meta data according to audio language support.
	 * 
	 * @return
	 */
	public String getPrimaryMetaDataAccordingToLocalization() {
		if (this.getAudio_lang() != null) {
			switch (this.getAudio_lang()) {
			case "fr":
				return this.name + " (FR)";
			default:
				return this.name;
			}
		} else
			return this.name;
	}

	/**
	 * Method to get meta data of a movie in in-line info module.
	 * 
	 * @return formatted meta data.
	 */
	public String getInlineInfoSecondaryMetaDataAccordingToLocalization() {
		jsonMetaData = new ArrayList<>();
		if (this.getAudio_lang() != null)
			switch (this.getAudio_lang()) {
			case "fr":
				jsonMetaData.add("French");
			default:
				;
			}
		if (getYear() != null)
			jsonMetaData.add(this.getYear());
		if (getGenre_list() != null)
			jsonMetaData.add(this.getGenre_list().get(0));
		jsonMetaData.add(getChild_protection_rating() != null ? getChild_protection_rating() : "Not Rated");
		if (getDurationinMinutes() != null)
			jsonMetaData.add(this.getDurationinMinutes());
		switch (ConfigurationConstants.deviceType) {
		case "TV":
			return utility.concatinateWith(" • ", jsonMetaData);
		case "Phone":
		case "Tablet":
			return utility.concatinateWith("  • ", jsonMetaData);
		default:
			return "Device not supported";
		}
	}

	/**
	 * Method to get meta data of a movie in more-info screen.
	 * 
	 * @return formatted meta data.
	 */
	public String getMoreInfoSecondaryMetaDataAccordingToLocalization() {
		jsonMetaData = new ArrayList<>();
		if (this.getAudio_lang() != null)
			switch (this.getAudio_lang()) {
			case "fr":
				jsonMetaData.add("French");
			default:
				;
			}
		if (getYear() != null)
			jsonMetaData.add(this.getYear());
		if (getGenre_list() != null)
			jsonMetaData.add(this.getGenre_list().get(0));
		jsonMetaData.add(getChild_protection_rating());
		switch (ConfigurationConstants.deviceType) {
		case "TV":
			return utility.concatinateWith(" . ", jsonMetaData);
		case "Phone":
		case "Tablet":
			return utility.concatinateWith(" . ", jsonMetaData);
		default:
			return "Device not supported";
		}
	}

	/**
	 * Method to get actors list.
	 * 
	 * @return formatted meta data.
	 */
	public String getActorsList() {
		jsonMetaData = new ArrayList<>();
		if (getActors_list() != null)
			for (String actor : this.getActors_list()) {
				jsonMetaData.add(actor);
			}
		switch (ConfigurationConstants.deviceType) {
		case "TV":
			return utility.concatinateWith(", ", jsonMetaData);
		case "Phone":
			return utility.concatinateWith(", ", jsonMetaData);
		case "Tablet":
			return utility.concatinateWith(", ", jsonMetaData);
		default:
			return "Device not supported";
		}
	}

	/**
	 * Method to get Duration in Minutes.
	 * 
	 * @return movie duration.
	 */
	public String getDurationinMinutes() {
		if (this.getType().equals("shared_ref")) {
			if (getAssets() != null) {
				String duration = getAssets().get(0).getDuration();
				String durationMinutes = DateAndTime.durationFromSecs(duration);
				return durationMinutes;
			}
		} else {
			return DateAndTime.durationFromSecs(this.getDuration());
		}
		return null;
	}
}
