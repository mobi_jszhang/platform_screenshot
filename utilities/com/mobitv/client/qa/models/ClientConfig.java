package com.mobitv.client.qa.models;
import java.util.List;
/**
 * 
 * @author MobiTV
 *
 */
public class ClientConfig {
	private List<String> valid_geo_location = null;
	private List<String> tv_transport = null;
	private List<Language> supported_languages = null;
	private int eas_default_poll_period_secs = 0;
	private String recording_transport = null;
	private List<String> filter_by_child_protection_rating_list = null;
	private int recording_delay_sec = 0;
	private int max_featured_items = 0;
	private boolean filter_by_child_protection_rating = false;
	private boolean enable_nielsen_logging = false;
	private int max_recommended_items = 0;
	private String blocked_child_protection_ratings = null;

	/**
	 * @return the valid_geo_location
	 */
	public List<String> getValid_geo_location() {
		return valid_geo_location;
	}

	/**
	 * @param valid_geo_location
	 *            the valid_geo_location to set
	 */
	public void setValid_geo_location(List<String> valid_geo_location) {
		this.valid_geo_location = valid_geo_location;
	}

	/**
	 * @return the tv_transport
	 */
	public List<String> getTv_transport() {
		return tv_transport;
	}

	/**
	 * @param tv_transport
	 *            the tv_transport to set
	 */
	public void setTv_transport(List<String> tv_transport) {
		this.tv_transport = tv_transport;
	}

	/**
	 * @return the supported_languages
	 */
	public List<Language> getSupported_languages() {
		return supported_languages;
	}

	/**
	 * @param supported_languages
	 *            the supported_languages to set
	 */
	public void setSupported_languages(List<Language> supported_languages) {
		this.supported_languages = supported_languages;
	}

	/**
	 * @return the eas_default_poll_period_secs
	 */
	public int getEas_default_poll_period_secs() {
		return eas_default_poll_period_secs;
	}

	/**
	 * @param eas_default_poll_period_secs
	 *            the eas_default_poll_period_secs to set
	 */
	public void setEas_default_poll_period_secs(int eas_default_poll_period_secs) {
		this.eas_default_poll_period_secs = eas_default_poll_period_secs;
	}

	/**
	 * @return the recording_transport
	 */
	public String getRecording_transport() {
		return recording_transport;
	}

	/**
	 * @param recording_transport
	 *            the recording_transport to set
	 */
	public void setRecording_transport(String recording_transport) {
		this.recording_transport = recording_transport;
	}

	/**
	 * @return the filter_by_child_protection_rating_list
	 */
	public List<String> getFilter_by_child_protection_rating_list() {
		return filter_by_child_protection_rating_list;
	}

	/**
	 * @param filter_by_child_protection_rating_list
	 *            the filter_by_child_protection_rating_list to set
	 */
	public void setFilter_by_child_protection_rating_list(List<String> filter_by_child_protection_rating_list) {
		this.filter_by_child_protection_rating_list = filter_by_child_protection_rating_list;
	}

	/**
	 * @return the recording_delay_sec
	 */
	public int getRecording_delay_sec() {
		return recording_delay_sec;
	}

	/**
	 * @param recording_delay_sec
	 *            the recording_delay_sec to set
	 */
	public void setRecording_delay_sec(int recording_delay_sec) {
		this.recording_delay_sec = recording_delay_sec;
	}

	/**
	 * @return the max_featured_items
	 */
	public int getMax_featured_items() {
		return max_featured_items;
	}

	/**
	 * @param max_featured_items
	 *            the max_featured_items to set
	 */
	public void setMax_featured_items(int max_featured_items) {
		this.max_featured_items = max_featured_items;
	}

	/**
	 * @return the filter_by_child_protection_rating
	 */
	public boolean isFilter_by_child_protection_rating() {
		return filter_by_child_protection_rating;
	}

	/**
	 * @param filter_by_child_protection_rating
	 *            the filter_by_child_protection_rating to set
	 */
	public void setFilter_by_child_protection_rating(boolean filter_by_child_protection_rating) {
		this.filter_by_child_protection_rating = filter_by_child_protection_rating;
	}

	/**
	 * @return the enable_nielsen_logging
	 */
	public boolean isEnable_nielsen_logging() {
		return enable_nielsen_logging;
	}

	/**
	 * @param enable_nielsen_logging
	 *            the enable_nielsen_logging to set
	 */
	public void setEnable_nielsen_logging(boolean enable_nielsen_logging) {
		this.enable_nielsen_logging = enable_nielsen_logging;
	}

	/**
	 * @return the max_recommended_items
	 */
	public int getMax_recommended_items() {
		return max_recommended_items;
	}

	/**
	 * @param max_recommended_items
	 *            the max_recommended_items to set
	 */
	public void setMax_recommended_items(int max_recommended_items) {
		this.max_recommended_items = max_recommended_items;
	}

	/**
	 * @return the blocked_child_protection_ratings
	 */
	public String getBlocked_child_protection_ratings() {
		return blocked_child_protection_ratings;
	}

	/**
	 * @param blocked_child_protection_ratings
	 *            the blocked_child_protection_ratings to set
	 */
	public void setBlocked_child_protection_ratings(String blocked_child_protection_ratings) {
		this.blocked_child_protection_ratings = blocked_child_protection_ratings;
	}

	/**
	 * 
	 * @return
	 */
	public String get_filter_by_child_protection_rating_list_asString() {
		return String.join(",", getFilter_by_child_protection_rating_list());
	}
}
