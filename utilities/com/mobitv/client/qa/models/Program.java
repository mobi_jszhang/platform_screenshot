package com.mobitv.client.qa.models;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.functions.DateAndTime;
import com.mobitv.client.qa.functions.Utility;
/**
 * 
 * @author MobiTV
 *
 */
public class Program {
	@SerializedName("id")
	private String ID = null;
	private String name = null;
	private String description = null;
	private String type = null;
	private String shared_ref_id = null;
	private String audio_lang = null;
	private String title_lang = null;
	private String description_lang = null;
	private Long start_time = null;
	private Long end_time = null;
	private int expires = 0;
	private int start_of_availability = 0;
	private int duration = 0;
	private boolean is_live = false;
	private List<String> provider_networks = null;
	private String channel_id = null;
	private String linked_channel_number = null;
	private List<String> genre_list = null;
	private String year = null;
	private boolean is_repeat = false;
	private String series_id = null;
	private String series_name = null;
	private int original_air_date = 0;
	private List<String> category = null;
	private String child_protection_rating = null;
	private String season_number = null;
	private String episode_number = null;
	private List<String> actors_list = null;
	private List<String> drm_rights = null;
	private boolean is_catchup_enabled = false;
	private int catchup_duration = 0;
	private boolean is_startover_enabled = false;
	private boolean is_timeshift_enabled = false;
	private int timeshift_duration = 0;
	private boolean is_recording_enabled = false;
	private List<String> regions = null;
	private ArrayList<String> jsonMetaData = null;
	private static Utility utility = new Utility();
	private DateAndTime dAndT = new DateAndTime();

	public Program() {}

	/**
	 * @return the iD
	 */
	public String getID() {
		return ID;
	}

	/**
	 * @param iD
	 *            the iD to set
	 */
	public void setID(String iD) {
		ID = iD;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the shared_ref_id
	 */
	public String getShared_ref_id() {
		return shared_ref_id;
	}

	/**
	 * @param shared_ref_id
	 *            the shared_ref_id to set
	 */
	public void setShared_ref_id(String shared_ref_id) {
		this.shared_ref_id = shared_ref_id;
	}

	/**
	 * @return the audio_lang
	 */
	public String getAudio_lang() {
		if (this.audio_lang != null && this.audio_lang.contains("-")) {
			return this.audio_lang.split("-")[0];
		}
		return audio_lang;
	}

	/**
	 * @param audio_lang
	 *            the audio_lang to set
	 */
	public void setAudio_lang(String audio_lang) {
		this.audio_lang = audio_lang;
	}

	/**
	 * @return the title_lang
	 */
	public String getTitle_lang() {
		if (this.title_lang != null && this.title_lang.contains("-")) {
			return this.title_lang.split("-")[0];
		}
		return title_lang;
	}

	/**
	 * @param title_lang
	 *            the title_lang to set
	 */
	public void setTitle_lang(String title_lang) {
		this.title_lang = title_lang;
	}

	/**
	 * @return the description_lang
	 */
	public String getDescription_lang() {
		if (this.description_lang != null && this.description_lang.contains("-")) {
			return this.description_lang.split("-")[0];
		}
		return description_lang;
	}

	/**
	 * @param description_lang
	 *            the description_lang to set
	 */
	public void setDescription_lang(String description_lang) {
		this.description_lang = description_lang;
	}

	/**
	 * @return the start_time
	 */
	public Long getStart_time() {
		return start_time;
	}

	/**
	 * @param start_time
	 *            the start_time to set
	 */
	public void setStart_time(Long start_time) {
		this.start_time = start_time;
	}

	/**
	 * @return the end_time
	 */
	public Long getEnd_time() {
		return end_time;
	}

	/**
	 * @param end_time
	 *            the end_time to set
	 */
	public void setEnd_time(Long end_time) {
		this.end_time = end_time;
	}

	/**
	 * @return the expires
	 */
	public int getExpires() {
		return expires;
	}

	/**
	 * @param expires
	 *            the expires to set
	 */
	public void setExpires(int expires) {
		this.expires = expires;
	}

	/**
	 * @return the start_of_availability
	 */
	public int getStart_of_availability() {
		return start_of_availability;
	}

	/**
	 * @param start_of_availability
	 *            the start_of_availability to set
	 */
	public void setStart_of_availability(int start_of_availability) {
		this.start_of_availability = start_of_availability;
	}

	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @param duration
	 *            the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * @return the is_live
	 */
	public boolean isIs_live() {
		return is_live;
	}

	/**
	 * @param is_live
	 *            the is_live to set
	 */
	public void setIs_live(boolean is_live) {
		this.is_live = is_live;
	}

	/**
	 * @return the provider_networks
	 */
	public List<String> getProvider_networks() {
		return provider_networks;
	}

	/**
	 * @param provider_networks
	 *            the provider_networks to set
	 */
	public void setProvider_networks(List<String> provider_networks) {
		this.provider_networks = provider_networks;
	}

	/**
	 * @return the channel_id
	 */
	public String getChannel_id() {
		return channel_id;
	}

	/**
	 * @param channel_id
	 *            the channel_id to set
	 */
	public void setChannel_id(String channel_id) {
		this.channel_id = channel_id;
	}

	/**
	 * @return the linked_channel_number
	 */
	public String getLinked_channel_number() {
		return linked_channel_number;
	}

	/**
	 * @param linked_channel_number
	 *            the linked_channel_number to set
	 */
	public void setLinked_channel_number(String linked_channel_number) {
		this.linked_channel_number = linked_channel_number;
	}

	/**
	 * @return the genre_list
	 */
	public List<String> getGenre_list() {
		return genre_list;
	}

	/**
	 * @param genre_list
	 *            the genre_list to set
	 */
	public void setGenre_list(List<String> genre_list) {
		this.genre_list = genre_list;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the is_repeat
	 */
	public boolean isIs_repeat() {
		return is_repeat;
	}

	/**
	 * @param is_repeat
	 *            the is_repeat to set
	 */
	public void setIs_repeat(boolean is_repeat) {
		this.is_repeat = is_repeat;
	}

	/**
	 * @return the series_id
	 */
	public String getSeries_id() {
		return series_id;
	}

	/**
	 * @param series_id
	 *            the series_id to set
	 */
	public void setSeries_id(String series_id) {
		this.series_id = series_id;
	}

	/**
	 * @return the series_name
	 */
	public String getSeries_name() {
		return series_name;
	}

	/**
	 * @param series_name
	 *            the series_name to set
	 */
	public void setSeries_name(String series_name) {
		this.series_name = series_name;
	}

	/**
	 * @return the original_air_date
	 */
	public int getOriginal_air_date() {
		return original_air_date;
	}

	/**
	 * @param original_air_date
	 *            the original_air_date to set
	 */
	public void setOriginal_air_date(int original_air_date) {
		this.original_air_date = original_air_date;
	}

	/**
	 * @return the category
	 */
	public List<String> getCategory() {
		return category;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(List<String> category) {
		this.category = category;
	}

	/**
	 * @return the child_protection_rating
	 */
	public String getChild_protection_rating() {
		return child_protection_rating;
	}

	/**
	 * @param child_protection_rating
	 *            the child_protection_rating to set
	 */
	public void setChild_protection_rating(String child_protection_rating) {
		this.child_protection_rating = child_protection_rating;
	}

	/**
	 * @return the season_number
	 */
	public String getSeason_number() {
		return season_number;
	}

	/**
	 * @param season_number
	 *            the season_number to set
	 */
	public void setSeason_number(String season_number) {
		this.season_number = season_number;
	}

	/**
	 * @return the episode_number
	 */
	public String getEpisode_number() {
		return episode_number;
	}

	/**
	 * @param episode_number
	 *            the episode_number to set
	 */
	public void setEpisode_number(String episode_number) {
		this.episode_number = episode_number;
	}

	/**
	 * @return the actors_list
	 */
	public List<String> getActors_list() {
		return actors_list;
	}

	/**
	 * @param actors_list
	 *            the actors_list to set
	 */
	public void setActors_list(List<String> actors_list) {
		this.actors_list = actors_list;
	}

	/**
	 * @return the drm_rights
	 */
	public List<String> getDrm_rights() {
		return drm_rights;
	}

	/**
	 * @param drm_rights
	 *            the drm_rights to set
	 */
	public void setDrm_rights(List<String> drm_rights) {
		this.drm_rights = drm_rights;
	}

	/**
	 * @return the is_catchup_enabled
	 */
	public boolean isIs_catchup_enabled() {
		return is_catchup_enabled;
	}

	/**
	 * @param is_catchup_enabled
	 *            the is_catchup_enabled to set
	 */
	public void setIs_catchup_enabled(boolean is_catchup_enabled) {
		this.is_catchup_enabled = is_catchup_enabled;
	}

	/**
	 * @return the catchup_duration
	 */
	public int getCatchup_duration() {
		return catchup_duration;
	}

	/**
	 * @param catchup_duration
	 *            the catchup_duration to set
	 */
	public void setCatchup_duration(int catchup_duration) {
		this.catchup_duration = catchup_duration;
	}

	/**
	 * @return the is_startover_enabled
	 */
	public boolean isIs_startover_enabled() {
		return is_startover_enabled;
	}

	/**
	 * @param is_startover_enabled
	 *            the is_startover_enabled to set
	 */
	public void setIs_startover_enabled(boolean is_startover_enabled) {
		this.is_startover_enabled = is_startover_enabled;
	}

	/**
	 * @return the is_timeshift_enabled
	 */
	public boolean isIs_timeshift_enabled() {
		return is_timeshift_enabled;
	}

	/**
	 * @param is_timeshift_enabled
	 *            the is_timeshift_enabled to set
	 */
	public void setIs_timeshift_enabled(boolean is_timeshift_enabled) {
		this.is_timeshift_enabled = is_timeshift_enabled;
	}

	/**
	 * @return the timeshift_duration
	 */
	public int getTimeshift_duration() {
		return timeshift_duration;
	}

	/**
	 * @param timeshift_duration
	 *            the timeshift_duration to set
	 */
	public void setTimeshift_duration(int timeshift_duration) {
		this.timeshift_duration = timeshift_duration;
	}

	/**
	 * @return the is_recording_enabled
	 */
	public boolean isIs_recording_enabled() {
		return is_recording_enabled;
	}

	/**
	 * @param is_recording_enabled
	 *            the is_recording_enabled to set
	 */
	public void setIs_recording_enabled(boolean is_recording_enabled) {
		this.is_recording_enabled = is_recording_enabled;
	}

	/**
	 * @return the regions
	 */
	public List<String> getRegions() {
		return regions;
	}

	/**
	 * @param regions
	 *            the regions to set
	 */
	public void setRegions(List<String> regions) {
		this.regions = regions;
	}

	/**
	 * Method to get primary meta data according to audio language support.
	 * 
	 * @return
	 */
	public String getPrimaryMetaDataAccordingToLocalization() {
		if (this.series_id == null) {
			if (getAudio_lang() != null) {
				switch (getAudio_lang()) {
				case "fr":
					return this.name + " (FR)";
				default:
					return this.name;
				}
			} else
				return this.name;
		} else {
			if (getAudio_lang() != null) {
				switch (getAudio_lang()) {
				case "fr":
					return this.series_name + " (FR)";
				default:
					return this.series_name;
				}
			} else
				return this.series_name;
		}
	}

	/**
	 * Method to get formatted episode name with season number and episode number.
	 * 
	 * @return
	 */
	public String getFormattedEpisodeName() {
		if (this.season_number == null && getName() == null) {
			return getPrimaryMetaDataAccordingToLocalization();
		}
		StringBuffer formattedEpisodeName = new StringBuffer();
		if (this.season_number != null) {
			formattedEpisodeName.append("Season " + getSeason_number() + " ");
			if (this.episode_number != null) {
				formattedEpisodeName.replace(1, 7, "");
				formattedEpisodeName.append(": E" + getEpisode_number());
			}
			if (!formattedEpisodeName.equals("")) {
				formattedEpisodeName.append(" - ");
			}
		}	
		if (getName() != null)
			formattedEpisodeName.append(getName());
		return formattedEpisodeName.toString();
	}

	/**
	 * Method to get meta data of a movie.
	 * 
	 * @return formatted meta data.
	 */
	public String getMovieMetadata() {
		jsonMetaData = new ArrayList<>();
		if (getAudio_lang() != null) {
			switch (getAudio_lang()) {
			case "fr":
				jsonMetaData.add("French");
			}
		}
		if (this.year != null)
			jsonMetaData.add(getYear());
		if (this.genre_list != null)
			jsonMetaData.add(this.genre_list.get(0));
		if (this.child_protection_rating != null)
			jsonMetaData.add(this.child_protection_rating);
		if (this.duration != 0)
			jsonMetaData.add(dAndT.convertSecondsToMinutes(this.duration));
		switch (ConfigurationConstants.deviceType) {
		case "TV":
			return utility.concatinateWith(" . ", jsonMetaData);
		case "Phone":
		case "Tablet":
			return utility.concatinateWith(" . ", jsonMetaData);
		default:
			return "Device not supported";
		}
	}

	/**
	 * Method to get meta data of a episode.
	 * 
	 * @return formatted meta data.
	 */
	public String getEpisodeMetadata() {
		jsonMetaData = new ArrayList<>();
		if (getAudio_lang() != null) {
			switch (getAudio_lang()) {
			case "fr":
				jsonMetaData.add("French");
			}
		}
		if (this.genre_list != null)
			jsonMetaData.add(this.genre_list.get(0));
		if (this.child_protection_rating != null)
			jsonMetaData.add(this.child_protection_rating);
		switch (ConfigurationConstants.deviceType) {
		case "TV":
			return utility.concatinateWith(" . ", jsonMetaData);
		case "Phone":
		case "Tablet":
			return utility.concatinateWith(" . ", jsonMetaData);
		default:
			return "Device not supported";
		}
	}

	/**
	 * Method to get actors list.
	 * 
	 * @return formatted meta data.
	 */
	public String getActorsList() {
		jsonMetaData = new ArrayList<>();
		if (this.actors_list != null)
			for (String actor : this.getActors_list()) {
				jsonMetaData.add(actor);
			}
		switch (ConfigurationConstants.deviceType) {
		case "TV":
			return utility.concatinateWith(", ", jsonMetaData);
		case "Phone":
		case "Tablet":
			return utility.concatinateWith(", ", jsonMetaData);
		default:
			return "Device not supported";
		}
	}
}
