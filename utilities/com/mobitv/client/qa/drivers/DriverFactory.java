package com.mobitv.client.qa.drivers;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.constants.Constants;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
/**
 * 
 * @author MobiTV
 *
 */
public class DriverFactory {
	private AndroidDriver<MobileElement> androidDriver = null;
	private DesiredCapabilities capabilities = null;

	/**
	 * Method to get Android driver with implicit wait.
	 * 
	 * @return AndroidDriver
	 */
	public AndroidDriver<MobileElement> getAndroidDriver() {
		capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.UDID, ConfigurationConstants.serialNumber);
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, ConfigurationConstants.deviceName);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, ConfigurationConstants.platformName);
		capabilities.setCapability(MobileCapabilityType.NO_RESET, ConfigurationConstants.noReset);
		capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ConfigurationConstants.appActivity);
		capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, ConfigurationConstants.appPackage);
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 30 * Constants.DRIVER_IMPLICIT_WAIT);
		try {
			androidDriver = new AndroidDriver<MobileElement>(new URL(ConfigurationConstants.appiumServer), capabilities);
		} catch (MalformedURLException e) {}
		androidDriver.manage().timeouts().implicitlyWait(Constants.DRIVER_IMPLICIT_WAIT, TimeUnit.SECONDS);
		return androidDriver;
	}
}
