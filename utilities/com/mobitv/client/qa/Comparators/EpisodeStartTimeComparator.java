package com.mobitv.client.qa.Comparators;

import java.util.Comparator;

import com.mobitv.client.qa.models.Episode;

public class EpisodeStartTimeComparator implements Comparator<Episode> {

	@Override
	public int compare(Episode episode1, Episode episode2) {
		// TODO Auto-generated method stub
		return (episode1.getStart_time()).compareTo(episode2.getStart_time());
	}

}
