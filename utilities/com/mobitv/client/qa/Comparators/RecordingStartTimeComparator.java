package com.mobitv.client.qa.Comparators;

import java.util.Comparator;

import com.mobitv.client.qa.models.Recording;

public class RecordingStartTimeComparator implements Comparator<Recording>{

	@Override
	public int compare(Recording recording1, Recording recording2) {
		return (recording1.getRecording_start_time()).compareTo(recording2.getRecording_start_time());
		
	}

}
