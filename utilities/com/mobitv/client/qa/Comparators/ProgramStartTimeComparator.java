package com.mobitv.client.qa.Comparators;

import java.util.Comparator;
import com.mobitv.client.qa.models.Program;

/**
 * Class to compair program start time to sort list of programs from API response in ascending order.
 */
public class ProgramStartTimeComparator implements Comparator<Program> {

	@Override
	public int compare(Program program1, Program program2) {
    	return (program1.getStart_time()).compareTo(program2.getStart_time());
    }
}