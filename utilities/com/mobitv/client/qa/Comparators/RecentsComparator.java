package com.mobitv.client.qa.Comparators;

import java.util.Comparator;

import com.mobitv.client.qa.models.Recents.RecentsListItem;

public class RecentsComparator implements Comparator<RecentsListItem> {

	@Override
	public int compare(RecentsListItem item1, RecentsListItem item2) {
		return (item2.creation_time).compareTo(item1.creation_time);
	}

}
