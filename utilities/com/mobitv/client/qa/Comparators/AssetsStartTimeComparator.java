package com.mobitv.client.qa.Comparators;

import java.util.Comparator;

import com.mobitv.client.qa.models.Assets;

public class AssetsStartTimeComparator implements Comparator<Assets> {

	@Override
	public int compare(Assets asset1, Assets asset2) {
		// TODO Auto-generated method stub
		return (asset1.getStart_time()).compareTo(asset2.getStart_time());
	}

}
