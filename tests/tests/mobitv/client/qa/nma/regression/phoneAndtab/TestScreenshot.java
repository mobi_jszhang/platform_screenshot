package tests.mobitv.client.qa.nma.regression.phoneAndtab;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.mobitv.client.qa.baseTests.AndroidPhoneAndTabBaseTest;
import com.mobitv.client.qa.baseTests.BaseTest;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.constants.XPaths;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
/**
 * All tests in this class are used to test Navigation menu functionality in
 * mobile and tablet application.
 * 
 * @author MobiTV
 *
 */
public class TestScreenshot extends AndroidPhoneAndTabBaseTest {
	private File file = null;
	private String filePath = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType);
	
	@BeforeMethod(alwaysRun = true)
	public void getScreenshotBeforeMethod(Method method) {
		try {
			System.out.println(" Beginning of Test : " + method.getName());
			System.out.println(filePath);
		} catch (Exception e) {}
	}
	
	/**
	 * At present this method is to hold framework structure.
	 */
	@AfterMethod(alwaysRun = true)
	public void getScreenshotAfterMethod(Method method) {
		System.out.println("End of Test : " + method.getName());
	}
	
	@AfterClass
	public void getScreenshotAfterClassMethod() {
		System.out.println("End of Test : " + this.getClass().getName());
		zipDirectory();
	}

	@Test(priority = 0)
	public void Test_OnLiveScreen() {
		Assert.assertTrue(selectLiveTab(), "Live tab is not selected properly using, navigation menu");
		String filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "Live");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
	}
	
	@Test(priority = 3)
	public void Test_OnFeaturedScreen() {
		waitForSomeTime(20);
		Assert.assertTrue(selectFeaturedTab(), "Featured tab is not selected properly using, navigation menu");
		
		String filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "Featured_Top");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
		scrollDown();
		scrollDown();
		scrollUp();
		filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "Featured_Middle");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
		scrollDown();
		scrollDown();
		scrollDown();
		scrollUp();
		filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "Featured_Bottom");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
	}
	
	@Test(priority = 1)
	public void Test_OnShowsScreen(){
		Assert.assertTrue(selectShowsTab(), "Didn't navigated to shows page");
		String filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "Shows");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
	}
	
	@Test(priority = 2)
	public void Test_OnMoviesScreen(){
		Assert.assertTrue(selectMoviesTab(), "Didn't navigated to movies page");
		String filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "Movies");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
	}
	
	@Test(priority = 4)
	public void Test_OnMoviesDetailPage() {
		Assert.assertTrue(selectMoviesTab(), "Movies tab is not selected properly using, navigation menu");
		List<MobileElement> movieElements = androidDriver.findElements(MobileBy.id(XPaths.cell_thumb));
		movieElements.get(0).click();
		waitForSomeTime(2);
		String filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "MovieDetail");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
		navigateBack(1);
		
	}

	// ******************** Functions useful for this class only.
	// ********************//
	
	/**
	 * Method to take screenshot.
	 * 
	 * @param fileName
	 * @return
	 */
	private boolean captureScreenshot(String fileName) {
		try {
			file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file, new File("./Screenshots/" + filePath + '/' + fileName + ".png"));
			return true;
		} catch (NullPointerException | IOException e) {
			return false;
		}
	}
	
	private void zipDirectory() {
        try {
        	File file = new File("./Screenshots/" + filePath ) ;
            File zipFile = new File("./Screenshots/" + filePath  + ".zip") ;   
            InputStream input = null ;  
            ZipOutputStream zipOut = null ; 
			zipOut = new ZipOutputStream(new FileOutputStream(zipFile)) ;
			int temp = 0 ;  
	        if(file.isDirectory()){ 
	            File lists[] = file.listFiles() ;
	            for(int i=0;i<lists.length;i++){  
	                input = new FileInputStream(lists[i]); 
	                zipOut.putNextEntry(new ZipEntry(file.getName()+File.separator+lists[i].getName())) ;
	                while((temp=input.read())!=-1){ 
	                    zipOut.write(temp) ;   
	                }  
	                input.close() ; 
	                lists[i].delete();
	            }  
	        }  
	        zipOut.close() ; 
	        file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}  
        
	}
	

	
}
