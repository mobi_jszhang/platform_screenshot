package tests.mobitv.client.qa.nma.regression.tv;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipEntry ;  

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.mobitv.client.qa.baseTests.AndroidTVBaseTest;
import com.mobitv.client.qa.constants.ConfigurationConstants;
import com.mobitv.client.qa.constants.Constants;
import com.mobitv.client.qa.constants.XPaths;
import io.appium.java_client.MobileBy;


public class TestScreenshot extends AndroidTVBaseTest {
	private File file = null;
	private String filePath = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType);
	private boolean onDRM = ConfigurationConstants.onDRM;
	
	@BeforeMethod(alwaysRun = true)
	public void getScreenshotBeforeMethod(Method method) {
		try {
			System.out.println(" Beginning of Test : " + method.getName());
			System.out.println(filePath);
		} catch (Exception e) {}
	}
	
	/**
	 * At present this method is to hold framework structure.
	 */
	@AfterMethod(alwaysRun = true)
	public void getScreenshotAfterMethod(Method method) {
		System.out.println("End of Test : " + method.getName());
	}
	
	@BeforeClass
	public void getScreenshotBeforeClassMethod() {
		navigateBack(3);
		waitForSomeTime(20);//wait for check parental control
		System.out.println("Preparing the recently watched section");
		waitForSomeTime(10);
		for(int i = 0; i < 10; i++) {
			navigateDown(1);
			waitForSomeTime(30);
		}
		waitForSomeTime(10);
	}
	
	@AfterClass
	public void getScreenshotAfterClassMethod() {
		System.out.println("End of Test : " + this.getClass().getName());
		zipDirectory();
	}

	@Test
	public void Test_OnGuideScreen() {
		Assert.assertTrue(selectGuideTab(), "Didn't navigated to guide screen");
		String filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "Guide");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
	}
	
	@Test
	public void Test_OnMenu() {
		if(onDRM)
			return;
		waitForSomeTime(20);
		navigateBack(1);//bring up the menu
		String filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "Menu");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
	}
	
	@Test
	public void Test_OnTrickPlay() {
		if(onDRM)
			return;
		waitForSomeTime(20);
		tap(1);//bring up the trickplay
		String filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "Trickplay");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
	}

	@Test
	public void Test_OnReplayScreen() {
		Assert.assertTrue(goToReplay(), "Didn't navigated to Replay screen");
		waitForSomeTime(15);
		String filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "Replay");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
	}

	@Test
	public void Test_OnMoviesScreen(){
		Assert.assertTrue(selectMoviesTab(), "Didn't navigated to movies page");
		String filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "Movies");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
	}
	
	@Test
	public void Test_OnMoviesDetailPage() {
		Assert.assertTrue(selectMoviesTab(), "Didn't navigated to Movies screen");
		navigateToAllMoviesScreen();
		waitForSomeTime(10);//load all movies page
		tap(1);
		navigateRight(3);
		tap(1);//go to detail screen
		waitForSomeTime(2);
		String filename = String.join( "_", ConfigurationConstants.carrier, ConfigurationConstants.buildVersion, ConfigurationConstants.deviceType, "MovieDetail");
		captureScreenshot(filename+"_0s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + ConfigurationConstants.delaySecond + "s");
		waitForSomeTime(ConfigurationConstants.delaySecond);
		captureScreenshot(filename+"_" + 2 * ConfigurationConstants.delaySecond + "s");
		
	}

	// ******************** Functions useful for this class only.
	// ********************//
	private void navigateToAllMoviesScreen() {
		waitForSomeTime(1);
		navigateUp(1);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.detailsSeeAllNavText));
		Assert.assertTrue(androidUtility.getElementStateAttribute(mElement, "focused"), "Focus is not on Browse All button (top)");
		tap(1);
		waitForSomeTime(Constants.VERY_SMALL_SLEEP);
		mElement = androidDriver.findElement(MobileBy.id(XPaths.primaryPageTitle));
		Assert.assertEquals(mElement.getText(), dictionaryReader.getDictionaryValueForKey("all_movies_text"), "Not in All Movies page");
	}
	
	/**
	 * Method to take screenshot.
	 * 
	 * @param fileName
	 * @return
	 */
	private boolean captureScreenshot(String fileName) {
		try {
			file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file, new File("./Screenshots/" + filePath + '/' + fileName + ".png"));
			return true;
		} catch (NullPointerException | IOException e) {
			return false;
		}
	}
	
	private void zipDirectory() {
        try {
        	File file = new File("./Screenshots/" + filePath ) ;
            File zipFile = new File("./Screenshots/" + filePath  + ".zip") ;   
            InputStream input = null ;  
            ZipOutputStream zipOut = null ; 
			zipOut = new ZipOutputStream(new FileOutputStream(zipFile)) ;
			int temp = 0 ;  
	        if(file.isDirectory()){ 
	            File lists[] = file.listFiles() ;
	            for(int i=0;i<lists.length;i++){  
	                input = new FileInputStream(lists[i]); 
	                zipOut.putNextEntry(new ZipEntry(file.getName()+File.separator+lists[i].getName())) ;
	                while((temp=input.read())!=-1){ 
	                    zipOut.write(temp) ;   
	                }  
	                input.close() ; 
	                lists[i].delete();
	            }  
	        }  
	        zipOut.close() ; 
	        file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}  
        
	}
}